#!/bin/bash
echo "--------------------"
echo "Enter st num (201211304) > 201211260"
echo "Enter problem num (1) > 1"
echo "Enter mid or fin (fin) > mid"
echo "Enter class 1 or 2 (1) > 2"
echo "Enter class main (ChocolatedBoilerTestDrive) > test1"
echo "Enter sourcepath (1 or 2) > 2"
echo " 1. ans/mid2015s2/j2_2/u201411183"
echo " 2. ans/mid2015s2/j2_2/u201411183/src/com/j2/u201411183"
echo "--------------------"
echo -n "Enter st num (201211304) > "
read stnum
echo -n "Enter problem num (1) > "
read pnum
echo -n "Enter mid or fin (fin) > "
read mp
echo -n "Enter class 1 or 2 (1) > "
read class
echo -n "Enter class main (ChocolatedBoilerTestDrive) > "
read main
echo "You entered:" $stno $pnum $mp $class $main
echo -n "Enter sourcepath (1 or 2) > "
read sourcepathtype
if [ $sourcepathtype = "1" ]; then
    sourcepath=ans/${mp}2015s2/j2_$class/u$stnum
elif [ $sourcepathtype = "2" ]; then
    sourcepath=ans/${mp}2015s2/j2_$class/u$stnum/src/com/j2/u$stnum
fi
packagedir=$sourcepath/p$pnum
packagename=com.j2.u$stnum.p$pnum
echo "sourcepath: " $sourcepath
echo "sourcedir: " $packagedir
echo "package name: " $packagename

javac -d classes/ -sourcepath $sourcepath $packagedir/*.java
java -cp classes/ $packagename.$main

