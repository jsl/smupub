def main():
  lab1_1()
  lab1_2()
  lab1_3()
  lab1_4()
  lab1_5()
  lab1_6()
def lab1_1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  lastpx=getPixel(barb,221,293)
  setColor(lastpx,blue)
  show (barb)


#2
def lab1_2():
  pic1=makePicture(getMediaPath('7inX95in.jpg'))
  pic2=makePicture(getMediaPath('640x480.jpg'))
  px1=getHeight(pic1)*getWidth(pic1)
  px2=getHeight(pic2)*getWidth(pic2)
  if px1>px2:
    show (pic1)
  if px1<px2:
    show (pic2)
      
def lab1_3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  for x in range(0,getWidth(barb)):
    for y in range(0,getHeight(barb)):
      barbpx=getPixel(barb,x,y)
      barbBlue=getBlue(barbpx)
      barbGreen=getGreen(barbpx)
      barbRed=getRed(barbpx)
      newC=makeColor((barbBlue+barbGreen)/2)
      barbRed=newC
      setColor(barbpx,newC)
  show(barb)

def lab1_4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  pic1=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceY in range(0,getHeight(barb)):
      barbpx=getPixel(barb,sourceX,sourceY)
      picpx=getPixel(pic1,targetX,targetY)
      barbC=makeColor(getColor(barbpx))
      setColor(picpx,barbC)
      targetX=targetX+1
    targetY=targetY+1
  show(pic1)
 