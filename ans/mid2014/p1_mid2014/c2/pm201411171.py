def main():
  lab1()
  lab3()
  lab4()

# 1. (O)
def lab1():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  pixel=getBlue(getPixel(barb,221,293))
  print pixel
  
# 2. (X)

# 3. (O)
def lab3():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  for p in getPixels(barb):
    average=(getGreen(p)+getBlue(p))/2
    setRed(p,average)
  show(barb)
  
# 4. (O)
def lab4():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  targetX=282
  for x in range(0,getWidth(barb)):
    targetY=390
    for y in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,x,y))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas
  
# 5. (X)

# 6. (X)