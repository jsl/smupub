def main():
  lab1()
  lab2()
  lab3file=lab3()
  show(lab3file)
  lab4file=lab4()
  show(lab4file)
  lab5file=lab5()
  show(lab5file)
  lab6file=lab6()
  show(lab6file)

def lab1():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  lastblue=getBlue(getPixel(barb,getWidth(barb)-1,getHeight(barb)-1))
  print lastblue

def lab2():
  canvas1f=getMediaPath('7inX95in.jpg')
  canvas1=makePicture(canvas1f)
  canvas2f=getMediaPath('640x480.jpg')
  canvas2=makePicture(canvas2f)
  canvas1px=getWidth(canvas1)*getHeight(canvas1)
  canvas2px=getWidth(canvas2)*getHeight(canvas2)
  if canvas1px>canvas2px:
    show(canvas1)
  else:
    show(canvas2)

def lab3():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  for px in getPixels(barb):
    setRed(px,(getGreen(px)+getBlue(px))/2)
  return barb
  
def lab4():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  sourceY=0
  for targetY in range(getHeight(canvas)-getHeight(barb)-1,getHeight(canvas)-1):
    sourceX=0
    for targetX in range(getWidth(canvas)-getWidth(barb)-1,getWidth(canvas)-1):
      sourcepx=getPixel(barb,sourceX,sourceY)
      targetpx=getPixel(canvas,targetX,targetY)
      sourcecol=getColor(sourcepx)
      setColor(targetpx,sourcecol)
      sourceX=sourceX+1
    sourceY=sourceY+1
  return canvas

def lab5():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  sourceY=0
  for targetY in range(0,getWidth(barb)):
    sourceX=0
    for targetX in range(0,getHeight(barb)):
      sourcepx=getPixel(barb,sourceY,sourceX)
      targetpx=getPixel(canvas,targetX,targetY)
      sourcecol=getColor(sourcepx)
      setColor(targetpx,sourcecol)
      sourceX=sourceX+1
    sourceY=sourceY+1
  return canvas
  
def lab6():
  buttf=getMediaPath('butterfly.jpg')
  butt=makePicture(buttf)
  for x in range(0,getWidth(butt)-1):
    for y in range(0,getHeight(butt)-1):
      herepx=(getPixel(butt,x,y))
      downpx=(getPixel(butt,x,y+1))
      rightpx=(getPixel(butt,x+1,y))
      here=((getRed(herepx)+getGreen(herepx)+getBlue(herepx))/3)
      down=((getRed(downpx)+getGreen(downpx)+getBlue(downpx))/3)
      right=((getRed(rightpx)+getGreen(rightpx)+getBlue(rightpx))/3)
      absdown=abs(here-down)
      absright=abs(here-right)
      if absdown>15 or absright>15:
        setColor(herepx,black)
      else:
        setColor(herepx,white)
  return butt