def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab6()

def lab1():
  file=getMediaPath('barbara.jpg')
  pic=makePicture(file)
  p=getPixel(pic,221,293)
  print p
  
def lab2():
  f1=getMediaPath('7inX95in.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('640x480.jpg')
  p2=makePicture(f2)
  a=getWidth(p1)*getHeight(p1)*3
  b=getWidth(p2)*getHeight(p2)*3
  if a>b:
    show(p1)
  else:
    show(p2)
      
def lab3():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  for px in getPixels(p1):
    r=getRed(px)
    g=getGreen(px)
    b=getBlue(px)
    newRed=(g+b)/3
    newRedC=makeColor(newRed,g,b)
    setColor(px,newRedC)
  repaint(p1)
  
def lab4():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('7inX95in.jpg')
  p2=makePicture(f2)
  targetX=282
  for sourceX in range(0,getWidth(p1)):
    targetY=390
    for sourceY in range(0,getHeight(p1)):
      color=getColor(getPixel(p1,sourceX,sourceY))
      setColor(getPixel(p2,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(p2)
  return(p2)
  

  
def lab6():
  f1=getMediaPath('butterfly1.jpg')
  p1=makePicture(f1)
  for x in range(0,getWidth(p1)-1):
    for y in range(0,getHeight(p1)-1):
      here=getPixel(p1,x,y)
      down=getPixel(p1,x,y+1)
      right=getPixel(p1,x+1,y)
      hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
      downL=(getRed(down)+getGreen(down)+getBlue(down))/3
      rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
      absDown=abs(hereL-downL)
      absRight=abs(hereL-rightL)
      if absDown>10 and absRight>10:
        setColor(here,black)
      else:
        setColor(here,white)
  show(p1)
