def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

#lab1-OK  
def lab1():
  p=makePicture(getMediaPath("barbara.jpg"))
  px=getPixel(p,221,293)
  g=getBlue(px)
  print g

#lab2-OK
def lab2():
  p=makePicture(getMediaPath("7inX95in.jpg"))
  p1=makePicture(getMediaPath("640x480.jpg"))
  v=getHeight(p)*getWidth(p)
  v1=getHeight(p1)*getWidth(p1)
  if v>v1:
    show(p)
  else:
    show(p1)
    
#lab3-OK  
def lab3():
  show(R())
def R():
  pic=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(pic):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    avg=(g+b)/2
    setRed(p,avg)
  return pic


#lab4-OK
def lab4():
  show(copyB())
def copyB():
  p=makePicture(getMediaPath("barbara.jpg"))
  c=makePicture(getMediaPath("7inX95in.jpg"))
  ty=getHeight(c)-getHeight(p)
  for sy in range(0,getHeight(p)):
    tx=getWidth(c)-getWidth(p)
    for sx in range(0,getWidth(p)):
      px=getPixel(p,sx,sy)
      pc=getColor(px)
      setColor(getPixel(c,tx,ty),pc)
      tx=tx+1
    ty=ty+1
  return c

#lab5-OK
def lab5():
  show(rotateB())
def rotateB():
  p=makePicture(getMediaPath("barbara.jpg"))
  c=makePicture(getMediaPath("7inX95in.jpg"))
  ty=0
  for sy in range(0,getHeight(p)):
    tx=0
    for sx in range(0,getWidth(p)):
      px=getPixel(p,sx,sy)
      pc=getColor(px)
      setColor(getPixel(c,ty,tx),pc)
      tx=tx+1
    ty=ty+1
  return c

#lab6-OK
def lab6():
  show(edge())
def edge():
  p=makePicture(getMediaPath("butterfly1.jpg"))
  for x in range(0,getWidth(p)-1):
    for y in range(0,getHeight(p)-1):
      here=getPixel(p,x,y)
      right=getPixel(p,x+1,y)
      down=getPixel(p,x,y+1)
      hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
      rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
      downL=(getRed(down)+getGreen(down)+getBlue(down))/3
      absRight=abs(hereL-rightL)
      absDown=abs(hereL-downL)
      if absRight<10 and absDown<10:
        setColor(here,white)
      else:
        setColor(here,black)
  return p
  
  

