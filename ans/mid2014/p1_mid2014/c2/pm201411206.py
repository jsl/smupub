def lab1():
  barbf=makePicture(getMediaPath('barbara.jpg'))
  for x in range(0,getWidth(barbf)):
    for y in range(0,getHeight(barbf)):
     sp=getBlue(221,293)
     show(sp)    
    
        
def lab2():
  p1=makePicture(getMediaPath('7inx95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  if nBytes(p1)>nBytes(p2):
    show(p1)
  else:
    show(p2)  
  
def nBytes(pic):
  w=getWidth(pic)
  g=getHeight(pic)    
  print w*g*3
  
  

def lab3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(barb):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    lum=(g+b)/2
    newc=makeColor(lum,g,b)
    setColor(p,newc)
  show(barb)  
  
  
  
def lab4():
  barbp=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inx95in.jpg'))
  targetX=282
  for sx in range(0,getWidth(barbp)):
    targetY=390
    for sy in range(0,getHeight(barbp)):
      px=getPixel(barbp,sx,sy)
      tx=getPixel(canvas,targetX,targetY)
      color=getColor(px)
      setColor(tx,color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)    
  
  
def lab5():
  barbt=makePicture(getMediaPath('barbara.jpg'))
  canvast=makePicture(getMediaPath('7inx95in.jpg'))
  targeX=50
  for sx in range(0,getWidth(barbt)):
    targetY=50
    for sy in range(0,getHeight(barbt)):  
      px=getPixel(barbt,sx,sy)
      pt=getPixel(canvast,sy,sx)
      color=getColor(px)
      setColor(pt,color)
  show(canvast)    
 

  
  
    
def lab6():
  put=makePicture(getMediaPath('butterfly1.jpg'))
  for x in range(0,getWidth(put)-1):
    for y in range(0,getHeight(put)-1):
      here=getPixel(put,x,y)
      right=getPixel(put,x+1,y)
      down=getPixel(put,x,y+1)
      hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
      rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
      downL=(getRed(down)+getGreen(down)+getBlue(down))/3
      absDown=abs(hereL-downL)
      absRight=abs(hereL-rightL)
      if absRight>5 and absDown>5:
        setColor(here,black)
      else:
        setColor(here,white) 
  show(put)   
  
def main():
  
  lab2()
  lab3()
  lab4()
  lab5()
  lab6() 
    
    