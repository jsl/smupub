def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()


def lab1():
  def barbLastPixelBlue():
    barb=makePicture(getMediaPath('barbara.jpg'))
    print getBlue(getPixel(barb,getWidth(barb)-1,getHeight(barb)-1))
  barbLastPixelBlue()
  

def lab2():
  def showMorePixelPic():
    p1=makePicture(getMediaPath('7inX95in.jpg'))
    p2=makePicture(getMediaPath('640x480.jpg'))
    pxCount1=getWidth(p1)*getHeight(p1)
    pxCount2=getWidth(p2)*getHeight(p2)
    if pxCount1>pxCount2:
      show(p1)
    if pxCount1<pxCount2:
      show(p2)
  showMorePixelPic()
    
    
def lab3():
  def barbGBAverage():
    barb=makePicture(getMediaPath('barbara.jpg'))
    for p in getPixels(barb):
      g=getGreen(p)
      b=getBlue(p)
      average=(g+b)/2
      setRed(p,average)
    repaint(barb)
  barbGBAverage()
  
  
def lab4():
  def copyBarb():
    barb=makePicture(getMediaPath('barbara.jpg'))
    canvas=makePicture(getMediaPath('7inX95in.jpg'))
    targetX=getWidth(canvas)-getWidth(barb)
    for sourceX in range(0,getWidth(barb)):
      targetY=getHeight(canvas)-getHeight(barb)
      for sourceY in range(0,getHeight(barb)):
        color=getColor(getPixel(barb,sourceX,sourceY))
        setColor(getPixel(canvas,targetX,targetY),color)
        targetY=targetY+1
      targetX=targetX+1
    show(canvas)
  copyBarb()
  
def lab5():
  def rotateBarb():
    barb=makePicture(getMediaPath('barbara.jpg'))
    canvas=makePicture(getMediaPath('7inX95in.jpg'))
    width=getWidth(barb)
    height=getHeight(barb)
    targetX=0
    for sourceX in range(0,getWidth(barb)):
      targetY=0
      for sourceY in range(0,getHeight(barb)):
        color=getColor(getPixel(barb,sourceX,sourceY))
        setColor(getPixel(canvas,targetY,width-targetX-1),color)
        targetY=targetY+1
      targetX=targetX+1
    show(canvas)
  rotateBarb()
      
      
def lab6():
  def lineDetect():
    butterfly=makePicture(getMediaPath('butterfly1.jpg'))
    for x in range(0,getWidth(butterfly)-1):
      for y in range(0,getHeight(butterfly)-1):
        here=getPixel(butterfly,x,y)
        down=getPixel(butterfly,x,y+1)
        right=getPixel(butterfly,x+1,y)
        hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
        downL=(getRed(down)+getGreen(down)+getBlue(down))/3
        rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
        if (hereL-downL)>10 or (hereL-rightL)>10:
          setColor(here,black)
        else:
          setColor(here,white)
    show(butterfly)
  lineDetect()