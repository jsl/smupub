def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  

def lab1():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  for p in getPixels(pb):
    px=getPixel(pb,getWidth(pb)-1,getHeight(pb)-1)
    b=getBlue(px)
  print (b)
  
  
  
def lab2():
  f1=getMediaPath('7inX95in.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('640x480.jpg')
  p2=makePicture(f2) 
  pixel1=getWidth(p1)*getHeight(p1)
  pixel2=getWidth(p2)*getHeight(p2)
  if pixel1>pixel2:
    show (p1)
  else:
    show(p2)  
    
    
def lab3():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  for p in getPixels(pb):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    col=(g+b)/2
    color=makeColor(col)
    setRed(p,col)
  show(pb)        
  
def lab4():
   p=copybarb()
   show(p)
  
      
def copybarb():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)   
  f1=getMediaPath('7inX95in.jpg')
  p1=makePicture(f1)
  targetY=getHeight(p1)-getHeight(pb)
  for sourceY in range(0,getHeight(pb)):
    targetX= getWidth(p1)-getWidth(pb)
    for sourceX in range(0,getWidth(pb)):
      spixel=getPixel(pb,sourceX,sourceY)
      tpixel=getPixel(p1,targetX,targetY)
      scolor=getColor(spixel)
      setColor(tpixel,scolor)
      targetX=targetX+1
    targetY=targetY+1
  return p1     
  
  
  
def lab5():
   y=copybarb()
   show(y)
  
      
def copybarb2():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)   
  f1=getMediaPath('7inX95in.jpg')
  p1=makePicture(f1)
  targetY=getHeight(p1)-getHeight(pb)
  for sourceY in range(0,getHeight(pb)):
    targetX= getWidth(p1)-getWidth(pb)
    for sourceX in range(0,getWidth(pb)):
      spixel=getPixel(pb,sourceX,sourceY)
      tpixel=getPixel(p1,targetY,targetX)
      scolor=getColor(spixel)
      setColor(tpixel,scolor)
      targetX=targetX+1
    targetY=targetY+1
  return p1       
  
def lab6():
   c=edgeDetection()
   show(c)

def edgeDetection():
  f=getMediaPath('butterfly1.jpg')
  p=makePicture(f)
  for y in range(0,getHeight(p)-1):
    for x in range(0,getWidth(p)-1):
      here=getPixel(p,x,y)
      down=getPixel(p,x,y+1)
      right=getPixel(p,x+1,y)
      hereL=(getRed(here)+getBlue(here)+getGreen(here))/3
      downL=(getRed(down)+getBlue(down)+getGreen(down))/3
      rightL=(getRed(right)+getBlue(right)+getGreen(right))/3
      absdown=abs(hereL-downL) 
      absRight=abs(hereL-rightL)
      if absdown>10 and absRight>10:
        setColor(getPixel(p,x,y),black)
      else:
        setColor(getPixel(p,x,y),white)
  return p      