# X
def lab1():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(barb):
    k=getPixel(barb,220.290)
    b=getBlue(k)
    t=getPixelAt(b)
  print t
    
   
  
# 0 
def lab2():
  p1=makePicture(getMediaPath("7inX95in.jpg"))
  p2=makePicture(getMediaPath("640X480.jpg"))
  p=getAllPixels(p1)
  q=getAllPixels(p2)
  if p<q:
    show(p2)
  if p>q:
    show(p1)
  
# 0    
def lab3():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(barb):
    b=getBlue(p)
    g=getGreen(p)
    r=getRed(p)
    color=makeColor((b+g)/2,g,b)
    setColor(p,color)
  show(barb)


# 0
def lab4():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("640X480.jpg"))
  targetX=418
  for sourceX in range(0,getWidth(barb)):
    targetY=186
    for sourceY in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas

# 0
def lab5():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceY in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(canvas,targetY,targetX),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas

# x
def lab6():
  p2=makePicture(getMediaPath("butterfly1.jpg"))
  

def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

