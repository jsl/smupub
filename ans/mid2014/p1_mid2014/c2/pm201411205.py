def main():
  Lab1()
  Lab2()
  Lab3()
  Lab4()
  Lab5()
  Lab6()


def Lab1():
  p1=makePicture(getMediaPath('barbara.jpg'))
  px=getPixel(p1,getWidth(p1)-1,getHeight(p1)-1)
  b=getBlue(px)
  print b

def Lab2():
  p1=makePicture(getMediaPath('7inX95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  count1=0
  for px1 in getPixels(p1):
    count1=count1+1
  count2=0
  for px2 in getPixels(p2):
    count2=count2+1
  if count1>count2:
    show(p1)
  if count2>count1:
    show(p2)
  
def Lab3():
  p1=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(p1):
    newred=(getBlue(p)+getGreen(p))/2
  print newred

def Lab4():
  p1=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('7inX95in.jpg'))
  tx=(getWidth(can)-1)-getWidth(p1)
  for sx in range(0,getWidth(p1)):
    ty=(getHeight(can)-1)-getHeight(p1)
    for sy in range(0,getHeight(p1)):
      px=getPixel(p1,sx,sy)
      color=getColor(px)
      canpx=getPixel(can,tx,ty)
      setColor(canpx,color)
      ty=ty+1
    tx=tx+1
  show(can)

def Lab5():
  p1=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('7inX95in.jpg'))
  tx=100
  for sx in range(0,getWidth(p1)):
    ty=100
    for sy in range(0,getHeight(p1)):
      px=getPixel(p1,sx,sy)
      color=getColor(px)
      canpx=getPixel(can,getWidth(can)-tx,getHeight(can)-ty)
      setColor(canpx,color)
      ty=ty+1
    tx=tx+1
  show(can)

def Lab6():
  p1=makePicture(getMediaPath('butterfly1.jpg'))
  for x in range(0,getWidth(p1)-1):
    for y in range(0,getHeight(p1)-1):
      here=getPixel(p1,x,y)
      down=getPixel(p1,x,y+1)
      right=getPixel(p1,x+1,y)
      hereL=(getRed(here)+getBlue(here)+getGreen(here))/3
      downL=(getRed(down)+getBlue(down)+getGreen(down))/3
      rightL=(getRed(right)+getBlue(right)+getGreen(right))/3
      absdown=abs(hereL-downL)
      absright=abs(hereL-rightL)
      if absdown>10 and absright>10:
        setColor(here,black)
      else:
        setColor(here,white)
  show(p1)
  
