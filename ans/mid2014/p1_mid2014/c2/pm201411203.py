def lab1():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  px=getPixel(pb,getWidth(pb)-1,getHeight(pb)-1)
  valblue=getBlue(px)
  print valblue
  
def lab2():
  p1=makePicture(getMediaPath('7inX95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  pxp1=getWidth(p1)*getHeight(p1)
  pxp2=getWidth(p2)*getHeight(p2)
  largest=max(pxp1,pxp2)
  print largest
  if pxp1>pxp2 :
    show(p1)
  if pxp1<pxp2 : 
    show(p2)
  
def lab3():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  for px in getPixels(pb):
    r=getRed(px)
    g=getGreen(px)
    b=getBlue(px)
    val=(g+b)/2
    setRed(px,val)
  print getRed(px)
    
def lab4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  #Now, we do actual copying
  targetX=getWidth(canvas)-getWidth(barb)
  for sourceX in range(0,getWidth(barb)):
    targetY=getHeight(canvas)-getHeight(barb)
    for sourceY in range(0,getHeight(barb)):
      px=getPixel(barb,sourceX,sourceY)
      cx=getPixel(canvas,targetX,targetY)
      color=getColor(px)
      setColor(cx,color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas
  
def lab5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceY in range(0,getHeight(barb)):
      px=getPixel(barb,sourceX,sourceY)
      cx=getPixel(canvas,targetX,targetY)
      color=getColor(px)
      setColor(getPixel(canvas,targetY,targetX),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas
  
def lab6():
  bf=makePicture(getMediaPath('butterfly1.jpg'))
  bfbw=makePicture(getMediaPath('butterfly1.jpg'))
  for x in range(0,getWidth(bf)):
    for y in range(0,getHeight(bf)):
     rightbf=getPixel(bf,x+1,y)
     downbf=getPixel(bf,x,y+1)
     herebf=getPixel(bf,x,y)
     rr=getRed(rightbf)
     rg=getGreen(rightbf)
     rb=getBlue(rightbf)
     dr=getRed(downbf)
     dg=getGreen(downbf)
     db=getBlue(downbf)
     hr=getRed(herebf)
     hg=getGreen(herebf)
     hb=getBlue(herebf)
     if abs(rr-hr) < 20 and abs(dr-hr) < 20:
       setColor(herebf,black)
     if abs(rg-hg) >= 20 and abs(dg-hg) >=20 :
       setColor(herebf,white)
  show(bf)
  return bf


def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
      
  
    

     