def lab1():
  f1=getMediaPath("barbara.jpg")
  p1=makePicture(f1)
  for p in getPixels(p1):
    value=getBlue(p)
    setBlue(p,value)
  explore(p1)
  return(p1) 
  
def lab2():
  f1=getMediaPath("7inX95in.jpg")
  p1=makePicture(f1)          
  f2=getMediaPath("640x480.jpg")
  p2=makePicture(f2)
  print p1
  print p2 
       
  show(p1)
  return p1
  
  

def lab3():
  f1=getMediaPath("barbara.jpg")
  p1=makePicture(f1)
  for p in getPixels(p1):
    green=getGreen(p)
    blue=getBlue(p)
    red=makeColor((getGreen(p)+getBlue(p))/2)
    value=getRed(p)
    setRed(p,value/2)
  show(p1)
  return p1  
  
def main():
  lab1()
  lab2()
  lab3()  
    
