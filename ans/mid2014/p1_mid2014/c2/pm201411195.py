def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()


def lab1():
  pic=bluepx()

def lab2():
  pic=px()
  show(pic)

def lab3():
  pic=redC()
  show(pic)

def lab4():
  pic=copyBarb()
  show(pic)

def lab5():
  pic=copyBarbRotate()
  show(pic)

def lab6():
  pic=edge()


#lab1
def bluepx():
  f1=getMediaPath("barbara.jpg")
  p1=makePicture(f1)
  px=getPixel(p1,221,293)
  pc=getBlue(px)
  print "Blue is",pc

#lab2
def px():
  f7=getMediaPath("7inX95in.jpg")
  p7=makePicture(f7)
  f6=getMediaPath("640X480.jpg")
  p6=makePicture(f6)
  px7=getPixels(p7)
  px6=getPixels(p6)
  if px7>px6:
    pic=p7
  if px7<px6:
    pic=p6
  return pic

#lab3
def redC():
  f1=getMediaPath("barbara.jpg")
  p1=makePicture(f1)
  for p in getPixels(p1):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    l=(g+b)/2
    setRed(p,l)
  return p1

#lab4
def copyBarb():
  barbf=getMediaPath("barbara.jpg")
  barb=makePicture(barbf)
  can=getMediaPath("7inX95in.jpg")
  canvas=makePicture(can)
  targetX=getWidth(canvas)-getWidth(barb)
  for sourceX in range(0,getWidth(barb)):
    targetY=getHeight(canvas)-getHeight(barb)
    for sourceY in range(0,getHeight(barb)):
      barbpx=getPixel(barb,sourceX,sourceY)
      canvaspx=getPixel(canvas,targetX,targetY)
      setColor(canvaspx,getColor(barbpx))
      targetY=targetY+1
    targetX=targetX+1
  return canvas

#lab5
def copyBarbRotate():
  barbf=getMediaPath("barbara.jpg")
  barb=makePicture(barbf)
  can=getMediaPath("7inX95in.jpg")
  canvas=makePicture(can)
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceY in range(0,getHeight(barb)):
      barbpx=getPixel(barb,sourceX,sourceY)
      canvaspx=getPixel(canvas,targetY,targetX)
      setColor(canvaspx,getColor(barbpx))
      targetY=targetY+1
    targetX=targetX+1
  return canvas

#lab6
def edge():
  f1=getMediaPath("butterfly1.jpg")
  p1=makePicture(f1)
  w=getWidth(p1)
  h=getHeight(p1)
  print "Edge is",w,"X",h