def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  c=getPixel(p1,221,293)
  a=getBlue(c)
  print a
  
  
def lab2():
  f1=getMediaPath('7inX95in.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('640x480.jpg')
  p2=makePicture(f2)
  c=getWidth(p1)*getHeight(p1)
  a=getWidth(p2)*getHeight(p2)
  if (c>a):
    show(p1)
  else:
    show(p2)
    
    
def lab3():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  for x in getPixels(p1):
    a=int((getGreen(x)+getBlue(x))/2)
    b=makeColor(a,getGreen(x),getBlue(x))
    setColor(x,b)
  show(p1)
  
def lab4():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('7inX95in.jpg')
  p2=makePicture(f2)
  targetX=282
  for sourceX in range(0,getWidth(p1)):
    targetY=390
    for sourceY in range(0,getHeight(p1)):
      p=getPixel(p1,sourceX,sourceY)
      setColor(getPixel(p2,targetX,targetY),getColor(p))
      targetY=targetY+1
    targetX=targetX+1
  show(p2)
  return p2
  
  
def lab5():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('7inX95in.jpg')
  p2=makePicture(f2)
  targetX=0
  for sourceX in range(0,getWidth(p1)):
    targetY=0
    for sourceY in range(0,getHeight(p1)):
      p=getPixel(p1,sourceX,sourceY)
      setColor(getPixel(p2,targetY,targetX),getColor(p))
      targetY=targetY+1
    targetX=targetX+1
  show(p2)
  return p2
  
def lab6():
  f1=getMediaPath('butterfly1.jpg')
  p1=makePicture(f1)
  p2=makePicture(f1)
  for x in range(0,getWidth(p1)-1):
    for y  in range(0,getHeight(p1)-1):
      here=getPixel(p2,x,y)
      down=getPixel(p1,x,y+1)
      right=getPixel(p1,x+1,y)
      hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
      downL=(getRed(down)+getGreen(down)+getBlue(down))/3
      rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
      if abs(hereL-downL)>10 and abs(hereL-rightL)>10:
        setColor(here,black)
      if abs(hereL-downL)<=10 and abs(hereL-rightL)<=10:
        setColor(here,white)
  show(p2)
  return p2