#before you use these functions, you have to set media path.

def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()


def lab1():
  f=getMediaPath('barbara.jpg')
  p=makePicture(f)
  w=getWidth(p)
  h=getHeight(p)
  lastPixel=getPixel(p,w-1,h-1)
  blue=getBlue(lastPixel)
  print blue
  
def lab2():
  fa=getMediaPath('7inX95in.jpg')
  pa=makePicture(fa)
  fb=getMediaPath('640x480.jpg')
  pb=makePicture(fb)
  aPixelNumber=getWidth(pa)*getHeight(pa)
  bPixelNumber=getWidth(pb)*getHeight(pb)
  if aPixelNumber>bPixelNumber:
    show(pa)
  else:
    show(pb)  
    
def lab3():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  for p in getPixels(pb):
    greenness=getGreen(p)
    blueness=getBlue(p)
    average=(greenness+blueness)/2 
    setRed(p,average)
  show(pb)    
    
    
def lab4():
  sourcef=getMediaPath('barbara.jpg')
  source=makePicture(fb)
  canvasf=getMediaPath('7inX95in.jpg') 
  canvas=makePicture(canvasf)
  sourceX=0
  for targetX in range(getWidth(canvas)-1-getWidth(source),getWidth(canvas)-1):
    sourceY=0
    for targetY in range(getHeight(canvas)-1-getHeight(source),getHeight(canvas)-1):
      ps=getPixel(source,sourceX,sourceY)
      color=getColor(ps)
      pc=getPixel(canvas,targetX,targetY)
      setColor(pc,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(canvas)    
  
  
def lab5():
  sourcef=getMediaPath('barbara.jpg')
  source=makePicture(fb)
  canvasf=getMediaPath('7inX95in.jpg') 
  canvas=makePicture(canvasf) 
  for x in range(0,getWidth(source)):
    for y in range(0,getHeight(source)):
      ps=getPixel(source,x,y)
      color=getColor(ps)
      pc=getPixel(canvas,y,getWidth(source)-x)
      setColor(pc,color)
  show(canvas)       
  
  
def lab6():
  sourcef=getMediaPath('butterfly1.jpg')
  source=makePicture(sourcef)
  canvas=makePicture(sourcef)
  for x in range(0,getWidth(source)-1):
    for y in range(0,getHeight(source)-1):
      pcenter=getPixel(canvas,x,y)
      pright=getPixel(source,x+1,y)
      pdown=getPixel(source,x,y+1)
      cc=(getRed(pcenter)+getGreen(pcenter)+getBlue(pcenter))/3
      cr=(getRed(pright)+getGreen(pright)+getBlue(pright))/3
      cd=(getRed(pdown)+getGreen(pdown)+getBlue(pdown))/3
      if abs(cc-cr)<50 and abs(cc-cd)<50:
        setColor(pcenter,white)
      if abs(cc-cr)>=50 and abs(cc-cd)>=50:
        setColor(pcenter,black)
  show(canvas)       
 
       