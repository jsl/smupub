def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()


#impossible  X
def lab1():
  p1=makePicture(getMediaPath('barbara.jpg'))
  

#impossible  X
def lab2():
  p3=makePicture(getMediaPath('7inX95in.jpg'))
  p4=makePicture(getMediaPath('640x480.jpg'))
  #distance(p3,p4)

#impossible  X
def lab3():
  p1=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(p1):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    color=(getGreen(p)+getBlue(p))
    #averageRed=makeColor(p,color)



#possible  O
def lab4():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  targetX=270
  for sourceX in range(0,getWidth(barb)):
    targetY=380
    for sourceY in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(barb)
  show(canvas)
  
#possible  O
def lab5():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  ty=0
  for sy in range(0,getHeight(barb)):
    tx=0
    for sx in range(0,getWidth(barb)):
      ps=getPixel(barb,sx,sy)
      cs=getColor(ps)
      pt=getPixel(canvas,ty,tx)
      setColor(pt,cs)
      tx=tx+1
    ty=ty+1
  show(barb)
  show(canvas)
  
#impossible X
def lab6():
  fp=getMediaPath('butterfly1.jpg')
  bp=makePicture(fp)
  