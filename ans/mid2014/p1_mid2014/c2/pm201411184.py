def lab1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  px=getPixel(barb,221,293)
  b=getBlue(px)
  print b

def lab2():
  p1=makePicture(getMediaPath('7inX95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  w1=getWidth(p1)
  h1=getHeight(p1)
  w2=getWidth(p2)
  h2=getWidth(p2)
  px1=w1*h1
  px2=w2*h2
  larger=max(px1,px2)
  if larger==px1:
    show (p1)
  if larger==px2:
    show (p2)

def lab3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(barb):
    g=getGreen(p)
    b=getBlue(p)
    color=(g+b)/2
    setRed(p,color)
  show (barb)

def lab4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('7inX95in.jpg'))
  sourceX=0
  for targetX in range(getWidth(can)-getWidth(barb),getWidth(can)-1):
    sourceY=0
    for targetY in range(getHeight(can)-getHeight(barb),getHeight(can)-1):
      px=getColor(getPixel(barb,sourceX,sourceY))
      cx=getPixel(can,targetX,targetY)
      setColor(cx,px)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(can)

def lab5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(barb)-1):
    targetY=0
    for sourceY in range(0,getHeight(barb)-1):
      px=getColor(getPixel(barb,sourceX,sourceY))
      cx=getPixel(can,targetY,targetX)
      setColor(cx,px)
      targetY=targetY+1
    targetX=targetX+1
  show (can)

def lab6():
  btf=makePicture(getMediaPath('butterfly1.jpg'))
  for x in range(0,getWidth(btf)-1):
    for y in range(0,getHeight(btf)-1):
      here=getPixel(btf,x,y)
      right=getPixel(btf,x+1,y)
      bottom=getPixel(btf,x,y+1)
      hereL=getRed(here)+getGreen(here)+getBlue(here)
      rightL=getRed(right)+getGreen(right)+getBlue(right)
      bottomL=getRed(bottom)+getGreen(bottom)+getBlue(bottom)
      absR=abs(hereL-rightL)
      absB=abs(hereL-bottomL)
      if absR>25 and absB>25:
        setColor(here,black)
      else:
        setColor(here,white)
  show (btf)

def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()