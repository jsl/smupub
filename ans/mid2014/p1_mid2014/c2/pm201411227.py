# 1. o
def lab1():
  file=getMediaPath("barbara.jpg")
  pic=makePicture(file)
  p=getPixel(pic,getWidth(pic)-1,getHeight(pic)-1)
  b=getBlue(p)
  print b
  
#2. o
def lab2():
  aa=getMediaPath("7inX95in.jpg")
  a=makePicture(aa)
  bb=getMediaPath("640X480.jpg")
  b=makePicture(bb)
  apx=getWidth(a)*getHeight(a)
  bpx=getWidth(b)*getHeight(b)
  print apx
  print bpx
  big=max(apx,bpx)
  print big
  show(b)
  
#3. o
def lab3():
  file=getMediaPath("barbara.jpg")
  pic=makePicture(file)
  for p in getPixels(pic): 
    g=getGreen(p)
    b=getBlue(p)
    color=makeColor((g+b)/2,g,b)
    setColor(p,color)
  repaint(pic)
  
#4. o
def lab4():
  bf=getMediaPath("barbara.jpg")
  b=makePicture(bf)
  cf=getMediaPath("7inX95in.jpg")
  c=makePicture(cf)
  targetX=getWidth(c)-getWidth(b)
  for sourceX in range(0,getWidth(b)):
    targetY=getHeight(c)-getHeight(b)
    for sourceY in range(0,getHeight(b)):
      color=getColor(getPixel(b,sourceX,sourceY))
      setColor(getPixel(c,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  repaint(c)
  
#5. x
def lab5():
  bf=getMediaPath("barbara.jpg")
  b=makePicture(bf)
  cf=getMediaPath("7inX95in.jpg")
  c=makePicture(cf)
  targetX=0
  for sourceX in range(0,getWidth(b)):
    targetY=0
    for sourceY in range(0,getHeight(b)):
      color=getColor(getPixel(b,sourceX,sourceY))
      setColor(getPixel(c,targetY,targetX),color)
      targetY=targetY+1
    targetX=targetX+1
  repaint(c)
  
#6. x
def lab6():
  bf=getMediaPath("butterfly1.jpg")
  b=makePicture(bf)
  for p in getPixels(b):
    px=getPixels(p)
    here=getPixels(p,x,y)
    hereL=getPixel(p,x-1,y)
    hereR=getPixel(p,x+1,y)  
    newR=(getRed(here)+getRed(hereL)+getRed(hereR))/3
    newG=(getGreen(here)+getGreen(hereL)+getBlue(hereR))/3
    newB=(getBlue(here)+getBlue(hereL)+getBlue(hereR))/3
    setColor(p,makeColor(sewR,newG,newB))
  repaint(pic)
    
