#,lab1() i do...? 
def lab1():
  b=makePicture(getMediaPath("barbara.jpg"))
  for x in getPixels(b):
    gb=getBlue(x)
  return(gb)
  
#,lab2() i don't know  
def lab2():
  c1=makePicture(getMediaPath("7inx95in.jpg"))
  c2=makePicture(getMediaPath("640x480.jpg"))
  for x in getPixels(c1):
    for x2 in getPixels(c2):
      choice=max(x,x2)
  repaint(c1)
  
#,lab3() i do!!  
def lab3():
  b=makePicture(getMediaPath("barbara.jpg"))
  for x in getPixels(b):
    avg=(getGreen(x)+getBlue(x))/2
    setRed(x,avg)
  repaint(b)

#,lab4() i do!! 
def lab4():
  b=makePicture(getMediaPath("barbara.jpg"))
  c=makePicture(getMediaPath("7inx95in.jpg"))
  targetx=282
  for x in range(0,getWidth(b)):
    targety=390
    for y in range(0,getHeight(b)):
      bpx=getPixel(b,x,y)
      bc=getColor(bpx)
      cpx=getPixel(c,targetx,targety)
      setColor(cpx,bc)
      targety=targety+1
    targetx=targetx+1
  repaint(c)
  
#,lab5() i do!! 
def lab5():
  b=makePicture(getMediaPath("barbara.jpg"))
  c=makePicture(getMediaPath("7inx95in.jpg"))
  targetx=0
  for x in range(0,getWidth(b)):
    targety=0
    for y in range(0,getHeight(b)):
      bpx=getPixel(b,x,y)
      bc=getColor(bpx)
      cpx=getPixel(c,targety,targetx)
      setColor(cpx,bc)
      targety=targety+1
    targetx=targetx+1
  repaint(c)

#,lab6() i do!! 
def lab6():
  b=makePicture(getMediaPath("butterfly1.jpg"))
  for x in range(0,getWidth(b)-1):
    for y in range(0,getHeight(b)-1):
      here=getPixel(b,x,y)
      right=getPixel(b,x+1,y)
      down=getPixel(b,x,y+1)
      hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
      rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
      downL=(getRed(down)+getGreen(down)+getBlue(down))/3
      absdown=hereL-downL
      absright=hereL-rightL
      if absdown>10 and absright>10:
        setColor(here,white)
      else:
        setColor(here,black)
  repaint(b)
  
def main():
  #lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  return(lab1())
  