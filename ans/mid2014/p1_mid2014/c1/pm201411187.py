def copyBarb():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=282
  for x in range(0,getWidth(barb)):
    targetY=390
    for y in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,x,y))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(barb)
  show(canvas)
  return canvas


  