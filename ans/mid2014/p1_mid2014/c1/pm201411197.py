def lab_1():
  p1=makePicture(getMediaPath('barbara.jpg'))
  Redpx=getRed(getPixel(p1,1,1))
  print Redpx
  
def lab_2():
  p1=makePicture(getMediaPath('7inX95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  p1px=getWidth(p1)
  p2px=getWidth(p2)
  if p1px>p2px:
    show(p1)
  else:
    show(p2)
    

def lab_3():
  p1=makePicture(getMediaPath('barbara.jpg'))
  for px in getPixels(p1):
    newRGB=makeColor(255-getRed(px),255-getGreen(px),255-getBlue(px))
    setColor(px,newRGB)
  repaint(p1)
  for px in getPixels(p1):
    newRGB=makeColor((getRed(px)+getGreen(px)+getBlue(px))/3,(getRed(px)+getGreen(px)+getBlue(px))/3,(getRed(px)+getGreen(px)+getBlue(px))/3)
    setColor(px,newRGB)
  repaint(p1)
  
  
def lab_4():
  p1=makePicture(getMediaPath('barbara.jpg'))
  bg=makePicture(getMediaPath('7inX95in.jpg'))
  tx=0
  for x in range(0,getWidth(p1)):
    ty=getHeight(bg)-getHeight(p1)
    for y in range(0,getHeight(p1)):
      barbpx=getPixel(p1,x,y)
      barbcol=getColor(barbpx)
      bgpx=getPixel(bg,tx,ty)
      setColor(bgpx,barbcol)
      ty=ty+1
    tx=tx+1
  repaint(bg)  
  
def lab_5():
  p1=makePicture(getMediaPath('barbara.jpg'))
  bg=makePicture(getMediaPath('7inX95in.jpg'))
  sx=0
  for x in range(0,getWidth(p1)*2):
    sy=0
    for y in range(0,getHeight(p1)*2):
      barbpx=getPixel(p1,int(sx),int(sy))
      barbcol=getColor(barbpx)
      bgpx=getPixel(bg,int(x),int(y))
      setColor(bgpx,barbcol)
      sy=sy+.5
    sx=sx+.5
  repaint(bg)
         
def lab_6():
  p1=makePicture(getMediaPath('blue-mark.jpg'))
  bg=makePicture(getMediaPath('waterfall.jpg'))
  tx=0
  for x in range(0,getWidth(p1)):
    ty=0
    for y in range(0,getHeight(p1)):
      p1px=getPixel(p1,x,y)
      r=getRed(p1px)
      g=getGreen(p1px)
      b=getBlue(p1px)
      bgcol=getColor(getPixel(bg,tx,ty))
      if b>r and b>g:
        setColor(p1px,bgcol)
      ty=ty+1
    tx=tx+1
  repaint(p1)
  
def main():
  lab_1()
  lab_2()
  lab_3()
  lab_4()
  lab_5()
  lab_6()