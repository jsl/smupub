def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  
def lab1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  p=getPixel(barb,0,0)
  r=getRed(p)
  print r
  
def lab2():
  canvas1=makePicture(getMediaPath('7inX95in.jpg'))
  canvas2=makePicture(getMediaPath('640x480.jpg'))
  if (getWidth(canvas1)*getHeight(canvas1))>(getWidth(canvas2)*getHeight(canvas2)):
    show(canvas1)
  else:
    show(canvas2)
    
def lab3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  negative(barb)
  grayscale(barb)
  show(barb)

def negative(pic):
  for p in getPixels(pic):
    newColor=makeColor(255-getRed(p),255-getGreen(p),255-getBlue(p))
    setColor(p,newColor)
  return(pic)
  
def grayscale(pic):
  for p in getPixels(pic):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    avg=(r+g+b)/3
    setColor(p,makeColor(avg,avg,avg))
  return(pic)
  
def lab4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=getHeight(canvas)-getHeight(barb)
    for sourceY in range(0,getHeight(barb)):
      spx=getPixel(barb,sourceX,sourceY)
      tpx=getPixel(canvas,targetX,targetY)
      sColor=getColor(spx)
      setColor(tpx,sColor)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return(canvas)
  
def lab5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  sourceX=0
  for targetX in range(0,getWidth(barb)*2):
    sourceY=0
    for targetY in range(0,getHeight(barb)*2):
      spx=getPixel(barb,int(sourceX),int(sourceY))
      tpx=getPixel(canvas,targetX,targetY)
      sColor=getColor(spx)
      setColor(tpx,sColor)
      sourceY=sourceY+0.5
    sourceX=sourceX+0.5
  show(canvas)
  
def lab6():
  bmp=makePicture(getMediaPath('blue-mark.jpg'))
  water=makePicture(getMediaPath('waterfall.jpg'))
  for x in range(0,getWidth(bmp)):
    for y in range(0,getHeight(bmp)):
      p=getPixel(bmp,x,y)
      if getBlue(p)>(getGreen(p)+getRed(p)):
        setColor(p,getColor(getPixel(water,x,y)))
  show(bmp)
  