def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  pict = makePicture(getMediaPath("barbara.jpg"))
  print getRed(getPixel(pict, 0, 0))
  
def lab2():
  pict1 = makePicture(getMediaPath("7inX95in.jpg"))
  pict2 = makePicture(getMediaPath("640x480.jpg"))
  if getWidth(pict1) > getWidth(pict2):
    show(pict1)
  else:
    show(pict2)
    
def lab3():
  pict = makePicture(getMediaPath("barbara.jpg"))
  show(grayscale(negative(pict)))
    
def negative(pict):
  for px in  getPixels(pict):
    setColor(px, makeColor(255 - getRed(px), 255 - getGreen(px), 255 - getBlue(px)))
  return pict
  
def grayscale(pict):
  for px in getPixels(pict):
    luminance = (getRed(px) + getGreen(px) + getBlue(px)) / 3
    setColor(px, makeColor(luminance,luminance,luminance))
  return pict
  
def lab4():
  pict = makePicture(getMediaPath("barbara.jpg"))
  canvas = makePicture(getMediaPath("7inX95in.jpg"))
  show(copyPicture(pict, canvas, 0, getHeight(canvas) - getHeight(pict)))
  
def copyPicture(pict, canvas, positionX, positionY):
  targetX = positionX
  for sourceX in range(0, getWidth(pict)):
    targetY = positionY
    for sourceY in range(0, getHeight(pict)):
      setColor(getPixel(canvas, targetX, targetY), getColor(getPixel(pict, sourceX, sourceY)))
      targetY = targetY + 1
    targetX = targetX + 1
  return canvas
  
def lab5():
  pict = makePicture(getMediaPath("barbara.jpg"))
  canvas = makePicture(getMediaPath("7inX95in.jpg"))
  show(copyLarger(pict, canvas)) 

def copyLarger(pict, canvas):
  sourceX = 0
  for targetX in range(0, getWidth(pict)*2):
    sourceY = 0
    for targetY in range(0, getHeight(pict)*2):
      setColor(getPixel(canvas, targetX, targetY), getColor(getPixel(pict, int(sourceX), int(sourceY))))
      sourceY = sourceY + 0.5
    sourceX = sourceX + 0.5
  return canvas
  
def lab6():
  pict = makePicture(getMediaPath("blue-mark.jpg"))
  background = makePicture(getMediaPath("waterfall.jpg"))
  show(chromakey(pict, background))
  
def chromakey(pict, background):
  for sourceX in range(0, getWidth(pict)):
    for sourceY in range(0, getHeight(pict)):
      px = getPixel(pict, sourceX, sourceY)
      if(getRed(px) + getGreen(px) < getBlue(px)):
        setColor(getPixel(pict, sourceX, sourceY), getColor(getPixel(background, sourceX, sourceY)))
  return pict