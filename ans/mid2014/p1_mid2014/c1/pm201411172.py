def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()


def lab1():
  pict1 = makePicture(getMediaPath("barbara.jpg"))
  pict2 = getPixel(pict1, 0, 0)
  print getRed(pict2)


def lab2():
  pict1 = makePicture(getMediaPath("7inX95in.jpg"))
  pict2 = makePicture(getMediaPath("640x480.jpg"))
  p1 = getWidth(pict1) * getHeight(pict1)
  p2 = getWidth(pict2) * getHeight(pict2)
  if p1 > p2:
    show(pict1)
  else:
    show(pict2)


def lab3():
  pict1 = makePicture(getMediaPath("barbara.jpg"))
  pict2 = changePicture(pict1)

def changePicture(p1):
  for x in range(0, getWidth(p1)):
    for y in range(0, getHeight(p1)):
      R = getRed(getPixel(p1, x, y))
      G = getGreen(getPixel(p1, x, y))
      B = getBlue(getPixel(p1, x, y))
      setRed(getPixel(p1, x, y), 255-R)
      setGreen(getPixel(p1, x, y), 255-G)
      setBlue(getPixel(p1, x, y), 255-B)
  for x in range(0, getWidth(p1)):
    for y in range(0, getHeight(p1)):
      sColor = getRed(getPixel(p1, x, y)) + getGreen(getPixel(p1, x, y)) + getBlue(getPixel(p1, x, y)) / 3
      setRed(getPixel(p1, x, y), sColor)
      setGreen(getPixel(p1, x, y), sColor)
      setBlue(getPixel(p1, x, y), sColor)
    repaint(p1)



def lab4():
  pict1 = makePicture(getMediaPath("barbara.jpg"))
  pict2 = makePicture(getMediaPath("7inX95in.jpg"))
  copyPicture(pict1, pict2, 0, 390)
  show(pict2)
  

def copyPicture(source, canvas, targX, targY):
  targetX = targX
  for sourceX in range(0, getWidth(source)):
    targetY = targY
    for sourceY in range(0, getHeight(source)):
      r = getRed(getPixel(source, sourceX, sourceY))
      g = getGreen(getPixel(source, sourceX, sourceY))
      b = getBlue(getPixel(source, sourceX, sourceY))
      cp = getPixel(canvas, targetX, targetY)
      setRed(cp, r)
      setGreen(cp, g)
      setBlue(cp, b)
      targetY = targetY + 1
    targetX = targetX + 1
  return canvas
  

def lab5():
  pict1 = makePicture(getMediaPath("barbara.jpg"))
  pict2 = makePicture(getMediaPath("7inX95in.jpg"))


def lab6():
  pict1 = makePicture(getMediaPath("blue-mark.jpg"))
  pict2 = makePicture(getMediaPath("waterfall.jpg"))
