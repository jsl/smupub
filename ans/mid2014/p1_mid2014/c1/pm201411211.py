#1
def showred():
  pic=makePicture(getMediaPath("barbara.jpg"))
  red=getRed(getPixel(pic,1,1))
  print red
  
#2
def showpic():
  pic1=makePicture(getMediaPath("7inX95in.jpg"))
  pic2=makePicture(getMediaPath("640x480.jpg"))
  pixel1=getWidth(pic1)
  pixel2=getWidth(pic2)
  if pixel1>pixel2:
    show(pic1)
  else:
    show(pic2)

#3
def grayscale():
  barb=makePicture(getMediaPath("barbara.jpg."))
  for p in getPixels(barb):
    red=getRed(p)
    green=getGreen(p)
    blue=getBlue(p)
    color=makeColor(255-red,255-green,255-blue)
    setColor(p,color)
  show(barb)
  for p in getPixels(barb):
    red1=getRed(p)
    green1=getGreen(p)
    blue1=getBlue(p)
    color1=makeColor(red1*0.5,green1*0.5,blue1*0.5)
    setColor(p,color1)
  repaint(barb)
    
#4
def move():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=getHeight(canvas)-getHeight(barb)
    for sourceY in range(0,getHeight(barb)):
      canvaspix=getPixel(canvas,targetX,targetY)
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(canvaspix,color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
    
#5
#6