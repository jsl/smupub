def main():
  lab1()
  
def lab1():
  four()

def abc():
  b=makePicture(getMediaPath("barbara.jpg"))
  for x in getPixels(b):
    for y in getPixels(b):
      px=getColor(getPixel(b,x,y))
      r=getRed(b)
      g=getGreen(b)
      b=getBlue(b)
      negCol=makeColor(255-r,255-g,255-b)
      setColor(px,negCol)
  show(b)


def four():
  b=makePicture(getMediaPath("barbara.jpg"))
  c=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=282
  for x in range(0,getWidth(b)):
    targetY=390
    for y in range(0,getHeight(b)):
      col=getColor(getPixel(b,x,y))
      setColor(getPixel(c,targetX,targetY),col)
      targetY=targetY+1
    targetX=targetX+1
  show(c)
  return c
  
def bigger():
  b=makePicture(getMediaPath("barbara.jpg"))
  c=makePicture(getMediaPath("7inX95in.jpg"))
  x=getWidth(b)
  for targetX in range(100,100+(200-45)*2):
    y=getHeight(b)
    for targetY in range(100,100+(200-45)*2):
      col=getColor(getPixel(b,x,y))     
      setColor(getPixel(c,targetX,targetY),col)
      y=y+0.5
    x=x+0.5
  show(c)
  return c

  
def chro():
  b=makePicture(getMediaPath("blue-mark.jpg"))
  w=makePicture(getMediaPath("waterfool.jpg"))
  bPixels=getColor(getPixel(b))
  wPixels=getColor(getPixel(w))