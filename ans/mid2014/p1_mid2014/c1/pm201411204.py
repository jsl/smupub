def main():
  lab1()
  lab2()
  lab3()
  lab4()

  
def lab1():
  p=makePicture(getMediaPath('barbara.jpg'))
  px=getPixel(p,0,0)
  r=getRed(px)
  print r

  
  
  
def lab2():
  p1=makePicture(getMediaPath('7inX95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  if getWidth(p1)*getHeight(p1) > getWidth(p2)*getHeight(p2):
    show(p1)
  if getWidth(p1)*getHeight(p1) < getWidth(p2)*getHeight(p2):
    show(p2)
    
def lab3():
  pic=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(pic):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    nc=makeColor(255-r,255-g,255-b)
    setColor(p,nc)
    gc=makeColor((r+g+b)/3,(r+g+b)/3,(r+g+b)/3)
    setColor(p,gc)   
  show(pic)
    
    
def lab4():
  s=makePicture(getMediaPath('barbara.jpg'))
  t=makePicture(getMediaPath('7inX95in.jpg'))
  for y in range(0,getHeight(s)):
    for x in range(0,getWidth(s)):
      ps=getPixel(s,x,y)
      pt=getPixel(t,x,390+y)
      cs=getColor(ps)
      setColor(pt,cs)
  show(t)
 

def lab5():
  s=makePicture(getMediaPath('barbara.jpg'))
  t=makePicture(getMediaPath('7inX95in.jpg'))
  for y in range(0,getHeight(s)):
    for x in range(0,getWidth(s)):