def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  file=getMediaPath('barbara.jpg')
  pic=makePicture(file)
  px00=getPixel(pic,0,0)
  r=getRed(px00)
  print(r)
  
def lab2():
  pic1=makePicture(getMediaPath('7inX95in.jpg'))
  pic2=makePicture(getMediaPath('640x480.jpg'))
  if getWidth(pic1)*getHeight(pic1) > getWidth(pic2)*getHeight(pic2):
    show(pic1)
  if getWidth(pic1)*getHeight(pic1) < getWidth(pic2)*getHeight(pic2):
    show(pic2)
    
def negative():
  file=getMediaPath('barbara.jpg')
  pic=makePicture(file)
  for x in range(0,getWidth(pic)):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      r=255-getRed(px)
      g=255-getGreen(px)
      b=255-getBlue(px)
      newColor=makeColor(r,g,b)
      setColor(px,newColor)
  show(pic)
  writePictureTo(pic,'pm201411167.jpg')
  
def lab3():
  file=getMediaPath('pm201411167.jpg')
  pic=makePicture(file)
  for x in range(0,getWidth(pic)):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      color=makeColor((getRed(px)+getGreen(px)+getBlue(px))/3)
      setColor(px,color)
  show(pic)
  print(' I use writePictureTo()function in the function which name is negative(), and I use the picture.')
  
def lab4():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  targetX=0
  for x in range(0,getWidth(barb)):
    targetY=getHeight(canvas)-getHeight(barb)
    for y in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,x,y))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  
def lab5():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  sourceX=45
  for targetX in range(100,100+(200-45)*2):
    sourceY=25
    for targetY in range(100,100+(200-25)*2):
      color=getColor(getPixel(barb,int(sourceX),int(sourceY)))
      setColor(getPixel(canvas,int(targetX),int(targetY)),color)
      sourceY=sourceY+0.5
    sourceX=sourceX+0.5
  show(canvas)
  
def lab6():
  file=getMediaPath('blue-mark.jpg')
  pic=makePicture(file)
  canvasf=getMediaPath('waterfall.jpg')
  canvas=makePicture(canvasf)
  targetX=0
  for x in range(0,getWidth(pic)):
    targetY=0
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      tx=getPixel(canvas,targetX,targetY)
      color=getColor(tx)
      if getBlue(px) > getGreen(px) and getBlue(px) > getRed(px):
        setColor(px,color)
      targetY=targetY+1
    targetX=targetX+1
  show(pic)
  
      
  
  