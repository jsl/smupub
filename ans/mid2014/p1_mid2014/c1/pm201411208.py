def lab1():
  pic=makePicture(getMediaPath("barbara.jpg"))
  r=getPixel(pic,0,0)
  print(r)

def lab2():
  p1=makePicture(getMediaPath("7inX95in.jpg"))
  p2=makePicture(getMediaPath("640x480.jpg"))
  if getPixels(p1)<getPixels(p2):
    print "p2"
  else:
    print "p1"

def negative(pic):
  for p in getPixels(pic):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    setColor(p,makeColor(255-r,255-g,255-b))

def greyscale(pic):
  for p in getPixels(pic):
    int=(getRed(p)+getGreen(p)+getBlue(p))/3
    setColor(p,makeColor(int,int,int))

def lab3():
  pic=makePicture(getMediaPath("barbara.jpg"))
  negative(pic)
  greyscale(pic)
  show(pic)

def lab4():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceX in range(0,getHeight(barb)):
      targetY=targetY+1
  targetX=targetX+1
  show(canvas)
  return(canvas)

def lab5():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceX in range(0,getHeight(barb)):
      targetY=targetY+1
  targetX=targetX+1
  show(canvas)
  return(canvas)

def lab6():
  p1=makePicture(getMediaPath("blue-mark.jpg"))
  p2=makePicture(getMediaPath("watetfall.jpg"))
  show(p1)

def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()