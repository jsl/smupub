def main():
  lab1()
  lab2()
  lab4()
  lab6()

def lab1():
  redPicker()
  
def lab2():
  biggerPicker()
  
def lab4():
  copy()

def lab5():
  bigger()
  
def lab6():
  Chromakey()


def redPicker():
  bp=makePicture(getMediaPath('barbara.jpg'))
  px=getPixel(bp,0,0)
  print getRed(px)
  return bp
  
  
def biggerPicker():
  pic1=makePicture(getMediaPath('7inX95in.jpg'))
  pic2=makePicture(getMediaPath('640x480.jpg'))
  p1=getWidth(pic1)*getHeight(pic1)
  p2=getWidth(pic2)*getHeight(pic2)
  if p1>p2:
    show(pic1)
  else:
    show(pic2)
    
def copy():
  bp=makePicture(getMediaPath('barbara.jpg'))
  cp=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for x in range(0,getWidth(bp)):
    targetY=getHeight(cp)-getHeight(bp)
    for y in range(0,getHeight(bp)):
      setColor(getPixel(cp,targetX,targetY),getColor(getPixel(bp,x,y)))
      targetY=targetY+1
    targetX=targetX+1
  show(cp)

def bigger():
  bp=makePicture(getMediaPath('barbara.jpg'))
  cp=makePicture(getMediaPath('7inX95in.jpg'))
  sourceX=0
  for x in range(0,getWidth(bp)*2):
    sourceY=0
    for y in range(0,getHeight(bp)*2):
      setColor(getPixel(bp,sourceX,sourceY),getColor(getPixel(cp,x,y)))
      sourceY=sourceY+0.5
    sourceX=sourceX+0.5
  show(cp)
    
def Chromakey():
  bp=makePicture(getMediaPath('blue-mark.jpg'))
  backp=makePicture(getMediaPath('waterfall.jpg'))
  for x in range(0,getWidth(bp)):
    for y in range(0,getHeight(bp)):
      if getGreen(getPixel(bp,x,y))+getRed(getPixel(bp,x,y))<getBlue(getPixel(bp,x,y)):
        setColor(getPixel(bp,x,y),getColor(getPixel(backp,x,y)))
  show(bp)

  