def lab1_():
  barb=makePicture(getMediaPath('barbara.jpg'))
  p=getPixel(barb,1,1)
  q=getRed(p)
  print q
  
def lab_2():
  a=makePicture(getMediaPath('7inX95in.jpg'))
  b=makePicture(getMediaPath('640x480.jpg'))
  a1=getWidth(a)
  b1=getWidth(b)
  if a1>b1:
    show(a)
  if a1<=b1:
    show(b)  
    
def lab_3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(barb):
    p1=getColor(p)
    setColor(p,p1)
  repaint(barb)  
 
  
   

    
def lab_4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  bg=makePicture(getMediaPath('7inX95in.jpg'))
  sourceX=0
  for targetX in range(0,getWidth(barb)):
    sourceY=390
    for targetY in range(0,getHeight(barb)):
      p=getPixel(barb,targetX,targetY)
      q=getColor(p)
      t=getPixel(bg,sourceX,sourceY)
      setColor(t,q)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(bg)
  return bg
      
def lab_5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  bg=makePicture(getMediaPath('7inX95in.jpg'))
  sourceX=0
  for targetX in range(0,getWidth(barb),2):
    sourceY=0
    for targetY in range(0,getHeight(barb),2):
      p=getPixel(barb,targetX,targetY)
      q=getColor(p)
      t=getPixel(bg,sourceX,sourceY)
      setColor(t,q)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=0
  for targetX in range(1,getWidth(barb),2):
    sourceY=0
    for targetY in range(1,getHeight(barb),2):
      p=getPixel(barb,targetX,targetY)
      q=getColor(p)
      t2=getPixel(bg,sourceX,sourceY)
      setColor(t2,q)  
      sourceY=sourceY+1
    sourceX=sourceX+1  
  show(bg)
  return bg

  

  
def lab_6():
  blu=makePicture(getMediaPath('blue-mark.jpg'))
  wat=makePicture(getMediaPath('waterfall.jpg'))
  sourceX=0
  for x in range(0,getWidth(blu)):
    sourceY=0
    for y in range(0,getHeight(blu)):
      p=getPixel(blu,x,y)
      q=getPixel(wat,sourceX,sourceY)
      q1=getColor(q)
      r=getRed(p)
      g=getGreen(p)
      b=getBlue(p)
      sourceY=sourceY+1
      if r<b and g<b :
        setColor(p,q1)
    sourceX=sourceX+1     
  show(blu)
  return(blu)    
    
    
def main():
  lab_1()
  lab_2()
  lab_3()
  lab_4()
  lab_5()
  lab_6()
  
    
      
  
      
   