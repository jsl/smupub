def main():
 lab1()
 lab2()
 lab4()

def lab1():
 pb=getMediaPath('barbara.jpg')
 mb=makePicture(pb)
 gb=getPixel(mb,0,0)
 print(gb)

def lab2():
 p7in=getMediaPath('7inX95in.jpg')
 m7in=makePicture(p7in)
 w7in=getWidth(m7in)
 h7in=getHeight(m7in)
 print(w7in*h7in)
 p640=getMediaPath('640x480.jpg')
 m640=makePicture(p640)
 w640=getWidth(m640)
 h640=getHeight(m640)
 print(w640*h640)
 if (w7in*h7in)<(w640*h640):
  show(m640)
 if (w7in*h7in)>(w640*h640):
  show(m7in)

def lab4():
 pb=getMediaPath('barbara.jpg')
 mb=makePicture(pb)
 pc=getMediaPath('7inX95in.jpg')
 mc=makePicture(pc)
 targetX=0
 for sourceX in range(0,getWidth(mb)):
  targetY=390
  for sourceY in range(0,getHeight(mb)):
   color=getColor(getPixel(mb,sourceX,sourceY))
   setColor(getPixel(mc,targetX,targetY),color)
   targetY=targetY+1
  targetX=targetX+1
 show(mc)
 return mc

def lab6():
 pb=getMediaPath('blue-mark.jpg')
 mb=makePicture(pb)
 pw=getMediaPath('waterfall.jpg')
 mw=makePicture(pw)
 targetX=0
 for sourceX in range(0,getWidth(mb)):
  targetY=0
  for sourceY in range(0,getHeight(mb)):
   color=getColor(getPixel(mb,sourceX,sourceY))
   if getRed(color)+getGreen(color)<getBlue(color):
    setColor(getPixel(mw,targetX,targetY),color)
   if getRed(color)+getGreen(color)>getBlue(color):
    setColor(getPixel(mw,targetX,targetY),color)
   targetY=targetY+1
  targetX=targetX+1
 show(mw)
 return mw
 