def lab1():
  pic=makePicture(getMediaPath("barbara.jpg"))
  px00=getPixel(pic,0,0)
  print getRed(px00)
def lab2():
  pic=makePicture(getMediaPath("7inx95in.jpg"))
  pid=makePicture(getMediaPath("640x480.jpg"))
  cw=getWidth(pic)
  dw=getWidth(pid)
  if cw>dw:
    show(pic)
  else:
    show(pid)
  
def lab3():
  pic=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(pic):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    lum=(r+g+b)/3
    color1=makeColor(255-r,255-g,255-b)
    color2=makeColor(lum,lum,lum)
    setColor(p,color1)
    setColor(p,color2)
  show(pic)
    
def lab4():
  pic=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inx95in.jpg"))
  targetX=0
  for x in range(0,getWidth(pic)):
    targetY=390
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      color=getColor(px)
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)

def lab5():
  pic=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inx95in.jpg"))
  sourceX=0
  for x in range(0,getWidth(pic)*2):
    sourceY=0
    for y in range(0,getHeight(pic)*2):
      px=getPixel(pic,int(sourceX),int(sourceY))
      color=getColor(px)
      setColor(getPixel(canvas,x,y),color)
      sourceY=sourceY+0.5
    sourceX=sourceX+0.5
  show(canvas)
  
def lab6():
  pic=makePicture(getMediaPath("blue-mark.jpg"))
  canvas=makePicture(getMediaPath("waterfall.jpg"))
  for x in range(0,getWidth(pic)):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      r=getRed(px)
      g=getGreen(px)
      b=getBlue(px)
      color=getColor(getPixel(canvas,x,y))
      if (r+g)<b:
        setColor(px,color)
  show(pic)
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()