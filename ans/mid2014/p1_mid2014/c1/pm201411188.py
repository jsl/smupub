def main():
    lab1_1()
    lab1_2()
    lab1_3()
    lab1_4()
    lab1_5()
   

def lab1_1():
     callRed()

def lab1_2():
      twoP(pic1,pic2)
 
def lab1_3():
     negative(pic) 

def lab1_4():
    copyBarb()
    
def lab1_5():
    copyBarb2()
    
    
    

def callRed():
  pic=makePicture(getMediaPath('barbara.jpg'))
  a=getRed(getPixel(pic,1,1))
  print a
  
def twoP(pic1,pic2):
  pic1=makePicture(getMediaPath('7inX95in.jpg'))
  pic2=makePicture(getMediaPath('640x480.jpg'))
  if getWidth(pic1)>getWidth(pic2):
    show(pic1)
  if getWidth(pic1)<getWidth(pic2):
    show(pic2)  
    
def negative(pic):
  for p in getPixels(pic):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    neg=makeColor(255-r,255-g,255-b)
    setColor(p,neg)
    intensity=(getRed(p)+getGreen(p)+getBlue(p))/3
    setColor(p,makeColor(intensity,intensity,intensity))
  show(pic)
  
def greyScale(pic):
  for p in getPixels(pic):
    intensity=(getRed(p)+getGreen(p)+getBlue(p))/3
    setColor(p,makeColor(intensity,intensity,intensity))
  show(pic)
  
def copyBarb():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(barb)):
    targetY=390
    for sourceY in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas
  
def copyBarb2():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  sourceX=45
  for targetX in range(100,100+(200-45)*2):
    sourceY=25
    for targetY in range(100,100+(200-25)*2):
      color=getColor(getPixel(barb,int(sourceX),int(sourceY)))
      setColor(getPixel(canvas,targetX,targetY),color)
      sourceY=sourceY+0.5
    sourceX=sourceX+0.5
  show(canvas)
  return canvas
  
