def lab_1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  p = getPixel(barb,0,0)
  wr = getRed(p)
  print wr 
  
def lab_2():
  can1=makePicture(getMediaPath('7inX95in.jpg'))
  can2=makePicture(getMediaPath('640x480.jpg'))
  
def lab_3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(barb):
    red=getRed(p)
    green=getGreen(p)
    blue=getBlue(p)
    negColor = makeColor(255-red,255-green,255-blue)
    setColor(p,negColor)
    mid = (getRed(p)+getGreen(p)+getBlue(p))/3
    color = makeColor(mid,mid,mid)
    setColor(p,color)
  show(barb)
  
def lab_4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('7inX95in.jpg'))
  targetX = 0
  for sourceX in range(0,getWidth(barb)):
    targetY = 390
    for sourceY in range(0,getHeight(barb)):
      color = getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(can,targetX,targetY),color)
      targetY = targetY + 1
    targetX = targetX + 1
  show(can)
  
def lab_5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('7inX95in.jpg'))
  targetX = 0
  for sourceX in range(0,getWidth(barb)*2):
    targetY = 0
    for sourceY in range(0,getHeight(barb)*2):
      color = getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(can,targetX,targetY),color)
      targetY = targetY + 0.5
    targetX = targetX + 0.5
  show(can)
  
def lab_6():
  bm=makePicture(getMediaPath('blue-mark.jpg'))
  bg=makePicture(getMediaPath('waterfall.jpg'))
  targetX = 0
  for sourceX in range(0,getWidth(bm)):
    targetY = 0
    for sourceY in range(0,getHeight(bm)):
      color = getColor(getPixel(bm,sourceX,sourceY))
      if distance(blue,color) < 10.0:
        setColor(bm,gcolor)
      targetY = targetY + 1
    targetX = targetX + 1
  show(bg)
  show(bm)
  return bg
  
def main():
  lab_1()
  lab_2()
  lab_3()
  lab_4()
  lab_5()
  lab_6()