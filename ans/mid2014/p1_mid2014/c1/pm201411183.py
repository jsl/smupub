def lab1():
  bp=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(bp):
    r=getRed(p)
  print r
  
def lab2():
  sp=makePicture(getMediaPath('7inX95in.jpg'))
  lp=makePicture(getMediaPath('640x480.jpg'))
  spw=getWidth(sp)
  lpw=getWidth(lp)
  if spw < lpw:
    show(lp)

def lab3():
  bp=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(bp):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    setColor(p,makeColor(255-r,255-g,255-b))
  for p in getPixels(bp):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    lum=(r+g+b)/3
    setColor(p,makeColor(lum,lum,lum))
  repaint(bp)
  
def lab4():
  bp=makePicture(getMediaPath('barbara.jpg'))
  sp=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(bp)):
    targetY=getHeight(sp)-getHeight(bp)
    for sourceY in range(0,getHeight(bp)):
      p=getPixel(bp,sourceX,sourceY)
      b=getPixel(sp,targetX,targetY)
      setColor(b,getColor(p))
      targetY=targetY+1
    targetX=targetX+1
  show(sp)
  return sp
  
def lab5():
  bp=makePicture(getMediaPath('barbara.jpg'))
  sp=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=10
  for sourceX in range(0,getWidth(bp)):
    targetY=10
    for sourceY in range(0,getHeight(bp)):
      p=getPixel(bp,sourceX,sourceY)
      b=getPixel(sp,targetX,targetY)
      setColor(b,getColor(p))
      targetY=targetY+2
    targetX=targetX+2
  show(sp)
  return sp

def lab6():
  bmp=makePicture(getMediaPath('blue-mark.jpg'))
  wp=makePicture(getMediaPath('waterfall.jpg'))
  for x in range(1,getWidth(bmp)):
    for y in range(1,getHeight(bmp)):
      bmpi=getPixel(bmp,x,y)
      r=getRed(bmpi)
      g=getGreen(bmpi)
      b=getBlue(bmpi)
      wpi=getPixel(wp,x,y)
      if r+g<b:
        setColor(bmpi,getColor(wpi))
  show(bmp)
  return bmp
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()