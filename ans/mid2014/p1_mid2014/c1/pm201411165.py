def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  getPixel(pb,0,0)
  print getPixel(pb,0,0)
  
def lab2():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  for p in getPixels(pb):
    red=getRed(p)
    green=getGreen(p)
    blue=getBlue(p)
    negColor=makeColor(255-red,255-green,255-blue)
    setColor(p,negColor)
  explore(pb)
  
def grayscale():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  for p in getPixels(pb):
    k=(getRed(p)+getGreen(p)+getBlue(p))/3
    setColor=makeColor(k,k,k)
  explore(pb)  
  
def lab4():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  fc=getMediaPath('7inX95in.jpg')
  pc=makePicture(fc)
  targetX=0
  for sourceX in range(0,getWidth(pb)):
    targetY=390
    for sourceY in range(0,getHeight(pb)):
      color=getColor(getPixel(pb,sourceX,sourceY))
      setColor(getPixel(pc,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(pb)
  show(pc)
  explore(pc) 

def lab5():
  fb=getMediaPath('barbara.jpg')
  pb=makePicture(fb)
  fc=getMediaPath('7inX95in.jpg')
  pc=makePicture(fc)
  targetX=2
  for sourceX in range(0,getWidth(pb)):
    targetY=0
    for sourceY in range(0,getHeight(pb)):
      color=getColor(getPixel(pb,sourceX,sourceY))
      setColor(getPixel(pc,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(pb)
  show(pc)
  explore(pc)                        