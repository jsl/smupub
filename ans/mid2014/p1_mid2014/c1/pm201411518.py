def main():
  lab1()
  lab2()

def lab1():
  fb=getMediaPath("barbara.jpg")
  pb=makePicture(fb)
  print getPixel(pb,0,0)

def lab2():
  f1=getMediaPath("7inX95in.jpg")
  p1=makePicture(f1)
  f2=getMediaPath("640x480.jpg")
  p2=makePicture(f2)
  if getWidth(p1)>getWidth(p2):
    show(p1)
  else:
    show(p2)