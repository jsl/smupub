def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  barbaralastpixel()
  
def lab2():
  bigger()
  
def lab3():
  pic=change()
  show(pic)


def lab4():
  pic=move()
  show(pic)
  
def lab5():
  pic=move2()
  show(pic)


def lab6():
  pic=edge()
  show(pic)
  

  
def barbaralastpixel():
  f=getMediaPath('barbara.jpg')
  p=makePicture(f)
  width=getWidth(p)
  height=getHeight(p)
  px=getPixel(p,width-1,height-1)
  bl=getBlue(px)
  print bl
  
  
def bigger():
  f1=getMediaPath('7inX95in.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('640x480.jpg')
  p2=makePicture(f2)
  width1=getWidth(p1)
  height1=getHeight(p1)
  width2=getWidth(p2)
  height2=getHeight(p2)
  if width1*height1>width2*height2:
    show(p1)
  else:
    show(p2)
    
    
def change():
  f=getMediaPath('barbara.jpg')
  p=makePicture(f)
  for x in range(0,getWidth(p)):
    for y in range(0,getHeight(p)):
      px=getPixel(p,x,y)
      b=getBlue(px)
      g=getGreen(px)
      r=getRed(px)
      myC=makeColor((b+g)/2,g,b)
      setColor(px,myC)
  return p
  
def move():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('7inX95in.jpg')
  p2=makePicture(f2)
  tx=getWidth(p2)-getWidth(p1)
  for x in range(0,getWidth(p1)):
    ty=getHeight(p2)-getHeight(p1)
    for y in range(0,getHeight(p1)):
      px=getPixel(p1,x,y)
      color=getColor(px)
      tgpx=getPixel(p2,tx,ty)
      setColor(tgpx,color)
      ty=ty+1
    tx=tx+1
  return p2
  
  
def move2():
  f1=getMediaPath('barbara.jpg')
  p1=makePicture(f1)
  f2=getMediaPath('7inX95in.jpg')
  p2=makePicture(f2)
  tx=0
  for x in range(0,getWidth(p1)):
    ty=0
    for y in range(0,getHeight(p1)):
      px=getPixel(p1,x,y)
      color=getColor(px)
      tgpx=getPixel(p2,ty,tx)
      setColor(tgpx,color)
      ty=ty+1
    tx=tx+1
  return p2
  
  
def edge():
  f1=getMediaPath('butterfly1.jpg')
  p1=makePicture(f1)
  for x in range(0,getWidth(p1)-1):
    for y in range(0,getHeight(p1)-1):
      here=getPixel(p1,x,y)
      right=getPixel(p1,x,y+1)
      color1=getRed(here)+getBlue(here)+getGreen(here)
      color2=getRed(right)+getBlue(right)+getGreen(right)
      if abs(color1-color2)>40:
        setColor(here,black)
      else:
        setColor(here,white)
  return p1
      
      
 
