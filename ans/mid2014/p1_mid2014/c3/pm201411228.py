def main():
  print lab1()
  print lab2()
  print lab3()
  print lab4()
  print lab5()
  print lab6()

def lab1():
  pb=makePicture(getMediaPath('barbara.jpg'))
  p=getBlue(getPixel(pb,getWidth(pb)-1,getHeight(pb)-1))
  print p

def lab2():
  pm=makePicture(getMediaPath('7inX95in.jpg'))
  ps=makePicture(getMediaPath('640x480.jpg'))
  p1=getWidth(pm)
  p2=getHeight(pm)
  p3=getWidth(ps)
  p4=getHeight(ps)
  m=p1*p2
  s=p3*p4  
  if m>s:
    show(pm)
  else:
    show(ps)
    
      
def lab3():
  pb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(pb):
    b=getBlue(p)
    g=getGreen(p)
    r=(b+g)/2
    setRed(p,r)
  show(pb)

def lab4():
  pb=makePicture(getMediaPath('barbara.jpg'))
  cv=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=281
  for x in range(0,getWidth(pb)):
    targetY=389
    for y in range(0,getHeight(pb)):
      color=getColor(getPixel(pb,x,y))
      setColor(getPixel(cv,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(cv)
  return cv
  
def lab5():
  pb=makePicture(getMediaPath('barbara.jpg'))
  cv=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  for x in range(0,getWidth(pb)):
    targetY=0
    for y in range(0,getHeight(pb)):
      color=getColor(getPixel(pb,x,y))
      setColor(getPixel(cv,targetY,targetX),color)
      targetY=targetY+1
    targetX=targetX+1
  show(cv)
  return cv
  
def lab6():
  pb=makePicture(getMediaPath('butterfly1.jpg'))
  for p in getPixels(pb):
    s=getColor(p)
    r=getRed(p)
    b=getBlue(p)
    g=getGreen(p)
    i=makeColor((r+g+b)/3)
    if distance(i,s)<15:
      setColor(p,black)
    else:
      setColor(p,white)
  show(pb)
  return pb