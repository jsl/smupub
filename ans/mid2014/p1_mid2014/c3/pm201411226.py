def lab1():
  bf=getMediaPath('barbara.jpg')
  bp=makePicture(bf)
  width=getWidth(bp)
  height=getHeight(bp)
  px=getPixel(bp,width-1,height-1)
  bluepx=getBlue(px)
  print bluepx
  
def lab2():
  f1=getMediaPath('7inX95in.jpg')
  f2=getMediaPath('640x480.jpg')
  p1=makePicture(f1)
  p2=makePicture(f2)
  
  width1=getWidth(p1)
  width2=getWidth(p2)
  height1=getHeight(p1)
  height2=getHeight(p2)
  totalpx1=width1*height1
  totalpx2=width2*height2
  if totalpx1>totalpx2:
    show(p1)
  if totalpx2>totalpx1:
    show(p2)

  
def lab3():
  bf=getMediaPath('barbara.jpg')
  bp=makePicture(bf)
  for p in getPixels(bp):
    green=getGreen(p)
    blue=getBlue(p)
    newRed=(green+blue)/2
    newColor=makeColor(newRed,green,blue)
    setColor(p,newColor)
  repaint(bp)
  
def lab4():
  bf=getMediaPath('barbara.jpg')
  bp=makePicture(bf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  targetX=getWidth(canvas)-getWidth(bp)
  for sourceX in range(0,getWidth(bp)):
    targetY=getHeight(canvas)-getHeight(bp)
    for sourceY in range(0,getHeight(bp)):
      bpx=getPixel(bp,sourceX,sourceY)
      tpx=getPixel(canvas,targetX,targetY)
      bcolor=getColor(bpx)
      setColor(tpx,bcolor)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas
  
def lab5():
  bf=getMediaPath('barbara.jpg')
  bp=makePicture(bf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  targetX=0
  for sourceX in range(0,getWidth(bp)):
    targetY=0
    for sourceY in range(0,getHeight(bp)):
      bpx=getPixel(bp,sourceX,sourceY)
      tpx=getPixel(canvas,targetY,targetX)
      bcolor=getColor(bpx)
      setColor(tpx,bcolor)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas
  
def lab6():
  bf=getMediaPath('butterfly1.jpg')
  bp=makePicture(bf)
  for x in range(0,getWidth(bp)-1):
    for y in range(0,getHeight(bp)-1):
      here=getPixel(bp,x,y)
      down=getPixel(bp,x,y+1)
      right=getPixel(bp,x+1,y)
      hereL=(getRed(here)+getGreen(here)+getBlue(here))/3
      downL=(getRed(down)+getGreen(down)+getBlue(down))/3
      rightL=(getRed(right)+getGreen(right)+getBlue(right))/3
      absDown=abs(hereL-downL)
      absRight=abs(hereL-rightL)
      if absDown>10. and absRight>10.:
        setColor(here,black)
      else:
        setColor(here,white)
  show(bp)
  return bp
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  
      
  
  

    