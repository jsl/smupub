def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()


def lab1():
  p=makePicture(getMediaPath("barbara.jpg"))
  px=getPixel(p, getWidth(p)-1, getHeight(p)-1)
  blue=getBlue(px)
  print blue
  
def lab2():
  p1=makePicture(getMediaPath("640x480.jpg"))
  p2=makePicture(getMediaPath("7inX95in.jpg"))
  
  c1=getWidth(p1)*getHeight(p1)
  c2=getWidth(p2)*getHeight(p2)
  
  if c1>c2 :
    show(p1)
  else:
    show(p2)
    
def lab3():
  p=makePicture(getMediaPath("barbara.jpg"))
  
  for px in getPixels(p):
    blue=getBlue(px)
    green=getGreen(px)
    setRed(px, (blue+green)/2)
  show(p)
  
def lab4():
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  p=makePicture(getMediaPath("barbara.jpg"))
  
  for x in range(0, getWidth(p)):
    for y in range(0, getHeight(p)):
      px=getPixel(p, x, y)
      color=getColor(px)
      setColor(getPixel(canvas, getWidth(canvas)-getWidth(p)+x,  getHeight(canvas)-getHeight(p)+y), color)
  show(canvas)
  
def lab5():
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  p=makePicture(getMediaPath("barbara.jpg"))
  
  for x in range(0, getWidth(p)):
    for y in range(0, getHeight(p)):
      px=getPixel(p, x, y)
      color=getColor(px)
      setColor(getPixel(canvas, y, x), color)
  show(canvas)
  
def lab6():
  p=makePicture(getMediaPath("butterfly1.jpg"))
  val=215
  for x in range(1, getWidth(p)):
    for y in range(1, getHeight(p)):
      center=getPixel(p, x, y)
      down=getPixel(p, x, y-1)
      left=getPixel(p, x-1, y)s

      Cred=getRed(center)
      Cgreen=getGreen(center)
      Cblue=getBlue(center)
      Dred=getRed(down)
      Dgreen=getGreen(down)
      Dblue=getBlue(down)
      Lred=getRed(left)
      Lgreen=getGreen(left)
      Lblue=getBlue(left)

      #if abs(Cgreen-Dgreen) + abs(Cgreen-Lgreen) < 100 and abs(Cblue-Dblue) + abs(Cblue-Lblue) < 100:
        #setColor(center, white)
      if (abs(Cgreen-Dgreen) < val and abs(Cgreen-Lgreen) <val) and (abs(Cblue-Dblue) < val and abs(Cblue-Lblue) <val) and (abs(Cred-Dred) < val and abs(Cred-Lred) <val):
      #if (abs(Cgreen-Dgreen) - abs(Cgreen-Lgreen) <10) or (abs(Cblue-Dblue) - abs(Cblue-Lblue) <10):
        setColor(center, white)
        #setColor(center, white)
      else: #if abs(Cblue-Dblue)  < 10 and abs(Cgreen-Lgreen) < 10  and abs(Cred-Dred) < 10:
        setColor(center, black)
  show(p)