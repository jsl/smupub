def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  
def lab1():
 p1=makePicture(getMediaPath("barbara.jpg"))
 c=getPixel(p1,221,293)
 p1blue=getBlue(c)
 print p1blue
 
def lab2():
 p1=makePicture(getMediaPath("7inX95in.jpg"))
 p2=makePicture(getMediaPath("640x480.jpg"))
 for px in getPixels(p1):
   gg=getColor(px)
 for cx in getPixels(p2):
   cc=getColor(cx)
 if cc<gg:
   show(p1)
 else:
   show(p2)
 
def lab3():
 p1=makePicture(getMediaPath("barbara.jpg"))
 for x in range(0,getWidth(p1)):
   for y in range(0,getHeight(p1)):
     px=getPixel(p1,x,y)
     color=(getBlue(px)+getGreen(px))/2
     setRed(px,color)
 show(p1)
 return p1

def lab4():
 p1=makePicture(getMediaPath("barbara.jpg"))
 p2=makePicture(getMediaPath("7inX95in.jpg"))
 targetX=282
 for sourceX in range(0,getWidth(p1)):
   targetY=390
   for sourceY in range(0,getHeight(p1)):
     bp=getPixel(p1,sourceX,sourceY)
     cp=getPixel(p2,targetX,targetY)
     bc=getColor(bp)
     setColor(cp,bc)
     targetY=targetY+1
   targetX=targetX+1
 show(p2)
 return p2

def lab5():
 p1=makePicture(getMediaPath("barbara.jpg"))
 p2=makePicture(getMediaPath("7inX95in.jpg"))
 targetX=400
 for sourceX in range(0,getWidth(p1)):
   targetY=200
   for sourceY in range(0,getHeight(p1)):
     bp=getPixel(p1,sourceX,sourceY)
     cp=getPixel(p2,targetY,targetX-getWidth(p1)-1)
     bc=getColor(bp)
     setColor(cp,bc)
     targetY=targetY+1
   targetX=targetX+1
 show(p2)
 return p2
 
def lab6():
 p1=makePicture(getMediaPath("butterfly1.jpg"))
 p2=makePicture(getMediaPath("butterfly1.jpg"))
 for x in range(0,getWidth(p1)-1):
   for y in range(0,getHeight(p1)-1):
     hereP=getPixel(p1,x,y)
     rightP=getColor(getPixel(p2,x+1,y))
     downP=getColor(getPixel(p2,x,y+1))
     t=(rightP+downP)/2    
     setColor(hereP,t)
 show(p1)

def lab6():
 p1=makePicture(getMediaPath("butterfly1.jpg"))
 p2=makePicture(getMediaPath("butterfly1.jpg"))
 for x in range(0,getWidth(p1)-1):
   for y in range(0,getHeight(p1)-1):
     hereP=getPixel(p1,x,y)
     rightP=getPixel(p2,x+1,y)
     downP=getPixel(p2,x,y+1)
     t=getColor(abs(rightP+downP)/2)   
     setColor(hereP,t)
 show(p1)
