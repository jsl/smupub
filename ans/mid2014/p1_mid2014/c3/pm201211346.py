def main():
  lab1(barb)
  lab2()
  lab3(barb)
  lab4()
  
def lab1():
 barb=makePicture(getMediaPath('barbara.jpg'))
 for x in getPixel(barb.x.y):
    blue=getBlue(x,222,294)
 print(blue)

def lab2():
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  canvas2=makePicture(getMediaPath('640x480.jpg'))
  for p in getPixels(canvas):
    value=getWidth(canvas)*getHeight(canvas)
  for p in getPixels(canvas2):
    value2=getWidth(canvas2)*getHeight(canvas2)
    if value>value2:
      show(canvas)
    if value<value2:
      show(canvas2)
  return canvas
  return canvas2

def lab3(barb):
  barb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(barb):
    vBlue=getBlue(p)
    vGreen=getGreen(p)
    newRed=getRed(p)
    value=vBlue+vGreen/3
    setRed=getRed(newRed*value)
  show(barb)

def lab4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=504-getWidth(barb)
  for sourceX in range(0,getWidth(barb)):
    targetY=684-getHeight(barb)
    for sourceY in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas

def lab5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=0
  width=getWidth(barb)
  for sourceX in range(0,getWidth(barb)):
    targetY=0
    for sourceY in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas


def lab6():
  butter=makePicture(getMediaPath('butterfly1.jpg'))
  for p in getPixels(butter):
    red=getRed(p)
    green=getGreen(p)
    blue=getBlue(p)
    color=makeColor(255-red,255-green,255-blue)
    setColor(p,color)
  show(butter)