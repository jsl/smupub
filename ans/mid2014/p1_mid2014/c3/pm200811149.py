# author    :  PARK. M. C
# date      :  2014-04-23
# contents  :  MIDTERM


def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  pic=makePicture(getMediaPath("barbara.jpg"))
  lb=getBlue(getPixel(pic,221,293))
  print "Last Pixel blue color is" , lb
  
def lab2():
  pic1=makePicture(getMediaPath("7inX95in.jpg"))
  pic2=makePicture(getMediaPath("640x480.jpg"))
  pic1Pix=getWidth(pic1)*getHeight(pic1)
  pic2Pix=getWidth(pic2)*getHeight(pic2)
  if pic1Pix > pic2Pix:
    show(pic1)
    print(pic1)
  else:
    show(pic2)
    print(pic2)
  
def lab3():
  pic3=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(pic3):
    bl=getBlue(p)
    gr=getGreen(p)
    av=(bl+gr)/2.0
    rd=getRed(p)
    setRed(p,av)
  repaint(pic3)
  
def lab4():
  pic4=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=getWidth(canvas)-getWidth(pic4)
  for sourceX in range(0,getWidth(pic4)):
    targetY=getHeight(canvas)-getHeight(pic4)
    for sourceY in range(0,getHeight(pic4)):
      bar=getPixel(pic4,sourceX,sourceY)
      can=getPixel(canvas,targetX,targetY)
      setColor(can,getColor(bar))
      targetY=targetY+1
    targetX=targetX+1
  repaint(canvas)

def lab5():
  pic5=makePicture(getMediaPath("barbara.jpg"))
  canvas2=makePicture(getMediaPath("7inX95in.jpg"))
  targetX=0
  for sourceX in range(0,getWidth(pic5)):
    targetY=0
    for sourceY in range(0,getHeight(pic5)):
      bar=getPixel(pic5,sourceX,sourceY)
      can=getPixel(canvas2,targetY,targetX)
      setColor(can,getColor(bar))
      targetY=targetY+1
    targetX=targetX+1
  repaint(canvas2)
  
def lab6():
  pic6=makePicture(getMediaPath("butterfly1.jpg"))
  for x in range(0,getWidth(pic6)-1):
    for y in range(0,getHeight(pic6)-1):
      a=getColor(getPixel(pic6,x,y))
      b=getColor(getPixel(pic6,x,y+1))
      if(distance(a,b)<20):
        setColor(getPixel(pic6,x,y),white)
      else:
        setColor(getPixel(pic6,x,y),black)
  repaint(pic6)
      
    
    