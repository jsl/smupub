def lab1():
  f = getMediaPath("barbara.jpg")
  p = makePicture(f)
  
  px = getPixel(p,getWidth(p)-1,getHeight(p)-1)
  lastblue = getBlue(px)
  print lastblue
  
def lab2():
  f1 = getMediaPath("7inX95in.jpg")
  p1 = makePicture(f1)
  f2 = getMediaPath("640x480.jpg")
  p2 = makePicture(f2)
  
  p1p = getWidth(p1)*getHeight(p1)
  p2p = getWidth(p2)*getHeight(p2)
  
  if p1p>p2p:
    show(p1)
  else:
    show(p2)
  
def lab3():
  f = getMediaPath("barbara.jpg")
  p = makePicture(f)
  
  for x in range(0,getWidth(p)):
    for y in range(0,getHeight(p)):
      px = getPixel(p,x,y)
      b = getBlue(px)
      g = getGreen(px)
      avg = (b+g)/2
      color = makeColor(avg,g,b)
      setColor(px,color)
  show(p)
  
def lab4():  
  f = getMediaPath("barbara.jpg")
  p = makePicture(f)
  fc = getMediaPath("7inX95in.jpg")
  pc = makePicture(fc)
  
  tx= getWidth(pc)-getWidth(p)
  for sx in range(0,getWidth(p)):
    ty=getHeight(pc)-getHeight(p)
    for sy in range(0,getHeight(p)):
      spx = getPixel(p,sx,sy)
      tpx = getPixel(pc,tx,ty)
      color = getColor(spx)
      setColor(tpx,color)
      ty = ty+1
    tx = tx+1
  show(pc)
  
def lab5():
  f = getMediaPath("barbara.jpg")
  p = makePicture(f)
  fc = getMediaPath("7inX95in.jpg")
  pc = makePicture(fc)
  
  tx= 0
  for sx in range(0,getWidth(p)):
    ty=0
    for sy in range(0,getHeight(p)):
      spx = getPixel(p,sx,sy)
      tpx = getPixel(pc,ty,tx)
      color = getColor(spx)
      setColor(tpx,color)
      ty = ty+1
    tx = tx+1
  show(pc)
  
def lab6():
  f = getMediaPath("butterfly1.jpg")
  p = makePicture(f)
  
  for x in range(0,getWidth(p)-1):
    for y in range(0,getHeight(p)-1):
      here = getPixel(p,x,y)
      hc = getColor(here)
      right = getPixel(p,x+1,y)
      rc = getColor(right)
      bottom = getPixel(p,x,y+1)
      bc = getColor(bottom)
      
      if distance(hc,rc)>25 or distance(hc,bc)>25:
        setColor(here,black)
      else :
        setColor(here,white)
      
  
  show(p)
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
