def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  
def lab1():
  pb=makePicture(getMediaPath('barbara.jpg'))
  barbcolor=getPixel(pb,getWidth(pb)-1,getHeight(pb)-1)
  barbblue=getBlue(barbcolor)
  print barbblue  
  
def lab2():
  p1=makePicture(getMediaPath('7inX95in.jpg'))
  p2=makePicture(getMediaPath('640x480.jpg'))
  if (getWidth(p1)*getHeight(p1)>getWidth(p2)*getHeight(p2)):
    show(p1)
  else:
    show(p2)  
    
def lab3():
  pb=makePicture(getMediaPath('barbara.jpg'))
  for x in range(0,getWidth(pb)):
    for y in range(0,getHeight(pb)):
      p=getPixel(pb,x,y)
      newred=getGreen(getPixel(pb,x,y))+getBlue(getPixel(pb,x,y))/2
      setRed(p,newred)    
  return pb    
  
def lab4():
  pb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  copyPictures(pb,canvas,684-294-1,504-222-1)
 
def copyPictures(source,target,targX,targY):
  targetX=targX
  for x in range(0,getWidth(source)):
    targetY=targY
    for y in range(0,getHeight(source)):
      px=getPixel(source,x,y)
      tx=getPixel(target,targetX,targetY)
      setColor(target,px,tx)
    targetY=targetY+1
  return target          

#def lab6():
# pbw=makePicture(getMediaPath('butterfly1.jpg'))
# for x in range(0,getWidth(pbw)):
#   for y in range(0,getHeight(pbw)):
#     hereL=getBlue(pbw,x,y)+getGreen(pbw,x,y)+getRed(pbw,x,y)/3
#     downL=getBlue(pbw,x,y+1)+getGreen(pbw,x,y+1)+getRed(pbw,x,y+1)/3
#     rightL=getBlue(pbw,x+1,y)+getGreen(pbw,x+1,y)+getRed(pbw,x,y+1)/3
#       if abs(hereL-downL) >10 and abs(hereL-rightL) >10 :
#         setColor(hereL,black)
#       else:
#         setColor(hereL,white)
# return pbw      





    
