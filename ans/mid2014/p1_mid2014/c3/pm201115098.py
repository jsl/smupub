def main():
  lab1()
  lab3()
  lab4()
  lab5()

def lab1():
  pic5=makePicture(getMediaPath("barbara.jpg"))
  
  px=getPixel(pic5,221,293)
  color=getBlue(px)
  print color
  



def lab3():
  pic3=makePicture(getMediaPath("barbara.jpg"))
  
  for p in getPixels(pic3):
    Red=(getGreen(p)+getBlue(p))/2
    color=makeColor(Red,getGreen(p),getBlue(p))
    setColor(p,color)
  show(pic3)
  return pic3


def lab4():
 pic=makePicture(getMediaPath("barbara.jpg"))
 canvas=makePicture(getMediaPath("7inX95in.jpg"))
 
 targetX=280
 for x in range(0,getWidth(pic)):
   targetY=390
   for y in range(0,getHeight(pic)):
     px=getPixel(pic,x,y)
     cx=getPixel(canvas,targetX,targetY)
     color=getColor(px)
     setColor(cx,color)
     targetY=targetY+1
   targetX=targetX+1
 show(canvas)
 return canvas
 
def lab5():
  pic2=makePicture(getMediaPath("barbara.jpg"))
  canvas2=makePicture(getMediaPath("7inX95in.jpg"))
  
  targetX=0
  for x in range(0,getWidth(pic2)):
    targetY=0
    for y in range(0,getHeight(pic2)):
      px=getPixel(pic2,x,y)
      cx=getPixel(canvas2,targetY,targetX)
      color=getColor(px)
      setColor(cx,color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas2)   
  return canvas2
  

     
  
