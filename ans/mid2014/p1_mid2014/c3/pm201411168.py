def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

# O
def lab1():
  myf=getMediaPath('barbara.jpg')
  myp=makePicture(myf)
  p0=getPixel(myp,221,293)
  b=getBlue(p0)
  print b
  
  
# O  
def lab2():
  myf=getMediaPath('7inX95in.jpg')
  myp=makePicture(myf)
  myf2=getMediaPath('640X480.jpg')  
  myp2=makePicture(myf2)
  p1=getWidth(myp)*getHeight(myp)
  p2=getWidth(myp2)*getHeight(myp2)
  if p2<p1:
    show(myp)
  else:
    show(myp2)   
    
# O            
def lab3():
  myf=getMediaPath('barbara.jpg')
  myp=makePicture(myf)
  for p in getPixels(myp):
    value=((getGreen(p)+getBlue(p))/2)
    setRed(p,value)
  show(myp)
  return myp

# O    
def lab4():
  myf=getMediaPath('barbara.jpg')
  myp=makePicture(myf)
  cf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(cf)
  tX=282
  for sX in range(0,getWidth(myp)):
    tY=390
    for sY in range(0,getHeight(myp)):
      color=getColor(getPixel(myp,sX,sY))
      setColor(getPixel(canvas,tX,tY),color)
      tY=tY+1
    tX=tX+1
  show(canvas)
  return canvas

# O    
def lab5():
  myf=getMediaPath('barbara.jpg')
  myp=makePicture(myf)
  cf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(cf)
  tX=0
  for sX in range(0,getWidth(myp)):
    tY=0
    for sY in range(0,getHeight(myp)):
      color=getColor(getPixel(myp,sX,sY))
      setColor(getPixel(canvas,tY,tX),color)
      tY=tY+1
    tX=tX+1
  show(canvas)
  return canvas

# O
def lab6():
  myf=getMediaPath('butterfly1.jpg')
  myp=makePicture(myf)        
  for p in getPixels(myp):
    r=getRed(p)
    g=getGreen(p)
    b=getBlue(p)
    lum=(r+g+b)/3
    if lum <45:
      setColor(p,gray)
    else:
      setColor(p,white)  
  show(myp)
  return myp
  
          
                    
                 
