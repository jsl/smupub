def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  pic=makePicture(getMediaPath('barbara.jpg'))
  bluepx=getPixel(pic,getWidth(pic)-1,getHeight(pic)-1)
  blueValue=getBlue(bluepx)
  print blueValue
  
def lab2():
  pic1=makePicture(getMediaPath('7inX95in.jpg'))
  pic2=makePicture(getMediaPath('640x480.jpg'))
  x1=getWidth(pic1)*getHeight(pic1)
  x2=getWidth(pic2)*getHeight(pic2)
  if x1>x2:
    show(pic1)
  else:
    show(pic2)

def lab3():
  pic=makePicture(getMediaPath('barbara.jpg'))
  for x in range(0,getWidth(pic)):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      r=getRed(px)
      g=getGreen(px)
      b=getBlue(px)
      newcolor=makeColor((g+b)/2,g,b)
      setColor(px,newcolor)
  repaint(pic)
  return(pic)

def lab4():
  pic=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=getWidth(canvas)-getWidth(pic)
  for x in range(0,getWidth(pic)):
    targetY=getHeight(canvas)-getHeight(pic)
    for y in range(0,getHeight(pic)):
      pxp=getPixel(pic,x,y)
      clrp=getColor(pxp)
      pxc=getPixel(canvas,targetX,targetY)
      setColor(pxc,clrp)
      targetY=targetY+1
    targetX=targetX+1
  repaint(canvas)
  return(canvas)
  
def lab5():
  pic=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetY=0
  for x in range(0,getWidth(pic)):
    targetX=getHeight(pic)
    for y in range(0,getHeight(pic)):
      pxp=getPixel(pic,x,y)
      clrp=getColor(pxp)
      pxc=getPixel(canvas,targetX,targetY)
      setColor(pxc,clrp)
      targetX=targetX-1
    targetY=targetY+1
  repaint(canvas)
  return(canvas)

def lab6():
  pic=makePicture(getMediaPath('butterfly1.jpg'))
  for x in range(0,getWidth(pic)-1):
    for y in range(0,getHeight(pic)-1):
      midpx=getPixel(pic,x,y)
      rightpx=getPixel(pic,x+1,y)
      bottompx=getPixel(pic,x,y+1)
      midclr=getColor(midpx)
      rightclr=getColor(rightpx)
      bottomclr=getColor(bottompx)
      if distance(midclr,rightclr)>15. and distance(midclr,bottomclr)>15.:
        setColor(midpx,black)
      else:
        setColor(midpx,white)
  repaint(pic)
  return(pic)