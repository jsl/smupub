def main():
 lab1()
 lab2()
 lab3()
 lab4()
 lab5()
 lab6()

def lab1():
 pb=makePicture(getMediaPath('barbara.jpg'))
 printBlue(pb)
   
def printLastBlue(pic): 
 end=getPixel(pic,getWidth(pic)-1,getHeight(pic)-1)
 print getBlue(end) 
 
def lab2(): 
 choiceLargerPic()
 
def choiceLargerPic():
 p1=makePicture(getMediaPath('640��480.jpg'))
 p2=makePicture(getMediaPath('7in��96in.jpg'))
 pix1=getPixels(p1)
 pix2=getPixels(p2)
 if pix1>pix2:
  show(p1)
 else:
  show(p2) 
   
def lab3():
 pb=makePicture(getMediaPath('barbara.jpg'))
 evRedValue(pb)
 
def evRedValue(pic):
 for p in getPixels(pic):
  ev=((getGreen(p)+getBlue(p))/2)
  setRed(p,ev)
 show(pic) 
   
def lab4():
 copyBarb1()

def copyBarb1():
 pb=makePicture(getMediaPath('barbara.jpg'))
 canvas=makePicture(getMediaPath('7in��96in.jpg')) 
 tx=getWidth(canvas)-getWidth(pb)
 for sx in range(0,getWidth(pb)):
  ty=getHeight(canvas)-getHeight(pb)
  for sy in range(0,getHeight(pb)):
   p=getPixel(pb,sx,sy)
   color=getColor(p)
   setColor(getPixel(canvas,tx,ty),color)
   ty=ty+1
  tx=tx+1 
 show(canvas) 

def lab5():
 copyBarb2()
 
def copyBarb2(): 
 pb=makePicture(getMediaPath('barbara.jpg'))
 canvas=makePicture(getMediaPath('7in��96in.jpg'))
 sy=0
 for ty in range(0,getWidth(pb)):
  sx=0
  for tx in range(0,getHeight(pb)):
   p=getPixel(pb,sx,sy)
   color=getColor(p)
   setColor(getPixel(canvas,tx,ty),color)
   sx=sx+1
  sy=sy+1 
 show(canvas)  
 
def lab6():
 pbf=makePicture(getMediaPath('butterfly1.jpg')) 
 