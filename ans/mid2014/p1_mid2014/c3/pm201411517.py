def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  bp=getPixel(barb,getWidth(barb)-1,getHeight(barb)-1)
  bl=getBlue(bp)
  print bl
  
def lab2():
  canvas1=makePicture(getMediaPath('7inX95in.jpg'))
  canvas2=makePicture(getMediaPath('640x480.jpg'))
  canvasp=getWidth(canvas1)*getHeight(canvas1)
  canvasp2=getWidth(canvas2)*getHeight(canvas2)
  if canvasp>canvasp2:
    show(canvas1)
  else:
    show(canvas2)  

def lab3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  for p in getPixels(barb):
    bg=getGreen(p)
    bb=getBlue(p)
    baverage=(bg+bb)/2
    setRed(p,baverage)
  
    
def lab4():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetX=getWidth(canvas)-getWidth(barb)
  for x in range(0,getWidth(barb)):
    targetY=getHeight(canvas)-getHeight(barb)
    for y in range(0,getHeight(barb)):
      pixel=getPixel(barb,x,y)
      color=getColor(pixel)
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  
  
def lab5():
  barb=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('7inX95in.jpg'))
  targetY=0
  for y in range(0,getHeight(barb)):
    targetX=0
    for x in range(0,getWidth(barb)):
      pixel=getPixel(barb,x,y)
      color=getColor(pixel)
      setColor(getPixel(canvas,targetY,targetX),color)
      targetX=targetX+1
    targetY=targetY+1
  

def lab6():
  butterfly=makePicture(getMediaPath('butterfly1.jpg'))
  show(butterfly)
  
