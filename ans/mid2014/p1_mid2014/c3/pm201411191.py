def lab1():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  p=getBlue(getPixel(barb,221,293))
  print p
  return barb

def lab2():
  a1=getPixels(getMediaPath('7inX95in.jpg'))
  a2=getPixels(getMediaPath('640x480.jpg'))
  show(p1)

def lab3():
  for p in getPixels(getMediaPath('barbara.jpg')):
    newred=(getBlue(p)+getGreen(p))/2
    setRed(p,newred)
  return
  
def lab4():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  sourceX=(barb,getWidth(barb))
  targetX=(canvas,getWidth(canvas))
  targetX=382
  for sourceX in range(0,221):
    sourceY=(barb,getHeight(barb))
    targetY=(canvas,getHeight(canvas))
    targetY=390
    for sourceY in range(0,293):
      color=getPixel(barb,sourceX,sourceY)
      setColor(canvas(targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas

def lab5():
  barbf=getMediaPath('barbara.jpg')
  barb=makePicture(barbf)
  canvasf=getMediaPath('7inX95in.jpg')
  canvas=makePicture(canvasf)
  sourceX=(barb,getWidth(barb))
  targetX=(canvas,getWidth(canvas))
  targetX=382
  for sourceX in range(0,221):
    sourceY=(barb,getHeight(barb))
    targetY=(canvas,getHeight(canvas))
    targetY=390
    for sourceY in range(0,293):
      color=getPixel(barb,sourceX,sourceY)
      setColor(canvas(targetY,targetX),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas


  