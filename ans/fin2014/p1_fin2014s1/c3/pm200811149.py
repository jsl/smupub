def main():
  problem1()
  problem2()
  problem4()
  
def problem1():
  pr=makeSound(getMediaPath("preamble.wav"))
  print getLength(pr)/22050.0
  
def problem2():
  pr10=makeSound(getMediaPath("preamble10.wav"))
  half(pr10)
  
def half(soundFile):
  len=getLength(soundFile)*2
  empt=makeEmptySound(len)
  sourceIndex=0
  for targetIndex in range(0,len):
    val=getSampleValueAt(soundFile,int(sourceIndex))
    setSampleValueAt(empt,targetIndex,val)
    sourceIndex=sourceIndex+0.5
  explore(empt)
  
def problem4():
  ls=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  num=0.0
  total=0
  for s in range(0,16):
    if ls[s]>=30:
      num=num+1
      total=total+ls[s]
  print "Average is ",float(total/num)
