def main():
  lab1()
  lab2()
  lab4()
  lab6()

def lab1():
  sound=makeSound(getMediaPath("preamble.wav"))
  print getLength(sound)/getSamplingRate(sound)
  
def lab2():
  sound=makeSound(getMediaPath("preamble10.wav"))
  half(sound)
  
def half(sound):
  target=makeEmptySound(getLength(sound))
  sourceindex=0
  for targetindex in range(0,getLength(sound)):
    value=getSampleValueAt(sound,int(sourceindex))
    setSampleValueAt(target,targetindex,value)
    sourceindex=sourceindex+0.5
  play(target)
  
def lab4():
  List=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  score=0
  count=0
  for list in List:
    if list>=0:
      score=score+1.0
  for list in List:
    if list>=30.0:
      count=count+1
  print count/score
    
def lab6():
  kid=makePicture(getMediaPath("kid-in-frame.jpg"))
  bg=makePicture(getMediaPath("bgframe.jpg"))
  new=makePicture(getMediaPath("beach.jpg"))
  for x in range(0,getWidth(bg)):
    for y in range(0,getHeight(bg)):
      p=getPixel(kid,x,y)
      b=getPixel(bg,x,y)
      n=getPixel(new,x,y)
      if not getColor(p)==getColor(b):
        setColor(n,getColor(p))
  repaint(new)