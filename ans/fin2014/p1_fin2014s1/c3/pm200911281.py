def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()

def lab1():
  f=getMediaPath("preamble.wav")
  s=makeSound(f)
  len=getLength(s)
  rate=getSamplingRate(s)  
  sec=len/rate
  #print len
  #print rate
  print "lab1:", sec
  
def lab2():
  f=getMediaPath("preamble10.wav")
  s=makeSound(f)
  #explore(s)
  target=half(s)
  explore(target)
  
def half(s):
  target=makeEmptySound(getLength(s)*2)  
  idx=0
  for i in range(0, getLength(s)):
    value=getSampleValueAt(s, i)
    setSampleValueAt(target, idx, value)
    idx+=2
  return target
  
def lab3():
  s=makeEmptySound(22050)
  s=SquareWave(s, 440, 2000)
  explore(s)
  
def SquareWave(s, fred, amp):
  n=0
  for i in range(0, getLength(s)):
    if i%fred==0:
      n+=1
    if n%2==1:
      setSampleValueAt(s, i, amp)
    if n%2==0:
      setSampleValueAt(s, i, 0)    
  return s
  
  
def lab4():
  list=[47, 20, 11, 45, 76, 31, 30, 98, 86, 25, 58, 63, 95, 29, 39, 75]
  count=0
  for l in list:
    if l >= 30:
      count+=1
    else:
      continue      
  per = 100.0*count/len(list)
  print "lab4: percent:", per
  
def lab5():
  print "lab5:"
  str="hello"
  str=encode(str)
  print "encoded str: " + str
  str=decode(str)
  print "decoded str: " + str
  
def encode(str):
  msg=""
  for i in range(0, len(str)):
    n=ord(str[i])+1
    msg+=chr(n)
  return msg
    
def decode(str):
  msg=""
  for i in range(0, len(str)):
    n=ord(str[i])-1
    msg+=chr(n)
  return msg
  