def main():
 lab1()
 lab2()

def lab1():
 s=makeSound(getMediaPath('preamble.wav'))
 second(s)
 
def second(): 
 print getLength(sound)/getSamplingRate(sound) 

def lab2():
 s=makeSound(getMediaPath('preamble10.wav'))
 mysound=half(s)
 explore(mysound)
   
def half(soundFile):
 sound=makeEmptySound(getLength(soundFile)*2)
 count=0
 for sample in range(getLength(soundFile)):
  soundIndex=getSampleValue(count)  
  setSampleValueAt(sound,soundIndex,getSampleValue(sound))
  count=count+0.5
 return sound 
  
def lab3():
 squareWave(440,2000)
      
def squareWave(frequency,amplitude):
 sound=makeSound(getMediaPath('sec1silence.wav'))
 interval=1/frequency
 for sample in range(getLength(sound)):
