def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab6()

def lab1():
  sound=makeSound(getMediaPath('preamble.wav'))
  l=getLength(sound)
  r=getSamplingRate(sound)
  answer=l/r
  print answer

def lab2():
  sound=makeSound(getMediaPath('preamble10.wav'))
  target=makeEmptySound((getLength(sound)/2))
  tindex=0
  for index in range(0,getLength(sound)-1,2):
    v1=getSampleValueAt(sound,index)
    setSampleValueAt(target,tindex,v1)
    tindex+=1
  explore(target)
  return target

def lab3():
  x=squareWave(440,2000)
  explore(x)

def squareWave(freq,amp):
  target=makeEmptySound(22050)
  indexa=0
  indexb=0
  for index in range(0,getLength(target)):
    if indexb==0:
      setSampleValueAt(target,index,-amp)
      indexa+=1
    if indexa==freq and indexb==0:
      indexa=0
      indexb+=1
      setSampleValueAt(target,index,amp)
    if indexb==1:
      setSampleValueAt(target,index,amp)
      indexa+=1
    if indexa==freq and indexb==1:
      indexa=0
      indexb-=1
      setSampleValueAt(target,index,-amp)
  return(target)
  
def lab4():
  list1=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  listt=[]
  for num in list1:
    if num>=30:
      listt+=[num]
  print listt


def lab6():
  import os
  if os.path.exists(getMediaPath()+'//output_j')==0:
    os.mkdir(getMediaPath()+'//output_j')
  dir=getMediaPath()+'//output_j'
  slowFadeout(dir)
  movie=makeMovieFromInitialFile(dir+"//fadeout_00.jpg")
  playMovie(movie)

def slowFadeout(dir):
  pic1=makePicture(getMediaPath('kid-in-frame.jpg'))
  pic2=makePicture(getMediaPath('bgframe.jpg'))
  pic3=makePicture(getMediaPath('beach.jpg'))
  for x in range(0,getWidth(pic1)):
    for y in range(0,getHeight(pic1)):
      px1=getPixel(pic1,x,y)
      px2=getPixel(pic2,x,y)
      px3=getPixel(pic3,x,y)
      clr1=getColor(px1)
      clr2=getColor(px2)
      if distance(clr1,clr2)>=30:
        setColor(px3,clr1)
  for cycle in range(0,32):
    pic4=makePicture(getMediaPath('beach.jpg'))
    for x in range(0,getWidth(pic1)):
      for y in range(0,getHeight(pic1)):
        px3=getPixel(pic3,x,y)
        px4=getPixel(pic4,x,y)
        clr3=getColor(px3)
        clr4=getColor(px4)
        if distance(clr3,clr4)>=30:
          r3=getRed(px3)
          g3=getGreen(px3)
          b3=getBlue(px3)
          r4=getRed(px4)
          g4=getGreen(px4)
          b4=getBlue(px4)
          newcolor=makeColor((r3*(32-cycle)+r4*cycle)/32,(b3*(32-cycle)+b4*cycle)/32,(b3*(32-cycle)+b4*cycle)/32)
          setColor(px4,newcolor)
    writePictureTo(pic4,dir+"//fadeout_%02d.jpg" %cycle)
