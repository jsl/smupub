def square(freq,amplitude):
  square=makeSound(getMediaPath("sec1silence.wav"))
  a=1.0/freq
  sr=getSamplingRate(square)
  for p in range(0,freq):
    setSampleValueAt(square,p,amplitude/2)
  for p in range(freq,freq*2):
    setSampleValueAt(square,p,amplitude/2*-1)
  for p in range(freq*2,freq*3):
    setSampleValueAt(square,p,amplitude/2)
  for p in range(freq*3,freq*4):
    setSampleValueAt(square,p,amplitude/2*-1)
  for p in range(freq*4,freq*5):
    setSampleValueAt(square,p,amplitude/2)
  for p in range(freq*5,freq*6):
    setSampleValueAt(square,p,amplitude/2*-1)
  for p in range(freq*6,freq*7):
    setSampleValueAt(square,p,amplitude/2)
  for p in range(freq*7,freq*8):
    setSampleValueAt(square,p,amplitude/2*-1)
  for p in range(freq*8,freq*9):
    setSampleValueAt(square,p,amplitude/2)
  for p in range(freq*9,freq*10):
    setSampleValueAt(square,p,amplitude/2*-1)
  return square
  
def square2(freq,amplitude):
  square=makeSound(getMediaPath("sec1silence.wav"))
  for p in range(0,40):
    if p%2==1:
      for s in range(freq*(p-1),freq*p):
        setSampleValueAt(square,s,amplitude/2)
    else:
      for s in range(freq*p,freq*(p+1)):
        setSampleValueAt(square,s,amplitude/2*-1)
  explore(square)
  
def lab3():
  square(440,2000)
  explore(square)
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  preamble=makeSound(getMediaPath("preamble.wav"))
  sec(preamble)

 
def lab2():
  pream=makeSound(getMediaPath("preamble.wav"))
  preamD=double(pream)
  explore(preamD)
  play(preamD)
  
def sec(sound):
  print getLength(sound)/getSamplingRate(sound)
  
def double(sound):
  canvas=makeEmptySound(getLength(sound)/2+1)
  i=0
  for index in range(0,getLength(sound),2):
    s=getSampleValueAt(sound,index)
    setSampleValueAt(canvas,i,s)
    i=i+1
  explore(canvas)
  
def lab4():
  list=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  b=0
  for a in range(0,len(list)):
    if list[a]>=30:
      b=b+1
  print b/16.0*100
  
def lab5():
  pic=makePicture(getMediaPath("barbaraS.jpg"))
  list=pictureToList(pic)
  print list
  p=listToPicture(list)
  show(p)

def pictureToList(picture):
  list=[]
  for p in getPixels(picture):
    list=list+[[getRed(p),getGreen(p),getBlue(p),getX(p),getY(p)]]
  return list

  
def listToPicture(list):
  canvas=makeEmptyPicture(111,147)
  a=0
  for p in getPixels(canvas):
    setRed(p,list[a][0])
    setGreen(p,list[a][1])
    setBlue(p,list[a][2])
    a=a+1
  return canvas
    

class SmartTurtle(Turtle):
  def drawRect(size):
    earth=makeWorld()
    s=makeTurtle(earth)
    s.forward(size)
    s.turn(90)
    s.forward(size)
    s.turn(90)
    s.forward(size)
    s.turn(90)
    s.forward(size)

def lab6():
  drawRect(100)

def drawRect(size):
  earth=makeWorld()
  s=makeTurtle(earth)
  s.forward(size)
  s.turn(90)
  s.forward(size)
  s.turn(90)
  s.forward(size)
  s.turn(90)
  s.forward(size)
    