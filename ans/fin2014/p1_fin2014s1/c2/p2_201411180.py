def main():
  lab1()
  lab3()
  lab2()
  

def lab1():
  a=makeSound(getMediaPath('preamble.wav'))
  b=getLength(a)
  c=getSamplingRate(a)
  print b/c
 
 
def lab2():
  sound=makeSound(getMediaPath('preamble10.wav'))
  double(sound)
def double(a):
  len = getLength(a) + 1
  target = makeEmptySound(len)
  targetIndex = 0
  for i in range(0,getLength(a),2):
    value=getSampleValueAt(a,i)
    setSampleValueAt(target,targetIndex,value)
    targetIndex=targetIndex + 1
  explore(target)
  
def lab3():
  build=makeSound(getMediaPath('sec1silence.wav'))
  freq=440
  am=2000
  sr= getSamplingRate(build)
  interval=  1.0 /freq
  sampleper = interval * sr
  max= pi * 2
  for pos in range(0,getLength(build)):
    raw = sin((pos/sampleper)*max)
    sample = int(raw*am)
    setSampleValueAt(build,pos,sample)
  explore(build)
  
  
 