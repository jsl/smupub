def main():
  lab1()
  lab2()

def lab1():
  fg=getMediaPath("preamble.wav")
  sg=makeSound(fg)
  soundLength = getLength(sg) / getSamplingRate(sg)
  print "It is ",soundLength

def lab2():
  fg=getMediaPath("preamble10.wav")
  sg=makeSound(fg)
  target=makeEmptySound(getLength(sg)/2)
  for sample in range(0,getLength(target)):
    setSampleValueAt(target,sample,getSampleValueAt(sg,sample))
  play(target)
  explore(target)

