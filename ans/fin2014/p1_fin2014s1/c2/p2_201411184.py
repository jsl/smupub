def lab1():
  pre=makeSound(getMediaPath('preamble.wav'))
  seconds=getLength(pre)/getSamplingRate(pre)
  print seconds

def lab2():
  pre10=makeSound(getMediaPath('preamble10.wav'))
  double(pre10)

def double(soundFile):
  len=getLength(soundFile)
  target=makeEmptySound(len/2+1)
  soundIndex=0
  for i in range(0,len):
    value=getSampleValueAt(soundFile,i)
    setSampleValueAt(target,soundIndex,value)
    soundIndex=soundIndex+2
  explore(target)

def lab3():
  squareWave(440,2000)

def squareWave(frequency,amplitude):
  target=makeSound(getMediaPath('sec1silence.wav'))
  spc=1.0/frequency
  for i in getSamples(target):
    setSampleValue(i,10)
  return target

def lab4():
  list=[]
  list+=47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75
  for num in list:
    if num>=30:
      print getContents(num)/16.*100.
      

def lab5():
  bar=makePicture(getMediaPath('barbaraS.jpg'))
  pictureToList(bar)
  listToPicture(list)

def pictureToList(picture):
  list=[]
  for i in getPixels(picture):
    r=getRed(i)
    if r>150:
      list+=10
    if r==150:
      list+=20
    if r<150:
      list+=30
  show(list)

def listToPicture(list):
  picture=makeEmptyPicture(294,222)
  for l in list:
    for i in getSamples(picture):
      if l==10:
        setRed(i,255)
      if l==20:
        setRed(i,150)
      if l==30:
        setRed(i,0)
  show(picture)


class SmartTurtle(Turtle):
  def drawRect(size):
    for x in range(0,4):
      turnRight(Turtle)
      forward(Turtle,size)

  def lab6():
    SmartTurtle.drawRect(size)


def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

