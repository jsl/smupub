def main():
  lab_1()
  pre10=getMediaPath('preamble10.wav')
  lab_2(pre10,1000)
  lab_4()

def lab_1():
  pre=makeSound(getMediaPath('preamble.wav'))
  largest=0
  for sample in getSamples(pre):
    largest=max(largest,getSampleValue(sample))
  print largest
  
def lab_2(soundFile,delay):
  s1=makeSound(soundFile)
  s2=makeSound(soundFile)
  for Index in range(delay,getLength(s1)):
    echo=0.6*getSampleValueAt(s2,Index-delay)
    combo=getSampleValueAt(s1,Index)+echo
    setSampleValueAt(s1,Index,combo)
  explore(s1)
  return(s1)
  
def lab_4():
  list=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  sum=0
  for Index in range(0,16):
    sum=sum+list[Index]
  average=sum/16
  print average
  