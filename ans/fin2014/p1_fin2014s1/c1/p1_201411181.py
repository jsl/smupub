def lab1():
  sound=makeSound(getMediaPath("preamble.wav"))
  largest=0
  for s in range(0,getLength(sound)):
    largest=max(largest,getSampleValueAt(sound,s))
  print largest
  
def echo(soundFile,delay):
  target=makeEmptySound(getLength(soundFile))
  for s in range(delay,getLength(soundFile)):
    echo=0.6*getSampleValueAt(soundFile,s-delay)
    combo=getSampleValueAt(soundFile,s)+echo
    setSampleValueAt(target,s,combo)
  play(target)

def lab2():
  sound=makeSound(getMediaPath("preamble10.wav"))
  echo(sound,2000)
  
def soundToPicture(sound):
  target=makePicture(getMediaPath("640x480.jpg"))
  for p in getPixels(target):
    for s in range(0,getLength(sound)):
      val=getSampleValueAt(sound,s)
      if val>=1000:
        setColor(p,red)
      if val>-1000 and val<1000:
        setColor(p,green)
      if val<=-1000:
        setColor(p,blue)
  show(target)
def lab5():
  sound=makeSound(getMediaPath("a.wav"))
  soundToPicture(sound)
  
  
def lab4():
  mylist=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  for i in mylist:
    ff=i
    avg=ff/16
  print avg


def main():
  lab1()
  lab2()
  lab4()
  lab5()


  
