# Programming Test

def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  sound = makeSound(getMediaPath("preamble.wav"))
  large = soundMax(sound)
  print "sample max =", large

def soundMax(sound):
  largest = -1
  for index in range(0, getLength(sound)):
    value = getSampleValueAt(sound, index)
    if value > largest:
      largest = value
  return largest





#def lab3():


def lab4():
  list = [47, 20, 11, 45, 76, 31, 30, 98, 86, 25, 58, 63, 95, 29, 39, 75]
  length = len(list)
  value = average(list, length, 0)
  print "list average =", value


def average(list, length, sum):
  for i in range(0, length):
    sum = sum + list[i]
  value = sum / length
  return value
 


def lab5():
  sound = makeSound(getMediaPath("preamble10.wav"))
  picture = soundToPicture(sound)
  explore(picture)


def soundToPicture(sound):
  canvas = makePicture(getMediaPath("400x300.jpg"))
  for index in range(0, getLength(sound)):
    value = getSampleValueAt(sound, index)
    for index in getPixels(canvas):
      if value >= 1000:
        setColor(index, red)
      if value > -1000 and value <1000:
        setColor(index, green)
      if value <= -1000:
        setColor(index, blue)
  return canvas
  


#def lab6():