def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  
def lab1():
  f=makeSound(getMediaPath("preamble.wav"))
  largest=0
  p=getSamples(f)
  for s in getSamples(f):
    largest=max(largest,getSampleValue(p))
  
def lab2():
  f=makeSound(getMediaPath("preamble10.wav"))
  delay=5000
  
def lab3():
  f=makeSound(getMediaPath("preamble10.wav"))
  freq=440
  amp=2000
  sr=getSamplingRate(f)
  
def lab4():
  list=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  avr=sum(list)/len(list)
  print avr 
  
def lab5():
  f=makeSound(getMediaPath("a.wav"))
  target=makePicture(getMediaPath("640x480.jpg"))
  sourcri=0
  for sourcei in getSamples(f):
    value=getSampleValue(sourcei)
    if value>=1000:
      setColor(target,red)
    if -1000<value<1000:
      setColor(target,green)      
    if value<=-1000:
      setColor(target,blue)
    sourcei=sourcei+1
  explore(target)


