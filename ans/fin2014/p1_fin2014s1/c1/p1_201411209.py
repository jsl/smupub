def main():
  num1()
  num2()
  num5()

def num1():
  p=makeSound(getMediaPath("preamble.wav"))
  index=0
  num=0
  for i in getSamples(p):
    fv=getSampleValue(i)
    bv=getSampleValue(i)
    if fv<bv:
      print getSampleValue(bv)
      
      

def num2():
  p=makeSound(getMediaPath("preamble10.wav"))
  target=makeEmptySound(getLength(p))
  delay=2000
  index=0
  for i in range(0,getLength(p)):
    value=getSampleValueAt(p,i)
    setSampleValueAt(target,i,value)
  index=index+1
  for i in range(delay,getLength(p)):
    echo=getSampleValueAt(p,i)
    setSampleValueAt(target,i,echo*0.6)
  index=index+1
  explore(target)
  return target
  
  
def num5():
  p=makeSound(getMediaPath("preamble.wav"))
  c=makePicture(getMediaPath("640X480.jpg"))
  index=0
  for i in range(0,getLength(p)):
    if getSampleValueAt(p,i)>=1000:
      setRed(c,i)
    if getSampleValueAt(p,i) <1000 and getSampleValueAt(p,i)>-1000:
      setGreen(c,i)
    if getSampleValueAt(p,i)>=-1000:
      setBlue(c,i)
    index=index+1
  return c
  explore(c)