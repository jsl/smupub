def lab2(delay):
  sound1=makeSound(getMediaPath('preamble10.wav'))
  sound2=makeSound(getMediaPath('preamble10.wav'))
  for p in range(0,getLength(sound1)):
    value=0.6*getSamplingRate(sound2)-delay
    setSampleValueAt(sound1,p,value)
  explore(sound1)
  
def lab3():
  sinbuild=makeSound(getMediaPath('preamble10.wav'))
  alt=1.0/440
  val=getSamplingRate(sinbuild)*alt
  amplitude=2000
  for p in range(0,getLength(sinbuild)):
    up=val*amplitude
    setSampleValueAt(sinbuild,p,up)
  explore(sinbuild)
 
def lab4():
  list=[47,20,11,45,76,31,30,98,86,25,58,63,95,29,39,75]
  print list
 
  

def lab5():
  canvas=makePicture(getMediaPath('640x480.jpg'))
  sound=makeSound(getMediaPath('a.wav'))
  for p in getPixels(canvas):
    sample=getSamples(sound)
    if sample>=1000:
      setRed(p)
    if sample>-1000 and sample<1000:
      setGreen(p)
    if sample<=-1000:
      setBlue(p)
  show(canvas)
    
    
def main():
  lab2(delay)
  lab3()
  lab4()
  lab5()
  
    