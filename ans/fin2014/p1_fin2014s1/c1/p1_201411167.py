def main():
  lab1()
  lab2()


def lab1():
  sound=makeSound(getMediaPath('preamble.wav'))
  largest=0
  for i in range(0,getLength(sound)):
    v=getSampleValueAt(sound,i)
    largest=max(largest,v)
  print largest
  
def lab2():
  echo('preamble10.wav',1000)
  
  
  
def echo(soundFile,delay):
  #sound=makeSound(getMediaPath('preamble10.wav'))
  sound=makeSound(getMediaPath(soundFile))
  target=makeSound(getMediaPath(soundFile))
  targetIndex=0
  for i in range(delay, getLength(sound)):
    v=getSampleValueAt(sound,i)*0.6
    v2=getSampleValueAt(sound,targetIndex)
    setSampleValueAt(target,targetIndex,v+v2)
    targetIndex=targetIndex+1
  explore(target)

def lab4():
  mylist=[47,20,45,76,31,30,98,86,25,58,63,95,29,39,75]
  for i in range(0,15):
    largest=0
    print largest
    
def lab5():
  soundToPicture(makeSound(getMediaPath('gettysburg.wav')))

  
def soundToPicture(sound):
  #sound=makeSound(getMediaPath('gettysburg.wav'))
  canvas=makeEmptyPicture(640,480)
  soundIndex=0
  s=getSampleValueAt(sound,soundIndex)
  if s >= 1000:
    for soundIndex in range(0, getWidth(canvas)):
        p=getPixel(canvas,soundIndex,0)
        setColor(p, red)
  if -1000 < s < 1000:
    for soundIndex in range(0, getWidth(canvas)):
        p=getPixel(canvas,soundIndex,100)
        setColor(p, green)
  if s <= -1000:
    for soundIndex in range(0, getWidth(canvas)):
        p=getPixel(canvas,soundIndex,200)
        setColor(p,blue)
    soundIndex=+1
  explore(canvas)

      