def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

def lab1():
  sound = makeSound(getMediaPath("preamble.wav"))
  largest = 0
  for sample in getSamples(sound):
    largest = max(largest, getSampleValue(sample))
  print "lab1 result :", largest
  
def lab2():
  explore(echo(getMediaPath("preamble10.wav"), 22050))
  
def echo(soundFile, delay):
  sound = makeSound(soundFile)
  for index in range(delay, getLength(sound)):
    value1 = getSampleValueAt(sound, index)
    value2 = getSampleValueAt(sound, index - delay) * 0.6
    setSampleValueAt(sound, index, value1 + value2)
  return sound
  
def lab3():
  explore(sineWave(440, 2000))
  
def sineWave(frequency, amplitude):
  sound = makeEmptySoundBySeconds(1)
  for sample in getSamples(sound):
    value = sin(frequency) * amplitude
    setSampleValue(sample, value)
    frequency -= 1
  return sound
  
def lab4():
  list = [47, 20, 11, 45, 76, 31, 30, 98, 86, 25, 58, 63, 95, 29, 39, 75]
  sum = 0
  for element in list:
    sum += element
  print "lab4 result :", sum / len(list)
  
def lab5():
  show(soundToPicture(makeSound(getMediaPath("a.wav"))))
  
def soundToPicture(sound):
  canvas = makePicture(getMediaPath("640x480.jpg"))
  pixels = getPixels(canvas)
  index = 0
  for sample in getSamples(sound):
    value = getSampleValue(sample)
    if value >= 1000:
      setColor(pixels[index], red)
    elif value <= -1000:
      setColor(pixels[index], blue)
    else:
      setColor(pixels[index], green)
    index += 1
  return canvas
  
  
def lab6():
  fontsize = 31
  for i in range(1, 61):
    canvas = makeEmptyPicture(600, 300)
    style = makeStyle(sansSerif, bold, fontsize)
    addTextWithStyle(canvas, i * 10, 150, "Hello", style)
    filename = "tickertype_%02d.jpg" %i
    writePictureTo(canvas, getMediaPath(filename))
    if(i < 30):
      fontsize -= 1
    else:
      fontsize += 1
  filename = "tickertype_%02d.jpg" %1
  movie = makeMovieFromInitialFile(filename)
  playMovie(movie)