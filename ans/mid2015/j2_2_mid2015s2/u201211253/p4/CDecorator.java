package com.j2.u201211253.p4;

public abstract class CDecorator extends Beverage{
  public abstract String getDescription();
}
  