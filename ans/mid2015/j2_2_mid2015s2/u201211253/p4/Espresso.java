package com.j2.u201211253.p4;

public class Espresso extends Beverage{
  public DarkRost(){
    description = "Espresso ";
  }
  
  public double cost();{
    return 1.20;
  }
}