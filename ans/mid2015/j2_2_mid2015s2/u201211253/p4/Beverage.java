package com.j2.u201211253.p4;
 
public abstract class Beverage{
  String description = "--";
  
  public String getDescription(){
    return description;
  }
  public abstract double cost();
}


  