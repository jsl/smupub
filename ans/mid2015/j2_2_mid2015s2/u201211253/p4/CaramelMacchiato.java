package com.j2.u201211253.p4;

public class CaramelMacchiato extends Beverage{
  public DarkRost(){
    description = "CaramelMacchiato";
  }
  
  public double cost();{
    return 1.20;
  }
}