public class P4Whip extends P4decorator{
  P4Beverage beverage;
   public string getDescription(P4Beverage beverage){
    return beverage.getDescription() + ", Whip";
  }
   public double cost(P4Beverage beverage){
     return 0.60 + beverage.cost();
   }

}