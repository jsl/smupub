public class P4Caramel extends P4decorator{
  P4Beverage beverage;
   public string getDescription(P4Beverage beverage){
    return beverage.getDescription() + ", Caramel";
  }
   public double cost(P4Beverage beverage){
     return 0.60 + beverage.cost();
   }

}