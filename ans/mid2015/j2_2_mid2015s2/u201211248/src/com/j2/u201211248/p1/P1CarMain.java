package com.j2.u201211248.p1;
public class P1CarMain{
  public static void main(String args[]){
    int sum = 0;
    P1Car car1 = new P1Sedan();
    P1Car car2 = new P1Van();
    P1Car car3 = new P1SportsCar();
    
    sum = car1.getCapacity()+car2.getCapacity()+car3.getCapacity();
    System.out.println("자동차 인승 합계는"+ sum +"입니다.");
  }
}