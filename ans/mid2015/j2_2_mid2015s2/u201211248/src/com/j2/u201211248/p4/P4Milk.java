public class P4Milk extends P4decorator{
  P4Beverage beverage;
   public string getDescription(P4Beverage beverage){
    return beverage.getDescription() + ", Milk";
  }
   public double cost(P4Beverage beverage){
     return 0.50 + beverage.cost();
   }

}