package com.j2.u201211248.p4;
public abstract class P4Beverage{
  String description = "Unknown Beverage";
  
  public void getDescription();
  public abstract double cost();
}
