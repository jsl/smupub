package com.j2.u201411198.p3;

public class P3CalcMain {
  public static void main(String[] args) {
    Pizza p1 = new CheesePizza();
    PizzaStore store = new PizzaStore(p1);
    store.orderPizza();
    System.out.println("We ordered a " + p1.getName());
  }
}