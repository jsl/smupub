package com.j2.u201411198.p3;

public class PizzaStore {
  Pizza pizza;
  
  public PizzaStore(Pizza pizza) { 
    this.pizza = pizza;
  }
  
  public Pizza createPizza() {
    if (pizza.equals("cheese")) {
      pizza = new CheesePizza();
    }
    else if (pizza.equals("veggie")) {
      pizza = new VeggiePizza();
    }
    else if (pizza.equals("clam")) {
      pizza = new ClamPizza();
    }
    return pizza;
  }
  
  public Pizza orderPizza() {
    PizzaStore store = new PizzaStore(pizza);
    pizza = store.createPizza();
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
}
  
    
    