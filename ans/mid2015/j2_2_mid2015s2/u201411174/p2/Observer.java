package com.j2.u201411174.p2;

public abstract interface Observer() {
  public void notify();
}