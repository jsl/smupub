package com.j2.u201411174.p1;

public class P1PrintCarMain {
  public static void main (String args[]){
    Car car;
    Sedan sedan;
    Van van;
    SportsCar sportsCar;

    Car car1 = new Sedan();
      System.out.println(car1.getCapacity() + "person in Sedan");
 
    Car car2 = new Van();
      System.out.println(car2.getCapacity() + "person in Van");
     
    Car car3 = new SportsCar();
      System.out.println(car3.getCapacity() + "person in SportsCar");
  }
}