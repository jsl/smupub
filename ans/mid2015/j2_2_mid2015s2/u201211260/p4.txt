@startuml


Beverage <|-- Espresso
Beverage <|-- DarkRost
Beverage <|-- Deco
Deco <|-- Milk
Deco <|-- Whip
Deco <|-- Caramel


abstract class Beverage{
 +description()
 +abstract cost()
}

class DarkRost{
 +DarkRost()
 +description()
 +cost()
}

class Espresso{
 +Espresso()
 +description()
 +cost()
}

abstract class Deco{
 +abstract decription()
 +abstract cost()
}

class Milk{
 +Milk()
 +description()
 +cost()
}

class Whip{
 +Whip()
 +description()
 +cost()
}

class Caramel{
 +Caramel()
 +description()
 +cost()
}

@enduml