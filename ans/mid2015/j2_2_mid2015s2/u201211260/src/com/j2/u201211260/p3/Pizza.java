package com.j2.u201211260.p3;

public abstract class Pizza{
  String name;
  abstract void preparing();
  abstract void baking();
  abstract void cutting();
  abstract void boxing();
}
        
  