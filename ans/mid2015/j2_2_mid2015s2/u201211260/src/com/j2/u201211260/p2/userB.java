package com.j2.u201211260.p2;

public class userB implements
  Observer{
  Subject RRS;
  public userB(Subject subject){
    RRS = subject;
    RRS.registerObserver(this);
  }
  public void update(){
    System.out.println("userB - New RSS ");
  }
}