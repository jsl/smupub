package com.j2.u201211260.p3;

public class ClamPizza extends Pizza{
  
  public ClamPizza(){
    name ="ClamPizza";
  }
  public void preparing(){
    System.out.println("preparing..");
  }
  public void baking(){
    System.out.println("Baking..");
  }
  public void cutting(){
    System.out.println("cutting..");
  }
  public void boxing(){
    System.out.println("boxing..");
  }
}