package com.j2.u201211260.p2;

import java.util.*;

public class RRS implements Subject{
  ArrayList A;
  public RRS(){
    A= new ArrayList();
  }
  
  public void registerObserver(Observer o){
    A.add(o);
  }
  public void removeObserver(Observer o){
    int a =A.indexOf(o);
    A.remove(a);
  }
  
  public void notifyObserver(){
    for(int i=0; i<A.size();i++){
      Observer o = (Observer)A.get(i);
      o.update();
    }
  }
  
  public void postNewRSS(){
    notifyObserver();
  }
}