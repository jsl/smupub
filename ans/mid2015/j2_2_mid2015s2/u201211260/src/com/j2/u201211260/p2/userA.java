package com.j2.u201211260.p2;

public class userA implements Observer{
  Subject RRS;
  public userA(Subject subject){
    RRS = subject;
    RRS.registerObserver(this);
  }
  public void update(){
    System.out.println("userA - New RSS ");
  }
}