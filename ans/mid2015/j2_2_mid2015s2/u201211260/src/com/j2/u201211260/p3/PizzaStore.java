package com.j2.u201211260.p3;

public abstract class PizzaStore{
  public Pizza orderPizza(String name){
    Pizza pizza;
    pizza = createPizza(name);
    pizza.preparing();
    pizza.baking();
    pizza.cutting();
    pizza.boxing();
    System.out.println("We ordered a " +pizza.name);
    return pizza;
  }
  abstract public Pizza createPizza(String name);
}