package com.j2.u201211260.p2;

public interface Subject{
  public void registerObserver(Observer o);
  public void removeObserver(Observer o);
  public void notifyObserver();
}
