package com.j2.u201211260.p3;

public class createPizzaStore extends PizzaStore{
  public Pizza createPizza(String name){
    Pizza pizza =null;
    if(name.equals("Cheese")){
      pizza =new CheesePizza();
    }
    else if(name.equals("Veggie")){
      pizza = new VeggiePizza();
    }
    else if(name.equals("Clam")){
      pizza = new ClamPizza();
    }
    return pizza;
  }
}

   
          