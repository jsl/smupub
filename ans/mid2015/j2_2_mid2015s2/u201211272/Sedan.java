

public class Sedan extends Car {
  public Sedan() {
    description = "Sedan";
  }
  public int getCapacity() {
    return 5;
  }
}