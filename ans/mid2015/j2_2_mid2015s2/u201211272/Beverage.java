package com.j2.u201211272.p4;

public abstract class Beverage {
  String description = "Unknown Beverage";
  
  public String getDescription() {
    return description;
  }
  
  public double cost() {
    return .11;
  }
}