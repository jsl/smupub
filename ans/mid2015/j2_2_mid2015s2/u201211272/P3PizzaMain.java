package com.j2.u201211272.p3;
  
  public class P3PizzaMain {
  public static void main(String[] args) {
    Pizza pizza = new CheesePizza();
    System.out.println("preparing...");
    System.out.println("Baking...");
    System.out.println("cutting...");
    System.out.println("boxing...");
    System.out.println("We ordered a Cheese Pizza");
    
    Pizza pizza2 = new VeggiePizza();
    System.out.println("preparing...");
    System.out.println("Baking...");
    System.out.println("cutting...");
    System.out.println("boxing...");
    System.out.println("We ordered a Veggie Pizza");
    
    Pizza pizza3 = new ClamPizza();
    System.out.println("preparing...");
    System.out.println("Baking...");
    System.out.println("cutting...");
    System.out.println("boxing...");
    System.out.println("We ordered a Clam Pizza");
  }
}
