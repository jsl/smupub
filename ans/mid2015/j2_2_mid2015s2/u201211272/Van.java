

public class Van extends Car {
  public Van() {
    description = "Van";
  }
  public int getCapacity() {
    return 9;
  }
}