

public abstract class Car {
  String description = "Unknown Car";
  
  public String getDescription() {
    return description;
  }
  
  public int getCapacity() {
    return 11;
  }
}