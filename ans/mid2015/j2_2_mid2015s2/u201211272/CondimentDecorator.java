package com.j2.u201211272.p4;

  public abstract class CondimentDecorator extends Beverage {
  public abstract String getDescription();
}