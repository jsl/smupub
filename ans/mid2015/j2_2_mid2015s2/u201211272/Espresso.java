package com.j2.u201211272.p4;

public class Espresso extends Beverage {
  public Espresso() {
    description = "Espresso Coffee";
  }
  public double cost() {
    return 3.2;
  }
}