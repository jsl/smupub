

public class SportsCar extends Car {
  public SportsCar() {
    description = "SportsCar";
  }
  public int getCapacity() {
    return 2;
  }
}