
  public class P1CarMain {
  public static void main(String[] args) {
    Car car = new Sedan();
    System.out.println(car.getDescription()+" "+car.getCapacity());
    
    Car car2 = new Van();

    System.out.println(car2.getDescription()+" "+car2.getCapacity());
    
    Car car3 = new SportsCar();

    System.out.println(car3.getDescription()+" "+car3.getCapacity());
    
    System.out.println("Sedan + Van + SportsCar");
    System.out.println(car.getCapacity()+car2.getCapacity()+car3.getCapacity());
  }
}
