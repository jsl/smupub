package com.j2.u201211272.p4;

public class Whip extends CondimentDecorator {
  Beverage beverage;
  
  public Whip(Beverage beverage) {
    this.beverage = beverage;
  }
  
  public String getDescription() {
    return beverage.getDescription()+ ", Whip";
  }
  
  public double cost() {
    return beverage.cost() + .15;
  }
}