package com.j2.u201211272.p3;

public abstract class Pizza {
  String description = "Unknown Pizza";
  
  public String getDescription() {
    return description;
  }
}