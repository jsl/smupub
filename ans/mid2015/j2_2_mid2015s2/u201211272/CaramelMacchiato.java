package com.j2.u201211272.p4;

public class CaramelMacchiato extends Beverage {
  public CaramelMacchiato() {
    description = "CaramelMacchiato Coffee";
  }
  public double cost() {
    return 2.3;
  }
}