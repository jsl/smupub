package com.j2.u201211272.p4;

public class DarkRoast extends Beverage {
  public DarkRoast() {
    description = "DarkRoast Coffee";
  }
  public double cost() {
    return 1.2;
  }
}