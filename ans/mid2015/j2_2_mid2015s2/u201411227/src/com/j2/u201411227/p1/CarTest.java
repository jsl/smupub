package com.j2.u201411227.p1;

public class CarTest{
  public static void main(String[] args){
    Car sedan=new Sedan();
    Car van=new Van();
    Car sportsCar=new SportsCar();
    
    sedan.display();
    van.display();
    sportsCar.display();
  }
}