package com.j2.u201411227.p4;

public class Caramel extends Things{
  public String getDescription(){
    return beverage.getDescription() + ", Caramel";
    //System.out.println( beverage.getDescription() + ", Caramel");
  }
  public double cost(){
    return 1.0 + beverage.cost();
  }
}