package com.j2.u201411227.p3;

public class PizzaStore{
  public void createPizza(String type);
  public void orderPizza(String type){
    Pizza pizza=createPizza(String type);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
  }
}