class Subject{}

class Observer{}

class RSS{}
 
class Observer{}

class UserA{}

class UserB{}

Subject -> Observer
Subject <|-- RSS
Observer <|-- UserA
Observer <|-- UserB
UserA -> RSS
UserB ->RSS