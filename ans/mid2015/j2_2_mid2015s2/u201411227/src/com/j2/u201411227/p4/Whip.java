package com.j2.u201411227.p4;

public class Whip extends Things{
  public String getDescription(){
    return beverage.getDescription() + ", Whip";
    //System.out.println( beverage.getDescription() + ", Whip");
  }
  public double cost(){
    return 1.0 + beverage.cost();
  }
}