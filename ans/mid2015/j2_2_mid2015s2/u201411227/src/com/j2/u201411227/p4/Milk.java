package com.j2.u201411227.p4;

public class Milk extends Things{
  public String getDescription(){
    return beverage.getDescription() + ", Milk";
    //System.out.println( beverage.getDescription() + ", Milk");
  }
  public double cost(){
    return 1.0 + beverage.cost();
  }
}