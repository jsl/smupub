package com.j2.u201411227.p3;

public abstract class Pizza{
  String name;
  public void getName(){
    return name;
  }
  public void prepare(){
    System.out.println("preparing...");
  }
  public void bake(){
    System.out.println("baking...");
  }
  public void cut(){
    System.out.println("cutting...");
  }
  public void box(){
    System.out.println("boxing...");
  }
  public abstract void display();
}