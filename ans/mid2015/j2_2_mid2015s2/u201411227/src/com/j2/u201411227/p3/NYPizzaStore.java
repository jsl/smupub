package com.j2.u201411227.p3;

public class NYPizzaStore extends PizzaStore{
  public void createPizza(String type){
    if(type.equals("cheese")){
      return new NYCheesePizza();
    }
    else if(type.equals("veggie")){
      return new NYVeggiePizza();
    }
    else if(type.equals("clam")){
      return new NYClamPizza();
    }
  }
}