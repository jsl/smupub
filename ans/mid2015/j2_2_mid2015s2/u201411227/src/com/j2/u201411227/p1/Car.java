package com.j2.u201411227.p1;

public interface Car{
  public int getCapacity();
  public void display();
}