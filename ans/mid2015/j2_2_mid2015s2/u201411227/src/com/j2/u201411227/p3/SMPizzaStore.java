package com.j2.u201411227.p3;

public class SMPizzaStore extends PizzaStore{
  public void createPizza(String type){
    if(type.equals("cheese")){
      return new SMCheesePizza();
    }
    else if(type.equals("veggie")){
      return new SMVeggiePizza();
    }
    else if(type.equals("clam")){
      return new SMClamPizza();
    }
  }
}