package com.j2.u201411231.p4;

class Caramel extends CondimentDecorator {
  
  public void description() {
    System.out.println("Add Caramel");
  }
  
  public void cost() {
    return 0.15;
  }
}