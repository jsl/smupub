package com.j2.u201411231.p4;

class P4CoffeeMain {
  
  public static void main(String[] args) {
    CondimentDecorator de1 = new Milk;
    CondimentDecorator de2 = new Whip;
    CondimentDecorator de3 = new Caramel;
    Beverage beverage1 = new DarkRost;
    System.out.println( beverage1.description +"is $"+ beverage1.cost +de1.cost+de2.cost+de3.cost);
    Beverage beverage2 = new Espresso;
    System.out.println( beverage2.description +"is $"+ beverage2.cost +de1.cost+de2.cost+de3.cost);
    Beverage beverage3 = new CaramelMacchiato;
    System.out.println( beverage3.description +"is $"+ beverage3.cost +de1.cost+de2.cost+de3.cost);
}