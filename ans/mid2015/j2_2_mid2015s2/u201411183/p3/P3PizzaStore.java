package com.j2.u201411183.p3;

public abstract class P3PizzaStore{
  abstract P3Pizza createPizza(String type);
  
  public P3Pizza orderPizza(String item){
    P3Pizza pizza = createPizza(item);
    pizza.prepare();
    return pizza;}}