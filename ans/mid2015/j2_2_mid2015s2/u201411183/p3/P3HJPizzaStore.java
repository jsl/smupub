package com.j2.u201411183.p3;

public class P3HJPizzaStore extends P3PizzaStore{
  P3Pizza createPizza(String type){
    P3Pizza pizza= null;
    if(type.equals("cheese")){
      pizza = new P3CheesePizza();}
    else if(type.equals("veggie")){
      pizza = new P3VeggiePizza();}
    else if(type.equals("clam")){
      pizza = new P3ClamPizza();}
    return pizza;}}