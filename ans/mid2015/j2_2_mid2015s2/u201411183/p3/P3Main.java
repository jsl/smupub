package com.j2.u201411183.p3;

public class P3Main{
  public static void main(String[] args){
    P3PizzaStore store = new P3HJPizzaStore();
    
    P3Pizza pizza = store.orderPizza("cheese");
    System.out.println("We ordered a "+pizza.getName());
    pizza = store.orderPizza("veggie");
    System.out.println("We ordered a "+pizza.getName());
    pizza = store.orderPizza("clam");
    System.out.println("We ordered a "+pizza.getName());}}