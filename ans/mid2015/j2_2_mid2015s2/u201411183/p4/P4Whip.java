package com.j2.u201411183.p4;

public class P4Whip extends P4Condiment{
  public P4Whip(P4Beverage beverage){
    this.beverage=beverage;}
  public String getDescription(){
  return beverage.getDescription() + ",Whip\n";}
  public double cost(){
    return .10 + beverage.cost();}}