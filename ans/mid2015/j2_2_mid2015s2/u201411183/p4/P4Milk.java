package com.j2.u201411183.p4;

public class P4Milk extends P4Condiment{
  public P4Milk(P4Beverage beverage){
    this.beverage=beverage;}
  public String getDescription(){
    return beverage.getDescription() + ",Milk\n";}
  public double cost(){
    return .10 + beverage.cost();}}