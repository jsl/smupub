package com.j2.u201411183.p4;

public class P4Caramel extends P4Condiment{
  public P4Caramel(P4Beverage beverage){
    this.beverage=beverage;}
  public String getDescription(){
  return beverage.getDescription()+",Caramel\n";}
  public double cost(){
    return .10 +beverage.cost();}}