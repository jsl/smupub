package com.j2.u201411183.p4;

public class P4Main{
  public static void main(String[] args){
    P4Beverage b1=new P4DarkRoast();
    b1=new P4Whip(b1);
    b1=new P4Milk(b1);
    b1=new P4Caramel(b1);
    
    P4Beverage b2=new P4Espresso();
    b2=new P4Whip(b2);
    b2=new P4Milk(b2);
    b2=new P4Caramel(b2);
    
    P4Beverage b3=new P4CaramelMacchiato();
    b3=new P4Whip(b3);
    b3=new P4Milk(b3);
    b3=new P4Caramel(b3);}}