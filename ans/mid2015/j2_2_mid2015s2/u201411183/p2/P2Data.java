package com.j2.u201411183.p2;

import java.util.*;
public class P2Data implements P2Subject{
  private ArrayList observers;
  private String name;
  
  public P2Data(){
    observers=new ArrayList();}
  
  public void registerObserver(Observer o){
    observers.add(o);}
  
  public void removeObserver(Observer o){
    int i = observers.indexOf(o);
    if(i>=0){
      observers.remove(i);}}
  
  public void notifyObservers(){
    for(int i=0; i<observers.size();i++){
      Observer observer = (Observer)observers.get(i);
      observer.update(name);}}
  
  public void measurementChanged(){
    notifyObservers();}
  
  public void setmeasureMents(String name){
    this.name=name;
    measurementChanged();}
  
  public String getName(){
    return name;}}
    
    