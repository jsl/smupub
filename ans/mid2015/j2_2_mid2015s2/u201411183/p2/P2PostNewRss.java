package com.j2.u201411183.p2;

import java.util.*;
public class P2PostNewRss implements P2Observer, P2Display{
  private Subject data;
  private String name;
   
  public P2PostNewRss(Subject data){
    this.data=data;
    data.registerObserver(this);
  }
  public void update(String name){
    this.name=name;
    display();}
  
  public void display(){
    System.out.println(name+" getting a new rss\n");}}