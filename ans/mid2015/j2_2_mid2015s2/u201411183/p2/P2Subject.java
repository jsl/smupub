package com.j2.u201411183.p2;

public interface P2Subject{
  public void registerObserver(Observer o);
  public void removeObserver(Observer o);
  public void notifyObservers();}