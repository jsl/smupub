package com.j2.u201311257.p4;

public abstract class Beverage {
  
  String description = "unknown Beverage";
  
  public abstract String getDescription();
  
  public abstract double cost();
  
  
}