package com.j2.u201311257.p4;

public class Milk extends CondimentDecorator {
    Beverage beverage;
  public Milk(Beverage beverage){
    this.beverage = beverage;
  }
  
  public String getDescription(){
    return beverage.getDescription() + ", Milk";
  }
  
  public double cost(){
    return 2 + beverage.cost();
  }
}