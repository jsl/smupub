//X
package com.j2.u201311257.p3;


abstract public class Pizza{
  String name;
  
  public String getName(){
    return name;
  }
  public void prepare(){
    System.out.println("Preparing...");
  }
  public void bake(){
    System.out.println("Baking...");
  }
  public void cut(){
    System.out.println("cutting...");
  }
  public void box(){
    System.out.println("Boxing...");
  }
  
  public String toString(){
    StringBuffer display = new StringBuffer();
    display.append(name + "\n");
    return display.toString();
  }
}