package com.j2.u201311257.p4;

public abstract class CondimentDecorator extends Beverage {
  Beverage beverage;
  
  public String getDescription(){
    return description;
  }
  
  public abstract double cost();
  
}