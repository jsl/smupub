package com.j2.u201311257.p4;

public class DarkRoast extends Beverage {
  
  public DarkRoast(){
    description = " Dark Roast";
  }
  
  public String getDescription(){
    return "Dark Roast ";
  }
  
  public double cost() {
    return 3;
  }
}