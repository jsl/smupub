//o
package com.j2.u201311257.p4;

public class P4CMMain{
  public static void main(String args[]){
    Beverage beverage = new DarkRoast();
    beverage = new Espresso(beverage);
    beverage = new Milk(beverage);
    beverage = new Whip(beverage);
    beverage = new Caramel(beverage);
    System.out.println("Caramel Macchiato: " + 
                       beverage.getDescription() + " $" + beverage.cost());
  }
}