package com.j2.u201311257.p4;

public class Espresso extends CondimentDecorator {
    Beverage beverage;
  public Espresso(Beverage beverage){
    this.beverage = beverage;
  }
  
  public String getDescription(){
    return beverage.getDescription() + ", Espresso";
  }
  
   public double cost(){
    return 5 + beverage.cost();
  }
   
}