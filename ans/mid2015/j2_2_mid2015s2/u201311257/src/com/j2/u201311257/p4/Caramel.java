package com.j2.u201311257.p4;

public class Caramel extends CondimentDecorator {
  
  public Caramel(Beverage beverage){
    this.beverage = beverage;
  }
  
  public String getDescription(){
    return beverage.getDescription() + ", Caramel";
  }
  
  public double cost(){
    return 1 + beverage.cost();
  }
}