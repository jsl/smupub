@startuml

interface Pizza{
	+void prepare();
	+void bake()
	+void cut()
	+void box()
}

interface PizzaFactory{
	+Pizza orderPizza(String type)
	+void createPizza(Pizza pizza)
}

class CheesePizza
class VeggiePizza
class ClamPizza

class SMPizzaFactory

Pizza <|-- CheesePizza
Pizza <|-- VeggiePizza
Pizza <|-- ClamPizza

PizzaFactory <|-- SMPizzaFactory
PizzaFactory ..> Pizza

@enduml