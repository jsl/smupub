package com.j2.u201411216.p1;

public abstract class P1AbCar implements P1Car{
  private int capacity;
  public void setCapacity(int c){
    capacity = c;
  }
  public int getCapacity(){
    return capacity;
  }
}