package com.j2.u201411216.p2;

public class P2User implements P2Observer{
  private String name;
  private String rss;
  private P2Subject sub;
  public P2User(String uname, P2Subject s){
    this.name = uname;
    rss = "";
    s.registerObserver(this);
  }
  public void update(String rss){
    this.rss = rss;
    System.out.println(name + " getting a new rss.");
  }
}
  
    

