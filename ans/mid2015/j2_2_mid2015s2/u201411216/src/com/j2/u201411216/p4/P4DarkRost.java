package com.j2.u201411216.p4;

public class P4DarkRost extends P4Beverage{
  public P4DarkRost(){
    description = "DarkRoast";
  }
  public double cost(){
    return 2.55;
  }
}