package com.j2.u201411216.p3;

public class P3SMPizzaFactory implements P3PizzaFactory{
  public P3Pizza orderPizza(String type){
    P3Pizza pizza = null;
    if(type.equals("cheese")){
      pizza = new P3CheesePizza();
      createPizza(pizza);
    }
    else if(type.equals("veggie")){
      pizza = new P3VeggiePizza();
      createPizza(pizza);
    }
    else if(type.equals("clam")){
      pizza = new P3ClamPizza();
      createPizza(pizza);
    }
    System.out.println("We ordered a " + pizza + "\n");
    return pizza;
  }
  public void createPizza(P3Pizza pizza){
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
  }
}
            