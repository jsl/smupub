package com.j2.u201411216.p4;

public class P4Whip extends P4Decorator{
  public P4Whip(P4Beverage bev){
    beverage = bev;
  }
  public String getDescription(){
    return beverage.getDescription() + " + Whip";
  }
  public double cost(){
    return beverage.cost() + 0.5;
  }
}