package com.j2.u201411216.p3;

public abstract class P3Pizza{
  public void prepare(){
    System.out.println("preparing...");
  }
  public void bake(){
    System.out.println("Baking...");
  }
  public void cut(){
    System.out.println("cutting...");
  }
  public void box(){
    System.out.println("boxxing...");
  }
}