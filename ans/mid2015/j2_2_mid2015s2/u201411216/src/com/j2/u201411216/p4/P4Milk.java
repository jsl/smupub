package com.j2.u201411216.p4;

public class P4Milk extends P4Decorator{
  public P4Milk(P4Beverage bev){
    beverage = bev;
  }
  public String getDescription(){
    return beverage.getDescription() + " + Milk";
  }
  public double cost(){
    return beverage.cost() + 0.3;
  }
}