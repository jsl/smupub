package com.j2.u201411216.p2;

import java.util.ArrayList;

public class P2RSS implements P2Subject{
  private String rss;
  private ArrayList observers;
  public P2RSS(){
    rss = "";
    observers = new ArrayList();
  }
  public void registerObserver(P2Observer o){
    observers.add(o);
  }
  public void removeObserver(P2Observer o){
    int i = observers.indexOf(o);
    observers.remove(i);
  }
  public void notifyObserver(){
    for(int i = 0; i < observers.size(); i++){
      P2Observer ob = (P2Observer)observers.get(i);
      ob.update(rss);
    }
  }
  public void setRss(String s){
    this.rss = s;
    notifyObserver();
  }
}