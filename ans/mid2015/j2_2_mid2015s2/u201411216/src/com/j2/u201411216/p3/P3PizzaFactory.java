package com.j2.u201411216.p3;

public interface P3PizzaFactory{
  public P3Pizza orderPizza(String type);
  public void createPizza(P3Pizza pizza);
}