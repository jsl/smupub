package com.j2.u201411216.p4;

public class P4Main{
  public static void main(String args[]){
    P4Beverage coffee = new P4Espresso();
    coffee = new P4Milk(coffee);
    coffee = new P4Milk(coffee);
    coffee = new P4Caramel(coffee);
    System.out.println(coffee.getDescription() + ", cost is $" + coffee.cost());
  }
}