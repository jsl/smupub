package com.j2.u201411216.p3;

public class P3Main{
  public static void main(String args[]){
    P3PizzaFactory sm = new P3SMPizzaFactory();
    sm.orderPizza("cheese");
    sm.orderPizza("veggie");
    sm.orderPizza("clam");
  }
}