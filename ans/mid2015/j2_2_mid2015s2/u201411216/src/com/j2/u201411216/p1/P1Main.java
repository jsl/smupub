package com.j2.u201411216.p1;

public class P1Main{
  public static void main(String args[]){
    P1Car van = new P1Van();
    P1Car sedan = new P1Sedan();
    P1Car sports = new P1SportsCar();
    System.out.println("Sum of Capacity : " + (van.getCapacity() + sedan.getCapacity()
                                                 + sports.getCapacity()));
  }
}