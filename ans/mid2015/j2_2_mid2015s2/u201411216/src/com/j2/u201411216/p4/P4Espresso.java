package com.j2.u201411216.p4;

public class P4Espresso extends P4Beverage{
  public P4Espresso(){
    description = "Espresso";
  }
  public double cost(){
    return 1.75;
  }
}