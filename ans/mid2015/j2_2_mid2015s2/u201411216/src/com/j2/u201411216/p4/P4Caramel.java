package com.j2.u201411216.p4;

public class P4Caramel extends P4Decorator{
  public P4Caramel(P4Beverage bev){
    beverage = bev;
  }
  public String getDescription(){
    return beverage.getDescription() + " + Caramel";
  }
  public double cost(){
    return beverage.cost() + 0.7;
  }
}