package com.j2.u201411216.p4;

public abstract class P4Beverage{
  protected String description;
  public String getDescription(){
    return description;
  }
  public abstract double cost();
}