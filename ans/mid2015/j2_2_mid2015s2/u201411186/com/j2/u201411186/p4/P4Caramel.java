package com.j2.u201411186.p4
  
public class Caramel extends CondimentDecorator {
 Beverage beverage;
 
 public Caramel(Beverage beverage) {
  this.beverage = beverage;
 }
 
 public String getDescription() {
  return beverage.getDescription() + ", Caramel";
 }
}