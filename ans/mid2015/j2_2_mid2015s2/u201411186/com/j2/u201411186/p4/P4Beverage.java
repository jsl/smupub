package com.j2.u201411186.p4

public abstract class Beverage {
 String description = "Unknown Beverage";
  
 public String getDescription() {
  return description;
 }
 
 public abstract double cost();
}
