package com.j2.u201211250.p4;

public class Whip extends CondimentDecorator{
  Whip()
  {
    description=", Whip";
  }
  public double cost()
  {
    return beverage.cost()+0.3;
  }
}