package com.j2.u201211250.p4;

public class Caramel extends CondimentDecorator{
  Caramel()
  {
    description=", Caramel";
  }
  public double cost()
  {
    return beverage.cost()+0.4;
  }
}