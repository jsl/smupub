package com.j2.u201211250.p4;

public class Milk extends CondimentDecorator{
  Milk()
  {
    description=", Milk";
  }
  public double cost()
  {
    return beverage.cost()+0.5;
  }
}