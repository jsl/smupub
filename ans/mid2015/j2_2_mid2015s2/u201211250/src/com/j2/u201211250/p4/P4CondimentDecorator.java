package com.j2.u201211250.p4;

public abstract class CondimentDecorator extends Beverage{
  Beverage beverage;
  public abstract String getDescription();
  public abstract double cost();
}