package com.j2.u201211293.p4;

public class Milk extends condi{
  
  public Milk(Beverage beverage){
    this.beverage = beverage;
  }
  
  public String getDescription(){
    return beverage.getDescription()+ "+ MILK";
  }
  
  public double cost(){
    return beverage.cost()+20;
  }
}