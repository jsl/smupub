package com.j2.u201211293.p3;

public class NYpizzaStore extends PizzaStore{
  public Pizza create(String type){
    Pizza pizza = null;
    
    if(type.equals("cheese")){
      pizza = new Cheesepizza();
    }
    if(type.equals("veggie")){
      pizza = new Veggiepizza();
    }
    if(type.equals("clam")){
      pizza = new Clampizza();
    }
    return pizza;
  }
}