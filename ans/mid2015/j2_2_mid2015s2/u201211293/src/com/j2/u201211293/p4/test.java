package com.j2.u201211293.p4;

public class test{
  public static void main(String[] args){
    
    Beverage beverage = new DarkRoast();
    beverage = new Caramel(beverage);
    beverage = new Milk(beverage);
    beverage = new Whip(beverage);
    
    System.out.println( "Caramel Macciato ingredient : " + beverage.getDescription() + "and cost : " + beverage.cost() + "$");
  }}