package com.j2.u201211293.p2;
import java.util.*;
public class information implements subject{
  
  private String a;
  private ArrayList<Observer> observers;
  
  public information(){
    observers = new ArrayList<Observer>();
}
  public void register(Observer o){
    observers.add(o);
  }
  
  public void notifyd(){
    for(Observer observer:observers)
      observer.update(a);
  }
  public void measure(){
    notifyd();
  }
  
  public void set(String a){
    this.a = a;
    measure();
  }
}