package com.j2.u201211293.p4;

public abstract class Beverage{
  public String description = "no";
  
  public String getDescription(){
    return description;
  }
  
  public abstract double cost();
}