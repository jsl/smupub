package com.j2.u201211293.p4;

public class Whip extends condi{
  
  public Whip(Beverage beverage){
    this.beverage = beverage;
  }
  
  public String getDescription(){
    return beverage.getDescription()+ "+ Whip";
  }
  
  public double cost(){
    return beverage.cost()+30;
  }
}