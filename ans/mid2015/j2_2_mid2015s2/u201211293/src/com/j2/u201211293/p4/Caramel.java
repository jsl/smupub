package com.j2.u201211293.p4;

public class Caramel extends condi{
  
  public Caramel(Beverage beverage){
    this.beverage = beverage;
  }
  
  public String getDescription(){
    return beverage.getDescription()+ "+ Caramel";
  }
  
  public double cost(){
    return beverage.cost()+10;
  }
}