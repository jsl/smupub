package com.j2.u201211293.p3;

public abstract class PizzaStore{
  public abstract Pizza create(String type);
  public Pizza order(String type){
    Pizza pizza =null;
    
    pizza = create(type);
    
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    
    return pizza;
  }}
         