package com.j2.u201211310.p4;

public class Caramel extends BeDecorator{
  
  public Caramel(Beverage b){
    this.b = b;
  }
  
  public String getDescription(){
   return b.getDescription() + ", Caramel"; 
  }
  
  public double cost(){
    return b.cost() + 0.6;
  }
  
  
}