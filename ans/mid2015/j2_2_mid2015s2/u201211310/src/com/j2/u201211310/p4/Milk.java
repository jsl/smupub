package com.j2.u201211310.p4;

public class Milk extends BeDecorator{
  
  public Milk(Beverage b){
    this.b = b;
  }
  
  public String getDescription(){
   return b.getDescription() + ", Milk"; 
  }
  
  public double cost(){
    return b.cost() + 0.5;
  }
  
  
}