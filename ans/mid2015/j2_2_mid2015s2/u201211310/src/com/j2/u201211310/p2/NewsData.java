package com.j2.u201211310.p2;
import java.util.*;


public class NewsData implements Subject{
  ArrayList<Observer> observers;
  
  public NewsData(){
   observers = new ArrayList(); 
  }
  
  public void registerObserver(Observer O){
    observers.add(O);
  }
  public void romoveObserver(Observer O){
   int n = observers.indexOf(O);
    observers.remove(n); 
  }
  public void notifyObserver(){
    for(Observer observer : observers){
     observer.update(); 
    }
  }
    
    
    public void changeNews(){
     notifyObserver(); 
    }
    
    public void postNewRss(){
     changeNews(); 
    }
    
  }
  
  
