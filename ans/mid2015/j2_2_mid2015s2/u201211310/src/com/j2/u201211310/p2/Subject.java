package com.j2.u201211310.p2;


public interface Subject{
  public void registerObserver(Observer O);
  public void romoveObserver(Observer O);
  public void notifyObserver();
  
  
}