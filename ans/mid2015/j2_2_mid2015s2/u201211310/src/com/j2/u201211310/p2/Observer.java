package com.j2.u201211310.p2;


public interface Observer{
 
  public void update();
}