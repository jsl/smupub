package com.j2.u201211310.p4;



public class DarkRoast extends Beverage{
 
  public DarkRoast(){
   description = "DarkRoast"; 
   cost = 1.0;
  }
  
  public double cost(){
   return cost; 
  }
  
}