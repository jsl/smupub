package com.j2.u201211310.p4;


public abstract class Beverage{
  String description;
  double cost;
  
  public Beverage(){
   description = "unknown"; 
  }
  
  
  public abstract double cost();
  
  public String getDescription(){
   return description; 
  }
  
}