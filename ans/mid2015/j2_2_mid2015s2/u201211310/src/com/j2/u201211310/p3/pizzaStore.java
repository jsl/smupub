package com.j2.u201211310.p3;


public abstract class pizzaStore{
  
  
  public abstract Pizza createPizza(String type);
  
  public Pizza orderPizza(String type){
   Pizza pizza = createPizza(type);
   pizza.prepare();
   pizza.bake();
   pizza.cut();
   pizza.box();
    return pizza;
  }
  
}