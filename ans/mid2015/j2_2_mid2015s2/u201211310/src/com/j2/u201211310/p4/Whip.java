package com.j2.u201211310.p4;

public class Whip extends BeDecorator{
  
  public Whip(Beverage b){
    this.b = b;
  }
  
  public String getDescription(){
   return b.getDescription() + ", Whip"; 
  }
  
  public double cost(){
    return b.cost() + 0.4;
  }
  
  
}