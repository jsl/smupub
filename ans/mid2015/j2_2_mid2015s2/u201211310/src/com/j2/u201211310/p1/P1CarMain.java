package com.j2.u201211310.p1;

public class P1CarMain{
 
  public static void main(String[] args){
   
    Car sedan= new Sedan();
    Car van= new Van();
    Car sports = new SportsCar();
   
   
    System.out.println(sedan.getName() + "can sit " + sedan.getCapacity());
    System.out.println(van.getName() + "can sit " + van.getCapacity());
    System.out.println(sports.getName() + "can sit " + sports.getCapacity());
    System.out.println("sum = "+ (sedan.getCapacity()+van.getCapacity()+sports.getCapacity()));
  }
  
}