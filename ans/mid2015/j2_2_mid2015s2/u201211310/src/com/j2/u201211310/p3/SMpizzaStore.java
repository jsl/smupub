package com.j2.u201211310.p3;



public class SMpizzaStore extends pizzaStore{
  
  public Pizza createPizza(String type){
   Pizza pizza = null; 
   if(type.equals("cheese")){
    pizza = new CheesePizza(); 
   }
   else if(type.equals("clam")){
     pizza = new ClamPizza(); 
   }
   else if (type.equals("veggie")){
    pizza= new VeggiePizza(); 
   }
   return pizza;
  }
  
  
}