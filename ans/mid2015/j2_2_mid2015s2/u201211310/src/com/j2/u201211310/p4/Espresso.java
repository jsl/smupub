package com.j2.u201211310.p4;



public class Espresso extends Beverage{
 
  public Espresso(){
   description = "Espresso"; 
   cost = 1.0;
  }
  
  public double cost(){
   return cost; 
  }
  
}