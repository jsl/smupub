package com.j2.u201211310.p4;


public class P4BeverageMain{
  public static void main(String[] args){
    
    Beverage b2 = new DarkRoast();
     System.out.println(b2.getDescription() + " $" +b2.cost());
    
    
    
    Beverage b1 = new Espresso();
    b1 = new Milk(b1);
    b1 = new Caramel(b1);
    b1 = new Caramel(b1);
    b1 = new Whip(b1);
    System.out.println("Caramel Macchiato = " + b1.getDescription() + " $" +b1.cost());
    
    
    
  }
  
}