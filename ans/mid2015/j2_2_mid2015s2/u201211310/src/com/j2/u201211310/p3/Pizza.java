package com.j2.u201211310.p3;

public abstract class Pizza{
  String name;
 
  public String getName(){
   return name; 
  }
  
  
  public void prepare(){
   System.out.println("preparing...");
  }
  public void bake(){
   System.out.println("Baking...");
  }
  public void cut(){
   System.out.println("cutting...");
  }
  public void box(){
   System.out.println("boxing"); 
  }
}
