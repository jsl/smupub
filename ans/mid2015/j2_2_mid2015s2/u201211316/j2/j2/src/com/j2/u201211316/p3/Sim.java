package com.j2.u201211316.p3;
public class Sim {
  public Pizza create(String type){
    Pizza pizza=null;
    if (type.equals("cheese")){
       pizza = new CheesePizza();
    }
    if (type.equals("Veggie")){
      pizza = new VeggiePizza();
    }
    if (type.equals("Clam")){
      pizza = new ClamPizza();
    }
    return pizza;
  }
}