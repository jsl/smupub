package com.j2.u201211316.p4;
public class Milk extends Com{
  public Milk(Be be){
    this.be =be;
  }
  public double cost(){
    return 20+be.cost();
  }
  public String getDis(){
    return be.getDis()+",Milk";
  }
}