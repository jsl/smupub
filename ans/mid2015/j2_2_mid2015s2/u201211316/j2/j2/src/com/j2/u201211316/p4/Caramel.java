package com.j2.u201211316.p4;
public class Caramel extends Com{
  public Caramel(Be be){
    this.be =be;
  }
  public double cost(){
    return 20+be.cost();
  }
  public String getDis(){
    return be.getDis()+",Caramel";
  }
}