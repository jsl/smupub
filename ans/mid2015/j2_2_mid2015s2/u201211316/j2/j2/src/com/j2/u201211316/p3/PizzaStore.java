package com.j2.u201211316.p3;
public class PizzaStore{
  Sim fa;
  public PizzaStore(Sim fa){
    this.fa = fa;
  }
  public Pizza order(String type){
    Pizza pizza;
   pizza = fa.create(type); 
   
    pizza.pre();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
}