
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  lab5()


def lab1():
  barb=makePicture(getMediaPath("barbara.jpg"))
  hpixel=getPixel(barb,0,getHeight(barb)-1)
  hpgreen=getGreen(hpixel)
  print hpgreen
  
def lab2():  
  mix(0,0,110,97,0,97)
  mix(110,0,220,97,220,0)
  mix(0,97,110,194,110,97)
  mix(110,97,220,194,0,0)
  mix(0,194,110,291,220,97)
  mix(110,194,220,291,110,0)
  
def mix(startX,startY,endX,endY,targX,targY):
  barb=makePicture(getMediaPath("barbara.jpg"))
  canv=makePicture(getMediaPath("640x480.jpg"))
  targetX=targX
  for sourceX in range(startX,endX):
    targetY=targY
    for sourceY in range(startY,endY):
      barbPixel=getPixel(barb,sourceX,sourceY)
      barbColor=getColor(barbPixel)
      setColor(getPixel(canv,targetX,targetY),barbColor)
      targetY=targetY+1
    targetX=targetX+1
  show(canv)   
  return canv    

def lab3():
  barb=makePicture(getMediaPath("barbara.jpg"))
  addLine(barb,0,50,220,50,red)
  addLine(barb,0,60,220,60,yellow)
  addLine(barb,0,70,220,70,red)
  addLine(barb,0,90,220,90,red)
  addLine(barb,0,80,220,80,yellow)
  addLine(barb,0,100,220,100,yellow)
  addLine(barb,110,0,110,292,yellow)
  addLine(barb,90,0,90,292,yellow)
  addLine(barb,120,0,120,292,red)
  addLine(barb,140,0,140,292,red)
  addLine(barb,100,0,100,292,red)
  addLine(barb,130,0,130,292,yellow)
  show(barb)
  return barb

#def lab4():

def lab5():
  teeth()
  eyebrow()
  hair()
     
def teeth():
  barb=makePicture(getMediaPath("barbara.jpg"))
  purple=makeColor(160,32,240)
  for x in range(108,153):
    for y in range(154,170): 
      px=getPixel(barb,x,y)
      pxcolor=getColor(px)
      if distance(pxcolor,white)<160:
         setColor(px,purple)
  show(barb)
  return barb     

def eyebrow():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for x in range(83,167):
    for y in range(100,107): 
      px=getPixel(barb,x,y)
      pxcolor=getColor(px)
      if distance(pxcolor,black)>10:
         setColor(px,red)
  show(barb)
  return barb     

def hair():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for x in range(38,208):
    for y in range(19,284): 
      px=getPixel(barb,x,y)
      pxcolor=getColor(px)
      if distance(pxcolor,black)<150:
         setColor(px,orange)
  show(barb)
  return barb 
         
lab1()
lab2()
lab3()
lab5()
