import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def lab1():
  file=getMediaPath("barbara.jpg")
  pic=makePicture(file)
  width=getWidth(pic)
  height=getHeight(pic)
  greencolor=getGreen(getPixel(pic,width-1,height-1))
  print greencolor
  
def lab2():
  file=getMediaPath("barbara.jpg")
  source=makePicture(file)
  canvasf=getMediaPath("7inX95in.jpg")
  canvas=makePicture(canvasf)
  gridX=getWidth(source)/3
  gridY=getHeight(source)/2  
  targetX=0
  for sourceX in range(0,gridX):
    targetY=0
    for sourceY in range(0,gridY):
      color=getColor(getPixel(source,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY+gridY),color)
      color=getColor(getPixel(source,sourceX+gridX,sourceY))
      setColor(getPixel(canvas,targetX+gridX*2,targetY),color)
      color=getColor(getPixel(source,sourceX+gridX*2,sourceY))
      setColor(getPixel(canvas,targetX+gridX,targetY+gridY),color)
      color=getColor(getPixel(source,sourceX,sourceY+gridY))
      setColor(getPixel(canvas,targetX,targetY),color)
      color=getColor(getPixel(source,sourceX+gridX,sourceY+gridY))
      setColor(getPixel(canvas,targetX+gridX*2,targetY+gridY),color)
      color=getColor(getPixel(source,sourceX+gridX*2,sourceY+gridY))
      setColor(getPixel(canvas,targetX+gridX,targetY),color)
      targetY+=1
    targetX+=1  
  show(canvas)
  return canvas

def lab4():
  file=getMediaPath("barbara.jpg")
  source=makePicture(file)
  canvasf=getMediaPath("640x480.jpg")
  canvas=makePicture(canvasf)
  gridX=getWidth(source)/2
  gridY=getHeight(source)/2
  targetX=0
  for sourceX in range(0,gridX):
    targetY=0
    for sourceY in range(0,gridY):
      px=getPixel(source,sourceX,sourceY)
      aver=(int)(getRed(px)+getGreen(px)+getBlue(px))/3
      color=makeColor(aver,aver,aver)
      whitecolor=makeBrighter(color)
      white1=makeBrighter(whitecolor)
      white2=makeBrighter(white1)
      white3=makeBrighter(white2)
      setColor(getPixel(canvas,targetX,targetY),white3)
      targetY+=1
    targetX+=1
   
  targetX=0
  for sourceX in range(0,gridX):
    targetY=0
    for sourceY in range(0,gridY):
      px=getPixel(source,sourceX+gridX,sourceY)
      aver=(int)(getRed(px)+getGreen(px)+getBlue(px))/3
      color=makeColor(aver,aver,aver)
      lightgray=makeLighter(color)
      setColor(getPixel(canvas,targetX+gridX,targetY),lightgray)
      targetY+=1
    targetX+=1
    
  targetX=0
  for sourceX in range(0,gridX):
    targetY=0
    for sourceY in range(0,gridY):
      px=getPixel(source,sourceX,sourceY+gridY)
      aver=(int)(getRed(px)+getGreen(px)+getBlue(px))/3
      color=makeColor(aver,aver,aver)
      darkgray1=makeDarker(color)
      darkgray2=makeDarker(darkgray1)
      setColor(getPixel(canvas,targetX,targetY+gridY),darkgray2)
      targetY+=1
    targetX+=1
    
  targetX=0
  for sourceX in range(0,gridX):
    targetY=0
    for sourceY in range(0,gridY):
      px=getPixel(source,sourceX+gridX,sourceY+gridY)
      aver=(int)(getRed(px)+getGreen(px)+getBlue(px))/3
      color=makeColor(aver,aver,aver)
      darkgray1=makeDarker(color)
      darkgray2=makeDarker(darkgray1)
      darkgray3=makeDarker(darkgray2)
      darkgray4=makeDarker(darkgray3)
      darkgray5=makeDarker(darkgray4)
      setColor(getPixel(canvas,targetX+gridX,targetY+gridY),darkgray5)
      targetY+=1
    targetX+=1  
  show(canvas)
  
def lab5():
  file=getMediaPath("barbara.jpg")
  source=makePicture(file)
  teethcolor=makeColor(220,200,180)
  eyeblows=makeColor(140,100,90)
  hair=makeColor(80,40,20)
  purple=makeColor(180,20,170)
  for x in range(108,150):
    for y in range(158,170):
      color=getColor(getPixel(source,x,y))
      if(distance(color,teethcolor)<70.0):
        setColor(getPixel(source,x,y),purple)
  
  for x in range(80,115):
    for y in range(105,110):
      color=getColor(getPixel(source,x,y))
      if(distance(color,eyeblows)<30.0):
        setColor(getPixel(source,x,y),red)
      
  for x in range(138,165):
    for y in range(96,105):
      color=getColor(getPixel(source,x,y))
      if(distance(color,eyeblows)<30.0):
        setColor(getPixel(source,x,y),red)
        
  for x in range(50,180):
    for y in range(18,102):
      color=getColor(getPixel(source,x,y))
      if(distance(color,hair)<50.0):
        setColor(getPixel(source,x,y),orange)
        
  for x in range(34,198):
    for y in range(130,291):
      color=getColor(getPixel(source,x,y))
      if(distance(color,hair)<50.0):
        setColor(getPixel(source,x,y),orange)        
  show(source)
        
def lab6():
  file=getMediaPath("barbara.jpg")
  barb=makePicture(file)
  file2=getMediaPath("Katie-smaller.jpg")
  katie=makePicture(file2)
  canvasf=getMediaPath("640x480.jpg")
  canvas=makePicture(canvasf)
  
def main():
  lab1()
  lab2()
  lab4()
  lab5()
 
lab1()
lab2()
lab4()
lab5()
