def main():
  lab1()
  lab2()
  lab3()
  #lab4()
  lab5()
  #lab6()

def lab1():
  barbf = getMediaPath("barbara.jpg")
  barb = makePicture(barbf)
  finalPixel = getPixel(barb,0,getHeight(barb)-1)
  finalGreen = getGreen(finalPixel)
  print finalGreen

def lab2():
  barbf=getMediaPath("barbara.jpg")
  canvasf=getMediaPath("7inX95in.jpg")
  barb=makePicture(barbf)
  canvas=makePicture(canvasf)
  width=getWidth(barb)
  height=getHeight(barb)
  pointX=width/3
  pointY=height/2
  for x in range(0,pointX):
    for y in range(0,pointY):
      color=getColor(getPixel(barb,x,y))
      setColor(getPixel(canvas,x,y+pointY),color)
      color=getColor(getPixel(barb,x+pointX,y))
      setColor(getPixel(canvas,x+(pointX*2),y),color)
      color=getColor(getPixel(barb,x+(pointX*2),y))
      setColor(getPixel(canvas,x+pointX,y+pointY),color)
      color=getColor(getPixel(barb,x,y+pointY))
      setColor(getPixel(canvas,x,y),color)
      color=getColor(getPixel(barb,x+pointX,y+pointY))
      setColor(getPixel(canvas,x+(pointX*2),y+pointY),color)
      color=getColor(getPixel(barb,x+(pointX*2),y+pointY))
      setColor(getPixel(canvas,x+pointX,y),color)
  show(canvas)
  return canvas
  
def lab3():
  barbf = getMediaPath("barbara.jpg")
  barb = makePicture(barbf)
  width = getWidth(barb)
  height = getHeight(barb)
  pointX = width/10
  pointY = height/10
  count = 1
  for x in range(0, width):
    for y in range(pointY, height, pointY):
      if ((count%2)==1):
        px = getPixel(barb,x,y)
        setColor(px,yellow)
      else :
        px = getPixel(barb,x,y)
        setColor(px,red)
      count+= 1
  count = 1
  for y in range(0, height):
    for x in range(pointX, width, pointX):
      if ((count%2)==1):
        px = getPixel(barb,x,y)
        setColor(px,yellow)
      else :
        px = getPixel(barb,x,y)
        setColor(px,red)
      count+= 1
  show(barb)
  return barb

def lab5():
  purple = makeColor(128,0,255)
  brown = makeColor(89,52,43)
  teeth = makeColor(255,237,227)
  eyebro1 = makeColor(121,82,67)
  eyebro2 = makeColor(148,105,99)
  barbf = getMediaPath("barbara.jpg")
  barb = makePicture(barbf)
  #first
  for x in range(111,149):
    for y in range(160,170):
      px = getPixel(barb,x,y)
      color = getColor(px)
      if distance(color,teeth)<150:
        setColor(px,purple)
  #second_1
  for x in range(134,165):
    for y in range(97,106):
      px = getPixel(barb,x,y)
      color = getColor(px)
      if distance(color,eyebro1)<70:
        setColor(px,red)
  #second_2
  for x in range(79,114):
    for y in range(106,113):
      px = getPixel(barb,x,y)
      color = getColor(px)
      if distance(color,eyebro2)<20:
        setColor(px,red)
  #third
  for x in range(37,219):
    for y in range(27,291):
      px = getPixel(barb,x,y)
      color = getColor(px)
      if distance(color,brown)<50:
        redness = getRed(px)*1.3
        setRed(px,redness)
  show(barb)
  return barb

def lab6():
  barbf = getMediaPath("barbara.jpg")
  barb = makePicture(barbf)
  katief = getMediaPath("Katie-smaller.jpg")
  katie = makePicture(katief)
  canvasf=getMediaPath("640x480.jpg")
  barb=makePicture(barbf)
  canvas=makePicture(canvasf)
  point = (getWidth(barb)/3)*2
  for x in range(0,point):
    for y in range(0,getHeight(barb)):
      color=getColor(getPixel(barb,x,y))
      setColor(getPixel(canvas,x,y),color)

  
  
 