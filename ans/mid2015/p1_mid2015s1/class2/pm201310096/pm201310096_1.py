import sys 
sys.path.append('/home/jsl/Code/git/bb/p1/src')
from goJES import *

def lab1():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  p=getPixel(BAR,0,293)
  gp=getGreen(p)
  print (gp)

def lab2():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  CAN=makePicture(getMediaPath("7inX95in.jpg"))
  #1
  sourceX=0
  for targetX in range(0,74):
    sourceY=0
    for targetY in range(147,294):
      color=getColor(getPixel(BAR,sourceX,sourceY))
      setColor(getPixel(CAN,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  #2  
  sourceX=74
  for targetX in range(148,222):
    sourceY=0
    for targetY in range(0,294):
      color=getColor(getPixel(BAR,sourceX,sourceY))
      setColor(getPixel(CAN,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  #3
  sourceX=148
  for targetX in range(74,148):
    sourceY=0
    for targetY in range(147,294):
      color=getColor(getPixel(BAR,sourceX,sourceY))
      setColor(getPixel(CAN,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  #4  
  sourceX=0
  for targetX in range(0,74):
    sourceY=147
    for targetY in range(0,147):
      color=getColor(getPixel(BAR,sourceX,sourceY))
      setColor(getPixel(CAN,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  #5
  sourceX=74
  for targetX in range(148,222):
    sourceY=147
    for targetY in range(147,294):
      color=getColor(getPixel(BAR,sourceX,sourceY))
      setColor(getPixel(CAN,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
    
  #6
  sourceX=148
  for targetX in range(74,148):
    sourceY=147
    for targetY in range(0,147):
      color=getColor(getPixel(BAR,sourceX,sourceY))
      setColor(getPixel(CAN,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(CAN)
 

  
def lab5():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  #teeth
  white=makeColor(214,191,177)
  purple=makeColor(160,32,240)
  for x in range(102,155):
    for y in range(155,171):
      p=getPixel(BAR,x,y)
      color=getColor(p)
      if distance(color,white)<60:
        setColor(p,purple)
  #eye
  brown=makeColor(62,30,15)
  for x in range(79,169):
    for y in range(100,105):
      p=getPixel(BAR,x,y)
      color=getColor(p)
      if distance(color,brown)<160:
        setColor(p,red)
  #hair
  brown=makeColor(57,23,11)
  for x in range(35,208):
    for y in range(21,284):
      p=getPixel(BAR,x,y)
      color=getColor(p)
      if distance(color,brown)<60:
        setColor(p,orange)
  show(BAR)
      
      

def lab4():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(BAR):
    red=getRed(p)
    blue=getBlue(p)
    green=getGreen(p)
    
    A=(red+blue+green)/3
    if 0< A <63:
      setColor(p,white)
    if 64< A <126:
      setColor(p,lightGray)
    if 127< A <189:
      setColor(p,darkGray)
    if 190< A < 254:
      setColor(p,black)
  show (BAR)
  

def lab3():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  horizontal()
  vertical()
  show (BAR)

def horizontal():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  for x in range(0,getHeight(BAR),10):
    for y in range(0,getHeight(BAR)):
      px=getPixel(BAR,x,y)
      setColor(px,red)

def vertical():
  BAR=makePicture(getMediaPath("barbara.jpg"))
  for x in range(0,getWidth(BAR),10):
    for y in range(0,getHeight(BAR)):
      setColor(getColor(getPixel(BAR,x,y)),yellow)


def main():
  lab1()
  lab2()
  #lab3()
  lab4()
  lab5()

lab2()
lab4()
lab5()
