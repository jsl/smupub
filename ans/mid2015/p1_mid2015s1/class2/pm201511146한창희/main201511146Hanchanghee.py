
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lub1()
  lub2()
  lub3()
  lub4()
  lub5()
  lub6()
  return main
  
def lub1():
  pict=makePicture(getMediaPath("barbara.jpg"))
  h=getHeight(pict)-1
  pixel=getPixel(pict,0,h)
  gree=getGreen(pixel)
  print gree
  return gree



def lub2():
  pict=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("640x480.jpg"))
  w=getWidth(pict)/3
  h=getHeight(pict)/2
  
 
  for x1 in range(0,w):
    for y1 in range(0,h):
      color1=getColor(getPixel(pict,x1,y1))
      pixel1=getPixel(canvas,x1,y1)
      
  for x2 in range(w,w+w):
    for y2 in range(0,h):
      color2=getColor(getPixel(pict,x2,y2))
      pixel2=getPixel(canvas,x2,y2)
     
  for x3 in range(w+w,w+w+w):
    for y3 in range(0,h):
      color3=getColor(getPixel(pict,x3,y3))
      pixel3=getPixel(canvas,x3,y3)
     
  for x4 in range(0,w):
    for y4 in range(h,h+h):
      color4=getColor(getPixel(pict,x4,y4))
      pixel4=getPixel(canvas,x4,y4)
     
  for x5 in range(w,w+w):
    for y5 in range(h,h+h):
      color5=getColor(getPixel(pict,x5,y5))
      pixel5=getPixel(canvas,x5,y5)
     
  for x6 in range(w+w,w+w+w):
    for y6 in range(h,h+h):
      color6=getColor(getPixel(pict,x6,y6))
      pixel6=getPixel(canvas,x6,y6)
      setColor(pixel1,color4) 
      setColor(pixel2,color6)
      setColor(pixel3,color2)
      setColor(pixel4,color1)
      setColor(pixel5,color3)
      setColor(pixel6,color5)
  show(canvas)
  return canvas

def lub3():
  pict=makePicture(getMediaPath("barbara.jpg"))
  lub31(pict)
  lub32(pict)
  lub33(pict)
  lub34(pict)
  show(pict)
  return pict


def lub31(src):
  for x in range(0, getWidth(src), 20):
    for y in range(0, getHeight(src)):
      pixel=getPixel(src,x,y)
      setColor(pixel,red)
  return src
  
def lub32(src):
  for x2 in range(10, getWidth(src), 20):
    for y2 in range(0, getHeight(src)):
      pixel2=getPixel(src,x2,y2)
      setColor(pixel2,yellow)
  return src
  
def lub33(src):
  for x in range(0, getWidth(src)):
    for y in range(0, getHeight(src), 20):
      pixel=getPixel(src,x,y)
      setColor(pixel,red)
  return src
  
def lub34(src):
  for x2 in range(0, getWidth(src)):
    for y2 in range(10, getHeight(src), 20):
      pixel2=getPixel(src,x2,y2)
      setColor(pixel2,yellow)
  return src

def lub4():
  pict=makePicture(getMediaPath("barbara.jpg"))
  
  for x in range(0,getWidth(pict)):
    for y in range(0,getHeight(pict)):
      pixel=getPixel(pict,x,y)
      r=getRed(pixel)
      g=getGreen(pixel)
      b=getBlue(pixel)
      lum=(r+g+b)/3
      if lum<20:
        setColor(pixel,black)
      if lum>21 and lum<80:
        setColor(pixel,darkGray)
      if lum>81 and lum<150:
        setColor(pixel,lightGray)
      if lum>151:
        setColor(pixel,white)
  show(pict)
  return pict

def lub5():
  purple=makeColor(158,46,171)
  brown=makeColor(94,63,28)
  pict=makePicture(getMediaPath("barbara.jpg"))
  for x1 in range (103,156):
    for y1 in range(147,174):
      pixel1=getPixel(pict,x1,y1)
      color1=getColor(pixel1) 
      if distance(color1,white)<160:
        setColor(pixel1,purple)
        
  for x2 in range (84,164):
    for y2 in range(99,106):
      pixel2=getPixel(pict,x2,y2)
      color2=getColor(pixel2) 
      if distance(color2,black)<240:
        setColor(pixel2,red)
        
  for x3 in range (32,210):
    for y3 in range(11,292):
      pixel3=getPixel(pict,x3,y3)
      color3=getColor(pixel3) 
      if distance(color3,brown)<40:
        setColor(pixel3,orange)
        
  
  show(pict)
  return pict

def lub6():
  pict=makePicture(getMediaPath("barbara.jpg"))
  pict2=makePicture(getMediaPath("Katie-smaller.jpg"))
  canvas=makePicture(getMediaPath("640x480.jpg"))
  X1=0
  for x in range(0,150):
    Y1=0
    for y in range(0,getHeight(pict)):
      pixel=getPixel(pict,x,y)
      color1=getColor(pixel)
      pixel1=getPixel(canvas,X1,Y1)
      setColor(pixel1,color1)
      Y1=Y1+1
    X1=X1+1
    
  overlap=getWidth(pict)-150
  X22=150
  for x2 in range(0,overlap):
    Y22=0
    for y2 in range(0,getHeight(pict2)):
      newr=getRed(getPixel(pict,X22,Y22))*0.75+getRed(getPixel(pict2,x2,y2))*0.25
      newg=getGreen(getPixel(pict,X22,Y22))*0.75+getGreen(getPixel(pict2,x2,y2))*0.25
      newb=getBlue(getPixel(pict,X22,Y22))*0.75+getBlue(getPixel(pict2,x2,y2))*0.25
      color2=makeColor(newr,newg,newb)
      pixel2=getPixel(canvas,X22,Y22)
      setColor(pixel2,color2)
      Y22=Y22+1
    X22=X22+1
  
  X33=150+overlap
  for x3 in range(overlap,getWidth(pict2)):
    Y33=0
    for y3 in range(0,getHeight(pict2)):
      pixel3=getPixel(pict2,x3,y3)
      color3=getColor(pixel3)
      pixel3=getPixel(canvas,X33,Y33)
      setColor(pixel3,color3)
      Y33=Y33+1
    X33=X33+1 
  show(canvas)
  return canvas
    
lub1()
lub2()
lub3()
lub4()
lub5()
lub6()
