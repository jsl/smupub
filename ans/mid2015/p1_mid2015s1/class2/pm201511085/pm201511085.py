import sys 
sys.path.append('/home/jsl/Code/git/bb/p1/src')
from goJES import *

def printGreen():
  pict=makePicture(getMediaPath("barbara.jpg"))
  for x in range(0, getWidth(pict)):
    if (x==getWidth(pict)-1):  
      for y in range(0, getHeight(pict)):
        if (y==getHeight(pict)-1):
          pix=getPixel(pict,x,y)
          greennum=getGreen(pix)
          print(greennum)
      
def recollectBarb():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("400x300.jpg"))
  point1=(getWidth(barb)/2)-1
  point2=(getHeight(barb)/3)-1
  point3=(getHeight(barb)/3)*2-1
  for x in range(0, point1):
    for y in range(0, point2):
      pix=getPixel(barb,x,y)
      pix2=getPixel(canvas,x+point1,y+point2)
      color=getColor(pix)
      setColor(pix2, color)
  for x in range(point1, getWidth(barb)):
    for y in range(0, point2):
      pix=getPixel(barb,x,y)
      pix2=getPixel(canvas,x-point1,y+point2)
      color=getColor(pix)
      setColor(pix2, color)
  for x in range(0, point1):
    for y in range(point2, point3):
      pix=getPixel(barb,x,y)
      pix2=getPixel(canvas,x,y+point2)
      color=getColor(pix)
      setColor(pix2, color)
  for x in range(point1, getWidth(barb)):
    for y in range(point2, point3):
      pix=getPixel(barb,x,y)
      pix2=getPixel(canvas,x-point1,y-point2)
      color=getColor(pix)
      setColor(pix2, color)
  for x in range(0, point1):
    for y in range(point3, getHeight(barb)):
      pix=getPixel(barb,x,y)
      pix2=getPixel(canvas,x+point1,y)
      color=getColor(pix)
      setColor(pix2, color)
  for x in range(point1, getWidth(barb)):
    for y in range(point3, getHeight(barb)):
      pix=getPixel(barb,x,y)
      pix2=getPixel(canvas,x,y-point3)
      color=getColor(pix)
      setColor(pix2, color)
  repaint(canvas)
      
def makeLine():
  pict=makePicture(getMediaPath("barbara.jpg"))
  count=1
  count2=1
  for x in range(0, getWidth(pict)):
    if x&14==0:
      if count==1:
        addLine(pict, x, 0, x, getHeight(pict), yellow)
        count=0
      else:
        addLine(pict, x, 0, x, getHeight(pict), red)
        count=1
  for y in range(0, getHeight(pict)):
    if y&14==0:
      if count2==1:
        addLine(pict, 0, y, getWidth(pict), y, yellow)
        count2=0
      else:
        addLine(pict, 0, y, getWidth(pict), y, red)
        count2=1
    
  repaint(pict)
  

def fixColor():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for x in range(83, 111):
    for y in range(105, 108):
      pix=getPixel(barb,x,y)
      color2=makeColor(getRed(pix)*4, getGreen(pix), getBlue(pix))
      setColor(pix, color2)
  for x in range(138, 163):
    for y in range(96, 100):
      pix=getPixel(barb,x,y)
      color2=makeColor(getRed(pix)*4, getGreen(pix), getBlue(pix))
      setColor(pix, color2)
  for x in range(60, 159):
    for y in range(52, 93):
      pix=getPixel(barb,x,y)
      color2=makeColor(getRed(pix)*2, getGreen(pix), getBlue(pix))
      setColor(pix, color2)
  for x in range(115, 146):
    for y in range(160, 167):
      pix=getPixel(barb,x,y)
      avg=(getGreen(pix)+getRed(pix)+getBlue(pix))/14
      color=makeColor(avg*5, avg, avg*8)
      setColor(pix, color)
  repaint(barb)
     
def blend():
  barb=makePicture(getMediaPath("barbara.jpg"))
  canvas=makePicture(getMediaPath("400x300.jpg"))
  katie=makePicture(getMediaPath("Katie-smaller.jpg"))
  
  for x in range(getWidth(barb)*2/3, getWidth(barb)):
    for y in range(0, getHeight(barb)):
      pix=getPixel(barb, x,y)
      pix2=getPixel(katie, x-getWidth(barb)*2/3,y) 
      pix3=getPixel(canvas,x,y)
  
#printGreen()     
#recollectBarb()
#makeLine()
blend()
