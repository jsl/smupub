import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  
def lab1():
  img = makePicture(getMediaPath("barbara.jpg"))
  a =getGreen(getPixel(img, 0, getHeight(img)-1))
  print(a)
  
def lab2():
  img = makePicture(getMediaPath("barbara.jpg")) 
  canvas = makePicture(getMediaPath("640x480.jpg")) 
  w = getWidth(img)/3 -1
  h = getHeight(img)/2 -1
  
  #4
  canvas =puzzle(img, canvas, 0, 0, 0, h)
  #1
  canvas = puzzle(img,canvas,  0, h, 0, 0)
  #6
  canvas = puzzle(img,canvas,  w, 0, w*2, h)
  #3
  canvas = puzzle(img,canvas,  w, h, w*2, 0)
  #2
  canvas = puzzle(img,canvas,  w*2, 0, w, 0)
  #5
  canvas = puzzle(img,canvas,  w*2, h, w, h)
  
  show(canvas)

def puzzle(img,canvas,  x1, y1, x2, y2):
  w = getWidth(img)/3
  h = getHeight(img)/2
  
  for x in range (0, w):
    for y in range (0,  h):
      px1 = getPixel(canvas, x1+ x, y1+y)
      px2 = getPixel(img, x2+x, y2 +y)
      setColor(px1, getColor(px2))
  
  return canvas
  
def lab3():
  img = makePicture(getMediaPath("barbara.jpg"))
  w = getWidth(img)
  h = getHeight(img)
  yNum = 0
  xNum = 0
  
  for x in range(0, w, w/10):
    xNum = xNum +1
    for y in range(0, h):
      if (xNum%2 ==0)  :
        setColor( getPixel(img, x, y), yellow)
      else : 
        setColor( getPixel(img, x, y), red)
        
  
  for y in range(0, h, h/10):
    yNum = yNum +1
    for x in range(0, w):
      
      if  (yNum%2 ==0) :
        setColor( getPixel(img, x, y),  yellow)
      else : 
        setColor( getPixel(img, x, y), red)
  
  show(img)
  
def lab4():
  img = makePicture(getMediaPath("barbara.jpg"))
  #luminance /4 
  
  for x in range(0,getWidth(img) ):
    for y in range(0,getHeight(img) ):
      px =  getPixel(img, x, y)
      c = (getRed(px) + getGreen(px) + getBlue(px)) /3
      if ( c > (255*3/4) ):
        setColor(px, white)
      if ( c <= (255*3/4) )&( c > (255/2) ):
        setColor(px, Color(255*2/3 ,255*2/3 , 255*2/3 ) )
      if ( c <= (255/2) ) &( c > (255/4) ):
        setColor(px,  Color(255/3 ,255/3 , 255/3 ))
      if (c <= (255/4)):
        setColor(px, black)
        
  show(img)
  
  
def lab5():
  img = makePicture(getMediaPath("barbara.jpg")) 
  purple = Color(202, 0, 238)
  tooth = Color(214, 193, 177)
  hair = Color(70, 43, 16)
  eyeBrow= Color(121, 82, 67)
  
  for x in range(111,157 ):
    for y in range(157,170 ):
      px =  getPixel(img, x, y)
      
      if distance( getColor(px), tooth)  < 70:
        setColor(px, purple)
       
  for x in range(35, 208):
    for y in range(24, 293):
      px =  getPixel(img, x, y)
      
      if ( (y < 105) |  (y > 120)) :   
        if distance( getColor(px), hair)  < 50:
          setColor(px, orange)
        
  for x in range(78, 168):
    for y in range(101, 109):
      px =  getPixel(img, x, y)
      
      if distance( getColor(px), eyeBrow)  < 50:
        setColor(px, red)      
  
  show(img)
  
  
def lab6():
  img1 = makePicture(getMediaPath("barbara.jpg")) 
  img2 = makePicture(getMediaPath("Katie-smaller.jpg")) 
  canvas = makePicture(getMediaPath("640x480.jpg")) 
  
  xImg1 =  getWidth(img1)
  yImg1 =  getHeight(img1)
  xImg2 =  getWidth(img2)
  yImg2 =  getHeight(img2)
  startBlend = xImg1 *2/3
  
  for x in range(0, startBlend):
    for y in range(0, yImg1):
      c1 = getColor(getPixel(img1 , x, y))
      setColor(getPixel(canvas, x, y) , c1)
      
  for x in range(startBlend, xImg1):
    for y in range(0, yImg2):
      px1 = getPixel(img1 , x, y)
      px2 = getPixel(img2 , x - startBlend, y )
      newRed = (getRed(px1)*0.75) + (getRed(px2)*0.25)
      newGreen = (getGreen(px1)*0.75) + (getGreen(px2)*0.25)
      newBlue = (getBlue(px1)*0.75) + (getBlue(px2)*0.25)
      newColor = Color(newRed, newGreen, newBlue)
      
      setColor(getPixel(canvas, x, y) , newColor)
  
  for x in range(xImg1 /3, xImg2):
    for y in range(0, yImg2):
      px2 = getPixel(img2 , x , y )
      
      setColor(getPixel(canvas, x+ startBlend, y) ,  getColor(px2))
  
  show(canvas)


lab2()
