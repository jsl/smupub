
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
 lab1()
 lab2()
 lab3()
 lab4()
 lab5()
 lab6()

def lab6():
 pict = makePicture(getMediaPath("barbara.jpg"))
 pict2 = makePicture(getMediaPath("Katie-smaller.jpg"))
 canvas = makePicture(getMediaPath("640x480.jpg"))
 
 temp = (getWidth(pict) / 3) * 2
 sourceX = 0
 for x in range(0, temp):
  sourceY = 0
  for y in range(0, getHeight(pict)):
   setColor(getPixel(canvas, sourceX, sourceY), getColor(getPixel(pict, x, y)))
   sourceY += 1
  sourceX += 1
  
 sourceX = temp
 rang = getWidth(pict) - temp
 for x in range(0, rang):
  sourceY = 0
  for y in range(0, getHeight(pict2)):
   newRed = getRed(getPixel(pict, x + temp, y)) * 0.5 + getRed(getPixel(pict2, x, y)) * 0.5
   newGreen = getGreen(getPixel(pict, x + temp, y)) * 0.5 + getGreen(getPixel(pict2, x, y)) * 0.5
   newBlue = getBlue(getPixel(pict, x + temp, y)) * 0.5 + getBlue(getPixel(pict2, x, y)) * 0.5
   setColor(getPixel(canvas, sourceX, sourceY), makeColor(newRed, newGreen, newBlue))
   sourceY += 1
  sourceX += 1
  
 sourceX = getWidth(pict)
 for x in range(getWidth(pict) - temp, getWidth(pict2)):
  sourceY = 0
  for y in range(0, getHeight(pict2)):
   setColor(getPixel(canvas, sourceX, sourceY), getColor(getPixel(pict2, x, y)))
   sourceY += 1
  sourceX += 1
  
 show(canvas)
 
def lab2():
 pict = makePicture(getMediaPath("barbara.jpg"))
 width = getWidth(pict)
 height = getHeight(pict)
 canvas = makeEmptyPicture(width, height)
 
 Ax = width / 3
 Ay = height / 2
 
 lab2_function(pict, canvas, Ax, Ay, 0, Ay, 0, 0)
 lab2_function(pict, canvas, Ax, Ay, Ax * 2, Ay, Ax, 0)
 lab2_function(pict, canvas, Ax, Ay, Ax, 0, Ax * 2, 0)
 lab2_function(pict, canvas, Ax, Ay, 0, 0, 0, Ay)
 lab2_function(pict, canvas, Ax, Ay, Ax * 2, 0, Ax, Ay)
 lab2_function(pict, canvas, Ax, Ay, Ax, Ay, Ax * 2, Ay)
      
 show(canvas)

def lab2_function(pict, canvas, Ax, Ay, x_range, y_range, x2_range, y2_range):
 for x in range(0, Ax):
  for y in range(0, Ay):
   sp = getPixel(pict, x + x_range, y + y_range)
   tp = getPixel(canvas, x + x2_range, y + y2_range)
   setColor(tp, getColor(sp))
 
def lab1():
 pict = makePicture(getMediaPath("barbara.jpg"))
 height = getHeight(pict)
 pixel = getPixel(pict, 0, height - 1)
 print getGreen(pixel)
 
def lab3():
 pict = makePicture(getMediaPath("barbara.jpg"))
 width = getWidth(pict)
 height = getHeight(pict)
 t_width = getWidth(pict) / 10
 t_height = getHeight(pict) / 10

 for x in range(t_width, width, t_width * 2):
  for y in range(1, height):
   setColor(getPixel(pict, x, y), yellow)
 for x in range(1, width):
  for y in range(t_height, height, t_height * 2):
   setColor(getPixel(pict, x, y), yellow)
 for x in range(0, width, t_width * 2):
  for y in range(0, height):
   setColor(getPixel(pict, x, y), red)
 for x in range(0, width):
  for y in range(0, height, t_height * 2):
   setColor(getPixel(pict, x, y), red)
 show(pict)
 
def lab4():
 pict = makePicture(getMediaPath("barbara.jpg"))
 lightgray = makeDarker(white)
 darkgray = makeDarker(lightgray)
 for p in getPixels(pict):
  luminance = (getRed(p) + getGreen(p) + getBlue(p)) / 3
  if(luminance < 64):
   setColor(p, white)
  if(luminance > 63 and luminance < 128):
   setColor(p, lightgray)
  if(luminance > 127 and luminance < 230):
   setColor(p, darkgray)
  if(luminance > 229):
   setColor(p, black)
 show(pict)
 
def lab5():
 pict = makePicture(getMediaPath("barbara.jpg"))
 purple = makeColor(153, 30, 153)
 brown = makeColor(100, 110, 105)
 color = makeColor(56, 22, 12)
 for x in range(109, 150):
  for y in range(155, 172):
   if(distance(getColor(getPixel(pict, x, y)), white) < 130):
    setColor(getPixel(pict, x, y), purple)
    
 for x in range(82, 113):
  for y in range(103, 108):
   if(distance(getColor(getPixel(pict, x, y)), brown) < 50):
    setColor(getPixel(pict, x, y), red)
 for x in range(136, 165):
  for y in range(97, 105):
   if(distance(getColor(getPixel(pict, x, y)), brown) < 60):
    setColor(getPixel(pict, x, y), red)
    
 for x in range(45, 201):
  for y in range(17, 293):
   if(distance(getColor(getPixel(pict, x, y)), color) < 25):
    setColor(getPixel(pict, x, y), orange)
 show(pict)

lab1()
lab2()
lab3()
lab4()
lab5()
lab6()
