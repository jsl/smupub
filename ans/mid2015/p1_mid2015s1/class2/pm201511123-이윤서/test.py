
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
# source lab2, lab5 modified jsl 20150625
def lab1(picture):
  for x in getPixels(picture):
    r=getRed(x)
    g=getGreen(x)
    b=getBlue(x)
    a=(getRed(x)+getBlue(x))/2
    setColor(x,makeColor(a,r,b))
  repaint(picture)
def lab2(picture):
  w=getWidth(picture)/2
  h=getHeight(picture)/3
  w2=w+w
  h2=h+h
  h3=h+h+h
  for x in range(w,0):
      for y in range(h,0):
          wPixel=getPixel(picture,w,y)
def lab5():
  pict=makePicture(getMediaPath("barbara.jpg"))
  teethPurple(pict)
  eyeRed(pict)
  hairOrange(pict)
  show(pict)
def teethPurple(picture):
  a=makeColor(104,34,147)
  for x in range(105,150):
    for y in range(157,170):
      p=getPixel(picture,x,y)
      if distance(white,getColor(p))<100:
        setColor(p,a)
  return picture
def eyeRed(picture):
  for x in range(82,166):
    for y in range(98,105):
      p=getPixel(picture,x,y)
      if distance(black,getColor(p))<90:
        setColor(p,red)
  return picture
def hairOrange(picture):
  b=makeColor(255,200,0)
  for x in range(50,207):
    for y in range(18,98):
      p=getPixel(picture,x,y)
      if distance(black,getColor(p))<200:
        setColor(p,b)
  return picture
def lab6():
  b=makePicture(getMediaPath("barbara.jpg"))
  k=makePicture(getMediaPath("Katie-smaller.jpg"))
  canvas=makePicture(getMediaPath("640x480.jpg"))
  sourceX=0
  for targetX in range(0,150):
    sourceY=0
    for targetY in range(0,getHeight(b)):
        color=getColor(getPixel(b,sourceX,sourceY))
        setColor(getPixel(canvas,targetX,targetY),color)
        sourceY=sourceY+1
        sourceX=sourceX+1
    overlap=getWidth(b)-150
    sourceX=0
def main():
  #lab1()
  #lab2()
  lab5()
  lab6()

lab5()
#lab6()
