import sys 
sys.path.append('/home/jsl/Code/git/bb/p1/src')
from goJES import *

def lab1():
  barb=makePicture(getMediaPath("barbara.jpg"))
  pixel=getPixel(barb,221,293)
  b=getBlue(pixel)
  print b

def lab4():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for sourceX in range(0,55.5):
    for sourceY in range(0,getHeight(barb)):
      pixel=getPixel(barb,sourceX,sourceY)
      white=makeColor(255,255,255)
      setColor(pixel,white)
  for sourceX in range(55.5,111):
    for sourceY in range(0,getHeight(barb)):
      pixel=getPixel(barb,sourceX,sourceY)
      lightgray=makeColor(192,192,192)
      setColor(pixel,lightgray)
  for sourceX in range(111,166.5):
    for sourceY in range(0,getHeight(barb)):
      pixel=getPixel(barb,sourceX,sourceY)
      darkgray=makeColor(64,64,64)
      setColor(pixel,darkgray)
  for sourceX in range(166.5,222):
    for sourceY in range(0,getHeight(barb)):
      pixel=getPixel(barb,sourceX,sourceY)
      darkgray=makeColor(0,0,0)
      setColor(pixel,darkgray)
  show(barb)
  return(barb)
  
def lab5():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for p in getPixels(barb):
    pixel=getPixel(barb,134,162)
    color=getColor(pixel)
    purple=makeColor(160,32,240)
    if distance(color,purple)>50:
      setColor(pixel,purple)
  show(barb)
  return(barb)

def lab6():
  barb=makePicture(getMediaPath("barbara.jpg"))
  katie=makePicture(getMediaPath("Katie-smaller.jpg"))
  canvas=makePicture(getMediaPath("640x480.jpg"))
  targetX=0
  for sourceX in range(0,148):
    targetY=0
    for sourceY in range(0,getHeight(barb)):
      pixel=getPixel(barb,sourceX,sourceY)
      color=getColor(pixel)
      setColor(getPixel(canvas,targetX,targetY),color)
  show(canvas)
  return(canvas)
  
def main():
  lab1()
  lab4()
  lab5()
  lab6()

lab1()
lab4()
