import sys 
sys.path.append('/home/jsl/Code/git/bb/p1/src')
from goJES import *

def lab1():
  s = getMediaPath('barbara.jpg')
  pic = makePicture(s)
  w = getWidth(pic)
  h = getHeight(pic)  
  pix = getPixel(pic,0,h-1)
  g = getGreen(pix)
  print(g)
  
def lab2():
  s = getMediaPath('barbara.jpg')
  pic = makePicture(s)
  
  pixels = getPixels(pic)
  
  
  
def lab3():
  s = getMediaPath('barbara.jpg')
  pic = makePicture(s)
  
  c = 0
  for x in range(0,getWidth(pic),getWidth(pic)/10):
    if c % 2 == 0:
     addLine(pic,x,0,x,getHeight(pic)-1,red)
    if c % 2 == 1:
      addLine(pic,x,0,x,getHeight(pic)-1,yellow)
    c = c+1
  
  c = 0
  for x in range(0,getHeight(pic),getHeight(pic)/10):
    if c % 2 == 0:
     addLine(pic,0,x,getWidth(pic)-1,x,red)
    if c % 2 == 1:
      addLine(pic,0,x,getWidth(pic)-1,x,yellow)
    c = c+1
  
  show(pic)


def lab4():
  s = getMediaPath('barbara.jpg')
  pic = makePicture(s)
  
  for x in range(0,getWidth(pic),1):
    for y in range(0,getHeight(pic),1):
      pixel = getPixel(pic,x,y)
      mycolor = (getRed(pixel) + getBlue(pixel) + getGreen(pixel))/3
      myG = makeColor(mycolor,mycolor,mycolor)
      setColor(pixel, myG)

  for x in range(0,getWidth(pic),1):
    for y in range(0,getHeight(pic),1):
      pixel = getPixel(pic,x,y)
      color = getRed(pixel)
    
      if color < 64:
        setColor(pixel,black)
    
      if color > 64:
        if color < 128:
          setColor(pixel,darkGray)
      
      if color > 128:
        if color < 192:
          setColor(pixel,lightGray)
    
      if color > 192:
        setColor(pixel,white)
  
  show(pic)
  

def lab5():
  s = getMediaPath('barbara.jpg')
  pic = makePicture(s)
  
  for x in range(0,getWidth(pic),1):
   for y in range(0,getHeight(pic),1):
     pixel = getPixel(pic,x,y)
     mycolor = (getRed(pixel) + getBlue(pixel) + getGreen(pixel))/3
   
     if mycolor < 64:
       setColor(pixel,orange)
    
     if mycolor > 64:
       if mycolor < 128:
         setColor(pixel,red)
    
     if mycolor > 192:
       color = makeColor(160,32,240)
       setColor(pixel,color)

  show(pic)
  
def lab6():
  a = getMediaPath('barbara.jpg')
  pic = makePicture(a)
  
  b = getMediaPath('Katie-smaller.jpg')
  pic = makePicture(b)
  
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()

lab6()
