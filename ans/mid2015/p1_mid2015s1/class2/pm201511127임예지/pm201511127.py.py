
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def lab1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  p=getPixel(barb,0,293)
  print getGreen(p)

def lab2(source,target):
  sourceX=0
  for targetX in range(0,73):
    sourceY=0
    for targetY in range(147,293):
      sp=getPixel(source,sourceX,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=74
  for targetX in range(149,221):
    sourceY=0
    for targetY in range(0,146):
      sp=getPixel(source,sourceX,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=149
  for targetX in range(74,148):
    sourceY=0
    for targetY in range(147,293):
      sp=getPixel(source,sourceX-1,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=0
  for targetX in range(0,73):
    sourceY=147
    for targetY in range(0,146):
      sp=getPixel(source,sourceX,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=74
  for targetX in range(149,221):
    sourceY=147
    for targetY in range(147,293):
      sp=getPixel(source,sourceX,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=149
  for targetX in range(74,148):
    sourceY=147
    for targetY in range(0,146):
      sp=getPixel(source,sourceX-1,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(target)
  return target

def lab5():
  barb=makePicture(getMediaPath('barbara.jpg'))  
  purpleTeeth(barb,105,155,148,174)
  redbrow(barb,80,104,164,103)
  orangehair(barb,43,29,203,246)
  show(barb)
  return barb
  
def purpleTeeth(pic,startX,startY,endX,endY):
    purple=makeColor(163,73,164)
    teeth=makeColor(255,237,229)
    for x in range(startX,endX):
      for y in range(startY,endY):
        p=getPixel(pic,x,y)
        pc=getColor(p)
        if (distance(pc,teeth))<100:
          setColor(p,purple)
    
def redbrow(pic,startX,startY,endX,endY):
    brow=makeColor(135,90,71)
    for x in range(startX,endX):
      for y in range(startY,endY):
        p=getPixel(pic,x,y)
        pc=getColor(p)
        if (distance(pc,brow))<100:
          setColor(p,red)

def orangehair(pic,startX,startY,endX,endY):
    hair=makeColor(105,67,48)
    for x in range(startX,endX):
      for y in range(startY,endY):
        p=getPixel(pic,x,y)
        pc=getColor(p)
        if (distance(pc,hair))<20:
          setColor(p,orange)

def lab6():
  barb=makePicture(getMediaPath('barbara.jpg'))
  kaite=makePicture(getMediaPath('katie-smaller.jpg'))
  canvas=makePicture(getMediaPath('640x480.jpg'))
  source
  show(canvas)
  return canvas
  
def blend1(source,target):
  sourceX=0
  for targetX in range(0,148):
    sourceY=0
    for targetY in range(0,getHeight(source)):
      sp=getPixel(source,sourceX,sourceY)
      tp=getPixel(target,targetX,targetY)
      color=getColor(sp)
      setColor(tp,color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(target)

def main():
  lab1()
  pic=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('barbara.jpg'))
  lab2(pic,can)
  lab5()

pic=makePicture(getMediaPath('barbara.jpg'))
can=makePicture(getMediaPath('barbara.jpg'))
lab2(pic,can)
lab5()
