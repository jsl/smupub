import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def lab1():
  file=makePicture(getMediaPath("barbara.jpg"))
  for x in range(219,getWidth(file)):
    for y in range(293,getHeight(file)):
      px=getPixel(file,x,y)
      p=getGreen(px)
  print p
  
def lab2():
  file=makePicture(getMediaPath("barbara.jpg"))
  width=getWidth(file)/3
  height=getHeight(file)/2
  for x in range(0,width):
    for y in range(0,height):
      px=getPixel(file,x,y+height)
      color=getColor(px)
      setColor(getPixel(file,x,y),color)
      
      px=getPixel(file,x,y)
      color=getColor(px)
      setColor(getPixel(file,x,y+height),color)
      
      px=getPixel(file,x+width,y)
      color=getColor(px)
      setColor(getPixel(file,x+width+width,y),color)
      
      px=getPixel(file,x+width+width,y+height)
      color=getColor(px)
      setColor(getPixel(file,x+width,y),color)
      
      px=getPixel(file,x+width+width,y)
      color=getColor(px)
      setColor(getPixel(file,x+width,y+height),color)
      
      
  show(file)
  
def lab3():
  file=makePicture(getMediaPath("barbara.jpg"))
  verticalLine(file)
  horizontalLine(file)
  show(file)
  
def verticalLine(src):
  
  for x in range(0,getWidth(src),22):
    for y in range(0,getHeight(src)):
      setColor(getPixel(src,x,y),yellow)
  for x in range(0,getWidth(src),44):
    for y in range(0,getHeight(src)):
      red=makeColor(255,0,0)
      setColor(getPixel(src,x,y),red)
      
def horizontalLine(src):
  for x in range(0,getWidth(src)):
    for y in range(0,getHeight(src),30):
      setColor(getPixel(src,x,y),yellow)
  for x in range(0,getWidth(src)):
    for y in range(0,getHeight(src),60):
      red=makeColor(255,0,0)
      setColor(getPixel(src,x,y),red)
      
def lab5():
  file=makePicture(getMediaPath("barbara.jpg"))
  white=makeColor(240,210,200)
 
  for x in range(110,146):
    for y in range(154,170):
      px=getPixel(file,x,y)
      color=getColor(px)
      if distance(color,white)<65:
        purple=makeColor(255,0,255)
        setColor(px,purple)
  brow=makeColor(145,98,80)
  for x in range(77,166):
    for y in range(102,105):
      px=getPixel(file,x,y)
      color=getColor(px)
      if distance(brow,color)<90:
        setColor(px,red)
  hair=makeColor(94,58,44)
  for x in range(50,210):
    for y in range(21,290):
      px=getPixel(file,x,y)
      color=getColor(px)
      if distance(hair,color)<65:
        
        setColor(px,orange)
  show(file)
    
def lab6():
  barb=makePicture(getMediaPath("barbara.jpg"))
  kat=makePicture(getMediaPath("Katie-smaller.jpg"))
  canvas=makePicture(getMediaPath("640x480.jpg"))
  
  sourceX=0
  for targetX in range(0,74):
    sourceY=0
    for targetY in range(0,getHeight(barb)):
      px=getPixel(barb,sourceX,sourceY)
      color=getColor(px)
      setColor(getPixel(canvas,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
      
  overlap=getWidth(barb)-74
  sourceX=0
  for targetX in range(74,getWidth(barb)):
    sourceY=0
    for targetY in range(0,getHeight(kat)):
      bpixel=getPixel(barb,sourceX+74,sourceY)
      kpixel=getPixel(kat,sourceX,sourceY)
      newR=getRed(bpixel)*0.75+getRed(kpixel)*0.25
      newG=getGreen(bpixel)*0.75+getGreen(kpixel)*0.25
      newB=getBlue(bpixel)*0.75+getBlue(kpixel)*0.25
      color1=makeColor(newR,newG,newB)
      setColor(getPixel(canvas,targetX,targetY),color1)
      sourceY=sourceY+1
    sourceX=sourceX+1
    
  sourceX=overlap
  for targetX in range(74+overlap,74+getWidth(kat)):
    sourceY=0
    for targetY in range(0,getHeight(kat)):
      px=getPixel(kat,sourceX,sourceY)
      color=getColor(px)
      setColor(getPixel(canvas,targetX,targetY),color)
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(canvas)
  
def main():
  lab1()
  lab2()
  lab3()
  lab5()
  lab6()

lab1()  
lab2()
lab3()
lab5()
lab6()
