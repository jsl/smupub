
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def horizony(picture):
  
  for y in range(0,getHeight(picture),20):
    for x in range(0,getWidth(picture)):
      setColor(getPixel(picture,x,y),yellow)
      
       
def verticaly(picture):
  
  for x in range(0,getWidth(picture),20):
    for y in range(0,getHeight(picture)):
      setColor(getPixel(picture,x,y),yellow)

def horizonR(picture):     
   for y in range(10,getHeight(picture),20):
    for x in range(0,getWidth(picture)):
      setColor(getPixel(picture,x,y),red)     
def verticalR(picture):
  for x in range(10,getWidth(picture),20):
    for y in range(0,getHeight(picture)):
      setColor(getPixel(picture,x,y),red)


def lab1():
  picture=makePicture(getMediaPath("barbara.jpg"))
  g=getGreen(getPixel(picture,0,getHeight(picture)-1))
  print g
def lab2():
  canvas=makePicture(getMediaPath("640x480.jpg"))
  picture=makePicture(getMediaPath("barbara.jpg"))
  three=getWidth(picture)/3
  two=getHeight(picture)/2
  for x in range(getWidth(picture)):
    for y in range(getHeight(picture)):
      setColor(getPixel(canvas,x,y),getColor(getPixel(picture,x,y)))
  three=getWidth(picture)/3
  two=getHeight(picture)/2
      
  for x in range(0,three):
   for y in range(0,two):
     setColor(getPixel(picture,x,y),getColor(getPixel(canvas,x,y))) 
  three=getWidth(picture)/3
  two=getHeight(picture)/2     
  for x in range(three,2*three):  
   for y in range(0,two):
     setColor(getPixel(picture,x,y),getColor(getPixel(canvas,x+three,y+two)))
  three=getWidth(picture)/3
  two=getHeight(picture)/2   
  for x in range(2*three,3*three):
   for y in range(0,two):
     setColor(getPixel(picture,x,y),getColor(getPixel(canvas,x-three,y)))
  three=getWidth(picture)/3
  two=getHeight(picture)/2   
  for x in range(0,three):
   for y in range(two,2*two):
     setColor(getPixel(picture,x,y),getColor(getPixel(canvas,x,y+two)))
  for x in range(three,2*three):
   for y in range(two,2*two):
     setColor(getPixel(picture,x,y),getColor(getPixel(canvas,x-three,y+two)))
  three=getWidth(picture)/3
  two=getHeight(picture)/2   
  for x in range(2*three,3*three):
   for y in range(two,2*two):  
     setColor(getPixel(picture,x,y),getColor(getPixel(canvas,x-three,y)))
  repaint(picture)     
  
def lab3():
  picture=makePicture(getMediaPath("barbara.jpg")) 
  horizony(picture)
  horizonR(picture)
  verticaly(picture)
  verticalR(picture)
  repaint(picture)
  
def lab4():

  picture=makePicture(getMediaPath("barbara.jpg")) 
  for x in range(0,getWidth(picture)):
    for y in range(0,getHeight(picture)):
      px=getPixel(picture,x,y)
      lumin=(getRed(px)+getGreen(px)+getBlue(px))/3
      if lumin<64:
        setColor(px,black)
      if lumin>=64 and lumin<128 :
        setColor(px,lightGray)
      if lumin>=128 and lumin<192 :
        setColor(px,darkGray)
      if lumin>=192 and lumin<=255 :
        setColor(px,white)
  show(picture)          
       
        
        
     
def lab5():
  purple=makeColor(163,73,164)
  barbara=makePicture(getMediaPath("barbara.jpg")) 
  for x in range(106,151):
    for y in range(150,170):
      teeth=makeColor(216,196,185)
      color=getColor(getPixel(barbara,x,y))
      if distance(color,teeth)<35:
        setColor(getPixel(barbara,x,y),purple)
  for x in range(81,163):
    for y in range(94,111):
      noon=makeColor(115,73,61)
      color=getColor(getPixel(barbara,x,y))
      if distance(color,noon)<35:
        setColor(getPixel(barbara,x,y),red)
  for x in range(59,163):
   for y in range(51,88) : 
     head=makeColor(62,30,15)
     color=getColor(getPixel(barbara,x,y))
     if distance(color,head)<35:
       setColor(getPixel(barbara,x,y),orange)
  repaint(barbara)    
  
def lab6():
  barbara=makePicture(getMediaPath("barbara.jpg"))
  katie=makePicture(getMediaPath("Katie-smaller.jpg")) 
  canvas=makePicture(getMediaPath("640x480.jpg"))
  blend=getWidth(barbara)*1/3
  katieX=0
  katieY=0
  for x in range(0,getWidth(barbara)-blend):
    for y in range(0,getHeight(barbara)):
      setColor(getPixel(canvas,x,y),getColor(getPixel(barbara,x,y)))
      
  for x in range(getWidth(barbara)-blend,getWidth(barbara)):
    katieY=0
    for y in range(0,getHeight(katie)):
      barPx=getPixel(barbara,x,y)
      katiePx=getPixel(katie,katieX,katieY)
      r=getRed(barPx)*0.75+getRed(katiePx)*0.25
      g=getGreen(barPx)*0.75+getGreen(katiePx)*0.25
      b=getBlue(barPx)*0.75+getBlue(katiePx)*0.25
      color=makeColor(r,g,b)
      setColor(getPixel(canvas,x,y),color)
      katieY=katieY+1
    katieX=katieX+1  
  for x in range(getWidth(barbara),getWidth(barbara)+getWidth(katie)-blend):
    katieY=0   
    for y in range(0,getHeight(katie)):
      setColor(getPixel(canvas,x,y),getColor(getPixel(katie,katieX,katieY)))
  show(canvas)
  
  
  
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
                          
lab1()
lab2()
lab3()
lab4()
lab5()
lab6()
