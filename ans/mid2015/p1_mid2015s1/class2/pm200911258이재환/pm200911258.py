
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
#1 
def printLastPixel():
  pic = makePicture(getMediaPath("barbara.jpg"))
  lpx = getPixel(pic, 0, getHeight(pic)-1)
  value = getGreen(lpx)
  print(value)
  
#2  
def rePrint():
  pic = makePicture(getMediaPath("barbara.jpg"))
  width = getWidth(pic)/2
  height = getHeight(pic)/3
  
  for x in range(width):
    for y in range(height):
      origPx = getPixel(pic, x, y) #1th
      replacePx = getPixel(pic, x+width, y+height) #4th
      origColor = getColor(origPx)
      replaceColor = getColor(replacePx)
      setColor(origPx, replaceColor)
      setColor(replacePx, origColor)
  for x in range(width):
    for y in range(height):   
      origPx = getPixel(pic, x+width, y) #2th
      replacePx = getPixel(pic, x+width, y+height+height) #6th
      origColor = getColor(origPx)
      replaceColor = getColor(replacePx)
      setColor(origPx, replaceColor)
      setColor(replacePx, origColor)     
  for x in range(width):
    for y in range(height):   
      origPx = getPixel(pic, x, y+height) #3th
      replacePx = getPixel(pic, x+width, y+height+height) #6th
      origColor = getColor(origPx)
      replaceColor = getColor(replacePx)
      setColor(origPx, replaceColor)
      setColor(replacePx, origColor) 
  for x in range(width):
    for y in range(height):   
      origPx = getPixel(pic, x+width, y+height+height) #7th
      replacePx = getPixel(pic, x, y+height+height) #5th
      origColor = getColor(origPx)
      replaceColor = getColor(replacePx)
      setColor(origPx, replaceColor)
      setColor(replacePx, origColor)       
  show(pic)

#3
def printHorizontalLine(pic):
  value = 0
  for x in range(0, getWidth(pic), getWidth(pic)/10):
    for y in range(0, getHeight(pic)):
      px = getPixel(pic, x, y)
      if value%2 == 0:
        setColor(px, red)
      else:
        setColor(px, yellow)
    value = value+1

def printVerticalLine(pic):
  for x in range(0, getWidth(pic)):    
    for y in range(0, getHeight(pic), getHeight(pic)/10):
      px = getPixel(pic, x, y)
      if y%2 == 0:
        setColor(px, red)
      else:
        setColor(px, yellow)
        
def printCrossLine():
  pic = makePicture(getMediaPath("barbara.jpg"))
  printVerticalLine(pic)
  printHorizontalLine(pic)      
  show(pic)

#4
def posterize():
  pic = makePicture(getMediaPath("barbara.jpg"))
  for x in range(getWidth(pic)):
    for y in range(getHeight(pic)):
      px = getPixel(pic, x, y)
      color = getColor(px)
      value = (getRed(px)+getGreen(px)+getBlue(px))/3
      if value < 64:
        setColor(px, makeColor(0, 0, 0))    
      elif value >= 64 and value < 128:
        setColor(px, lightGray)
      elif value >= 128 and value < 192:
        setColor(px, darkGray)
      else:
        setColor(px, makeColor(255, 255, 255))      
  show(pic)
  
#5
def replaceColor(pic, startX, startY, endX, endY, originColor, replaceColor):  
  targetX = startX
  targetY = startY
  for x in range(0, endX-startX):
    for y in range(0, endY-startY):
      px = getPixel(pic, targetX, targetY)
      color = getColor(px)
      if distance(originColor, color) < 128:
        setColor(px, replaceColor)
      targetY = targetY+1
    tartgetX = targetX+1
      
def replaceTooth():
  pic = makePicture(getMediaPath("barbara.jpg"))
  replaceColor(pic, 109, 155, 151, 170, white, blue)
  show(pic)

#6 
def blending():
  picA = makePicture(getMediaPath("barbara.jpg"))
  picB = makePicture(getMediaPath("Katie-smaller.jpg"))
  picC = makePicture(getMediaPath("640x480.jpg"))
  
  point = (getWidth(picA)/3)*2
  for x in range(point):
    for y in range(getHeight(picA)):
      px = getPixel(picA, x, y)
      newPx = getPixel(picC, x, y)
      color = getColor(px)   
      setColor(newPx, color)
  
  targetX = 0
  targetY = 0    
  for x in range(point, getWidth(picA)):
    for y in range(getHeight(picB)): 
      px1 = getPixel(picA, x, y)
      px2 = getPixel(picB, targetX, targetY)
       
      newPx = getPixel(picC, x, y)
      colorR = getRed(px1)*0.75 + getRed(px2)*0.25
      colorG = getGreen(px1)*0.75 + getGreen(px2)*0.25
      colorB = getBlue(px1)*0.75 + getBlue(px2)*0.25
      setColor(newPx, makeColor(colorR, colorG, colorB))
      targetY = targetY+1
    targetX = targetX+1
  show(picC)


#rePrint()   
blending()
