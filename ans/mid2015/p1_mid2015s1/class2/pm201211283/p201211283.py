
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def lab1(picture):
   y = getHeight(picture)
   pix = getPixel(picture,0,y-1)   
   print getGreen(pix)

def lab3(picture):
  width = getWidth(picture)
  height = getHeight(picture)
  for x1 in range(1,10):
   for y1 in range(1,height):
     x = x1*(width/10)
     pix = getPixel(picture,x,y1)
     a = x1%2
     if a < 1:
      setColor(pix,red)
     else:
      setColor(pix,yellow)
   for y2 in range(1,10):
    for x2 in range(1,width):
     y = y2*(height/10)
     pix = getPixel(picture,x2,y)
     a = y2%2
     if a < 1:
      setColor(pix,red)
     else:
      setColor(pix,yellow)
  repaint(picture)

pic=makePicture(getMediaPath('barbara.jpg'))
lab1(pic)
lab3(pic)
