import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

#lab6 modified jsl 20150625
def lab1():
  pic=makePicture(getMediaPath("barbara.jpg"))
  pixel=getPixel(pic,0,(getHeight(pic)-1))
  picLastGreen=getGreen(pixel)
  print picLastGreen
  
def lab2():
   pic=makePicture(getMediaPath("barbara.jpg"))
   bg=makePicture(getMediaPath("7inX95in.jpg"))
   #4
   targetX=0
   for sourceX in range(111,222):
     targetY=0
     for sourceY in range(98,196):
       barC=getColor(getPixel(pic,sourceX,sourceY))
       setColor(getPixel(bg,targetX,targetY),barC)
       targetY+=1
     targetX+=1
   #6
   targetX=111
   for sourceX in range(111,222):
     targetY=0
     for sourceY in range(196,294):
       barC=getColor(getPixel(pic,sourceX,sourceY))
       setColor(getPixel(bg,targetX,targetY),barC)
       targetY+=1
     targetX+=1 
   #2
   targetX=0
   for sourceX in range(111,222):
     targetY=98
     for sourceY in range(0,98):
       barC=getColor(getPixel(pic,sourceX,sourceY))
       setColor(getPixel(bg,targetX,targetY),barC)
       targetY+=1
     targetX+=1  
   #1
   targetX=111
   for sourceX in range(0,111):
     targetY=98
     for sourceY in range(0,98):
       barC=getColor(getPixel(pic,sourceX,sourceY))
       setColor(getPixel(bg,targetX,targetY),barC)
       targetY+=1
     targetX+=1
   #3
   targetX=0
   for sourceX in range(0,111):
     targetY=196
     for sourceY in range(98,196):
       barC=getColor(getPixel(pic,sourceX,sourceY))
       setColor(getPixel(bg,targetX,targetY),barC)
       targetY+=1
     targetX+=1
   #5
   targetX=111
   for sourceX in range(0,111):
     targetY=196
     for sourceY in range(196,294):
       barC=getColor(getPixel(pic,sourceX,sourceY))
       setColor(getPixel(bg,targetX,targetY),barC)
       targetY+=1
     targetX+=1
     
   show(bg)
   return bg
   
def lab3():
  pic=makePicture(getMediaPath("barbara.jpg"))
  horizontalLine1(pic)
  horizontalLine2(pic)
  verticalLine1(pic)
  verticalLine2(pic)
  show(pic)
  return pic
#red  
def horizontalLine1(src):
  for y in range(0,getHeight(src)):
    for x in range(10,getWidth(src),20):
      setColor(getPixel(src,x,y),yellow)
#yellow
def horizontalLine2(src):
  for y in range(0,getHeight(src)):
    for x in range(20,getWidth(src),20):
      setColor(getPixel(src,x,y),red)
#red
def verticalLine1(src):
  for x in range(0,getWidth(src)):
    for y in range(10,getHeight(src),20):
      setColor(getPixel(src,x,y),yellow)
#yellow
def verticalLine2(src):
  for x in range(0,getWidth(src)):
    for y in range(20,getHeight(src),20):
      setColor(getPixel(src,x,y),red)
    
#def lab4():
  
def lab5():
  pic=makePicture(getMediaPath("barbara.jpg"))
  #tooth make purple
  defaultToothC=makeColor(233,201,188)
  purple=makeColor(64,0,128)
  for x in range(100,150):
    for y in range(150,170):
      tooth=getColor(getPixel(pic,x,y))
      if distance(tooth,defaultToothC)<50:
        setColor(getPixel(pic,x,y),purple)
  #eyebrow turn red 
  defaultEyebrowC=makeColor(120,80,80)
  for x in range(85,165):
    for y in range(96,105):
      eyebrow=getColor(getPixel(pic,x,y))
      if distance(eyebrow,defaultEyebrowC)<50:
        setColor(getPixel(pic,x,y),red)
  #hair make orange
  defaultHairC=makeColor(80,55,40)
  for x in range(30,215):
    for y in range(20,290):
      hair=getColor(getPixel(pic,x,y))
      if distance(hair,defaultHairC)<40:
        setColor(getPixel(pic,x,y),orange)         
  show(pic)
  
def lab6():
  bar=makePicture(getMediaPath("barbara.jpg"))
  kat=makePicture(getMediaPath("Katie-smaller.jpg"))
  bg=makePicture(getMediaPath("640x480.jpg"))
  #part1
  targetX=0
  for sourceX in range(0,int(getWidth(bar)*0.75)):
    targetY=0
    for sourceY in range(0,getHeight(bar)):
      barC=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(bg,targetX,targetY),barC)
      targetY+=1
    targetX+=1
  #part2
  targetX=int(getWidth(bar)*0.75)
  for sourceX in range(int(getWidth(bar)*0.75),getWidth(bar)):
    targetY=0
    for sourceY in range(0,getHeight(kat)):
      barP=getPixel(bar,sourceX,sourceY)
      #katP=getPixel(kat,sourceX,sourceY)
      newRed=int(getRed(barP)*0.75)#int(getRed(katP)*0.25))
      newGreen=int(getGreen(barP)*0.75)#int(getGreen(katP)*0.25))
      newBlue=int(getBlue(barP)*0.75)#int(getBlue(katP)*0.25))
      setColor(getPixel(bg,targetX,targetY),makeColor(newRed,newGreen,newBlue))
      targetY+=1
    targetX+=1
    
  targetX=int(getWidth(bar)*0.75)
  for sourceX in range(0,int(getWidth(kat)*0.75)):
    targetY=0
    for sourceY in range(0,getHeight(kat)):
      #barP=getPixel(bar,sourceX,sourceY)
      katP=getPixel(kat,sourceX,sourceY)
      newRed=int(getRed(katP)*0.25)
      newGreen=int(getGreen(katP)*0.25)
      newBlue=int(getBlue(katP)*0.25)
      setColor(getPixel(bg,targetX,targetY),makeColor(newRed,newGreen,newBlue))
      targetY+=1
    targetX+=1
  #part3
  targetX=getWidth(bar)
  for sourceX in range(getWidth(kat)-int(getWidth(bar)*0.25),getWidth(kat)):
    targetY=0
    for sourceY in range(0,getHeight(kat)):
      katC=getColor(getPixel(kat,sourceX,sourceY))
      setColor(getPixel(bg,targetX,targetY),katC)
      targetY+=1
    targetX+=1 
  
  show(bg)
  return bg

def main():
  lab1()
  lab2()
  lab3()
  #lab4()
  lab5()
  lab6()

lab1()
lab2()
lab3()
#lab4()
lab5()
lab6()
