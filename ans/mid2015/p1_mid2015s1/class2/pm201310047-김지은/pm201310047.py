import sys 
sys.path.append('/home/jsl/Code/git/bb/p1/src')
from goJES import *

def lab1():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for y in range(0,getHeight(barb)):
    px=getPixel(barb,0,y)
    a=getGreen(px)
  print a
  
def lab5():
#Set Color first
  brown=makeColor(102,51,0)
  purple=makeColor(102,0,255)
  red=makeColor(255,0,0)
  orange=makeColor(255,102,0)
#Call the picture
  barb=makePicture(getMediaPath("barbara.jpg"))
#teeth change
  for x in range(101,154):
    for y in range(151,177):
      teeth=getPixel(barb,x,y)
      color=getColor(teeth)
      if distance(white,color)<120:
        setColor(teeth,purple)
#eyebrow change        
  for x in range(73,170):
    for y in range(98,107):
      eye=getPixel(barb,x,y)
      color=getColor(eye)
      if distance(brown,color)<120:
        setColor(eye,red)
#hair change        
  for x in range(15,212):
    for y in range(17,293):    
      hair=getPixel(barb,x,y)
      color=getColor(hair)
      if distance(brown,color)<120:
        setColor(hair,orange)
  show(barb)
  return barb
  
def main():
  lab1()
  lab5()

lab1()
