
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab5()
  lab6()

def lab1():
  barpic=makePicture(getMediaPath('barbara.jpg'))
  bpixel=getPixel(barpic,1,293)
  g=getGreen(bpixel)
  print g
  
  
def lab2():
  barpic=makePicture(getMediaPath('barbara.jpg'))
  canpic=makePicture(getMediaPath('7inX95in.jpg'))
  for x in range(0,74):
    for targetX in range(0,168):
      for y in range(0,147):
        for targetY in range(342,684):
          bpixel1=getPixel(barpic,x,y)
          bcolor1=getColor(bpixel1)
          cpixel1=getPixel(canpic,targetX,targetY)
          setColor(cpixel1,bcolor1)
    targetX=targetX+1
    targetY=targetY+1
  for x1 in range(0,74):
    for targetX1 in range(337,504):
      for y1 in range(148,294):
        for targetY1 in range(0,342):
          bpixel2=getPixel(barpic,x1,y1)
          bcolor2=getColor(bpixel2)
          cpixel2=getPixel(canpic,targetX1,targetY1)
          setColor(cpixel2,bcolor2)
  targetX1=targetX1+1
  targetY1=targetY1+1
  for x2 in range(75,148):
    for targetX2 in range(169,336):
      for y2 in range(0,147):
        for targetY2 in range(343,684):
          bpixel3=getPixel(barpic,x2,y2)
          bcolor3=getColor(bpixel3)
          cpixel3=getPixel(canpic,targetX2,targetY2)
          setColor(cpixel3,bcolor3)
    targetX2=targetX2+1
    targetY2=targetY2+1
  for x3 in range(75,148):
    for targetX3 in range(0,168):
      for y3 in range(148,294):
        for targetY3 in range(0,342):
          bpixel4=getPixel(barpic,x3,y3)
          bcolor4=getColor(bpixel4)
          cpixel4=getPixel(canpic,targetX3,targetY3)
          setColor(cpixel4,bcolor4)
    targetX3=targetX3+1
    targetY3=targetY3+1
  for x4 in range(149,222):
    for targetX4 in range(337,504):
      for y4 in range(0.147):
        for targetY4 in range(343,684):
          bpixel5=getPixel(barpic,x4,y4)
          bcolor5=getColor(bpixel5)
          cpixel5=getPixel(canpic,targetX4,targetY4)
          setColor(cpixel5,bcolor5)
    targetX4=targetX4+1
    targetY4=targetY4+1
  for x5 in range(149,222):
    for targetX5 in range(169,336):
      for y5 in range(148,294):
        for targetY5 in range(0,342):
          bpixel6=getPixel(barpic,x5,y5)
          bcolor6=getColor(bpixel6)
          cpixel6=getPixel(canpic,targetX5,targetY5)
          setColor(cpixel6,bcolor6)
    targetX5=targetX5+1
    targetY5=targetY5+1
  show(canpic)
  return canpic
  

def lab5():
  barpic=makePicture(getMediaPath('barbara.jpg'))
  purple=makeColor(89,94,149)
  for x in range(110,150):
    for y in range(156,173):
      bpixel=getPixel(barpic,x,y)
      bcolor=getColor(bpixel)
      if distance(bcolor,purple)>140.0:
        setColor(bpixel,purple)
  for x in range(79,165):
    for y in range(96,113):
      bpixel=getPixel(barpic,x,y)
      bcolor=getColor(bpixel)
      if distance(bcolor,red)<175.0:
        setColor(bpixel,red)
  for x in range(29,194):
    for y in range(16,285):
      bpixel=getPixel(barpic,x,y)
      bcolor=getColor(bpixel)
      if distance(bcolor,orange)>250.0:
        setColor(bpixel,orange)
  repaint(barpic)


def lab6():
  barpic=makePicture(getMediaPath('barbara.jpg'))
  kpic=makePicture(getMediaPath('Katie-smaller.jpg'))
  canpic=makePicture(getMediaPath('7inX95in.jpg'))
  for x in range(0,148):
    for targetX in range(0,getWidth(barpic)-148):
      for y in range(0,getHeight(barpic)):
        for targetY in range(0,getHeight(barpic)):
          bpixel=getPixel(barpic,x,y)
          cpixel=getPixel(canpic,targetX,targetY)
          bcolor=getColor(bpixel)
          setColor(cpixel,bcolor)
  for x in range(149,222):
    for targetX in range(getWidth(barpic)-148,getWidth(barpic)):
      for x1 in range(0,147):
        for y in range(0,getHeight(barpic)):
          for targetY in range(0,getHeight(barpic)):
            for y1 in range(0,getHeight(kpic)):
              bpixel=getPixel(barpic,x,y)
              cpixel=getPixel(canpic,targetX,targetY)
              kpixel=getPixel(kpic,x1,y1)
              newRed=getRed(bpixel)*0.75+getRed(kpixel)*0.25
              newGreen=getGreen(bpixel)*0.75+getGreen(kpixel)*0.25
              newBlue=getBlue(kpic)*0.75+getBlue(kpic)*0.25
              newColor=makeColor(newRed,newGreen,newBlue)
              setColor(cpixel,newColor)
  for x in range(getWidth(kpic)-74,getWidth(kpic)):
    for targetX in range(getWidth(barpic),getWidth(kpic)):
      for y in range(0,getHeight(kpic)):
        for targetY in range(0,getHeight(kpic)):
          kpixel=getPixel(kpic,x,y)
          kcolor=getColor(kpixel)
          cpixel=getPixel(canpic,targetX,targetY)
          setColor(cpixel,kcolor)
  show(canpic)
  return canpic
          

lab1()
#lab2()
lab5()
lab6()
      
