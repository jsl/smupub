def main():
  lab1()
  #lab2()
  lab3()
  lab4()
  lab5()
  lab6()


def lab1():
  pic=makePicture(getMediaPath('barbara.jpg'))
  lastPx=getPixel(pic,0,getHeight(pic)-1)
  lastGreen=getGreen(lastPx)
  print (lastGreen)

def lab2():
  pic=makePicture(getMediaPath('barbara.jpg'))
  w=getWidth(pic)
  h=getHeight(pic)
  for x in range(0,w/3):
    for y in range(0,h/2):
      part1=getPixel(pic,x,y)
      color1=getColor(part1)
      return(part1)
      return(color1)
  for x in range(w/3,w*2/3):
    for y in range(0,h/2):
      part2=getPixel(pic,x,y)
      color2=getColor(part2)
      return(part2)
      return(color2)
  for x in range(w*2/3,w):
    for y in range(0,h/2):
      part3=getPixel(pic,x,y)
      color3=getColor(part3)
      return(part3)
      return(color3)
  for x in range(0,w/3):
    for y in range(h/2,h):
      part4=getPixel(pic,x,y)
      color4=getColor(part4)
      return(part4)
      return(color4)
  for x in range(w/3,w*2/3):
    for y in range(h/2,h):
      part5=getPixel(pic,x,y)
      color5=getColor(part5)
      return(part5)
      return(color5)
  for x in range(w*2/3,w):
    for y in range(h/2,h):
      part6=getPixel(pic,x,y)
      color6=getColor(part6)
      return(part6)
      return(color6)
  setColor(part1,color4)
  setColor(part2,color6)
  setColor(part3,color2)
  setColor(part4,color1)
  setColor(part5,color3)
  setColor(part6,color5)
  repaint(pic)

def lab3():
  pic=makePicture(getMediaPath('barbara.jpg'))
  horizontalLine(pic)
  verticalLine(pic)
  repaint(pic)

def verticalLine(pic):
  for x in range(0,getWidth(pic),20):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      setColor(px,red)
  for x in range(10,getWidth(pic),20):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      setColor(px,yellow)

def horizontalLine(pic):
  for x in range(0,getWidth(pic)):
    for y in range(0,getHeight(pic),20):
      px=getPixel(pic,x,y)
      setColor(px,red)
  for x in range(0,getWidth(pic)):
    for y in range(10,getHeight(pic),20):
      px=getPixel(pic,x,y)
      setColor(px,yellow)

def lab4():
  pic=makePicture(getMediaPath('barbara.jpg'))
  lightgray=makeColor(133,123,121)
  darkgray=makeColor(85,79,83)
  for x in range(0,getWidth(pic)):
    for y in range(0,getHeight(pic)):
      px=getPixel(pic,x,y)
      lum=(getRed(px)+getGreen(px)+getBlue(px))/3
      if lum<50:
        setColor(px,white)
      if 50<=lum<80:
        setColor(px,lightgray)
      if 80<=lum<120:
        setColor(px,darkgray)
      if lum>=120:
        setColor(px,black)
  repaint(pic)

def lab5():
  pic=makePicture(getMediaPath('barbara.jpg'))
  turnTeeth(pic)
  turnBrow(pic)
  turnHair(pic)
  repaint(pic)

def turnTeeth(pic):
  purple=makeColor(134,108,179)
  teeth=makeColor(255,237,277)
  for x in range(102,155):
    for y in range(154,175):
      px=getPixel(pic,x,y)
      if distance(getColor(px),teeth)<150:
        setColor(px,purple)

def turnBrow(pic):
  brow1=makeColor(128,91,75)
  brow2=makeColor(66,28,19)
  for x in range(80,166):
    for y in range(96,107):
      px=getPixel(pic,x,y)
      if distance(getColor(px),brow1)<50:
        setColor(px,red)
      if distance(getColor(px),brow2)<50:
        setColor(px,red)

def turnHair(pic):
  hair1=makeColor(75,36,21)
  hair2=makeColor(107,74,55)
  for x in range(56,184):
    for y in range(28,106):
      px=getPixel(pic,x,y)
      if distance(getColor(px),hair1)<30:
        setColor(px,orange)
      if distance(getColor(px),hair2)<30:
        setColor(px,orange)
  for x in range(27,123):
    for y in range(152,292):
      px=getPixel(pic,x,y)
      if distance(getColor(px),hair1)<30:
        setColor(px,orange)
      if distance(getColor(px),hair2)<30:
        setColor(px,orange)
  for x in range(142,206):
    for y in range(152,292):
      px=getPixel(pic,x,y)
      if distance(getColor(px),hair1)<30:
        setColor(px,orange)
      if distance(getColor(px),hair2)<30:
        setColor(px,orange)

def lab6():
  pic1=makePicture(getMediaPath('barbara.jpg'))
  pic2=makePicture(getMediaPath('Katie-smaller.jpg'))
  canvas=makePicture(getMediaPath('640x480.jpg'))
  sourceX=0
  for targetX in range(0,getWidth(pic1)*2/3):
    sourceY=0
    for targetY in range(0,getHeight(pic1)):
      px1=getPixel(pic1,sourceX,sourceY)
      color1=getColor(px1)
      bg=getPixel(canvas,targetX,targetY)
      setColor(bg,color1)
      sourceY=sourceY+1
    sourceX=sourceX+1
  overlap=getWidth(pic1)*1/3
  sourceX=0
  for targetX in range(getWidth(pic1)*2/3,getWidth(pic1)):
    sourceY=0
    for targetY in range(0,getHeight(pic2)):
      px1=getPixel(pic1,sourceX+getWidth(pic1)*2/3,sourceY)
      px2=getPixel(pic2,sourceX,sourceY)
      newR=getRed(px1)*0.75+getRed(px2)*0.25
      newG=getGreen(px1)*0.75+getGreen(px2)*0.25
      newB=getBlue(px1)*0.75+getBlue(px2)*0.25
      blending=makeColor(newR,newG,newB)
      bg=getPixel(canvas,targetX,targetY)
      setColor(bg,blending)
      sourceY=sourceY+1
    sourceX=sourceX+1
  sourceX=overlap
  for targetX in range(getWidth(pic1),getWidth(pic1)*2/3+getWidth(pic2)):
    sourceY=0
    for targetY in range(0,getHeight(pic2)):
      px1=getPixel(pic2,sourceX,sourceY)
      bg=getPixel(canvas,targetX,targetY)
      setColor(bg,getColor(px1))
      sourceY=sourceY+1
    sourceX=sourceX+1
  repaint(canvas)