
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
 lab1()
 lab2()
 lab3()
 lab4()
 lab5()
 lab6()


def lab1():
 pic=makePicture(getMediaPath('barbara.jpg'))
 last=getPixel(pic,getWidth(pic)-1,getHeight(pic)-1)
 gree=getGreen(last)
 print gree
 return gree
 
def lab2():
 pic=makePicture(getMediaPath('barbara.jpg'))
 tar=makePicture(getMediaPath('barbara.jpg'))
 w1=getWidth(pic)/2
 w2=w1+w1
 h1=getHeight(pic)/3
 h2=h1+h1
 h3=h1+h1+h1
 sx=w1
 for tx in range(0,w1):
  sy=h1
  for ty in range(0,h1):
   px=getPixel(pic,sx,sy)
   can=getPixel(tar,tx,ty)
   setColor(can,getColor(px))
   sy+=1
  sx+=1
 sx=w1
 for tx in range(w1,w2):
  sy=h2
  for ty in range(0,h1):
   px=getPixel(pic,sx,sy)
   can=getPixel(tar,tx,ty)
   setColor(can,getColor(px))
   sy+=1
  sx+=1
 sx=w1
 for tx in range(0,w1):
  sy=0
  for ty in range(h1,h2):
   px=getPixel(pic,sx,sy)
   can=getPixel(tar,tx,ty)
   setColor(can,getColor(px))
   sy+=1
  sx+=1
  
 sx=0
 for tx in range(w1,w2):
  sy=0
  for ty in range(h1,h2):
   px=getPixel(pic,sx,sy)
   can=getPixel(tar,tx,ty)
   setColor(can,getColor(px))
   sy+=1
  sx+=1
 sx=0
 for tx in range(0,w1):
  sy=h1
  for ty in range(h2,h3):
   px=getPixel(pic,sx,sy)
   can=getPixel(tar,tx,ty)
   setColor(can,getColor(px))
   sy+=1
  sx+=1
  
 sx=0
 for tx in range(w1,w2):
  sy=h2
  for ty in range(h2,h3):
   px=getPixel(pic,sx,sy)
   can=getPixel(tar,tx,ty)
   setColor(can,getColor(px))
   sy+=1
  sx+=1
 
 
 show(tar)
 return tar

def lab3():
 pic1=makePicture(getMediaPath('barbara.jpg'))
 w=getWidth(pic1)/11
 h=getHeight(pic1)/11
 addLine(pic1,0,h,getWidth(pic1),h,yellow)
 addLine(pic1,0,h+h,getWidth(pic1),h+h,red)
 addLine(pic1,0,h+h+h,getWidth(pic1),h+h+h,yellow)
 addLine(pic1,0,h+h+h+h,getWidth(pic1),h+h+h+h,red)
 addLine(pic1,0,h+h+h+h+h,getWidth(pic1),h+h+h+h+h,yellow)
 addLine(pic1,0,h+h+h+h+h+h,getWidth(pic1),h+h+h+h+h+h,red)
 addLine(pic1,0,h+h+h+h+h+h+h,getWidth(pic1),h+h+h+h+h+h+h,yellow)
 addLine(pic1,0,h+h+h+h+h+h+h+h,getWidth(pic1),h+h+h+h+h+h+h+h,red)
 addLine(pic1,0,h+h+h+h+h+h+h+h+h,getWidth(pic1),h+h+h+h+h+h+h+h+h,red)
 addLine(pic1,0,h+h+h+h+h+h+h+h+h+h,getWidth(pic1),h+h+h+h+h+h+h+h+h+h,yellow)
 
 addLine(pic1,w,0,w,getHeight(pic1),yellow)
 addLine(pic1,w+w,0,w+w,getHeight(pic1),red)
 addLine(pic1,w+w+w,0,w+w+w,getHeight(pic1),yellow)
 addLine(pic1,w+w+w+w,0,w+w+w+w,getHeight(pic1),red)
 addLine(pic1,w+w+w+w+w,0,w+w+w+w+w,getHeight(pic1),yellow)
 addLine(pic1,w+w+w+w+w+w,0,w+w+w+w+w+w,getHeight(pic1),red)
 addLine(pic1,w+w+w+w+w+w+w,0,w+w+w+w+w+w+w,getHeight(pic1),yellow)
 addLine(pic1,w+w+w+w+w+w+w+w,0,w+w+w+w+w+w+w+w,getHeight(pic1),red)
 addLine(pic1,w+w+w+w+w+w+w+w+w,0,w+w+w+w+w+w+w+w+w,getHeight(pic1),yellow)
 addLine(pic1,w+w+w+w+w+w+w+w+w+w,0,w+w+w+w+w+w+w+w+w+w,getHeight(pic1),red)
 show(pic1)
 return pic1
def lab4():
 pic1=makePicture(getMediaPath('barbara.jpg'))
 for x in range(0,getWidth(pic1)):
  for y in range(0,getHeight(pic1)):
   px=getPixel(pic1,x,y)
   r=getRed(px)
   g=getGreen(px)
   b=getBlue(px)
   luminance=r+g+b
   color=getColor(px)
   if luminance > 149:
    setColor(px,white)
   if luminance < 150 and luminance > 300 :
    makeDarker(color)
   if luminance < 299 and luminance > 500 :
    makeLighter(color)
   if luminance < 499:
    setColor(px,black)
  
 show(pic1)
 return pic1
 
 
def lab5():
 pic1=makePicture(getMediaPath('barbara.jpg'))
 color=makeColor(255,255,255)
 purple=makeColor(163,73,164)
 
 for x in range(0,getWidth(pic1)):
  for y in range(0,getHeight(pic1)):
   px=getPixel(pic1,x,y)
   if distance(getColor(px),color) > 320:
    setColor(px,orange)
 for x in range(113,152):
  for y in range(158,172):
   px=getPixel(pic1,x,y)
   if distance(getColor(px),color) < 160:
    setColor(px,purple)
    
 for x in range(131,164):
  for y in range(95,104):
   px=getPixel(pic1,x,y)
   if distance(getColor(px),color) > 200:
    setColor(px,red)
 for x in range(83,114):
  for y in range(102,114):
   px=getPixel(pic1,x,y)
   if distance(getColor(px),color) > 200:
    setColor(px,red)

 
   
 show(pic1)
 return pic1
 
def lab6():
 pic1=makePicture(getMediaPath('barbara.jpg'))
 pic2=makePicture(getMediaPath('Katie-smaller.jpg'))
 can=makePicture(getMediaPath('640x480.jpg'))
 pw=getWidth(pic1)/3
 bp=pw+pw
 x=0
 for tx in range(0,bp):
  y=0
  for ty in range(0,getHeight(pic1)):
   px=getPixel(pic1,x,y)
   setColor(getPixel(can,tx,ty),getColor(px))
   y+=1
  x+=1
 
 over=getWidth(pic1)-bp
 
 sx=0
 for tx in range(bp,getWidth(pic1)):
  sy=0
  for ty in range(0,getHeight(pic2)):
   bax=getPixel(pic1,sx+bp,sy)
   kax=getPixel(pic2,sx,sy)
   r=(0.70*getRed(bax)+0.25*getRed(kax))
   g=(0.70*getGreen(bax)+0.25*getGreen(kax))
   b=(0.70*getBlue(bax)+0.25*getBlue(kax))
   color=makeColor(r,g,b)
   setColor(getPixel(can,tx,ty),color)
   sy+=1
  sx+=1
 
 x=0
 for tx in range(getWidth(pic1),bp+getWidth(pic2)):
  y=0
  for ty in range(0,getHeight(pic2)):
   px=getPixel(pic2,x,y)
   setColor(getPixel(can,tx,ty),getColor(px))
   y+=1
  x+=1
 show(can)
 return can


lab1()
lab2()
lab3()
lab4()
lab5()
lab6()
