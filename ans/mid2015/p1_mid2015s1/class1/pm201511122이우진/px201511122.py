
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab3()
  lab5()

def lab1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  px=getPixel(barb,0,getHeight(barb)-1)
  print px

def lab2():
  barb=makePicture(getMediaPath('barbara.jpg'))
  can=makePicture(getMediaPath('640x480.jpg'))
  for a in range(0,74):
    for b in range(0,146):
      px1=getPixel(barb,a,b)
      p1=getColor(px1)
  for c in range(0,148):
    for d in range(0,146):
      px2=getPixel(barb,c,d)
      p2=getColor(px2)
  for e in range(0,221):
    for f in range(0,146):
      px3=getPixel(barb,e,f)
      p3=getColor(px3)
  for g in range(0,148):
    for h in range(146,293):
      px4=getPixel(barb,g,h)
      p4=getColor(px4)
  for i in range(0,148):
    for j in range(146,293):
      px5=getPixel(barb,i,j)
      p5=getColor(px3)
  for k in range(0,148):
    for l in range(146,293):
      px6=getPixel(barb,k,l)
      p6=getColor(px6)
      

def lab3():
  barb=makePicture(getMediaPath('barbara.jpg'))
  line1(barb,22,yellow)
  line1(barb,44,red)
  line1(barb,66,yellow)
  line1(barb,88,red)
  line1(barb,110,yellow)
  line1(barb,132,red)
  line1(barb,154,yellow)
  line1(barb,176,red)
  line1(barb,198,yellow)
  line2(barb,28,yellow)
  line2(barb,56,red)
  line2(barb,84,yellow)
  line2(barb,112,red)
  line2(barb,140,yellow)
  line2(barb,168,red)
  line2(barb,196,yellow)
  line2(barb,224,red)
  line2(barb,252,yellow)
  line2(barb,280,red)
  repaint(barb)

def line1(pic,s,color):
  addLine(pic,s,0,s,getHeight(pic),color)
  
def line2(pic,s,color):
  addLine(pic,0,s,getWidth(pic),s,color)

def lab5():
  makePurple()
  makeRed1()
  makeRed2()
  
  
def makePurple():
  barb=makePicture(getMediaPath('barbara.jpg'))
  purple=makeColor(160,74,163)
  color=makeColor(255,237,299)
  for x in range(108,150):
    for y in range(157,168):
      p=getPixel(barb,x,y)
      px=getColor(p)
      if distance(purple,px) >100 :
        setColor(p,purple)
  repaint (barb)

def makeRed1():
  barb=makePicture(getMediaPath('barbara.jpg'))
  Red=makeColor(255,0,0)
  color=makeColor(255,237,299)
  for x in range(134,165):
    for y in range(96,106):
      p=getPixel(barb,x,y)
      px=getColor(p)
      if distance(red,px) <200 :
        setColor(p,red)
  repaint (barb)
  
def makeRed2():
  barb=makePicture(getMediaPath('barbara.jpg'))
  Red=makeColor(255,0,0)
  color=makeColor(255,237,299)
  for x in range(78,116):
    for y in range(103,112):
      p=getPixel(barb,x,y)
      px=getColor(p)
      if distance(red,px) <200 :
        setColor(p,red)
  repaint (barb)

def makeOrange():
  barb=makePicture(getMediaPath('barbara.jpg'))
  Red=makeColor(255,0,0)
  color=makeColor(255,237,299)
  for x in range(23,220):
    for y in range(10,290):
      p=getPixel(barb,x,y)
      px=getColor(p)
      if distance(orange,px) <100 :
        setColor(p,orange)
  repaint (barb)


lab1()
lab3()
lab5()
