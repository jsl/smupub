
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  lab5()
  lab6()
def lab1():
  barPic=makePicture(getMediaPath('barbara.jpg'))
  px=getPixel(barPic,0, getHeight(barPic)-1)
  g=getGreen(px)
  print g
def lab2():
  barPic=makePicture(getMediaPath('barbara.jpg'))
  w1=getWidth(barPic)/3
  w2=(getWidth(barPic)/3)*2
  w3=getWidth(barPic)
  h1=getHeight(barPic)/2
  h2=getHeight(barPic)
  #4
  for x in range(w1):
    for y in range(h1):
      px=getPixel(barPic, x, y)
      newpx=getPixel(barPic, x, y+h1)
      color=getColor(newpx)
      setColor(px, color)
  #1
  for x in range(w1):
    for y in range(h1, h2):
      px=getPixel(barPic, x, y)
      newpx=getPixel(barPic, x, y-h1)
      color=getColor(newpx)
      setColor(px, color)
  #3
  for x in range(w1, w2):
    for y in range(h1, h2):
      px=getPixel(barPic, x, y)
      newpx=getPixel(barPic, x+w1, y-h1)
      color=getColor(newpx)
      setColor(px, color)
  #5
  for x in range(w2, w3):
    for y in range(h1, h2):
      px=getPixel(barPic, x, y)
      newpx=getPixel(barPic, x-w1, y)
      color=getColor(newpx)
      setColor(px, color)
  #2
  for x in range(w2, w3):
    for y in range(h1):
      px=getPixel(barPic, x, y)
      newpx=getPixel(barPic, x-w1, y)
      color=getColor(newpx)
      setColor(px, color)
  #6
  for x in range(w1, w2):
    for y in range(h1):
      px=getPixel(barPic, x, y)
      newpx=getPixel(barPic, x+w1, y+h1)
      color=getColor(newpx)
      setColor(px, color)
  
  show(barPic)
def lab3():
  barPic=makePicture(getMediaPath('barbara.jpg'))
  w1=getWidth(barPic)/10
  w2=getWidth(barPic)*2/10
  w3=getWidth(barPic)*3/10
  w4=getWidth(barPic)*4/10
  w5=getWidth(barPic)*5/10
  w6=getWidth(barPic)*6/10
  w7=getWidth(barPic)*7/10
  w8=getWidth(barPic)*8/10
  w9=getWidth(barPic)*9/10
  w10=getWidth(barPic)
  h1=getHeight(barPic)/10
  h2=getHeight(barPic)*2/10
  h3=getHeight(barPic)*3/10
  h4=getHeight(barPic)*4/10
  h5=getHeight(barPic)*5/10
  h6=getHeight(barPic)*6/10
  h7=getHeight(barPic)*7/10
  h8=getHeight(barPic)*8/10
  h9=getHeight(barPic)*9/10
  h10=getHeight(barPic)
  addLine(barPic, w1,0, w1,h10,yellow)
  addLine(barPic, w2, 0,w2, h10, red)
  addLine(barPic, w3,0, w3,h10,yellow)
  addLine(barPic, w4, 0,w4, h10, red)
  addLine(barPic, w5,0, w5,h10,yellow)
  addLine(barPic, w6, 0,w6, h10, red)
  addLine(barPic, w7,0, w7,h10,yellow)
  addLine(barPic, w8, 0,w8, h10, red) 
  addLine(barPic, w9,0, w9,h10,yellow)
  addLine(barPic, w10, 0,w10, h10, red)
  addLine(barPic, 0, h1, w10, h1, yellow)
  addLine(barPic, 0, h2, w10, h2, red)
  addLine(barPic, 0, h3, w10, h3, yellow)
  addLine(barPic, 0, h4, w10, h4, red)
  addLine(barPic, 0, h5, w10, h5, yellow)
  addLine(barPic, 0, h6, w10, h6, red)
  addLine(barPic, 0, h7, w10, h7, yellow)
  addLine(barPic, 0, h8, w10, h8, red)
  addLine(barPic, 0, h9, w10, h9, yellow)
  addLine(barPic, 0, h10, w10, h10, red)
  show(barPic)
def lab5():
  barPic=makePicture(getMediaPath('barbara.jpg'))
  for x in range(107, 148):
    for y in range(153, 171):
      px=getPixel(barPic, x,y)
      c=getColor(px)
      if distance(c,makeColor(226, 202, 192))<75:
        setColor(px, makeColor(163, 73, 164))
  for x in range(83, 164):
    for y in range(97, 114):
      px=getPixel(barPic, x,y)
      c=getColor(px)
      if distance(c,makeColor(136, 102, 93))<30:
        setColor(px, red)
  for x in range(37, 180):
   for y in range(15, 283):
     px=getPixel(barPic, x,y)
     c=getColor(px)
     if distance(c,makeColor(63, 30, 11))<30:
       setColor(px, orange)
  show(barPic)
def lab6():
  barPic=makePicture(getMediaPath('barbara.jpg'))
  katePic=makePicture(getMediaPath('Katie-smaller.jpg'))
  canPic=makePicture(getMediaPath('640x480.jpg'))
  targetX=0
  for sourceX in range((getWidth(barPic)*2)/3):
    targetY=0
    for sourceY in range(getHeight(barPic)):
      px=getPixel(barPic, sourceX, sourceY)
      color=getColor(px)
      canpx=getPixel(canPic, targetX, targetY)
      setColor(canpx, color)
      targetY+=1
    targetX+=1
  inter=(getWidth(barPic)*2)/3
  targetX=0
  for sourceX in range(inter, getWidth(barPic)):
    targetY=0
    for sourceY in range(getHeight(katePic)):
      bp=getPixel(barPic, sourceX, sourceY)
      kp=getPixel(katePic, targetX, targetY)
      canpx=getPixel(canPic, inter+targetX, targetY)
      newRed=getRed(bp)*0.75+getRed(kp)*0.25
      newGreen=getGreen(bp)*0.75+getGreen(kp)*0.25
      newBlue=getBlue(bp)*0.75+getBlue(kp)*0.25
      color=makeColor(newRed, newGreen, newBlue)
      setColor(canpx, color)
      targetY+=1
    targetX+=1
  targetX=0
  for sourceX in range(getWidth(barPic)-inter,getWidth(katePic)):
    targetY=0
    for sourceY in range(getHeight(katePic)):
      px=getPixel(katePic, sourceX, sourceY)
      color=getColor(px)
      canpx=getPixel(canPic, getWidth(barPic)+targetX, targetY)
      setColor(canpx, color)
      targetY+=1
    targetX+=1
  show(canPic)
     
    
lab1()
lab2()
lab3()
lab5()
lab6()
