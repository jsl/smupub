
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def lab6():
  bar=makePicture(getMediaPath('barbara.jpg'))
  katie=makePicture(getMediaPath('Katie-smaller.jpg'))
  canvas=makePicture(getMediaPath('640x480.jpg'))
  sourceX=0
  for targetX in range(0,2*getWidth(bar)/3):
    sourceY=0
    for targetY in range(0,getHeight(bar)):
      color=getColor(getPixel(bar,sourceX-1,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      sourceX=sourceX+1
    sourceY=sourceY+1
  show(canvas)
  return canvas

def lab1():
  bar=makePicture(getMediaPath('barbara.jpg'))
  a=getPixel(bar,0,293)
  print a

def lab2():
  bar=makePicture(getMediaPath('barbara.jpg'))
  canvas=makePicture(getMediaPath('barbara.jpg'))
  targetX=0
  for sourceX in range(0,getWidth(bar)/3):
    targetY=0
    for sourceY in range(147,294):
      color=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  targetX=getWidth(bar)/3
  for sourceX in range(2*getWidth(bar)/3,getWidth(bar)):
    targetY=0
    for sourceY in range(147,294):
      color=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  targetX=2*getWidth(bar)/3
  for sourceX in range(getWidth(bar)/3,2*getWidth(bar)/3):
    targetY=0
    for sourceY in range(0,147):
      color=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  targetX=0    
  for sourceX in range(0,getWidth(bar)/3):
    targetY=147
    for sourceY in range(0,147):
      color=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  targetX=getWidth(bar)/3
  for sourceX in range(2*getWidth(bar)/3,getWidth(bar)):
    targetY=147
    for sourceY in range(0,147):
      color=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  targetX=2*getWidth(bar)/3
  for sourceX in range(getWidth(bar)/3,2*getWidth(bar)/3):
    targetY=147
    for sourceY in range(147,294):
      color=getColor(getPixel(bar,sourceX,sourceY))
      setColor(getPixel(canvas,targetX,targetY),color)
      targetY=targetY+1
    targetX=targetX+1
  show(canvas)
  return canvas

def lab3():
  bar=makePicture(getMediaPath('barbara.jpg'))
  addLine(bar,20,0,20,294,yellow)
  addLine(bar,40,0,40,294,red)
  addLine(bar,60,0,60,294,yellow)
  addLine(bar,80,0,80,294,red)
  addLine(bar,100,0,100,294,yellow)
  addLine(bar,120,0,120,294,red)
  addLine(bar,140,0,140,294,yellow)
  addLine(bar,160,0,160,294,red)
  addLine(bar,180,0,180,294,yellow)
  addLine(bar,200,0,200,294,red)
  addLine(bar,0,27,220,27,yellow)
  addLine(bar,0,54,220,54,red)
  addLine(bar,0,81,220,81,yellow)
  addLine(bar,0,108,220,108,red)
  addLine(bar,0,135,220,135,yellow)
  addLine(bar,0,162,220,162,red)
  addLine(bar,0,189,220,189,yellow)
  addLine(bar,0,216,220,216,red)
  addLine(bar,0,243,220,243,yellow)
  addLine(bar,0,270,220,270,red)
  show(bar)
  return bar
def main():
  lab1()
  lab2()
  lab3()
  
lab1()
lab2()
lab3()
