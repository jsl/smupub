
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  
def lab1():
  barb=makePicture(getMediaPath("barbara.jpg"))
  barbPixel=getPixel(barb,0,getHeight(barb)-1)
  barbGreen=getGreen(barbPixel)
  print barbGreen
  
  
def lab2():
  source=makePicture(getMediaPath("barbara.jpg"))
  target=makePicture(getMediaPath("640x480.jpg"))
  gW=(getWidth(source)/2)
  gH=(getHeight(source)/3)
  copy(source,target,gW,gH,2*gW,2*gH,0,0)
  copy(source,target,gW,2*gH,2*gW,3*gH,gW,0)
  copy(source,target,gW,0,2*gW,gH,0,gH)
  copy(source,target,0,0,gW,gH,gW,gH)
  copy(source,target,0,gH,gW,2*gH,0,2*gH)
  copy(source,target,0,2*gH,gW,3*gH,gW,2*gH) 
  show(target)
  
def copy(source,target,sX,sY,eX,eY,tX,tY):
  targetX=tX
  for x in range(sX,eX):
     targetY=tY
     for y in range(sY,eY):
      sP=getPixel(source,x,y)
      sC=getColor(sP)
      tP=getPixel(target,targetX,targetY)
      tC=getColor(tP)
      setColor(tP,sC)
      targetY=targetY+1
     targetX=targetX+1
   
###############l
def lab3():
  source=makePicture(getMediaPath("barbara.jpg"))
  yellowline(source)
  redline(source)
  yellowline2(source)
  redline2(source)  
  show(source)
  
def yellowline(source):
   
   for x in range(10,getWidth(source),20):
     for y in range(0,getHeight(source)):   
       sP=getPixel(source,x,y)
       sC=getColor(sP)
       setColor(sP,yellow)
   return
     
def redline(source):
   for x in range(0,getWidth(source),20):
     for y in range(0,getHeight(source)):   
       sP=getPixel(source,x,y)
       sC=getColor(sP)
       setColor(sP,red)
   return    
def yellowline2(source):
   for x in range(10,getHeight(source),20):
     for y in range(0,getWidth(source)):   
       sP=getPixel(source,y,x)
       sC=getColor(sP)
       setColor(sP,yellow) 
   return 
def redline2(source):
   for x in range(0,getHeight(source),20):
     for y in range(0,getWidth(source)):   
       sP=getPixel(source,y,x)
       sC=getColor(sP)
       setColor(sP,red) 
   return       
     
     
def lab4():
    source=makePicture(getMediaPath("barbara.jpg"))
    for p in getPixels(source):
      sC=getColor(p)
      r=getRed(p)
      g=getGreen(p)
      b=getBlue(p)
      L=(r+g+b)/3
      luminance=makeColor(L,L,L)
      setColor(p,luminance)
    show(source)  
#teeth(107,157)>(152,169)   ,color 222 198 186 
#eyeline(80,107)>(112,112)
#eyeline2(131,98)>(165,105)
#head(37,27),(186,101)

def lab5():
   source=makePicture(getMediaPath("barbara.jpg"))
   eyeline(source,80,107,112,112,red)
   eyeline(source,131,98,165,105,red)
   teeth(source,107,157,152,169,black)
   head(source,37,27,186,101,orange)
   head(source,30,121,102,289,orange)
   head(source,154,144,204,288,orange)
   
   show(source)

def eyeline(source,sX,sY,eX,eY,color):
   for x in range(sX,eX):
     for y in range(sY,eY):   
       sP=getPixel(source,x,y)
       sC=getColor(sP)
       CC=makeColor(140,98,84)
       if distance(sC,CC)<50:
         setColor(sP,color)
def teeth(source,sX,sY,eX,eY,color):
   for x in range(sX,eX):
     for y in range(sY,eY):   
       sP=getPixel(source,x,y)
       sC=getColor(sP)
       CC=makeColor(255,221,209)
       if distance(sC,CC)<150:
         setColor(sP,color)
def head(source,sX,sY,eX,eY,color):
   for x in range(sX,eX):
     for y in range(sY,eY):   
       sP=getPixel(source,x,y)
       sC=getColor(sP)
       CC=makeColor(62,28,16)
       if distance(sC,CC)<100:
         setColor(sP,color)
         
 

def b():
   barb=makePicture(getMediaPath("barbara.jpg"))
   kat=makePicture(getMediaPath("Katie-smaller.jpg"))
   canvas=makePicture(getMediaPath("7inX95in.jpg"))
   copy(barb,canvas,0,0,getWidth(barb),getHeight(barb),0,0)
   for x in range(1,getWidth(barb)/3):
     for y in range(1,getHeight(barb)):   
       sP=getPixel(barb,(2*getWidth(barb)/3)+x,y)
       sC=getColor(sP)
       tP=getPixel(kat,x,y)
       tC=getColor(tP)
       canvasP=getPixel(canvas,1+(2*getWidth(barb)/3)+x,1+y)       
       newRed=0.75*getRed(sP)+0.25*getRed(tC)
       newGreen=0.75*getGreen(sC)+0.25*getGreen(tC)
       newBlue=0.75*getBlue(sC)+0.25*getBlue(tC)
       cc=makeColor(newRed,newGreen,newBlue)
       setColor(canvasP,cc)
   show(canvas)
   return(canvas)
       
       
        
def blending(sX,sY,eX,eY):
   barb=makePicture(getMediaPath("barbara.jpg"))
   kat=makePicture(getMediaPath("Katie-smaller.jpg"))
   canvas=makePicture(getMediaPath("7inX95in.jpg"))
   for x in range(sX,eX):
     for y in range(sY,eY):   
       sP=getPixel(barb,2*getWidth(barb)/3+x,y)
       sC=getColor(sP)
       tP=getPixel(kat,x,y)
       tC=getColor(tP)
       canvasP=getPixel(canvas,x,y)
       newRed=0.75*getRed(sP)+0.25*getRed(tP)
       newGreen=0.75*getGreen(sP)+0.25*getGreen(tP)
       newBlue=0.75*getBlue(sP)+0.25*getBlue(tP)
       cc=makeColor(newRed,newGreen,newBlue)
       setColor(canvasP,cc)
   show(canvas)
   return(canvas)
       
       
   
     
       
lab1()
lab2()
lab3()
lab4()
lab5()
