
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  #lab4()
  lab5()
  lab6()

def lab1():
  b=makePicture(getMediaPath("barbara.jpg"))
  a=getPixel(b,0,getHeight(b)-1)
  print getGreen(a)
  
def lab2():
  b=makePicture(getMediaPath("barbara.jpg"))
  
  targetX=0
  for x in range(0,74):
    targetY=0
    for y in range(147,294):
      bp=getPixel(b,x,y)
      cp=getPixel(b,targetX,targetY)
      setColor(cp,getColor(bp))
      targetY=targetY+1
    targetX=targetX+1
  
  targetX=74
  for x in range(148,222):
    targetY=0
    for y in range(147,294):
      bp=getPixel(b,x,y)
      cp=getPixel(b,targetX,targetY)
      setColor(cp,getColor(bp))
      targetY=targetY+1
    targetX=targetX+1
  
  targetX=148
  for x in range(148,222):
    targetY=0
    for y in range(0,147):
      bp=getPixel(b,targetX,targetY)
      cp=getPixel(b,x,y)
      setColor(cp,getColor(bp))
      targetY=targetY+1
    targetX=targetX+1
    
  show(b)
  return(b)
  
def lab3():
  b=makePicture(getMediaPath("barbara.jpg"))
  w=22
  addLine(b,22,0,22,294,yellow)
  addLine(b,44,0,44,294,red)
  addLine(b,66,0,66,294,yellow)
  addLine(b,88,0,88,294,red)
  addLine(b,110,0,110,294,yellow)
  addLine(b,120,0,22,294,yellow)
  
def lab5():
  b=makePicture(getMediaPath("barbara.jpg"))
  teeth=makeColor(243,211,198)
  for x in range(102,152):
    for y in range(150,172):
      bp=getPixel(b,x,y)
      bc=getColor(getPixel(b,x,y))
    if distance(bc,teeth)>120:
      setColor(bp,makeColor(124,102,174))
  show(b)
    
  

def lab6():
  b=makePicture(getMediaPath("barbara.jpg"))
  k=makePicture(getMediaPath("Katie-smaller.jpg"))
  c=makePicture(getMediaPath("640x480.jpg"))
  
  sourceX=0
  for x in range(0,128):
    sourceY=0
    for y in range(0,getHeight(b)):
      bp=getPixel(b,sourceX,sourceY)
      cp=getPixel(c,x,y)
      setColor(cp,getColor(bp))
      sourceY=sourceY+1
    sourceX=sourceX+1
    
  overlap=getWidth(b)-128
  sourceX=0
  for x in range(128,getWidth(b)):
    sourceY=0
    for y in range(0,getHeight(k)):
      bp=(getPixel(b,sourceX+128,sourceY))
      cp=(getPixel(c,x,y))
      kp=(getPixel(k,sourceX,sourceY))
      nr=(getRed(bp)*0.75) + (getRed(kp)*0.25)
      ng=(getGreen(bp)*0.75)+(getGreen(kp)*0.25)
      nb=(getBlue(bp)*0.75)+(getBlue(kp)*0.25)
      ncolor=makeColor(nr,ng,nb)
      setColor(cp,ncolor)
    sourceY=sourceY+1
  sourceX=sourceX+1
  
  sourceX=overlap
  for x in range(getWidth(b),getWidth(k)):
    sourceY=0
    for y in range(0,getHeight(k)):
      kp=getPixel(k,sourceX,sourceY)
      cp=getPixel(c,x,y)
      setColor(cp,getColor(kp))
      sourceY=sourceY+1
    sourceX=sourceX+1
  show(c)
  return(c)
      
      
main()    
