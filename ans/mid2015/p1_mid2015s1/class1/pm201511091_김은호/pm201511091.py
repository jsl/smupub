
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def num1():
  barbpic = makePicture(getMediaPath('barbara.jpg'))
  h = getHeight(barbpic)
  lastpx = getPixel(barbpic,0,h-1)
  g = getGreen(lastpx)
  print g

def num2():
  barbpic = makePicture(getMediaPath('barbara.jpg'))
  w = getWidth(barbpic)
  h = getHeight(barbpic)
  canvas = makeEmptyPicture(w,h)
  div_w = w/2
  div_h = h/3
  for n in range(1,4):
    for m in range(1,3):
      if n == 1 and m == 1:
        targetX = 0
        targetY = div_h
      if n == 2 and m == 1:
        targetX = div_w*2
        targetY = 0
      if n == 3 and m == 1:
        targetX = div_w
        targetY = div_h
      if n == 1 and m == 2:
        targetX = 0
        targetY = 0
      if n == 2 and m == 2:
        targetX = div_w*2
        targetY = div_h
      if n == 3 and m == 2:
        targetX = div_w
        targetY = 0
      for x in range(div_w*(n-1),div_w*n):
        for y in range(div_h*(m-1),div_h*m-1):
          px = getPixel(barbpic,x,y)
          color = getColor(px)
          canvaspx = getPixel(canvas,targetX,targetY)
          setColor(canvaspx,color)
          targetY += 1
        targetX += 1

def lineHorizantal(pic):
  pw = getWidth(pic)
  ph = getHeight(pic)
  for y in range(0,pw,10):
    for x in range(0,ph):
      if x < pw and y < ph:
        pixel = getPixel(pic,x,y)
        if (y / 10) % 2 == 1:
          setColor(pixel,yellow)
        else:
          setColor(pixel,red)
      else:
        pass
     
def lineVertical(pic):
  pw = getWidth(pic)
  ph = getHeight(pic)
  for x in range(0,pw,10):
    for y in range(0,ph):
      pixel = getPixel(pic,x,y)
      if (x / 10) % 2 == 1:
        setColor(pixel,yellow)
      else:
        setColor(pixel,red)

def num3():
  barbpic = makePicture(getMediaPath('barbara.jpg'))
  lineVertical(barbpic)
  lineHorizantal(barbpic)
  show(barbpic)
  
def num4():
  barbpic = makePicture(getMediaPath('barbara.jpg'))
  w = getWidth(barbpic)
  h = getHeight(barbpic)
  div_w = w/4
  for n in range(1,5):
    for x in range(div_w*(n-1),div_w*n):
      for y in range(0,h):
        px = getPixel(barbpic,x,y)
        r = getRed(px)
        g = getGreen(px)
        b = getBlue(px)
        if n == 1:
          setColor(px,white)
        if n == 2:
          lightlum = (r+g+b/3)*2
          lightgrey=makeColor(lightlum,lightlum,lightlum)
          setColor(px,lightgrey)
        if n == 3:
          dakerlum = r+g+b/3
          darkgrey=makeColor(dakerlum,dakerlum,dakerlum) 
          setColor(px,darkgrey)
        if n == 4:
          setColor(px,black)
  show(barbpic)

def num5():
  barbpic = makePicture(getMediaPath('barbara.jpg'))
  ex_S = 82
  ey_S = 105
  ex_E = 165
  ey_E = 103
  
  tx_S = 105
  ty_S = 155
  tx_E = 154
  ty_E = 170
  
  hx_S = 41
  hy_S = 22
  hx_E = 196
  hy_E = 93  
  
  purple = makeColor(155,40,140)
  brown = makeColor(66,58,19)
  
  for x in range(ex_S,ex_E):
    for y in range(ey_S,ey_E):
      px = getPixel(barbpic,x,y)
      ecolor = getColor(px)
      if distance(brown,ecolor) < 400:
        setColor(px,red)
                
  for x in range(tx_S,tx_E):
   for y in range(ty_S,ty_E):
     px = getPixel(barbpic,x,y)
     tcolor = getColor(px)
     if distance(white,tcolor) < 120:
       setColor(px,purple)             
  
  for x in range(hx_S,hx_E):
    for y in range(hy_S,hy_E):
      px = getPixel(barbpic,x,y)
      hcolor = getColor(px)
      if distance(black,hcolor) < 100:
        setColor(px,orange)
  
  explore(barbpic)
              
def num6():
  barbpic = makePicture(getMediaPath('barbara.jpg'))
  katiepic = makePicture(getMediaPath('Katie-smaller.jpg'))
  bw = getWidth(barbpic)
  bh = getHeight(barbpic)  
  kw = getWidth(katiepic)
  kh = getHeight(katiepic) 
  blendsize = bw/3
  blendpoint = (bw/3)*2
  canvas = makeEmptyPicture(bw+kw-blendsize,bh)
  n = 0.75
  m = 0.25
  targetX = 0
  for x in range(0,bw+kw-blendsize):
    targetY = 0
    for y in range(0,bh):
      if y < bh and x < bw:
        bpx = getPixel(barbpic,x,y)
      else:
        pass
      if y < kh and x < kw:
        kpx = getPixel(katiepic,x,y)
      else:
        pass        
      canvaspx = getPixel(canvas,targetX,targetY)
      bcolor = getColor(bpx)
      kcolor = getColor(kpx)
      br = getRed(bpx)
      bg = getGreen(bpx)
      bb = getBlue(bpx)
      kr = getRed(kpx)
      kg = getGreen(kpx)
      kb = getBlue(kpx)
      if x < blendpoint:
        setColor(canvaspx,bcolor)
      elif x >= blendpoint and x < bw:
        blendedcolor = makeColor(br*n+kr*m,bg*n+kg*m,bb*n+kb*m)
        setColor(canvaspx,blendedcolor)
      else:
        targetX = bw
        canvaspx2 = getPixel(canvas,targetX,targetY)
        setColor(canvaspx2,kcolor)
      targetY += 1
    targetX += 1
  show(canvas)
     
def main():
  num1()
  num3()
  num4()
  num5()
  num6()
      
num1()
num3()
num4()
num5()
num6()
