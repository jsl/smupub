
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def barGreen():
  barb=makePicture(getMediaPath("barbara.jpg"))
  barbpx=getPixel(barb, 0, 293)
  gr=getGreen(barbpx)
  print gr
  
def barMosaik():
  barb=makePicture(getMediaPath("barbara.jpg"))
  
#def barLine():
#  barb=makePicture(getMediaPath("barbara.jpg"))
#  sourceX=21
#  for x in range(getWidth(barb),soureX):
#    sourceY=19
#    for y in range(getHeight(barb),soureY):
      
def barTeeth(barb):
  teeth=makeColor(255,237,227)
  purple=makeColor(128, 0, 128)
  for x in range(117, 141):
   for y in range(160, 170):
     barbpx=getPixel(barb, x, y)
     barbcolor=getColor(barbpx)
     if distance (teeth,barbcolor) < 150:
       setColor(barbpx, purple)
  return(barb)
  
def barEye(barb):
  eye=makeColor(66,28,19)
  for x in range(82, 161):
   for y in range(101, 103):
     barbpx=getPixel(barb, x, y)
     barbcolor=getColor(barbpx)
     if distance (eye,barbcolor) < 200:
       setColor(barbpx, red)
  return(barb)
   
def barHair(barb):
  hair=makeColor(63,29,17)
  for x in range(39, 200):
   for y in range(26, 280):
     barbpx=getPixel(barb, x, y)
     barbcolor=getColor(barbpx)
     if distance (hair,barbcolor) < 50:
       setColor(barbpx, orange)
  return(barb)
  
def barChange():
  barb=makePicture(getMediaPath("barbara.jpg"))
  barTeeth(barb)
  barEye(barb)
  barHair(barb)
  show(barb)

#def blendBar():
#  pic1=makePicture(getMediaPath("barbara.jpg"))
#  pic2=makePicture(getMediaPath("Katie-smaller.jpg"))
#  canvas=makePicture(getMediaPath("7inX95in.jpg"))
#  for x in range(146,221):
#    for y in range(getHeight(pic1)):
#      pic1px=getPixel(pic1, x, y)
#      pic1red=setRed(pic1px, getRed(pic1px)*0.75)
#      pic1green=setGreen(pic1px, getGreen(pic1px)*0.75)
#      pic1blue=setBlue(pic1px, getBlue(pic1px)*0.75)
#      newColor=makeColor(pic1red, pic1green, pic1blue)
#      setColor=(pic1px, newColor)
#  show(pic1)

def lum():
  barb=makePicture(getMediaPath("barbara.jpg"))
  for x in range (0, 55):
    for y in range (getHeight(barb)):
      px=getPixel(barb, x, y)
      barbcolor=getColor(px)
      setColor(px, white)
  for x in range(55, 110):
    for y in range (getHeight(barb)):
      px=getPixel(barb, x, y)
      barbcolor=getColor(px)
      brighter=makeBrighter(barbcolor)
      setColor(px, brighter)
  for x in range(110, 165):
    for y in range (getHeight(barb)):
      px=getPixel(barb, x, y)
      barbcolor=getColor(px)
      brighter=makeDarker(barbcolor)
      setColor(px, brighter)
  for x in range(165, 222):
    for y in range (getHeight(barb)):
      px=getPixel(barb, x, y)
      setColor(px, black)
  show(barb)
    
def main():
  barGreen()
  lum()
  barChange()

barGreen()
lum()
barChange()
