
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab3()

def lab1():
  printGreen()

def lab3():
  drawLine()
  
  
def printGreen():
  bar=makePicture(getMediaPath("barbara.jpg"))
  inform=getPixel(bar,0,getHeight(bar)-1)
  color=getGreen(inform)
  print color
  show(bar)
  return bar

def drawLine():
  bar=makePicture(getMediaPath("barbara.jpg"))
  drawHorizontal1(bar)
  drawHorizontal2(bar)
  drawVertical1(bar)
  drawVertical2(bar)
  show(bar)
  return bar

def drawHorizontal1(picture):
  for y in range(0,getHeight(picture)):
    for x in range(0,getWidth(picture)-2,44):
      inform=getPixel(picture,x,y)
      setColor(inform,yellow)
  
  
def drawHorizontal2(picture):
  for y in range(0,getHeight(picture)):
    for x in range(22,getWidth(picture)-2,44):
      inform=getPixel(picture,x,y)
      setColor(inform,red)
  
  
def drawVertical1(picture):
  for x in range(0,getWidth(picture)):
    for y in range(0,getHeight(picture)-4,58):
      inform=getPixel(picture,x,y)
      setColor(inform,yellow)
  

def drawVertical2(picture):
  for x in range(0,getWidth(picture)):
    for y in range(29,getHeight(picture)-4,58):
      inform=getPixel(picture,x,y)
      setColor(inform,red)
  
lab1()
lab3()
