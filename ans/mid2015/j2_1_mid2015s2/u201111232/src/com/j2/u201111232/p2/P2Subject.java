package com.j2.u201111232.p2;

public interface P2Subject {
  public void registerObserver(P2Observer o);
  public void removeObserver(P2Observer o);
  public void notifyObservers();
}