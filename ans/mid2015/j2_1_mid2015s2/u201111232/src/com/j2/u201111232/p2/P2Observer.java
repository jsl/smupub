package com.j2.u201111232.p2;

public interface P2Observer {
  public void update(float temperature, float humidity);
}