package com.j2.u201111232.p3;

public class PizzaStore {
  SimpleFactory factory;
  public PizzaStore(SimpleFactory factory) {
    this.factory = factory;
  }
  
  public Pizza orderPizza(String type) {
    Pizza pizza;
    pizza = factory.createPizza(type);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
}