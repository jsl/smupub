package com.j2.u201111232.p3;

public class Pizza {
  String name;
  
  public void prepare() {
    System.out.println("preaparing...");
  }
  public void bake() {
    System.out.println("Baking...");
  }
  public void cut() {
    System.out.println("Cutting...");
  }
  public void box() {
    System.out.println("Boxing...");
  }
  
  public String getName(){
    return name;
  }
}