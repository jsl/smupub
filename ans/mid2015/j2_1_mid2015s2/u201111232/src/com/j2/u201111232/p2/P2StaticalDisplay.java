package com.j2.u201111232.p2;

public class P2StaticalDisplay implements P2Observer, P2DisplayElement {
  P2Subject weatherData;
  private float temperature;
  private float humidity;
  
  public P2StaticalDisplay(P2Subject weatherData) {
    this.weatherData = weatherData;
    weatherData.registerObserver(this);
  }
  
  public void update(float temperature, float humidity) {
    this.temperature = temperature;
    this.humidity = humidity;
    display();
  }
  
  public void display() {
    System.out.println("Static Display");
    System.out.println("Current conditions: "
                         +temperature+"F degrees and "
                         +humidity+"% humidity");
  }
}