package com.j2.u201111232.p2;

import java.util.*;
public class P2WeatherData implements P2Subject {
  private float temperature;
  private float humidity;
  ArrayList<P2Observer> observers;
  
  public P2WeatherData(){
    observers = new ArrayList<P2Observer>();
  }
  
  public void registerObserver(P2Observer o) {
    observers.add(o);
  }
  public void removeObserver(P2Observer o) {
    int i = observers.indexOf(o);
    if (i>=0) {
      observers.remove(o);
    }
  }
  public void notifyObservers() {
    
    for(int i=0; i<observers.size(); ++i) {
      P2Observer observer = observers.get(i);
      observer.update(temperature, humidity);
    }
  }
  
  public void setMeasurements(float temperature, float humidity) {
    this.temperature = temperature;
    this.humidity = humidity;
    measurementsChanged();
  }
  public void measurementsChanged() {
    notifyObservers();
  }
  
  public float getTemperature() {
    return temperature;
  }
  public float getHumidity() {
    return humidity;
  }
}
    