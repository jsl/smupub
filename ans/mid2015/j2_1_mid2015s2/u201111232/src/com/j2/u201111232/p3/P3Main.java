package com.j2.u201111232.p3;

public class P3Main {
  public static void main (String[] args) {
    
    SimpleFactory factory = new SimpleFactory();
    PizzaStore store = new PizzaStore(factory);
    
    Pizza pizza = store.orderPizza("cheese");
    System.out.println("We ordered a "+pizza.getName());
    
    pizza = store.orderPizza("veggie");
    System.out.println("We ordered a "+pizza.getName());
    
    pizza = store.orderPizza("clam");
    System.out.println("We ordered a "+pizza.getName());
  }
}