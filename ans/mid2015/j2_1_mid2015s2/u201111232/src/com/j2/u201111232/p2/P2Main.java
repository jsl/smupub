package com.j2.u201111232.p2;

public class P2Main {
  public static void main (String[] args) {
    
    P2WeatherData weatherData = new P2WeatherData();
    P2StaticalDisplay staticalDisplay = new P2StaticalDisplay(weatherData);
    weatherData.setMeasurements(80, 65);
    
    weatherData = new P2WeatherData();
    P2ForeCastingDisplay foreCastingDisplay = new P2ForeCastingDisplay(weatherData);
    weatherData.setMeasurements(82, 70);
    
  }
}