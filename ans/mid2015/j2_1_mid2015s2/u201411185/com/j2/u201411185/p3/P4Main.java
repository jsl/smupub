package com.j2.u201411185.p4;

public class P4Main {
  public static void main(String[] args){
    P4Computer com1 = new P4i5();
    com1 = new P4CD(com1);
    System.out.println(com1.getDe() + " \ " + com1.cost());
    
    P4Computer com2 = new P4i7();
    com2 = new P4CD(com2);
    com2 = new P4CD(com2);
    com2 = new P4RAM8G(com2);
    System.out.println(com2.getDe() + " \ " + com2.cost());
  }
}
