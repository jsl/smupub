package com.j2.u201411185.p3;
import java.util.*;

public class P3SimplePizzaFactory {
  public P3Pizza createPizza(String type){
    Pizza pizza = null;
    if(type.equals, "cheese")
      pizza = new P3CheesePizza();
    else if (type.equals, "veggie")
      pizza = new P3VeggiePizza();
    else if (type.equals, "clam")
      pizza = new P3ClamPizza();
    
    return pizza;
  }
}
