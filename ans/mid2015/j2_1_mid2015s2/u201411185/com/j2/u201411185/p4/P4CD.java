package com.j2.u201411185.p4;

public class P4CD extends P4Decorator {
  public P4CD(P4Computer com) {
   com = new P4Computer();
  }
  
  public String getDe(){
    return com.getDe() + ", CD";
  }
  
  public double cost(){
    return com.cost() + 10000;
  }
}
