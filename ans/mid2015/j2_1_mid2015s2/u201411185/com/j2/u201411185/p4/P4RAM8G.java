package com.j2.u201411185.p4;

public class P4RAM8G extends P4Decorator {
  public P4RAM8G(P4Computer com) {
    com = new P4Computer();
  }
  
  public String getDe(){
    return com.getDe() + ", RAM 8G";
  }
  
  public double cost(){
    return com.cost() + 50000;
  }
}
