package com.j2.u201411185.p2;
import java.util.*;

public class P2WeatherData implements P2Subject{
  private float temp;
  private float hum;
  private float pressure;
  ArrayList P2Observer observers;
 
  public P2WeatherData() {
    this.observers = new ArrayList();
  }
  
  public void registerOb(P2Observer o){
    observers.add(o);
  }
  
  public void removeOb(P2Observer o){
    int i = observers.indexOf(o);
    if(i >=0)
      observers.remove(i);
  }
  
  public void notifyOb(){
    for(int i = 0; observer.siez(); i++){
      P2Observer ob = (P2Observer)ob.get(i);
      ob.update(i);
    }
  }
  
  public void MeasurementsChanged(){
    notifyOb();
  }
  
  public void setMeasurements(float temp, float hum){
    this.temp = temp;
    this.hum = hum;
    MeasurementsChanged();
  }
  
  public float getTemp(){
    return temp;
  }
  
  public float getHum(){
    return hum;
  }
  
  public float getPressure(){
    return pressure;
  }
}
