package com.j2.u201411185.p2;
import java.util.*;

public class P2StatisticalDisplay implements P2Observer,P2Display{
  private float temp;
  private float hum;
  private P2WeatherData weather;
  
  public P2StatisticalDisplay(){
   P2WeatherData wd = new P2WeatherData();
   weather.registerOb(wd);
  }
  
  public void update(float temp, float hum){
    this.temp = temp;
    this.hum = hum;
    display();
  }
  
  public void display(){
    System.out.println("Statistical Display\nCurrent conditions : " + temp + "F deggrees and" + hum + "% humidity\n");
  }
}
