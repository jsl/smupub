package com.j2.u201411185.p2;

public interface P2Subject {
  public void registerOb(P2Observer o);
  public void removeOb(P2Observer o);
  public void notifyOb();
}
