package com.j2.u201111192.p2;
public class WeatherStation{
  public static void main(String ar[]){
    WeatherData weatherdata=new WeatherData();
    StatisticalDisplay sd=new StatisticalDisplay(weatherdata);
    ForecastingDisplay fd=new ForecastingDisplay(weatherdata);
    weatherdata.setMeasurements(80,65);
    weatherdata.setMeasurements(82,70);
  }
}