package com.j2.u201111192.p3;
public class PizzaFactory{
 Pizza pizza;
 public Pizza create(String type){
   if(type.equals("cheese")){
   pizza=new CheesePizza();
   }
   else if(type.equals("clam")){
    pizza=new ClamPizza(); 
   }
   else if(type.equals("veggie"))
   {
    pizza=new VeggiePizza(); 
    
   }
   else 
    pizza=null;
   
   return pizza;
 }
  
}