package com.j2.u201111192.p2;
public class StatisticalDisplay implements Observer,DisplayElement{
  double temperature;
  double humidity;
  WeatherData weatherdata;
  public StatisticalDisplay(WeatherData weatherdata){
    this.weatherdata=weatherdata;
    weatherdata.registerObserver(this);
  }
  public void update(double temperature,double humidity)  {
    this.temperature=temperature;
    this.humidity=humidity;
    display();
  }
  public void display(){
   System.out.println("Statistical Display");
   System.out.println("Current conditions: "+temperature+"F degrees and "+humidity+" % humidity"); 
  }
  
}