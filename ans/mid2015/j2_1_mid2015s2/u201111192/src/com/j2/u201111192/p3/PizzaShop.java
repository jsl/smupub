package com.j2.u201111192.p3;
public class PizzaShop{
  PizzaFactory fac;
  public PizzaShop(PizzaFactory fac){
    this.fac=fac;
  }
  public void orderPizza(String type){
    Pizza pizza; 
    pizza=fac.create(type);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
  }
  
}