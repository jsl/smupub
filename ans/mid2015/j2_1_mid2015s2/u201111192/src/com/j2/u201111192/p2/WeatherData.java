package com.j2.u201111192.p2;
import java.util.*;
public class WeatherData implements Subject{
double temperature;
double humidity;

ArrayList observers;
public WeatherData(){
 observers=new ArrayList(); 
}
public  void registerObserver(Observer o){
observers.add(o); 
}
public  void removeObserver(Observer o)
{
  for(int i=0;i<observers.size();i++){
    if(o==(Observer)observers.get(i)){
     observers.remove(i);
    }
  }
}
public void notifyObserver()
{
  Observer observer;
  
  for(int i=0;i<observers.size();i++){
    observer=(Observer)observers.get(i);
    observer.update(temperature,humidity);
    
  }
}
public void setMeasurements(double temperature,double humidity){
 this.temperature=temperature;
 this.humidity=humidity;
 measuredmentsChanged();
}
public void measuredmentsChanged(){
notifyObserver();
}
  
}