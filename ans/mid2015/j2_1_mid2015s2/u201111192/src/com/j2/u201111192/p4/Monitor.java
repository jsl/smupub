package com.j2.u201111192.p4;
public  class Monitor extends addition{
  public Monitor(Computer cpu){
    this.cpu=cpu;
  }
  
  public String getDescription(){
   return cpu.getDescription()+", Monitor";
  }
  public double Cost(){
   return cpu.Cost()+5; 
  }
}