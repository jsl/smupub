package com.j2.u201111192.p4;
public  class RAMeight extends addition{
  public RAMeight(Computer cpu){
    this.cpu=cpu;
  }
  
  public String getDescription(){
   return cpu.getDescription()+", Ram 8G";
  }
  public double Cost(){
   return cpu.Cost()+3; 
  }
}