package com.j2.u201111192.p3;
public class PizzaTestDrive{
  public static void main(String ar[]){
    PizzaFactory fac=new PizzaFactory();
    PizzaShop ps=new PizzaShop(fac);
    ps.orderPizza("cheese");
    ps.orderPizza("veggie");
    ps.orderPizza("clam");
  }
}