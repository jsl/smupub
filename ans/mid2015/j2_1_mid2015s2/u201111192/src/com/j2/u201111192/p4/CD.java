package com.j2.u201111192.p4;
public class CD extends addition{
  public CD(Computer cpu){
    this.cpu=cpu;
  }
  
  public String getDescription(){
   return cpu.getDescription()+", CD";
  }
  public double Cost(){
   return cpu.Cost()+1; 
  }
}