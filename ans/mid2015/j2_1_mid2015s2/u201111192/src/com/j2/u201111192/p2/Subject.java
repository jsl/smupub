package com.j2.u201111192.p2;
public interface Subject{
public abstract void registerObserver(Observer o);
public abstract void removeObserver(Observer o);
public abstract void notifyObserver();
  
}