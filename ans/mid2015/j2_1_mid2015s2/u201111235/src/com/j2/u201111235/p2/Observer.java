package com.j2.u201111235.p2;

public interface Observer
{
  public void update(float temp,float hum);
  
}