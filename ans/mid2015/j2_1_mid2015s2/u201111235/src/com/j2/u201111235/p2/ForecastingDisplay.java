package com.j2.u201111235.p2;

import java.util.Observable;
import java.util.Observer;

public class ForecastingDisplay implements Observer
{
  Observable observable;
  
  private float temp;
  private float hum;
  
  public ForecastingDisplay(Observable observable)
  {
    
    this.observable=observable;
     observable.addObserver(this);
    
  }
  public void update(Observable obs, Object arg)
  {
    if(obs instanceof WeatherData)
    {
      WeatherData weatherData=(WeatherData)obs;
      this.temp = weatherData.getTemp();
      this.hum =  weatherData.getHum();
       display();
      
    }
    
  }
  public void display()
  {
    System.out.println("ForecastingDisplay");
    System.out.println("Current Conditions: "+temp+"F dgrees and "+hum+"% humidity" );
  }
  
}