package com.j2.u201111235.p2;

import java.util.Observable;
import java.util.Observer;


public class WeatherData extends Observable
{
  private float temp;
  private float hum;
  
  public void MeasurementsChange()
  {
    setChanged();
    notifyObservers();
    
  }
  public void setMeasurements(float temp,float hum)
  {
    this.temp=temp;
    this.hum=hum;
    MeasurementsChange();
    
  }
  public float getTemp()
  {
    return temp;
  }

  public float getHum()
  {
    return hum;
  }
}