 package com.j2.u201111235.p4;
 
 public class RAM8G extends CDescription
 {
   Computer computer;
   public RAM8G(Computer computer)
   {
     this.computer=computer;
   }
   public String setDescription()
   {
     return computer.setDescription() + ",RAM8G";
   }
   public int cost()
   {
     return computer.cost() + 120000;
   }
     
 }