 package com.j2.u201111235.p4;
 
 public class CD extends CDescription
 {
   Computer computer;
   public CD(Computer computer)
   {
     this.computer=computer;
   }
   public String setDescription()
   {
     return computer.setDescription() + ",CD";
   }
   public int cost()
   {
     return computer.cost()+ 5000 ;
   }
     
 }