package com.j2.u201111235.p2;

import java.util.Observable;
import java.util.Observer;

public class WeatherStation
{
  public static void main(String arg [])
  {
    WeatherData weatherData =new WeatherData();
    CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);
 
    ForecastingDisplay forecastingDisplay = new ForecastingDisplay(weatherData);
    
    
    
    weatherData.setMeasurements(80,65);
    weatherData.setMeasurements(82,70);
    
  }
}