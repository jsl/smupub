package com.j2.u201211274.p1;
  import java.util.*;
 public class Sorter{
  Compare compare;
  
  public int comparing(Object o1,Object o2){
    if(o1 instanceof String && o2 instanceof String)
      compare=new StringCompare();
    else if(o1 instanceof Integer && o2 instanceof Integer)
      compare=new IntCompare();
   return compare.compare(o1,o2);
  }
  public void sort(Object o1,Object o2){
    
    int result=comparing(o1,o2);
    if(result>0)
      System.out.println(o1+"is bigger than"+ o2);
    else
      System.out.println(o2+"is bigger than"+ o1);
  }
}