package com.j2.u201211274.p2;
import java.util.*;
public class WeatherData implements Subject{
  float temperature;
  float humid;
  ArrayList arrayList;
  public WeatherData(){
    arrayList=new ArrayList();
  }
  public void setMeasurements(float temperature,float humid){
    this.temperature=temperature;
    this.humid=humid;
    notifyObserver();
  }
  
  public void registerObserver(Observer o){
    arrayList.add(o);
   // notifyObserver(float temperature,float humid);
  }
 //public void  removeObserver();
 public void  notifyObserver(){
   Observer observer;
   int cnt=arrayList.size();
   for(int i=0;i<cnt;i++){
     observer=(Observer)arrayList.get(i);
   observer.update(temperature,humid);
   }
 }
} 