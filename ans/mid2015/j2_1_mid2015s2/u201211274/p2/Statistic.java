package com.j2.u201211274.p2;

public class Statistic implements Observer, DisplayElement{
   float temperature;
   float humid;
   float Subject;
   public void update(float temperature,float humid){
     this.temperature=temperature;
     this.humid=humid;
     display();
   }
   public void display(){
     System.out.println("Statistical Display");
     System.out.println("Current Conditions "+ temperature+"F degrees and "+humid+"% humidity");
   }
}