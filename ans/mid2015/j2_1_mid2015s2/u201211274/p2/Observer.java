package com.j2.u201211274.p2;

public interface Observer{
  public void update(float temperature,float humid);
}