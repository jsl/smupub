package com.j2.u201211274.p2;

public interface Subject{
  public void registerObserver(Observer o);
 //public void  removeObserver();
 public void  notifyObserver();
}