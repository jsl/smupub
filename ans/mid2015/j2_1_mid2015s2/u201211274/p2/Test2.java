package com.j2.u201211274.p2;

public class Test2{
  public static void main(String args[]){
    WeatherData weatherdata=new WeatherData();
    Statistic statistic=new Statistic();
    Forecast forecast=new Forecast();
    weatherdata.registerObserver(statistic);
    weatherdata.registerObserver(forecast);
    
    weatherdata.setMeasurements(80.0f,65.0f);
     weatherdata.setMeasurements(82.0f,70.0f);
  }
}