package com.j2.u201211274.p3;
  
  public class SimpleFactory{
  Pizza pizza;
  public Pizza create(String type){
    if(type.equals("cheese")){
      pizza=new CheesePizza();
    }
    else if(type.equals("veggie")){
      pizza=new VggiePizza();
    }
    else if(type.equals("clam")){
      pizza=new ClamPizza();
    }
    return pizza;
  }
}