package com.j2.u201211274.p3;
  
  public class PizzaStore{
  Pizza pizza;
  SimpleFactory simpleFactory;
  public Pizza order(String type){
    simpleFactory=new SimpleFactory();
    pizza=simpleFactory.create(type);
    pizza.display();
    return pizza;
  }
}