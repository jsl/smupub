package com.j2.u201211291.p2;

public class ForecastringDisplay implements Observer{
  public ForecastringDisplay(Subject s){
    s.registerObservers(this);
  }
  float humidity;
  float degree;
  public void update(float degree, float humidity){
    this.humidity =humidity;
    this.degree = degree;
  }
  public void display(){
    System.out.println("humidity : " + humidity + " %" + "degrees: " + degree);
  }
}