package com.j2.u201211291.p2;

public class P2WeatherMain{
  public static void main(String args[]){
  Subject s = new WeatherData();
  
  Observer o1 = new ForecastringDisplay(s);
  Observer o2 =  new StatisticalDisplay(s);
  s.setMeasurements(80.0f,65.0f);
  o1.display();
  o2.display();
  s.setMeasurements(82.0f,70.0f);
  o1.display();
  o2.display();
}
}