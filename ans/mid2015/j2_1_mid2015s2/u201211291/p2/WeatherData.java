package com.j2.u201211291.p2;
import java.util.*;
public class WeatherData implements Subject{
  ArrayList arraylist;
  float degree;
  float humidity;
  public void setMeasurements(float a, float b){
    degree = a;
    humidity = b;
    notifyObservers();
  }
  public void registerObservers(Observer o){
    arraylist.add(o);
  }

  public void notifyObservers(){
   int k = arraylist.size(); 
   for(int i=0; i<k; k++){
    Observer o = (Observer)arraylist.get(i); 
    o.update(degree,humidity);
   }
  }
}
