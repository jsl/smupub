package com.j2.u201211291.p2;

public interface Observer{
  void update(float degree , float humidity);
  void display();
}