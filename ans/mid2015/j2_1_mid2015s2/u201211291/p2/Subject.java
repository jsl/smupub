package com.j2.u201211291.p2;

public interface Subject{
  void notifyObservers();
  void registerObservers(Observer o);
void setMeasurements(float a, float b);
}