package com.j2.u201211291.p2;

public class StatisticalDisplay implements Observer{
  public StatisticalDisplay(Subject s){
    s.registerObservers(this);
  }
  float humidity;
  float degree;
  public void update(float degree, float humidity){
    this.humidity =humidity;
    this.degree = degree;
  }
  public void display(){
    System.out.println("humidity : " + humidity + " %" + "degrees: " + degree);
  }
}