package com.j2.u201211291.p4;

public abstract class Computer{
  Monitor monitor;
  Cd cd;
  Ram ram;
  abstract void purchase();
  
  abstract void cost();
}