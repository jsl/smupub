package com.j2.u201211291.p4;

public class P4ComputerMain{
  public static void main(String args[]){
    Computer computer1 = new I5Computer();
    Computer computer2= new I7Computer();
    computer1.purchase();
    computer1.cost();
    computer2.purchase();
    computer2.cost();
  }
}