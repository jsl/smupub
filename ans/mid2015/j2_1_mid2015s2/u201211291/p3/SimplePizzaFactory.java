package com.j2.u201211291.p3;

public class SimplePizzaFactory{
  Pizza orderPizza(String type){
   Pizza pizza;
    pizza = createPizza(type); 
    pizza.preparing();
    pizza.baking();
    pizza.cutting();
    pizza.boxing();
    return pizza;
  }
  Pizza createPizza(String type){
    if(type.equals("clam")){
      Pizza pizza = new ClamPizza();
      return pizza;
    }
     if(type.equals("cheese")){
      Pizza pizza = new CheesePizza();
      return pizza;
    }
       if(type.equals("veggie")){
         Pizza pizza =  new VeggiePizza();
         return pizza;
       }
       return null;
    }

}

