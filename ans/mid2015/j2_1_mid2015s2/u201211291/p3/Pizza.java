package com.j2.u201211291.p3;

public abstract class Pizza{
  String name;
  public void preparing(){
    System.out.println("preparing...");
  }
  public void baking(){
    System.out.println("Baking...");
  }
  public void cutting(){
    System.out.println("Cutting...");
  }
  public void boxing(){
    System.out.println("Boxing...");
  }
  public abstract void setName();
  public abstract String toString();
}