package com.j2.u201211291.p3;

public class PizzaStore{
  SimplePizzaFactory pizzafactory;
  Pizza OrderPizza(String type){
    Pizza pizza = CreatePizza(type);
    return pizza;
  }
  Pizza CreatePizza(String type){
    pizzafactory = new SimplePizzaFactory();
    Pizza pizza = pizzafactory.orderPizza(type);
    return pizza;
  }
}