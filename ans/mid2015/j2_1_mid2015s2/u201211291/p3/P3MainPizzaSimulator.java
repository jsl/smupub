package com.j2.u201211291.p3;

public class P3MainPizzaSimulator{
  public static void main(String args[]){
    PizzaStore ps = new PizzaStore();
    Pizza pizza= ps.OrderPizza("cheese");
    System.out.println("We ordered a" + pizza);
    Pizza pizza1= ps.OrderPizza("clam");
    System.out.println("We ordered a" + pizza1);
    Pizza pizza2= ps.OrderPizza("veggie");
    System.out.println("We ordered a " + pizza2);
  }
}
