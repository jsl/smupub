package com.j2.u201211291.p3;

public class VeggiePizza extends Pizza{
  public void setName(){
    name = "Veggie PIZZA";
  }
  public VeggiePizza(){
    setName();
  }
  public String toString(){
    return name;
  }
}