package com.j2.u201411184.p3;

public class PizzaStore {
  SimplePizzaFactory factory = new SimplePizzaFactory();
  
  public Pizza orderPizza(String item) {
    Pizza pizza =null;
    pizza = factory.createPizza(item);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    
    return pizza;
  }
}