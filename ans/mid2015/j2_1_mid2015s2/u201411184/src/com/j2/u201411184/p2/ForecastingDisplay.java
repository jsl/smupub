package com.j2.u201411184.p2;

public class ForecastingDisplay implements DisplayElement, Subject {
  WeatherData weatherData;
  public void display() {
    System.out.println("Forcasting Display");
    System.out.println("Current conditions: " + weatherData.getTemp() +
                       "F degrees and " + weatherData.getHumid() + "% humidity");
  }
}