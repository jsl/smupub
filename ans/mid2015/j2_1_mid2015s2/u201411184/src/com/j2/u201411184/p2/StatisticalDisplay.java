package com.j2.u201411184.p2;

public class StatisticalDisplay implements DisplayElement, Subject {
  WeatherData weatherData = new WeatherData();
  public void display() {
    System.out.println("Statistical Display");
    System.out.println("Current conditions: " + weatherData.getTemp() +
                       "F degrees and " + weatherData.getHumid() + "% humidity");
  }
}