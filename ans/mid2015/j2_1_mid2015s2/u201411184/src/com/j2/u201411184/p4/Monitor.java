package com.j2.u201411184.p4;

public class Monitor extends ComputerDecorator {
  Computer computer;
  
  public Monitor() {
    this.computer = computer;
  }
  public String getName() {
    return ", Monitor";
  }
  public double cost() {
    return computer.cost() + 30.89;
  }
}