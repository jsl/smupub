package com.j2.u201411184.p3;

public class Pizza {
  String name;
  
  void prepare() {
    System.out.println("preparing...");
  }
  void bake() {
    System.out.println("Baking...");
  }
  void cut() {
    System.out.println("cutting...");
  }
  void box() {
    System.out.println("boxing...");
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String toString() {
    StringBuffer display = new StringBuffer();
    display.append("----- " + name + " -----\n");
    return display.toString();
  }
}