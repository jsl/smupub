package com.j2.u201411184.p4;

public abstract class Computer {
  String name;
  public String getName() {
    return name;
  }
  public abstract double cost();
}