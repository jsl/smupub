package com.j2.u201411184.p2;

public interface DisplayElement {
  public void display();
}