package com.j2.u201411184.p2;

public class WeatherData {
  float temp;
  float humid;
  
  public void setMeasurements(float temp, float humid) {
    this.temp = temp;
    this.humid = humid;
  }
  public float getTemp() {
    return temp;
  }
  public float getHumid() {
    return humid;
  }
}