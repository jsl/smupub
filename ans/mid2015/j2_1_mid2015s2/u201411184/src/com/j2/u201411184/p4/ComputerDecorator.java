package com.j2.u201411184.p4;

public abstract class ComputerDecorator {
  public abstract double cost();
}