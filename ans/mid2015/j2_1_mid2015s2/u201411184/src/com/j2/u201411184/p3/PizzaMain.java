package com.j2.u201411184.p3;

public class PizzaMain {
  public static void main(String[] args) {
    PizzaStore store = new PizzaStore();
    
    Pizza pizza = store.orderPizza("cheese");
    System.out.println("We ordered a " + pizza.getName());
  }
}