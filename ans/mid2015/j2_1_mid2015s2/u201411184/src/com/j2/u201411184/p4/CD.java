package com.j2.u201411184.p4;

public class CD extends ComputerDecorator {
  Computer computer;
  
  public CD() {
    this.computer = computer;
  }
  public String getName() {
    return ", CD";
  }
  public double cost() {
    return computer.cost() + 2.99;
  }
}