package com.j2.u201411184.p4;

public class RAM extends ComputerDecorator {
  Computer computer;
  
  public RAM() {
    this.computer = computer;
  }
  public String getName() {
    return ", RAM 8G";
  }
  public double cost() {
    return computer.cost() + 60.99;
  }
}