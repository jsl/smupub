@startuml
Computer <|-- I5
Computer <|-- I7
Computer <-- ComputerDecorator
Computer <|-- ComputerDecorator
ComputerDecorator <|.. Monitor
ComputerDecorator <|.. CD
ComputerDecorator <|.. RAM
@enduml