package com.j2.u201285092.p2;
public interface Observer {
 public void update(float temp, float humidity, float pressure);
}
