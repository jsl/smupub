package com.j2.u201285092.p2;
public interface Subject {
 Observer o;
 public void registerObserver(Observer o);
 public void removeObserver(Observer o);
 public void notifyObservers();
}
