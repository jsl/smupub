package com.test;

public class WeatherStation{
 
public static void main(String[] args)
{
  WeatherData weatherData = new WeatherData();
  ForecastingDisplay display1 = new ForecastingDisplay(weatherData);
  StatisticalDisplay display2 = new StatisticalDisplay(weatherData);

  weatherData.setMeasurements(80,65);
  weatherData.setMeasurements(82,70);
}
  
}