package com.test;

abstract public interface DisplayElement
{
  public void display();
}