package com.test;

abstract public interface Observer
{
  public void update(float temperature,float humidity);
}