package com.test1;

public class SimplePizzaFactory{
  
  public Pizza createPizza(String item)
  {
    if(item.equals("Cheese"))
    {
      return new CheesePizza();
    }
    else if(item.equals("Veggie"))
    {
      return new VeggiePizza();
    }
    else if(item.equals("Clam"))
    {
      return new ClamPizza();
    }
    else
    {
      return null;
    }
    
  }

}
 
  
  
