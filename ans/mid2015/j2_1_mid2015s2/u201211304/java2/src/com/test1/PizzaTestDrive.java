package com.test1;

public class PizzaTestDrive{

  public static void main(String[] args)
  {
    SimplePizzaFactory factory = new SimplePizzaFactory();
    PizzaStore store = new PizzaStore(factory);
    
    
    
    Pizza pizza1;
    pizza1 = store.orderPizza("Cheese");
    
    System.out.println("We ordered a"+pizza1);
    
    Pizza pizza2;
    pizza2 = store.orderPizza("Veggie");
    
    System.out.println("We ordered a"+pizza2);
    
    Pizza pizza3;
    pizza3 = store.orderPizza("Clam");
    
    System.out.println("We ordered a"+pizza3);
    
    
  }

}