package com.j2.u201411196.p4;

public abstract class P4Com{
  String description;
  public abstract double cost();
  
  public String getDescription(){
    return description;
  }
}