package com.j2.u201411196.p4;

public class P4Main{
  public static void main(String[] args){
    P4Com com1 = new P4i5();
    com1 = new P4Monitor(com1);
    com1 = new P4CD(com1);
    com1 = new P4RAM8G(com1);
    System.out.println(com1.getDescription() +", cost: " +com1.cost());
    P4Com com2 = new P4i7();
    com2 = new P4Monitor(com2);
    com2 = new P4CD(com2);
    com2 = new P4RAM8G(com2);
    System.out.println(com2.getDescription() +", cost: " +com2.cost());
  }
}
