package com.j2.u201411196.p4;

public class P4Monitor extends P4CondimentDecorator{
  public P4Monitor(P4Com com){
    this.com = com;
    description = "Monitor";
  }
  public String getDescription(){
    return com.getDescription() +",  "+ description;
  }
  
  public double cost(){
    return com.cost()+ 3;
  }
}
  