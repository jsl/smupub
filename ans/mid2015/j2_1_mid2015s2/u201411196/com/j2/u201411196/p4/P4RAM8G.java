package com.j2.u201411196.p4;

public class P4RAM8G extends P4CondimentDecorator{
  public P4RAM8G(P4Com com){
    this.com = com;
    description = "RAM 8G";
  }
  public String getDescription(){
    return com.getDescription() +",  "+ description;
  }
  
  public double cost(){
    return com.cost()+4;
  }
}
  