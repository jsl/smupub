package com.j2.u201411196.p4;

public class P4CD extends P4CondimentDecorator{
  public P4CD(P4Com com){
    this.com = com;
    description = "P4CD";
  }
  public String getDescription(){
    return  com.getDescription() +",  "+ description;
  }
  
  public double cost(){
    return com.cost()+0.2;
  }
}