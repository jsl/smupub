package com.j2.u201411196.p3;

public class P3SimpleFactory{
  P3Pizza pizza= null;
  public P3Pizza createPizza(String type){
    if(type.equals("cheese")){
      pizza = new P3CheesePizza();
    }
    else if(type.equals("veggie")){
      pizza = new P3VeggiePizza();
    }
    else if(type.equals("clam")){
      pizza = new P3ClamPizza();
    } 
    return pizza;
   }
}