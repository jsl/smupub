package com.j2.u201411196.p3;

public class P3Main{
  public static void main(String[] args){
    P3SimpleFactory factory = new P3SimpleFactory();
    P3PizzaStore store = new P3PizzaStore(factory);
    P3Pizza pizza1 = store.orderPizza("cheese");
    System.out.println("We ordered a "+ pizza1.getName());
    P3Pizza pizza2 = store.orderPizza("veggie");
    System.out.println("We ordered a "+ pizza2.getName());
    P3Pizza pizza3 = store.orderPizza("clam");
    System.out.println("We ordered a "+ pizza3.getName());
  }
}