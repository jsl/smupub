package com.j2.u201411196.p3;

public class P3PizzaStore{
  P3SimpleFactory factory;
  public P3PizzaStore(P3SimpleFactory factory){
    this.factory = factory;
  }
  public P3Pizza orderPizza(String type){
    P3Pizza pizza = factory.createPizza(type);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
}
