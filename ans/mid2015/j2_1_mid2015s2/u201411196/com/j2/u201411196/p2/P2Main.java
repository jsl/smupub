package com.j2.u201411196.p2;

public class P2Main{
  public static void main(String[] args){
    P2WeatherData weatherdata1 = new P2WeatherData();
    P2StatisticalDisplay sd = new  P2StatisticalDisplay(weatherdata1);
    System.out.println("Statistical Display");
    weatherdata1.setMeasurements(80,65);
    P2WeatherData weatherdata2 = new P2WeatherData();
    P2ForecastingDisplay fd = new P2ForecastingDisplay(weatherdata2);
    System.out.println("Forecasting Display");
    weatherdata2.setMeasurements(82,70);
  }
}