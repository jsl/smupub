package com.j2.u201411196.p2;

public interface P2Observer{
  public abstract void update(float temp,float hum);
}
