package com.j2.u201411196.p2;
import java.util.*;

public class P2WeatherData implements P2Subject{
  float temp, hum;
  ArrayList <P2Observer> observers;
  
  public P2WeatherData(){
    observers = new ArrayList();
  }
  public void registerObserver(P2Observer o){
    observers.add(o);
  }
  
  public void removeObserver(P2Observer o){
    int i = observers.indexOf(o);
    if(i >=0) observers.remove(i);
  }
  public void notifyObservers(){
    for(int i=0; i<observers.size(); i++){
      P2Observer observer = (P2Observer) observers.get(i);
      observer.update(temp,hum);
    }
  }
  public void setMeasurements(float temp, float hum){
    this.temp = temp;
    this.hum = hum;
    measurementsChanged();
  }
  public void measurementsChanged(){
    notifyObservers();
  }
}

    