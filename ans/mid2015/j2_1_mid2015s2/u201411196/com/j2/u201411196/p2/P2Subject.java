package com.j2.u201411196.p2;

public interface P2Subject{
  public abstract void registerObserver(P2Observer o);
  public abstract void removeObserver(P2Observer o);
  public abstract void notifyObservers();
}
