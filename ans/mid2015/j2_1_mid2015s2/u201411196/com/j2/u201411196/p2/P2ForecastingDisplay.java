package com.j2.u201411196.p2;

public class P2ForecastingDisplay implements P2Observer{
  P2Subject weatherdata;
  float temp, hum;
  public P2ForecastingDisplay(P2Subject weatherdata){
    this.weatherdata = weatherdata;
    weatherdata.registerObserver(this);
  }
  public void update(float temp,float hum){
    this.temp = temp;
    this.hum = hum;
    display();
  }
  public void display(){
     System.out.println("Current conditions: " + temp + "F degrees and "+ hum + "% humidity");
  }
}
   