abstract class CondimentConditionMain()
interface Display()
interface Observer()
class WeatherData() {
	setMeasurements()
}
class StatisticalDisplay()
class ForecastingDisplay()

CondimentConditionMain <|-under- Display
CondimentConditionMain <-right- WeatherData
Display <-right- StatiscalDisplay
Display <-right- ForecastingDisplay