package com.j2.u201211261.p2;

public abstract class CondimentConditionMain {
  
  WeatherData weatherdata;
  
  float Temperature;
  float Humidity;
  
  public interface Observer(o);
  public void setMeasurements();
  
  weatherData.setMeasurements(80,65);
  weatherData.setMeasurements(82,70);
  
  public interface Display(Observer o);
 
} 
  