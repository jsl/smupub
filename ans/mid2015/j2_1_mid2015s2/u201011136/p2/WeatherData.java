package com.j2.u201011136.p2;
import java.util.*;

public class WeatherData implements Subject{
  float temp, humid;
  ArrayList observers = new ArrayList();
  
  public void registerObserver(Observer o) {
    observers.add(o);
  }
  
  public void removeObserver(Observer o) {
    int i = observers.indexOf(o);
    
    if(i!=-1) {
      observers.remove(o);
    }
  }
  
  public void notifyObservers() {
    for(int i = 0; i < observers.size(); i++) {
      Observer observer = (Observer)observers.get(i);
      observer.update(temp, humid);
    }
  }
  
  public void setMeasurements(float temp, float humid) {
    this.temp = temp;
    this.humid = humid;
    
    notifyObservers();
  }
    
}