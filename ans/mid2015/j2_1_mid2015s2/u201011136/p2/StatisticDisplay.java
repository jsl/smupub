package com.j2.u201011136.p2;

public class StatisticDisplay implements Observer {
  float temp;
  float humid;
  Subject subject;
  
  public StatisticDisplay(Subject subject) {
    this.subject = subject;
    subject.registerObserver(this);
  }
  
  public void update(float temp, float humid) {
    this.temp = temp;
    this.humid = humid;
    System.out.println("StatisticDisplay");
    System.out.println("Current conditions = " + temp + "degree and " + humid + "humidity");
  }
}