package com.j2.u201011136.p2;

public interface Observer {
  public void update(float temp, float humid);
}