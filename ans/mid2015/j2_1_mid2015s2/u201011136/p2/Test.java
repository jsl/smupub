package com.j2.u201011136.p2;

public class Test {
  public static void main(String[] args) {
    WeatherData weatherData = new WeatherData();
    StatisticDisplay dis1 = new StatisticDisplay(weatherData);
    ForecastingDisplay dis2 = new ForecastingDisplay(weatherData);
    
    weatherData.setMeasurements(80, 65);
  }
}