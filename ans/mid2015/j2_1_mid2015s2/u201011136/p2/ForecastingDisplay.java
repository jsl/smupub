package com.j2.u201011136.p2;

public class ForecastingDisplay implements Observer {
  float temp;
  float humid;
  Subject subject;
  
  public ForecastingDisplay(Subject subject) {
    this.subject = subject;
    subject.registerObserver(this);
  }
  
  public void update(float temp, float humid) {
    this.temp = temp;
    this.humid = humid;
    System.out.println("ForecastingDisplay");
    System.out.println("Current conditions = " + temp + "degree and " + humid + "humidity");
  }
}