package com.j2.u201011136.p4;

public abstract class Computer {
  String des;
  int cost;
  
  public String getDiscription() {
    return des;
  }
  
  public abstract int getCost();
}