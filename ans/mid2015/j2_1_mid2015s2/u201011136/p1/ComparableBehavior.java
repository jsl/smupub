package com.j2.u201011136.p1;

public interface ComparableBehavior {
  public int compare(Object o1, Object o2);
}