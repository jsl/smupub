package com.j2.u201011136.p1;

public class CompareInt implements ComparableBehavior {
  public int compare(Object o1, Object o2) {
    int n1 = (int)o1;
    int n2 = (int)o2;
    
    return n1 - n2;
  }
}