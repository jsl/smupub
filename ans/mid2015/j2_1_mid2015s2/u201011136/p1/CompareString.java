package com.j2.u201011136.p1;

public class CompareString implements ComparableBehavior {
  public int compare(Object o1, Object o2) {
    String s1 = (String)o1;
    String s2 = (String)o2;
    
    return s1.compareTo(s2);
  }
}