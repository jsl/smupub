package com.j2.u201011136.p1;

public class Sorter {
  char[] str = {'h','e','l','l','o','s','m','u','n','i'};
  int[] num = {1,1,0,7,4,3};
  
  public void performCompareString(ComparableBehavior cb) {
    /*
    char temp = 'a';
    
    for(int i = 0; i < str.length; i++) {
      for(int j  = 0; j < str.length-i-1; j++) {
        if(cb.compare(str[j], str[j+1]) > 0) {
        temp = str[j];
        str[j] = str[j+1];
        str[j+1] = temp;
      }
      }
    }
    
    for(int i = 0; i < str.length; i++) {
      System.out.print(str[i] + " ");
    }*/
  }
  
  public void performCompareInt(ComparableBehavior cb) {
    int temp = 0;
    
    for(int i = 0; i < num.length; i++) {
      for(int j  = 0; j < num.length-i-1; j++) {
        if(cb.compare(num[j], num[j+1]) > 0) {
        temp = num[j];
        num[j] = num[j+1];
        num[j+1] = temp;
      }
      }
    }
    
    for(int i = 0; i < num.length; i++) {
      System.out.print(num[i] + " ");
    }
  }
}