package com.j2.u201011136.p3;

public class Test {
  public static void main(String[] args) {
    PizzaStore store = new PizzaStore();
    store.orderPizza("cheese");
    store.orderPizza("veggie");
    store.orderPizza("clam");
  }
}