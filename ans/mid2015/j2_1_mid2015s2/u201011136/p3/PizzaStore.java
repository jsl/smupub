package com.j2.u201011136.p3;

public class PizzaStore {
  SimpleFactory factory = new SimpleFactory();
  
  public void orderPizza(String type) {
    Pizza pizza = factory.createPizza(type);
    pizza.prepare();
    pizza.baking();
    pizza.cutting();
    pizza.boxing();
    System.out.println("we oredered " + pizza.name);
  }
}