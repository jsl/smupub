package com.j2.u201011098.p3;

public class Pizza {
  String name;
  
  void prepare(){
    System.out.println("preparing...");
  }
  
  void bake(){
    System.out.println("Baking...");
  }
   
  void cut(){
    System.out.println("cutting...");
  }
  
  void box(){
    System.out.println("boxing...");
  }
  
}