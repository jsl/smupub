package com.j2.u201411227.p1;

public class ChocolateBoiler{
  private boolean empty;
  private boolean boiled;
  private ChocolateBoiler uniqueInstance;
  
  public ChocolateBoiler(){
    empty=false;
    boiled=false;
  }
  
  public ChocolateBoiler getInstance(){
    if(uniqueInstance==null){
      ChocolateBoiler choco=new ChocolateBoiler();
      uniqueInstance=choco;
    }
    return uniqueInstance;
  }
  
  public boolean isEmpty(){
    return empty;
  }
  public boolean isBoiled(){
    return boiled;
  }
  
  public void drain(){
    if(!isEmpty()&&isBoiled()){
      System.out.println("Drain");
    }
  }
  
  public void boil(){
    if(!isEmpty()&isBoiled()){
      boiled=true;
      System.out.println("Boil");
    }
  }
}