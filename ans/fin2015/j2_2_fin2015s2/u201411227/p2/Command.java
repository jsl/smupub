package com.j2.u201411227.p2;

public interface Command{
  public void execute();
}