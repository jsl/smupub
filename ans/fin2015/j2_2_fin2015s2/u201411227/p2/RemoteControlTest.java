package com.j2.u201411227.p2;

public class RemoteControlTest{
  public static void main(String[] args){
    GarageDoorOpen open=new GarageDoorOpen();
    GarageDoorClose close=new GarageDoorClose();
    RemoteControl r=new RemoteControl();
    
    r.setCommand(open,close);
    r.onButtonWasPressed();
    r.offButtonWasPressed();
  }
}