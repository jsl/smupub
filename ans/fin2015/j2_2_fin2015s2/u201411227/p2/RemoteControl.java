package com.j2.u201411227.p2;

public class RemoteControl{
  Command on;
  Command off;
  
  public RemoteControl(){}
  public void setCommand(Command on, Command off){
    this.on=on;
    this.off=off;
  }
  public void onButtonWasPressed(){
    on.execute();
  }
  public void offButtonWasPressed(){
    off.execute();
  }
}