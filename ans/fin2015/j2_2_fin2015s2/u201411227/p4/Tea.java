package com.j2.u201411227.p4;

public class Tea extends Caffeine{
  public Tea(){
    System.out.println("Making Tea");
  }
  public void brew(){
    System.out.println("Steeping the tea");
  }
  public void addCondiments(){
    System.out.println("Adding Lemon");
  }
}