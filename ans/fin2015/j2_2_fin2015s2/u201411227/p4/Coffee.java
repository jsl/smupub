package com.j2.u201411227.p4;

public class Coffee extends Caffeine{
  public Coffee(){
    System.out.println("Making Coffee");
  }
  public void brew(){
    System.out.println("Brewing Coffee");
  }
  public void addCondiments(){
    System.out.println("Adding milk and sugar");
  }
}