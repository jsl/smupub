package com.j2.u201411227.p4;

public abstract class Caffeine{
  public void prepare(){
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  public void boilWater(){
    System.out.println("Boiling Water");
  }
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }
  abstract void brew();
  abstract void addCondiments();
}