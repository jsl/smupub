package com.j2.u201311257.p2;

public class GarageDoorOpenCommand implements Command {
  GarageDoor gd;
  
  public GarageDoorOpenCommand(GarageDoor gd) {
    this.gd = gd;
  }
  
  public void execute(){
    gd.open();
  }
}
