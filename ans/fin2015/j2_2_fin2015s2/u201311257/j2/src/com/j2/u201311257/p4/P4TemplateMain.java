package com.j2.u201311257.p4;

public class P4TemplateMain {
  public static void main(String[] args) {
    Tea tea = new Tea();
    
    System.out.println("Making tea...");
    tea.prepare();
  }
}