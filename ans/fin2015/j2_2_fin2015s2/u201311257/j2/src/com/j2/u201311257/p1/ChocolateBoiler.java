package com.j2.u201311257.p1;

public class ChocolateBoiler {
  private boolean isEmpty;
  private boolean isBoiled;
  private ChocolateBoiler uniqueInstance = new ChocolateBoiler();
  //private int num=0;
  
  public ChocolateBoiler() {
    isEmpty = true;
    isBoiled = false;
  }
  
  public ChocolateBoiler getInstance() {
      
      return uniqueInstance;
    }
    
 
  public void boil() {
    if(!IsEmpty() && !IsBoiled()){
      System.out.println("boil Chocolate");
      isBoiled = true;
    }
  }
  public void drain() {
    if(!IsEmpty() && IsBoiled()){
      System.out.println("drain Chocolate");
      isEmpty = true;
    }
  }
  public void fill() {
    if(IsEmpty()){
      System.out.println("fill Chocolate");
      isEmpty = false;
    }
  }
  public boolean IsEmpty() {
    return isEmpty;
  }
  public boolean IsBoiled() {
    return isBoiled;
  }
}