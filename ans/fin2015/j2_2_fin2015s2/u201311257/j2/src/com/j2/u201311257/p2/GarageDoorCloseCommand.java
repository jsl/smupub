package com.j2.u201311257.p2;

public class GarageDoorCloseCommand implements Command {
  GarageDoor gd;
  
  public GarageDoorCloseCommand(GarageDoor gd) {
    this.gd = gd;
  }
  
  public void execute(){
    gd.close();
  }
}
