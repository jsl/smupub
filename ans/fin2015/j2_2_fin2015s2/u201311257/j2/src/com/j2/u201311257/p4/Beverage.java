package com.j2.u201311257.p4;

public abstract class Beverage {
  
  public void boilWater(){}
  public void brew(){}
  public void pourInCup(){}
  public void addCondiments(){}
  public void prepare() {
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
}