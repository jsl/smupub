package com.j2.u201311257.p2;

public class RemoteControl{
  Command command;
  
  public RemoteControl() {
    
  }
  
  public void setCommand(Command command) {
    this.command = command;
  }
  
  public void buttonWasPressed(){
    command.execute();
  }
}
