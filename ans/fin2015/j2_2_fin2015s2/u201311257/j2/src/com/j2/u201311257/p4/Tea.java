package com.j2.u201311257.p4;

public class Tea extends Beverage {
  public Tea() {
  }
  public void boilWater() {
    System.out.println("Boiling Water");
  }
  public void brew(){
    System.out.println("Steeping the tea");
  }
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }
  public void addCondiments(){
    System.out.println("Adding Lemon");
  }
}