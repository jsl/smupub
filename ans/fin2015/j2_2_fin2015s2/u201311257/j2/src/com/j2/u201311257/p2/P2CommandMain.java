package com.j2.u201311257.p2;

public class P2CommandMain{
  public static void main(String[] args) {
    RemoteControl remote = new RemoteControl();
    GarageDoor gd = new GarageDoor();
    GarageDoorOpenCommand gdO = new GarageDoorOpenCommand(gd);
    GarageDoorCloseCommand gdC = new GarageDoorCloseCommand(gd);
  
    remote.setCommand(gdO);
    remote.buttonWasPressed();
    remote.setCommand(gdC);
    remote.buttonWasPressed();
  }
}
