package com.j2.u201311257.p2;

public interface Command {
  public void execute();
}
