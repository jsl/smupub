package com.j2.u201285091.p2

public class GarageDoor{
  public void setCommand(){
    System.out.println("Garage door is up");
  }
  public void buttonWasPressed(){
    System.out.println("Garage door is down");
  }
  public static void main(String[] args){
    GarageDoor ga = new GarageDoor();
    ga.setCommand();
    ga.buttonWasPressed();
  }
}
