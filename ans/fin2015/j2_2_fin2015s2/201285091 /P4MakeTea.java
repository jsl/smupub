package com.j2.u201285091.p4
  public class MakeTea{
  public void boilWater(){
    System.out.println("Boiling water");
  }
  public void brew(){
    System.out.println("Steep the tea");
  }
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }
  public void addCondiments(){
    System.out.println("Adding lemon")
  }
  public static void main(String[] args){
    MakeTea mt = new MakeTea();
    System.out.println("Making tea...")
    mt.boilWater();
    mt.brew();
    mt.pourInCup();
    mt.addCondiments();
  }
  
}