package com.j2.u201211305.p2;

public class GarageDoorCloseCommand  {
  GarageDoor garagedoor;
  public GarageDoorCloseCommand(GarageDoor garagedoor){
    this.garagedoor = garagedoor;
  }
  public void execute(){
    garagedoor.close();
  }
}