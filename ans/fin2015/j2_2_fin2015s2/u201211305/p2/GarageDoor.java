package com.j2.u201211305.p2;

public class GarageDoor {
  public void open() {
    System.out.println("Garage Door is up");
  }
  public void close() {
    System.out.println("Garage Door is down");
  }
}