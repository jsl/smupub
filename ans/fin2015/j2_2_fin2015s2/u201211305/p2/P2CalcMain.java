package com.j2.u201211305.p2;

public class P2CalcMain {
  public static void main(String args[]) {
    GarageDoor garagedoor = new GarageDoor();
    GarageDoorOpenCommand GDOpen = new  GarageDoorOpenCommand(garagedoor);
    GarageDoorCloseCommand GDClose = new GarageDoorCloseCommand(garagedoor);
    RemoteControl remote = new RemoteControl();
    
    remote.execute();
    }
  }
