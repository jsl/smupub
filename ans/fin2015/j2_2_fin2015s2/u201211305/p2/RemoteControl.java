package com.j2.u201211305.p2;

public class RemoteControl {
  Command slot;
  GarageDoor garagedoor;
  GarageDoorOpenCommand GDOpen;
  GarageDoorCloseCommand GDClose;
  public void GarageDoorOpenCommand (GarageDoorOpenCommand GDOpen) {
    this.GDOpen = GDOpen;
  }
  public void GarageDoorCloseCommand (GarageDoorCloseCommand GDClose) {
    this.GDClose = GDClose;
  }
  public void execute() {
    GDOpen.execute();
    GDClose.execute();
  }
  public void buttonWasPressed() {
    slot.execute();
  }
}
    