package com.j2.u201211305.p2;

public interface Command {
  public void execute();
}