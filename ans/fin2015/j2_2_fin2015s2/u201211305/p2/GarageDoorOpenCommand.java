package com.j2.u201211305.p2;

public class GarageDoorOpenCommand {
  GarageDoor garagedoor;
  public GarageDoorOpenCommand(GarageDoor garagedoor){
    this.garagedoor = garagedoor;
  }
  public void execute(){
    garagedoor.open();
  }
}