package com.j2.u201211305.p4;

public class P4CalcMain {
  public static void main(String args[]){
    Tea tea = new Tea();
    Barista barista = new Barista(tea);
    
    barista.makeTea();
  }
}