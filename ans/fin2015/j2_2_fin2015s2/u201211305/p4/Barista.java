package com.j2.u201211305.p4;

public class Barista {
  Tea tea;
  public Barista(Tea tea)
  {
    this.tea = tea;
  }
  public void makeTea() {
    System.out.println("Making Tea,,,");
    tea.boilWater();
    tea.brew();
    tea.pourInCup();
    tea.addCondiments();
  }
}
    