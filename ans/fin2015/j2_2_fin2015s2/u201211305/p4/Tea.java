package com.j2.u201211305.p4;

public class Tea implements prepareRecipe{
  public void boilWater() {
    System.out.println("Boiling Water");
  }
  public void brew() {
    System.out.println("Steepig the tea");
  }
  public void pourInCup() {
    System.out.println("Pouring into a cup");
  }
  public void addCondiments() {
    System.out.println("Adding Lemon");
  }
}
  