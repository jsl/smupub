package com.j2.u201211305.p4;

public interface prepareRecipe {
  public abstract void boilWater(); 
  public abstract void pourInCup(); 
  public abstract void brew(); 
  public abstract void addCondiments();
}