package com.j2.u201211310.p2;
public class doorDownCommand implements Command{
 GarageDoor door;
  
  public doorDownCommand(GarageDoor d){
    this.door=d;    
  }
  
  public void execute(){
    door.doorDown();
  }
  
}