package com.j2.u201211310.p3;

public interface Iterator{
 public Object next();
 public boolean hasNext();
  
}