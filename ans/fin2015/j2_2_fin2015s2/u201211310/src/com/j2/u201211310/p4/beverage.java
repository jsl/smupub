package com.j2.u201211310.p4;

public abstract class beverage{
  public String des;
  
  public final void prepare(){
  System.out.println("Making " + des);
  boilWater();
  brew();
  pourInCup();
  addCondiments();
  }
  public void boilWater(){
  System.out.println("Boiling water");
  }
  
  public void pourInCup(){
  System.out.println("Pouring into cup");
  }
  
  public abstract void brew();
  public abstract void addCondiments();
  
}