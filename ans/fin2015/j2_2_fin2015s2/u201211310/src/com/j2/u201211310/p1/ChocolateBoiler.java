package com.j2.u201211310.p1;

public class ChocolateBoiler{
  private static ChocolateBoiler C=new ChocolateBoiler();
  private boolean empty=true;
  private boolean boil=false;
  
  private ChocolateBoiler(){};
  
  
  public static ChocolateBoiler getInstance(){
    return C;
  }
  public void pour(){ //채워넣는것
    if(isEmpty()){
    System.out.println("pouring in a cup"); 
    empty = false;}
    else
       System.out.println("poured in a cup already");
  }
  public void drain(){
    if(!isEmpty()&&isBoiled()){
     System.out.println("Drain in cup"); 
   empty = true;
   boil = false;
    }
  }
  public void boil(){
    if(!isEmpty()){ 
    System.out.println("boiling water"); 
    boil = true; }
    else{
     System.out.println("cup is empty, pour water first!!! "); 
    }
  }
  public boolean isBoiled(){
    if(boil){
      return true;
    }
    else
      return false;
  }
  public boolean isEmpty(){
    if(empty){
      return true;}
    else
     return false;
  }
  
}