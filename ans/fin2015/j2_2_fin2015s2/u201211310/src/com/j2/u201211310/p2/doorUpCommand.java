package com.j2.u201211310.p2;
public class doorUpCommand implements Command{
 GarageDoor door;
  
  public doorUpCommand(GarageDoor d){
    this.door=d;    
  }
  
  public void execute(){
    door.doorUp();
  }
  
}