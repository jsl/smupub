package com.j2.u201411230.p4;
  
  public abstract class P4CaffeinBeverage{
  public void prepareRecipe(){
   boilWater();
   brew();
   pourInCup();
   addCondiments();
    
  }
  public void boilWater(){
    System.out.println("Boiling water");
    
  }
  public  abstract void brew();
  
  public void pourInCup() {
    System.out.println("Pouring into cup");
  }
  
  public abstract void addCondiments();
    
    





}