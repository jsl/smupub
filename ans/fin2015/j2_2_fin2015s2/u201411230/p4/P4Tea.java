package com.j2.u201411230.p4;

public class P4Tea extends P4CaffeinBeverage 
{
  
  public P4Tea() {
   System.out.println("Making tea");
    
  }
  public void brew(){
   
    System.out.println("Steeping the tea");
  }
  
  public void addCondiments(){
   System.out.println("Adding Lemon");
    
  }
  
  
}