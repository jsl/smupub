package com.j2.u201411230.p2;

public class P2GarageDoorOffCommand implements P2Command{
  P2GarageDoor garagedoor;
  
  public P2GarageDoorOffCommand(P2GarageDoor garagedoor){
    this.garagedoor=garagedoor;
  }
  
  public void execute() {
    garagedoor.down();
  }
  
  
}
