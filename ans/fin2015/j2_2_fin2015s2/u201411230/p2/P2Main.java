package com.j2.u201411230.p2;

public class P2Main{
  public static void main(String[] args){
  
    P2GarageDoor G1=new P2GarageDoor();
    
   P2Command G2=new P2GarageDoorOnCommand(G1);
   P2Command G3=new P2GarageDoorOffCommand(G1);
    
    P2RemoteControl remote = new P2RemoteControl();
    remote.setCommand(G2);
    remote.buttonWasPressed();
    remote.setCommand(G3);
    remote.buttonWasPressed();


    
    
    
  }



}