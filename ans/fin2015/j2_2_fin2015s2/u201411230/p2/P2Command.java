package com.j2.u201411230.p2;

public interface P2Command{
  public void execute();
}