package com.j2.u201411230.p3;

public interface P3Iterator{
  boolean hasNext();
  Object next();
}