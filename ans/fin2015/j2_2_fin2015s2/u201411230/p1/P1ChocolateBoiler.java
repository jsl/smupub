package com.j2.u201411230.p1;

 public class P1ChocolateBoiler {
  
  private boolean empty;
  private boolean boiled;
  
  int numCalled=0;
  public P1ChocolateBoiler uniqueInstance=new P1ChocolateBoiler();
  
  
  private P1ChocolateBoiler(){
    empty=true;
    boiled=false;
  }
 
  public P1ChocolateBoiler getInstance() {
   
      System.out.println("numcalled : "+numCalled++);
      return uniqueInstance;
    
  
  }
  public void fill() {
    if(isEmpty()){
      empty=false;
      boiled=false;
    }
      
  }
  public void drain() {
    if(!isEmpty()&&isBoiled()){
      empty=true;
    }
  }
  
  public void boil(){
    if(!isEmpty()&&!isBoiled()){
      boiled=true;
    }
  }
  public boolean isEmpty(){
   return empty; 
  }
  public boolean isBoiled(){
    return boiled;
  }
  
    
  
  
  
}