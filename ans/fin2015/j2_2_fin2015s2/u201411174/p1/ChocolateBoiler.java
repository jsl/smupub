package com.j2.u201411174.p1;

public class ChocolateBoiler {
  private boolean empty;
  private boolean boiled;
  private static ChocolateBoiler uniqueInstance = new ChocolateBoiler();
  private ChocolateBoiler() {
    empty = false;
    boiled = false;
  }
  public static ChocolateBoiler getInstance() {
      return uniqueInstance;
  }
  public boolean isEmpty () {
    if(getEmpty()==true){
      return true;
    }
    else
      return false;
  }
  public boolean isBoiled() {
    if(getBoiled()==true){
      return true;}
    else
      return false;
  }
  public void drain() {
    if (!isBoiled() && !isEmpty()) {
      System.out.println("Drain for Chocolate");
    }
  }
  public boolean getEmpty(){
    return empty;
  }
  public boolean getBoiled(){
    return boiled;
  }
}