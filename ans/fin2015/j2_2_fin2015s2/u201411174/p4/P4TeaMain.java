package com.j2.u201411174.p4;

public class P4TeaMain {
  public static void main(String args[]){

    Tea tea = new Tea();
    System.out.println("Making tea...");
    tea.prepareBeverage();
  }
}