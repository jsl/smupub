package com.j2.u201411174.p4;

public abstract class CaffeinBeverage{
  final void prepareBeverage(){
    waterBoil();
    brew();
    pourInCup();
    addCondiments();
  }  
  public void waterBoil() {
    System.out.println("Boiling water");
  }
  public void pourInCup() {
    System.out.println("Pouring into cup");
  }
  public abstract void addCondiments();
  public abstract void brew();
}