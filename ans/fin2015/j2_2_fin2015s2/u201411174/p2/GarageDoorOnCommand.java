package com.j2.u201411174.p2;

public class GarageDoorOnCommand implements Command{
  GarageDoor garageDoor;
  public GarageDoorOnCommand(GarageDoor garageDoor){
    this.garageDoor = garageDoor;
  }
  public void execute() {
    garageDoor.on();
  }

}