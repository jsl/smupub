package com.j2.u201411174.p2;

public interface Command {
  public void execute();
}