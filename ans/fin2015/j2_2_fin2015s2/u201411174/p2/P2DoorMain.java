package com.j2.u201411174.p2;

public class P2DoorMain{
  public static void main(String args[]){
    GarageDoor gd = new GarageDoor();
    GarageDoorRemote remote = new GarageDoorRemote(gd);
    GarageDoorOffCommand off = new GarageDoorOffCommand(gd);
    GarageDoorOnCommand on = new GarageDoorOnCommand(gd);

    remote.setCommand(gd,on,off);
    remote.onButtonWasPressed();
    remote.offButtonWasPressed();
  }
}