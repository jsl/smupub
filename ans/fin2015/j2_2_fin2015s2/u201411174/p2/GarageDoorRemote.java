package com.j2.u201411174.p2;

public class GarageDoorRemote{
  GarageDoor garageDoor;
  GarageDoorOnCommand garageDoorOnCommand;
  GarageDoorOffCommand garageDoorOffCommand;
  
  public GarageDoorRemote(GarageDoor garageDoor){
    this.garageDoor = garageDoor;
  }
  public void setCommand(GarageDoor garageDoor, GarageDoorOnCommand garageDoorOnCommand,GarageDoorOffCommand garageDoorOffCommand) {
    
    this.garageDoorOnCommand = garageDoorOnCommand;
    this.garageDoorOffCommand = garageDoorOffCommand;
  }
  
  public void onButtonWasPressed() {
    garageDoorOnCommand.execute();
  }
  public void offButtonWasPressed() {
    garageDoorOffCommand.execute();
  }
}