package com.j2.u201411174.p2;

public class GarageDoorOffCommand implements Command{
  GarageDoor garageDoor;
  
  public GarageDoorOffCommand(GarageDoor garageDoor){
    this.garageDoor = garageDoor;
  }
  public void execute() {
    garageDoor.off();
  }

}