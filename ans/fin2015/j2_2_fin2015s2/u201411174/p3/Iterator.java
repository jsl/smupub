package com.j2.u201411174.p3;
import java.util.*;

public interface Iterator{
  public void HasNext();
  public Object Next();
}