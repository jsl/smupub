package com.j2.u201211253.p2;

public interface Command {
  
  public void execute();
 
}