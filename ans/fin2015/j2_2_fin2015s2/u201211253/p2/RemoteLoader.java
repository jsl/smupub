package com.j2.u201211253.p2;

public class RemoteLoader{
  public static void main(String[] args){
    RemoteControl remoteControl = new  RemoteControl();
    GargeDoor door  = new GargeDoor("Garge");
    GargeDoorOnCommand doorOn = new GargeDoorOnCommand(door);
    GargeDoorOffCommand doorOff = new GargeDoorOffCommand(door);
    
    Command []  partyOn = {doorOn};
   Command [] partyOff = {doorOff};
    
    MacroCommand partyOnCommand = new MacroCommand(partyOn);
    MacroCommand partyOffCommand = new MacroCommand(partyOn);
    
    remoteControl.setCommand(0,partyOnCommand,partyOffCommand);
    
    remoteControl.ButtonWasPressed(0);
    remoteControl.ButtonWasNotPressed(0);
  }
}