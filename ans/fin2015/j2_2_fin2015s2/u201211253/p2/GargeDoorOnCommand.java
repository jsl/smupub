package com.j2.u201211253.p2;

public class GargeDoorOnCommand implements Command{
  int level;
 GargeDoor door;
  public GargeDoorOnCommand(GargeDoor door){
    this.door = door;
  }
  public void execute(){
    level = door.getLevel();
    door.on();
    
  }
}
    