package com.j2.u201211253.p2;

public class GargeDoorOffCommand implements Command{
  int level;
   GargeDoor door;
  public GargeDoorOffCommand(GargeDoor door){
    this.door = door;
  }
  public void execute(){
    level = door.getLevel();
    door.off();
      
  }
  }

    