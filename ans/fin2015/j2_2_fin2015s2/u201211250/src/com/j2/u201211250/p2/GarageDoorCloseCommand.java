package com.j2.u201211250.p2;

public class GarageDoorCloseCommand implements Command{
  GarageDoor garageDoor;
  
  public GarageDoorCloseCommand(GarageDoor door){
    this.garageDoor=door;
  }
  
  public void excute(){
    garageDoor.down();
  }


}