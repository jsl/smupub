package com.j2.u201211250.p2;

public class GarageDoorOpenCommand implements Command{
  GarageDoor garageDoor;
  
  public GarageDoorOpenCommand(GarageDoor door){
    this.garageDoor=door;
  }
  
  public void excute(){
    garageDoor.up();
  }


}