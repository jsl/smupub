package com.j2.u201211250.p2;

public class RemoteControl{
  Command slot;
  
  public RemoteControl() {}
  
  public void setCommand(Command c)
  {
    slot=c;
  }
  
  public void buttonWasPressed()
  {
    slot.excute();
  }
}