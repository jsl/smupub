package com.j2.u201211250.p4;

public abstract class CaffeinBeverage{
  public void prepare(String condiment){
    boilWater();
    brew();
    pourInCup();
    addCondiments(condiment);
  }
  
  public void boilWater(){
    System.out.println("Boiling water");
  }
  
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }
  
  public abstract void brew();
  
  public abstract void addCondiments(String condiment);
}
 
  
