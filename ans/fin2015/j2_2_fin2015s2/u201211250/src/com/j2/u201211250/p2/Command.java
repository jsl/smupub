package com.j2.u201211250.p2;

public interface Command{
  public void excute();
}