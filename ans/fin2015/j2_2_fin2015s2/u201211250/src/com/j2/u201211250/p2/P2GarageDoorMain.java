package com.j2.u201211250.p2;

public class P2GarageDoorMain{
  public static void main(String args[]){
    RemoteControl remote=new RemoteControl();
    GarageDoor garage=new GarageDoor();
    GarageDoorOpenCommand garageOpen=new GarageDoorOpenCommand(garage);
    GarageDoorCloseCommand garageClose=new GarageDoorCloseCommand(garage);
    
    remote.setCommand(garageOpen);
    remote.buttonWasPressed();
    remote.setCommand(garageClose);
    remote.buttonWasPressed();
  }
}