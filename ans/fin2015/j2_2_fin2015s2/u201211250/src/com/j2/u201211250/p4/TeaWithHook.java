package com.j2.u201211250.p4;

public class TeaWithHook extends CaffeinBeverage{
  
  public void brew()
  {
    System.out.println("Steeping the tea");
  }
  
  public void addCondiments(String condiment){
    String condi=condiment;
    System.out.println("Adding "+condi);
  
  }
  
}