package com.j2.u201211250.p1;
  
  public class ChocolateBoiler{
  private boolean empty;
  private boolean boiled;
  private static ChocolateBoiler uniqueInstance;
  
  public ChocolateBoiler(){
    empty=true;
    boiled=false;
  }
  
  public static ChocolateBoiler getInstance(){
    if(uniqueInstance==null)
    {
      System.out.println("Creating unique instance of ChocolateBoiler...");
      uniqueInstance=new ChocolateBoiler();
    }
    System.out.println("Unique instance of ChocolateBoiler is now created!");
    return uniqueInstance;
  }
  
  public boolean isEmpty(){
    return empty;
  }
  
  public boolean isBoiled(){
    return boiled;
  }
  
  public void fill(){
    if(isEmpty())
    {
      System.out.println("ChocolateBoiler is filling");
      empty=false;
    }
  }
  
  public void boiling(){
    if(!isEmpty()&&!isBoiled())
    {
      System.out.println("ChocolateBoiler is boiling");
      boiled=true;
    }
  }
  
  public void drain(){
    if(!isEmpty()&&isBoiled())
    {
      System.out.println("ChocolateBoiler is draining");
      empty=true;
      boiled=false;
    }
  }
  }