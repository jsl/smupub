package com.j2.u201211250.p1;
  
  public class P1ChocolateBoilerMain{
  public static void main(String args[]){
    ChocolateBoiler chocolateBoiler1=ChocolateBoiler.getInstance();
    chocolateBoiler1.fill();
    chocolateBoiler1.boiling();
    chocolateBoiler1.drain();
  }
}
    