package com.j2.u201211260.p2;

public interface Command{
  public void execute();
}