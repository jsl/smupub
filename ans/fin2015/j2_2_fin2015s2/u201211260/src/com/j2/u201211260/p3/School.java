package com.j2.u201211260.p3;

import java.util.*;

public class School{
  ArrayList array;
  
  public School(){
    array= new ArrayList();
  }
  
  public void setSubject(Subject subject){
    array.add(subject);
  }
  
  public void print(){
    ArrayListIterator it = new ArrayListIterator(array);
    while(it.hasnext()){
      System.out.println(it.next());
    }
  }
}