package com.j2.u201211260.p3;

public class p3main{
  public static void main(String args[]){
    School sc= new School();
    sc.setSubject(new Subject("이산수학"));
    sc.setSubject(new Subject("알고리즘"));
    sc.setSubject(new Subject("자바2"));
    sc.setSubject(new Subject("신호해석"));
    sc.setSubject(new Subject("교향곡의 이해"));
    sc.setSubject(new Subject("융합소프트웨어 개발"));
    sc.setSubject(new Subject("컴퓨터 구조"));
    sc.print();
  }
}