package com.j2.u201211260.p1;

public class ChocolateBoiler{
  private static ChocolateBoiler cb = new ChocolateBoiler(); 
  
  private boolean empty;
  private boolean boiled;
  
  private ChocolateBoiler(){
    empty = true;
    boiled = false;
  }
  public static ChocolateBoiler getInstance(){
    return cb;
  }
  
  private boolean isEmpty(){
    return empty;
  }
  
  private boolean isBoiled(){
    return boiled;
  }
  
  public void drain(){
    if(isEmpty()&&isBoiled()){
      System.out.println(" drain  !");
    }
  }
}
    
      

