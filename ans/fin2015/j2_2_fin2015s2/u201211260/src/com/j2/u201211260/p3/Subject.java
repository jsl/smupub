package com.j2.u201211260.p3;

public class Subject{
  String name;
  
  public Subject(String name){
    this.name =name;
  }
  
  public String toString(){
    return "Subject " + name;
  }
}