package com.j2.u201211260.p2;

public class GarageCloseCommand implements Command{
  GarageDoor gd;
  
  public GarageCloseCommand(GarageDoor gd){
    this.gd = gd;
  }
  
  public void execute(){
    gd.closeDoor();
  }
}