package com.j2.u201211260.p2;

public class GarageOpenCommand implements Command{
  GarageDoor gd;
  
  public GarageOpenCommand(GarageDoor gd){
    this.gd = gd;
  }
  
  public void execute(){
    gd.openDoor();
  }
}