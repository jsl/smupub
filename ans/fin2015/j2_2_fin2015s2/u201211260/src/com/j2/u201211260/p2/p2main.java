package com.j2.u201211260.p2;

public class p2main{
  public static void main(String args[]){
    GarageDoor gd = new GarageDoor();
    Command open = new GarageOpenCommand(gd);
    Command off = new GarageCloseCommand(gd);
    
    RemoteControl rc = new RemoteControl();
    rc.setCommand(0,open,off);
    rc.onButtonWasPressed(0);
    rc.offButtonWasPressed(0);
  }
}