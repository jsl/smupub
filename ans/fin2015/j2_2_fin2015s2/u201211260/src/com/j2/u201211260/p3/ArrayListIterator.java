package com.j2.u201211260.p3;

import java.util.*;

public class ArrayListIterator{
  ArrayList array;
  int num;
  public ArrayListIterator(ArrayList array){
    this.array= array;
  }
  public boolean hasnext(){
    if(num>=array.size())
      return false;
    else
      return true;
  }
  
  public Object next(){
    return array.get(num++);
  }
}
