package com.j2.u201211260.p4;

public class Tea{
  
  public final void prepareRecipe(){
    System.out.println("Making tea..");
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  public void boilWater(){
    System.out.println("Boiling water");
  }
    
  public void brew(){
    System.out.println("Steeping the tea");
  }
  
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }

  public void addCondiments(){
    System.out.println("Adding Lemon");
  }
}