package com.j2.u201211260.p2;

public class RemoteControl{
  Command[] onCommand;
  Command[] offCommand;
  int slot;
  
  public RemoteControl(){
    onCommand = new Command[7];
    offCommand = new Command[7];
  }
  
  public void setCommand(int slot,Command open,Command off){
    onCommand[slot] = open;
    offCommand[slot] = off;
  }
  
  public void onButtonWasPressed(int slot){
    onCommand[slot].execute();
  }
  
  public void offButtonWasPressed(int slot){
    offCommand[slot].execute();
  }
}
    