package com.j2.u201211293.p2;

public class GarageDoorOnCommand implements Command{
  GarageDoor garage;
  
  public GarageDoorOnCommand(GarageDoor garage){
    this.garage = garage;
  }
  
  public void execute(){
    garage.up();
  }
}