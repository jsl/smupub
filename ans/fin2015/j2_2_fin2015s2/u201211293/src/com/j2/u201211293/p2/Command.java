package com.j2.u201211293.p2;

public interface Command{
  public void execute();
}