package com.j2.u201211293.p3;
import java.util.ArrayList;
public class SubjectIterator implements Iterator{
  ArrayList A;
  int position=0;
  public SubjectIterator(ArrayList A){
    this.A = A;
  }
  
  public boolean hasNext(){
    if(position>=A.size()){
      return false;
    }
    else{
      return true;
    }
  }
  
  public Object next(){
    Object object = A.get(position);
    position++;
    return object;
  }
}