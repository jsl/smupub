package com.j2.u201211293.p3;

public interface Iterator{
  public boolean hasNext();
  public Object next();
}