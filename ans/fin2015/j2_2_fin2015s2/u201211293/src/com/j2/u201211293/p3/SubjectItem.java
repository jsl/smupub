package com.j2.u201211293.p3;

public class SubjectItem {
  String n;
  int a;
  
  public SubjectItem(String n,int a){
    this.n = n;
    this.a = a;
  }
  
  public String getName(){
    return n;
  }
  public int getA(){
    return a;
  }
}