package com.j2.u201211293.p3;

public class Professor{
  MySubject my;
  
  public Professor(MySubject my){
    this.my = my;
  }
  
  public void printSubject(){
    Iterator A = my.createIterator();
    System.out.println(" YOON MIN SEOB SUBJECT ");
    print(A);
  }
  public void print(Iterator A){
    while(A.hasNext()){
      SubjectItem sub = (SubjectItem)A.next();
      System.out.println("Subject : " + sub.getName() + " , TA: " + sub.getA());
    }
  }
}