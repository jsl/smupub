package com.j2.u201211293.p1;

public class ChocolateBoiler{
  private static ChocolateBoiler uniqueinstance = new ChocolateBoiler();
  private boolean empty;
  private boolean boiled;
  
  private ChocolateBoiler(){
    empty = true;
    boiled = false;
  }
  
  public static ChocolateBoiler getInstance(){
    return uniqueinstance;
  }
  
  public void fill(){
    if(isEmpty()){
      System.out.println("Fill...");
      empty = false;
    }
  }
  public void boil(){
    if(!isEmpty()&&!isBoiled()){
      System.out.println("Boil...");
      boiled = true;
    }
  }
  public void drain(){
    if(!isEmpty()&&isBoiled()){
      System.out.println("Drain..");
      empty = true;
      boiled = false;
    }
  }
       
  public boolean isEmpty(){
      return empty;
  }
  public boolean isBoiled(){
    return boiled;
  }
}