package com.j2.u201211293.p2;

public class SimpleRemoteControler{
  Command[] onCommands;
  Command[] offCommands;
  Command noCommand = new NoCommand();
  public SimpleRemoteControler(){
    onCommands = new Command[7];
    offCommands = new Command[7];
    for(int i=0; i<7;i++){
      onCommands[i] = noCommand;
      offCommands[i] = noCommand;
    }
  }
  
  public void SetButton(int slot,Command on,Command off){
    onCommands[slot] = on;
    offCommands[slot] = off;
  }
  
  public void ButtonWasPressedOn(int slot){
    onCommands[slot].execute();
  }
  public void ButtonWasPressedOff(int slot){
    offCommands[slot].execute();
  }
}
    