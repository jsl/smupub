package com.j2.u201211293.p2;

public class GarageDoorOffCommand implements Command{
  GarageDoor garage;
  
  public GarageDoorOffCommand(GarageDoor garage){
    this.garage = garage;
  }
  
  public void execute(){
    garage.down();
  }
}