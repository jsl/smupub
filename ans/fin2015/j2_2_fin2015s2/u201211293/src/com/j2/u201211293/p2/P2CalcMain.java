package com.j2.u201211293.p2;

public class P2CalcMain{
  public static void main(String[] args){
    GarageDoor garage = new GarageDoor();
    
    GarageDoorOnCommand A = new GarageDoorOnCommand(garage);
    GarageDoorOffCommand B = new GarageDoorOffCommand(garage);
    
    SimpleRemoteControler simple = new SimpleRemoteControler();
    
    simple.SetButton(0,A,B);
    simple.ButtonWasPressedOn(0);
    simple.ButtonWasPressedOff(0);
  }
}