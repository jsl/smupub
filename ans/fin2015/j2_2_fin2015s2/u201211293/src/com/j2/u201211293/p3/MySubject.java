package com.j2.u201211293.p3;
import java.util.ArrayList;
public class MySubject implements Subject{
  ArrayList A;
  public MySubject(){
    A = new ArrayList();
    addSubject("Java2",2);
    addSubject("algorithm",2);
    addSubject("Spanish",1);
    addSubject("signal",2);
    addSubject("Computer system",1);
    addSubject("software",1);
    addSubject("economic",1);
  }
  
  public void addSubject(String n,int c){
    SubjectItem si = new SubjectItem(n,c);
    A.add(si);
  }
  
  public Iterator createIterator(){
    return new SubjectIterator(A);
  }
}