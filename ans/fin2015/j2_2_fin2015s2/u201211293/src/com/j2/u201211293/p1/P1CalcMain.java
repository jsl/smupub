package com.j2.u201211293.p1;

public class P1CalcMain{
  public static void main(String[] args){

    ChocolateBoiler chocolateBoiler = ChocolateBoiler.getInstance();
    
    chocolateBoiler.fill();
    chocolateBoiler.boil();
    chocolateBoiler.drain();
  }
}