package com.j2.u201211293.p3;

public interface Subject{
  public Iterator createIterator();
}