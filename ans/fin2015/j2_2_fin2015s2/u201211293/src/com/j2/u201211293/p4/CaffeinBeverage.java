package com.j2.u201211293.p4;

public abstract class CaffeinBeverage{
  final void Prepare(){
    System.out.println("Making tea...");
    boilWater();
    brew();
    pourInCup();
    if(customer()){
    addCondiments();
    }
  }
  
  public void boilWater(){
    System.out.println("Boiling water");
  }
  abstract void brew();
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }
  abstract void addCondiments();
  public boolean customer(){
    return true;
  }
}