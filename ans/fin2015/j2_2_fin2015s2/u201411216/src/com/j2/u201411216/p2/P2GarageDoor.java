package com.j2.u201411216.p2;

public class P2GarageDoor{
  public boolean isOpen = false;
  public void opened(){
    if(!isOpen){
      System.out.println("Garage door is up");
      isOpen = true;
    }
  }
  public void closed(){
    if(isOpen){
      System.out.println("Garage door is down");
      isOpen = false;
    }
  }
}