package com.j2.u201411216.p3;

import java.util.*;

public class P3ArrayListIterator implements P3Iterator{
  public ArrayList al;
  public int position;
  public P3ArrayListIterator(ArrayList al){
    this.al = al;
    position = 0;
  }
  public boolean hasNext(){
    if(al.size() > position && al.get(position) != null){
      return true; 
    }
    else return false;
  }
  public Object next(){
    position++;
    return al.get(position-1);
  }
}