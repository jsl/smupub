package com.j2.u201411216.p3;

public interface P3Iterator{
  public boolean hasNext();
  public Object next();
}