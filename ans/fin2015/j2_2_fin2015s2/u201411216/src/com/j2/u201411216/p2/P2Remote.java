package com.j2.u201411216.p2;

public class P2Remote{
  private P2GarageDoor g;
  private P2Command c;
  public P2Remote(P2GarageDoor d){
    g = d;
    c = new P2NoCommand();
  }
  public void setCommand(){
    c = new P2OpenCloseCommand(g);
  }
  public void buttonWasPressed(){
    c.excute();
  }
}