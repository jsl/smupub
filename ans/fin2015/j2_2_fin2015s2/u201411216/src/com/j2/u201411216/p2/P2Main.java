package com.j2.u201411216.p2;

public class P2Main{
  public static void main(String[] args){
    P2GarageDoor door = new P2GarageDoor();
    P2Remote remote = new P2Remote(door);
    remote.setCommand();
    remote.buttonWasPressed();
    remote.buttonWasPressed();
  }
}