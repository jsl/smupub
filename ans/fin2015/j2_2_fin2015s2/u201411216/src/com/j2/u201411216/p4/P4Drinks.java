package com.j2.u201411216.p4;

public abstract class P4Drinks{
  public void recipe(){
    System.out.println("Making tea...");
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  public void boilWater(){
    System.out.println("Boiling water");
  }
  public void brew(){
    System.out.println("Steeeping the tea");
  }
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }
  abstract public void addCondiments();
}