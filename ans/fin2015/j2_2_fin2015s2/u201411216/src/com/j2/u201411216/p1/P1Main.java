package com.j2.u201411216.p1;

public class P1Main{
  public static void main(String[] args){
    P1Singleton barista = new P1Singleton();
    barista.drain();
    barista.fillBoiler();
    barista.boilBoiler();
    barista.drain();
    barista.drain();
  }
}