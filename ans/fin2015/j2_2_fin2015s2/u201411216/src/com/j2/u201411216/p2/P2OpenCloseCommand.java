package com.j2.u201411216.p2;

public class P2OpenCloseCommand implements P2Command{
  private P2GarageDoor door;
  public P2OpenCloseCommand(P2GarageDoor d){
    door = d;
  }
  public void excute(){
    if(door.isOpen){
      door.closed();
      return;
    }
    door.opened();
    return;
  }
}