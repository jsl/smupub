package com.j2.u201411216.p1;

public class P1Singleton{
  private static P1ChocolateBoiler boiler;
  public P1Singleton(){
    boiler = null;
  }
  public static P1ChocolateBoiler getInstance(){
    if(boiler == null){
      boiler = new P1ChocolateBoiler();
    }
    return boiler;
  }
  public void drain(){
    boiler = getInstance();
    if(!boiler.isEmpty()){
      if(boiler.isBoiled()){
        System.out.println("Drain boiler!");
        boiler.drain();
        return;
      }
       System.out.println("You must fill boiler!");
    }
    System.out.println("You must boil boiler!");
  }
  public void fillBoiler(){
    boiler.fill();
  }
  public void boilBoiler(){
    boiler.boil();
  }
}