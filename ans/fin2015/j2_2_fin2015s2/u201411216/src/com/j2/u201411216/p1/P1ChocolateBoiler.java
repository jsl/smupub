package com.j2.u201411216.p1;

public class P1ChocolateBoiler{
  public boolean empty;
  public boolean boiled;
  public P1ChocolateBoiler(){
    empty = true;
    boiled = false;
  }
  public void fill(){
    System.out.println("filling...");
    empty = false;
  }
  public void boil(){
    if(!isEmpty()){
      System.out.println("boiling...");
      boiled = true;
    }
  }
  public void drain(){
    empty = true;
    boiled = false;
  }
  public boolean isEmpty(){
    return empty;
  }
  public boolean isBoiled(){
    return boiled;
  }
}