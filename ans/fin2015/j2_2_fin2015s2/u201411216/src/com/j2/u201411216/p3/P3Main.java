package com.j2.u201411216.p3;

import java.util.*;

public class P3Main{
  public static void main(String[] args){
    ArrayList<String> al = new ArrayList<>();
    al.add("java programming2");
    al.add("algorithm");
    al.add("software developing with convergence");
    al.add("SM career start");
    al.add("SM study plan");
    al.add("Computer Instruction");
    P3ArrayListIterator ali = new P3ArrayListIterator(al);
    while(ali.hasNext()){
      System.out.println(ali.next());
    }
  }
}