package com.j2.u201211316.p4;
  public abstract class Be{
    public Be(){}
    public void pre(){
    System.out.println("Making tea");
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  public abstract void brew();
  public abstract void addCondiments();
  public void boilWater(){
    System.out.println("Boiling water");
  }
  public void pourInCup(){
     System.out.println("Pouring into cup");
  }
}