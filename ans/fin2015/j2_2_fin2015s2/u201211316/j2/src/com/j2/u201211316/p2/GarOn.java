package com.j2.u201211316.p2;
public class GarOn implements Command{
  GarageDoor g;
  public GarOn(){
    
  }
  public void setCommand (GarageDoor g){
    this.g =g;
  }
  public void excute(){
    g.on();
  }
}