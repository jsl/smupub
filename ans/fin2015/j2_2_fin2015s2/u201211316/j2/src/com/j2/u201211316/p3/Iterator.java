package com.j2.u201211316.p3;
public interface Iterator{
  Object next();
  boolean hasNext();
}