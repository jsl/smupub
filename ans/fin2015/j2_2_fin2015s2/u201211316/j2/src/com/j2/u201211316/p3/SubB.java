package com.j2.u201211316.p3;
import java.util.*;
public class SubB implements Iterator{
  ArrayList sub;
  int n = 0;
  public SubB(ArrayList sub){
    this.sub = sub;
  }
  public Object next(){
  Object object = sub.get(n); 
    n = n+1;
    return object;
  }
  public boolean hasNext(){
    if(n>=sub.size()){
      return false;
    }else{
      return true;
    }
  }
}