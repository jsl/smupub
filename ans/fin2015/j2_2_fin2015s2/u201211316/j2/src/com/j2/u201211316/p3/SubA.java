package com.j2.u201211316.p3;
public class SubA implements Iterator{
  Sub[] sub;
  int n = 0;
  public SubA(Sub[] sub){
    this.sub = sub;
  }
  public Object next(){
   Sub subs = sub[n]; 
    n = n+1;
    return subs;
  }
  public boolean hasNext(){
    if(n>=sub.length||sub[n]==null){
      return false;
    }else{
      return true;
    }
  }
}