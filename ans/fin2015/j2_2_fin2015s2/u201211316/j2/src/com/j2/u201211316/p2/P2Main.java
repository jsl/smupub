package com.j2.u201211316.p2;
public class P2Main{
  public static void main(String args[]){
    GarageDoor g= new GarageDoor();
    GarOff off = new GarOff();
    GarOn on = new GarOn();
    on.setCommand(g);
    off.setCommand(g);
    on.excute();
    off.excute();
  }
}