package com.j2.u201211316.p2;
public class GarOff implements Command{
  GarageDoor g;
  public GarOff(){ }
  public void setCommand (GarageDoor g){
    this.g =g;
  }
  public void excute(){
    g.off();
  }
}