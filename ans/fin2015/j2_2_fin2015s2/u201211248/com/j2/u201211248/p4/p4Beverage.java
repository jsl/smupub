package com.j2.u201211248.p4;
public abstract class p4Beverage{
  public void makeBeverage(){
    System.out.println("Making tea...");
    boilWater();
    brew();
    PourInCup();
    addCondiments();
  }
  public void boilWater(){
    System.out.println("Boiling water");
  }
  public void PourInCup(){
    System.out.println("Pouring into cup");
  }
  public abstract void brew();
  public abstract void addCondiments();

}