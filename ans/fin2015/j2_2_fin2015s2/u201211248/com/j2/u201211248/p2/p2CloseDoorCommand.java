package com.j2.u201211248.p2;
public class p2CloseDoorCommand implements p2command{
  p2GarageDoor garageDoor;
  public p2CloseDoorCommand(p2GarageDoor garageDoor){
    this.garageDoor = garageDoor;
  }
  public void execute(){
    garageDoor.closeDoor();
  }
}