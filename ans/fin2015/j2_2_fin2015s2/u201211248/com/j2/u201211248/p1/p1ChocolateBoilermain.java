package com.j2.u201211248.p1;
public class p1ChocolateBoilermain{
private boolean boiled = true;
private boolean empty = false;
private static p1ChocolateBoilermain uniqueInstance;
private static p1ChocolateBoilermain getInstance(){
  if(uniqueInstance == null){
    uniqueInstance =  new p1ChocolateBoilermain();
  }
  return uniqueInstance;
}
public void drain(){
  if(this.boiled){
    isEmpty();
    System.out.println("drained");
  }else
    System.out.println("아직 끓지 않았습니다.");
}
public void isEmpty(){
  this.empty = true;
}
public void isBoiled(){
  this.boiled = true;
}
public static void main(String args[]){
  p1ChocolateBoilermain boiler = new p1ChocolateBoilermain();
  boiler.getInstance();
  boiler.drain();
}
}