package com.j2.u201411198.p1;

public class ChocolateBoiler {
  private static ChocolateBoiler uniqueInstance = new ChocolateBoiler();
  private static boolean empty;
  private static boolean boiled;
  
  private ChocolateBoiler() {
    empty = true;
    boiled = false;
  }
  
  public static ChocolateBoiler getInstance() {
    if (uniqueInstance == null) {
      uniqueInstance = new ChocolateBoiler();
    }
    return uniqueInstance;
  }
  
  public void drain() {
    if (!isEmpty()) {
      empty = true;
    }
    if (isBoiled()) {
      boiled = false;
    }
  }
  
  public boolean isEmpty() {
    return empty;
  }
  
  public boolean isBoiled() {
    return boiled;
  }
}
  
  