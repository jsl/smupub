package com.j2.u201411198.p3;
import java.util.*;

public class ArrayListIterator implements Iterator {
  ArrayList arrayList;
  int position;
  
  public ArrayListIterator(ArrayList arrayList) {
    this.arrayList = arrayList;
  }
  
  public boolean hasNext() {
    if (arrayList.size() <= position){
      return false;
    } else {
      return true;
    }
  }
   
  public Object next() {
    Object object = arrayList.get(position);
    position += 1;
    return object;
  }
}
    
          
         
  