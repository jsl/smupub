package com.j2.u201411198.p3;

public interface Iterator {
  public boolean hasNext();
  public Object next();
}