package com.j2.u201411198.p4;

public class Tea {
  public final void prepareRecipe() {
    System.out.println("Making tea...");
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  
  void boilWater() {
    System.out.println("Boiling water");
  }
     
  void pourInCup() {
    System.out.println("Pouring into Cup");
  }
  
  void brew() {
    System.out.println("Steeping the tea");
  }
  
  void addCondiments() {
    System.out.println("Adding Lemon");
  }
}