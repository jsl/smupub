package com.j2.u201411198.p2;

public interface Command {
  public void execute();
}
