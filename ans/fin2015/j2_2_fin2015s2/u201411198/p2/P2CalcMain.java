package com.j2.u201411198.p2;

public class P2CalcMain {
  public static void main(String[] args) {
    GarageDoor garageDoor = new GarageDoor();
    RemoteControl control = new RemoteControl();
    control.setCommand(garageDoor);
    
    control.upButtonWasPressed();
    control.downButtonWasPressed();
  }
}