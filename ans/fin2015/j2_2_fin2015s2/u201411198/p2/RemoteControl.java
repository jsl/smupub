package com.j2.u201411198.p2;

public class RemoteControl {
  UpGarageDoorCommand upCommand;
  DownGarageDoorCommand downCommand;
  
  public void setCommand(GarageDoor garageDoor) {
    upCommand = new UpGarageDoorCommand(garageDoor);
    downCommand = new DownGarageDoorCommand(garageDoor);
  }
  
  public void upButtonWasPressed() {
    upCommand.execute();
  }
  
  public void downButtonWasPressed() {
    downCommand.execute();
  }
}