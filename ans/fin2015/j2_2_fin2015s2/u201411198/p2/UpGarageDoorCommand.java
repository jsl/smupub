package com.j2.u201411198.p2;

public class UpGarageDoorCommand implements Command {
  GarageDoor garageDoor;
  
  public UpGarageDoorCommand(GarageDoor garageDoor) {
    this.garageDoor = garageDoor;
  }
  
  public void execute() {
    garageDoor.up();
  }
}
    