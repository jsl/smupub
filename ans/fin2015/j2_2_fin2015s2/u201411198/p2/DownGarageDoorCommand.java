package com.j2.u201411198.p2;

public class DownGarageDoorCommand implements Command {
  GarageDoor garageDoor;
  
  public DownGarageDoorCommand(GarageDoor garageDoor) {
    this.garageDoor = garageDoor;
  }
  
  public void execute() {
    garageDoor.down();
  }
}
    