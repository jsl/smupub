package com.j2.u201411183.p2;
public interface Command{
  public void execute();
}