package com.j2.u201411183.p2;

public class P2CalcMain{
  public static void main(String[] args){
    RemoteControl remote=new RemoteControl();
    
    GarageDoor garage=new GarageDoor("GarageDoor");
    OpenCommand ongarage=new OpenCommand(garage);
    CloseCommand offgarage=new CloseCommand(garage);
    
    remote.setCommand(0,ongarage, offgarage);
    remote.onButtonWasPressed(0);
    remote.offButtonWasPressed(0);
  }
}