package com.j2.u201411183.p2;
public class GarageDoor{
  String name;
  public GarageDoor(String name){
    this.name=name;
  }
  public void on(){
    System.out.println("Garage door is up");
  }
  public void off(){
    System.out.println("Garage door is down");
  }
}