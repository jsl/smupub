package com.j2.u201411183.p2;
import java.util.*;
public class RemoteControl{
  Command[] onCommands;
  Command[] offCommands;
  
  public RemoteControl(){
    onCommands=new Command[2];
    offCommands=new Command[2];
    Command noCommand=new NoCommand();
    for(int i=0; i<2;i++){
      onCommands[i]=noCommand;
      offCommands[i]=noCommand;
    }
  }
  public void setCommand(int slot, Command onCommand, Command offCommand){
      onCommands[slot]=onCommand;
    offCommands[slot]=offCommand;
  }
  public void onButtonWasPressed( int slot)
  {
    onCommands[slot].execute();
  }
  public void offButtonWasPressed(int slot){
    offCommands[slot].execute();
  }
}