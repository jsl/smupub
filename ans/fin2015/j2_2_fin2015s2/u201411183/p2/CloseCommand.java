package com.j2.u201411183.p2;

public class CloseCommand implements Command{
  GarageDoor garageDoor;
  public CloseCommand(GarageDoor garageDoor){
    this.garageDoor=garageDoor;
  }
  public void execute(){
    garageDoor.off();
  }
}