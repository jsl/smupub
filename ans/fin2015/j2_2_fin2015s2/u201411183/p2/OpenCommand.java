package com.j2.u201411183.p2;

public class OpenCommand implements Command{
  GarageDoor garageDoor;
  public OpenCommand(GarageDoor garageDoor){
    this.garageDoor=garageDoor;
  }
  public void execute(){
    garageDoor.on();
  }
}