package com.j2.u201411183.p4;

public abstract class CaffeinBeverage{
  final void prepare(){
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  abstract void brew();
  abstract void addCondiments();
  
  void boilWater(){
    System.out.println("Boiling Water");
  }
  void pourInCup(){
    System.out.println("Pouring into cup");
  }
}