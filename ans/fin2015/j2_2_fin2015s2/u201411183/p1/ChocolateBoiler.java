package com.j2.u201411183.p1;

public class ChocolateBoiler{
  private boolean empty;
  private boolean boiled;
  private ChocolateBoiler uniqueInstance;
  
  private int count=0;
  public ChocolateBoiler(){
    empty=true;
    boiled=false;
  }
  public ChocolateBoiler getInstance(){
    if(uniqueInstance==null){
      uniqueInstance= new ChocolateBoiler();
    }    
    System.out.println("ChocolateBoiler");
    System.out.println("count: "+count++);
   
    return uniqueInstance;
  }
  
  void fill(){
    if(isEmpty()){
      empty=false;
      boiled=false;
    }
  }
  void boiled(){
    if(!isEmpty()&&!isBoiled()){
      boiled=true;
    }
  }
  void drain(){
    if(!isEmpty()&&isBoiled()){
      empty=true;
    }
  }
  boolean isEmpty(){
    return empty;
  }
  boolean isBoiled(){
    return boiled;
  }
}
  