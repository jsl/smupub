package com.j2.u201411183.p1;

public class P1CalcMain{
  public static void main(String[] args){
    ChocolateBoiler chocolateBoiler=new ChocolateBoiler();
    chocolateBoiler.getInstance();
 
    chocolateBoiler.fill();
    chocolateBoiler.boiled();
    chocolateBoiler.drain();
    
    chocolateBoiler.getInstance();
    chocolateBoiler.fill();
    chocolateBoiler.boiled();
    chocolateBoiler.drain();
  }}