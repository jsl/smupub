  package headfirst.command.undo:
      
    import java.until.*;
    
    publc class RemoteControlWithUndo{
          Command[] onCommand;
          Command[] offCommand;
          Command undoCommand;
          
          pubilc RemoteCotrolWithUndo() {
                 onCommands = new Command[7];
                 offCommands = new Command[7];
                 
                 Command noCommand = new NoCommand();
                 for(int i=0;i<7;i++){
                         onCommands[i] = noCommand;
                         offCommand = noCommand;
                 }
                 undoCommand =noCommand;
          }
          public void setCommand(int slot, Command noCommand,Command offCommand){
                 onCommands[solt] = onCommand;
                 offCommands[solt] = offCommand;
          }
          public void onButtonWashPushed(int slot){
                 onCommands[solt].execute();
                 undoCommand = onCommands[slot];
          }
 
          public void offButtonWasPushed(int slot) {
                 offCommands[slot].execute();
                 undoCommand = offCommands[slot];
          }
 
          public void undoButtonWasPushed() {
                 undoCommand.undo();
          }
  
          public String toString() {
                 StringBuffer stringBuff = new StringBuffer();
                 stringBuff.append("\n------ Remote Control -------\n");
                 for (int i = 0; i < onCommands.length; i++) {
                 stringBuff.append("[slot " + i + "] " + onCommands[i].getClass().getName()
                 + "    " + offCommands[i].getClass().getName() + "\n");
          }
                 stringBuff.append("[undo] " + undoCommand.getClass().getName() + "\n");
          return stringBuff.toString();
          }
        }