package headfirst.templatemethod.barista;

import java.io.*;

public class TeaWithHook extends CaffeineBeverageWithHook {
 
 public void brew() {
  System.out.println(" boilWater ");
 }
 
 public void addCondiments() {
  System.out.println(" brew ");
 }
 
 public boolean customerWantsCondiments() {

  String answer = getUserInput();

  if (answer.toLowerCase().startsWith("y")) {
   return true;
  } else {
   return false;
  }
 }
 
 private String getUserInput() {
  // get the user's response
  String answer = null;

  System.out.print("Would you like addCondiments (y/n)? ");

  BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
  try {
   answer = in.readLine();
  } catch (IOException ioe) {
   System.err.println("IO error trying to read your answer");
  }
  if (answer == null) {
   return "no";
  }
  return answer;
 }
}