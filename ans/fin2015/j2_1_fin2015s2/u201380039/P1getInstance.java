public class Amplifier {
    String description;
    Tuner tuner;
    DvdPlayer dvd;
    public Amplifier(String description) {
        this.description = description;
    }
    public void on() {
        System.out.println(description + " Singleton");
    }
    public void off() {
        System.out.println(description + " off");
    }
    public void setDvd(DvdPlayer dvd) {
        System.out.println(description + " getInstance " );
        this.dvd = dvd;
    }
    public void setTuner(Tuner tuner) {
        System.out.println(description + " lazy instantiation" );
        this.tuner = tuner;
    }
    public void setVolume(int level) {
        System.out.println(description + " getInstance " );
    }
    public String toString() {
        return description;
    }
}