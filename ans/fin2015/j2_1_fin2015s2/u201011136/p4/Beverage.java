package com.j2.u201011136.p4;

public abstract class Beverage {
  final void prepare() {
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  
  void boilWater() {
    System.out.println("boiling water");
  }
  
  void pourInCup() {
    System.out.println("pouring into cup");
  }
  
  abstract void brew();
  abstract void addCondiments();
}

