package com.j2.u201011136.p4;

public class Coffee extends Beverage {
  void brew() {
    System.out.println("dripping coffee through filter");
  }
  
  void addCondiments() {
    System.out.println("adding sugar and milk");
  }
}