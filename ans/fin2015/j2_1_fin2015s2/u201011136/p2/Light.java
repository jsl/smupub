package com.j2.u201011136.p2;

public class Light {
  public void on() {
    System.out.println("Light is on");
  }
  
  public void off() {
    System.out.println("Light is off");
  }
}