package com.j2.u201011136.p2;

public class LightOnCommand implements Command {
  Light light;
  
  public LightOnCommand(Light light) {
    this.light = light;
  }
  
  public void execute() {
    light.on();
  }
}