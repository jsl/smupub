package com.j2.u201011136.p2;

public interface Command {
  public void execute();
}