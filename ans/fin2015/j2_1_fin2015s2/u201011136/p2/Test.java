package com.j2.u201011136.p2;

public class Test {
  public static void main(String[] args) {
    Light light = new Light();
    LightOnCommand loc = new LightOnCommand(light);
    LightOffCommand loffc = new LightOffCommand(light);
    RemoteControl remote = new RemoteControl();
    
    remote.setCommand(loc, loffc);
    remote.onButtonWasPressed();
    remote.offButtonWasPressed();
  }
}