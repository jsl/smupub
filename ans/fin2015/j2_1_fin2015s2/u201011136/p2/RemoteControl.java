package com.j2.u201011136.p2;

public class RemoteControl {
  Command onCommand;
  Command offCommand;
  
  public void setCommand(Command onCommand, Command offCommand) {
    this.onCommand = onCommand;
    this.offCommand = offCommand;
  }
  
  public void onButtonWasPressed() {
    onCommand.execute();
  }
  
  public void offButtonWasPressed() {
    offCommand.execute();
  }
}