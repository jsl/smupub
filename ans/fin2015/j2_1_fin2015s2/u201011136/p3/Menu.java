package com.j2.u201011136.p3;

import java.util.*;

public class Menu {
  MenuItem[] items;
  int position;
  
  public Menu() {
    items = new MenuItem[5];
    addItem("java2", "learn for design pattern");
    addItem("computer network", "learn for computer network");
    addItem("web programming", "learn for web programming");
    addItem("database programming", "learn for database programming");
    addItem("android programming", "learn for android programming");
  }
  
  public void addItem(String name, String description) {
    if(position >= 5) {
      System.out.println("you cannot add item");
    } else {
       MenuItem item = new MenuItem(name, description);
       items[position] = item;
       position++;
    }
  }
  
  public Iterator createIterator() {
    return new MenuIterator(items);
  }
}