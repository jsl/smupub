package com.j2.u201011136.p3;

import java.util.*;

public class Waitress {
  Menu menu;
  
  public Waitress(Menu menu) {
    this.menu = menu;
  }
  
  public void printMenu() {
    Iterator it = menu.createIterator();
    printMenu(it);
  }
  
  public void printMenu(Iterator it) {
    System.out.println("2015 second semester");
    while(it.hasNext()) {
      MenuItem item = (MenuItem)it.next();
      System.out.println("name : " + item.getName() + ", content : " + item.getDescription());
    }
  }
}