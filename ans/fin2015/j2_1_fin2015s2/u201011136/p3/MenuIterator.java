package com.j2.u201011136.p3;

import java.util.*;

public class MenuIterator implements Iterator {
  MenuItem[] items;
  int position;
  
  public MenuIterator(MenuItem[] items) {
    this.items = items;
  }
  
  public boolean hasNext() {
    if(position >= items.length || items[position] == null) {
      return false;
    } else {
      return true;
    }
  }
  
  public Object next() {
    Object o = items[position];
    position++;
    return o;
  }
  
  public void remove() {
  }
}