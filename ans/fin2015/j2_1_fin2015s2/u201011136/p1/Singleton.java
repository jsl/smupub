package com.j2.u201011136.p1;

public class Singleton {
  
  private static Singleton instance = new Singleton();
  private boolean state;
  
  private Singleton() {
    state = false;
  }
  
  public static Singleton getInstance() {
    return instance;
  }
  
  public boolean isEmpty() {
    return state;
  }
  
  public void isBoiled() {
    if(!isEmpty()) {
      System.out.println("boiled");
    }
  }
  
  public void drain() {
    System.out.println("drain");
    state = true;
  }
}


