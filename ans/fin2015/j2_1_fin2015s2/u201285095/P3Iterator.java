package com.j2.201285095.p3;

public interface Iterator {
    boolean hasNext();
    Object next();
}
