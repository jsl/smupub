package com.j2.201285095.p2;

import java.util.*;

public class SimpleRemoteControl {
 Command slot;
 
 public SimpleRemoteControl() {}
 
 public void setCommand(Command command) {
  slot = command;
 }
 
 public void buttonWasPressed() {
  slot.execute();
 }
}


public class RemoteControlTest {
    public static void main(String[] args) {
        SimpleRemoteControl remote = new SimpleRemoteControl();
        Light light = new Light();
        GarageDoor garageDoor = new GarageDoor();
        LightOnCommand lightOn = new LightOnCommand(light);
        GarageDoorOpenCommand garageOpen = new GarageDoorOpenCommand(garageDoor);
 
        remote.setCommand(lightOn);
        remote.buttonWasPressed();
        remote.setCommand(garageOpen);
        remote.buttonWasPressed();
    }
}