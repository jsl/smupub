package com.j2.201285095.p2;

public class Light {

 public Light() {
 }

 public void on() {
  System.out.println("Light is on");
 }

 public void off() {
  System.out.println("Light is off");
 }
}