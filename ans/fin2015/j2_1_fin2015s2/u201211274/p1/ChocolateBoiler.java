package com.j2.u201211274.p1;

public class ChocolateBoiler{
  private static ChocolateBoiler unique;
  boolean empty;
  boolean boil=false;
 
  public static ChocolateBoiler getInstance(){
    if(unique==null)
      unique=new ChocolateBoiler();
 
      return unique;
   
  }
  public void drain(){
    if(!isEmpty() && isBoiled())
      System.out.println("drain");
    else
      System.out.println("not boiled");
  }

  public void boiling(){
    empty=false;
    boil=true;
  }
  public boolean isEmpty(){
    return empty;
  }
  public boolean isBoiled(){
    return boil;
  }
}