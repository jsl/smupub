package com.j2.u201211274.p1;

public class P1Main{
  public static void main(String args[]){
    ChocolateBoiler boiler1=ChocolateBoiler.getInstance();
    boiler1.boiling();
    boiler1.drain();
    ChocolateBoiler boiler2=ChocolateBoiler.getInstance();
    boiler2.drain();
  }
}