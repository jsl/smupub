package com.j2.u201211274.p2;

public class P2Main{
  public static void main(String args[]){
    Light light=new Light();
    LightOnCommand lightOnCommand=new LightOnCommand(light);
    LightOffCommand lightOffCommand=new LightOffCommand(light);
    RemoteControl control=new RemoteControl();
    control.setCommand(lightOnCommand);
    control.buttonWasPressed();
    control.setCommand(lightOffCommand);
    control.buttonWasPressed();
  }
}