package com.j2.u201211274.p2;

public interface Command{
  public void execute();
}