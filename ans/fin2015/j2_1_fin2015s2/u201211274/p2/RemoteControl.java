package com.j2.u201211274.p2;

public class RemoteControl{
  Command command;
  public void setCommand(Command command){
    this.command=command;
  }
  public void buttonWasPressed(){
    command.execute();
  }
}