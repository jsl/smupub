package com.j2.u201211274.p4;

public class Coffee{
  public void prepare(){
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  
  public void boilWater(){
   System.out.println("Boiling water");
  }
  public void brew(){
   System.out.println("Dripping Coffee through filer");
  }
  public void pourInCup(){
   System.out.println("Pouring into cup");
  }
  public void addCondiments(){
   System.out.println("Adding Sugar and Milk");
  }
  
  
}