package com.j2.u201211274.p3;

public class SubjectIterator implements Iterator{
  Subject[] subject;
  int position=0;
  public SubjectIterator(Subject[] subject){
    this.subject=subject;
  }
  public boolean hasNext(){
    if(position>subject.length || subject[position]==null){
      return false;
    }
    else
      return true;
  }
  public Object next(){
    
    Subject temp=subject[position];
    position+=1;
    return temp;
  }
}