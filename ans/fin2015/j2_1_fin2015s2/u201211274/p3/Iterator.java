package com.j2.u201211274.p3;

public interface Iterator{
  public Object next();
  public boolean hasNext();
}