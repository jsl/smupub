package com.j2.u201411185.p2;

public class P2Main {
  public static void main(String[] args){
    Light light = new Light("Room");
    RemoteControl remote = new RemoteControl();
    
    LightOnCommand lightOn = new LightOnCommand(light);
    LightOffCommand lightOff = new LightOffCommand(light);
    
    remote.setCommand(lightOn, lightOff);
    remote.onButtonWasPressed();
    remote.offButtonWasPressed();
  }
}
