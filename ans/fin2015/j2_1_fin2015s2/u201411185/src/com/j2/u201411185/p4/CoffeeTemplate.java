package com.j2.u201411185.p4;

public abstract class CoffeeTemplate{
  void prepare(){
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  
  void boilWater(){
    System.out.println("Boiling water");
  }
  
  abstract void brew();
  abstract void addCondiments();
  
  void pourInCup(){
    System.out.println("Pouring into Cup");
  }
}
