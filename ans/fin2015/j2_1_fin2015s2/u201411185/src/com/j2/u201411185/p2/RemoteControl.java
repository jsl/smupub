package com.j2.u201411185.p2;

public class RemoteControl {
  Command on;
  Command off;
  NoCommand no = new NoCommand();
  Light light;
  
  public RemoteControl(){
    on = no;
    off = no;
  }
  
  public void setCommand(Command on, Command off){
    this.on = on;
    this.off = off;
  }
  
  public void onButtonWasPressed(){
    on.execute();
  }
  
  public void offButtonWasPressed(){
    off.execute();
  }
}
