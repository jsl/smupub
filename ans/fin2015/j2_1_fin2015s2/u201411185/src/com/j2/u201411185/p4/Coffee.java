package com.j2.u201411185.p4;

public class Coffee extends CoffeeTemplate {
  void brew(){
    System.out.println("Dripping Coffee through filter");
  }
  
  void addCondiments(){
    System.out.println("Adding Sugar and Milk");
  }
}
