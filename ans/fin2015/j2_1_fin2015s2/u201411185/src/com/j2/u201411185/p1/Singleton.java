package com.j2.u201411185.p1;

public class Singleton {
  private static Singleton boiler;
  private static int call = 0;
  
  private Singleton() {
  }
  
  public Singleton getInstance(){
    if(boiler == null){
      boiler = new Singleton();
    }
    System.out.println("called : " +call++);
    return boiler;
  }
  
  public boolean isEmpty(){
    if(boiler == null)
      return false;
    else
      return true;
  }
  
  public void isBoiled(){
    if(boiler != null)
      System.out.println("Chocolate is boiled");
  }
  
  public void drain(){
    if(boiler == null) {
      getInstance();
    }
    else
      System.out.println("x");
  }
  
  
}
