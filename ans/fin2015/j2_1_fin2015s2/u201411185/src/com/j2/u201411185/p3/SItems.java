package com.j2.u201411185.p3;

public class SItems implements SubjectArray{
  Subject[] s;
  String subject;
  int n = 0;
  static int MAX = 3;
  
  public SItems(Subject[] s){
    this.s = s;
    addItem("Java2");
    addItem("Algo");
    addItem("Computer Structure");
  }
  
  public void addItem(String s){
    if(n < MAX){
      Subject list = new Subject(s);
      s[n] = list;
      n = n + 1;
    }
    else
      Systme.out.print("You can't add. It's full");
  }
  
  public Iterator createIterator(){
    return SubjectIterator(s);
  }
}