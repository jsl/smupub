package com.j2.u201411185.p2;

public interface Command {
  public void execute();
}
