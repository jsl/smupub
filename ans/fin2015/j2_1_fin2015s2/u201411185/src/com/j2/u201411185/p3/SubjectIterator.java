package com.j2.u201411185.p3;

public class SubjectIterator implements Iterator {
  Subject[] s;
  int n;
  
  public SubjectIterator(Subject[] s){
    this.s = s;
  }
  
  public boolean hasNext(){
    if(n >= s.length || s[n] == null)
      return false;
    else
      return true;
  }
  
  public Object next(){
    Subject subject = s[n];
    n = n + 1;
    return subject;
  }
}
