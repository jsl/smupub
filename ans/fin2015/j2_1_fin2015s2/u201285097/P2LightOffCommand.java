com.j2.u201285097.p4; 
public class P2LightOffCommand implements Command {
 Light light;
 
 public P2LightOffCommand(Light light) {
  this.light = light;
 }
 
 public void execute() {
  light.off();
 }
}