com.j2.u201285097.p4;
public class P4makingcoffe {
 
 void prepareRecipe() {
  boilWater();
  brew();
  pourInCup();
  addCondiments();
 }
 
 public void boilWater() {
  System.out.println("Boiling water");
 }
 
 public void brew() {
  System.out.println("Dripping Coffee through filter");
 }
 
 public void pourInCup() {
  System.out.println("Pouring into cup");
 }
 
 public void addCondiments() {
  System.out.println("Adding Sugar and Milk");
 }
}
 public void boilWater() {
  System.out.println("Boiling water");
 }
 
 
 public void pourInCup() {
  System.out.println("Pouring into cup");
 }
}