com.j2.u201285097.p2;
public class P2LightOnCommand implements Command {
 Light light;
  
 public P2LightOnCommand(Light light) {
  this.light = light;
 }
 
 public void execute() {
  light.on();
 }
} 

