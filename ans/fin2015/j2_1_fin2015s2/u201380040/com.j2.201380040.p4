package headfirst.templatemethod.applet;

import java.applet.Applet;
import java.awt.Graphics;

public class MyApplet extends Applet {
    String message;
 
    public void init() {
        message = "Making coffee";
        repaint();
    }
 
    public void start() {
        message = "Boiling water";
        repaint();
    }
 
    public void stop() {
        message = "Dripping Coffee through filter";
        repaint();
    }
 
    public void destroy() {
        message = "Pouring into Cup";
        repaint();
    }
 
    public void paint(Graphics g) {
        g.drawString(message, 5, 15);
    }