package com.j2.u201011098.p2;

public class P2RemoteMain{
  public static void main(String[] args){
  
    P2RemoteControl remote = new P2RemoteControl();
    P2RemoteControl remote2 = new P2RemoteControl();
    
    P2Light light = new P2Light();
    P2LightOnCommand lightOn = new P2LightOnCommand(light);
    P2LightOffCommand lightOff = new P2LightOffCommand(light);
    
    remote.setCommand(lightOn);
    remote.buttonWasPressed();
    remote2.setCommand(lightOff);
    remote2.buttonWasPressed();
  }
}