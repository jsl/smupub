package com.j2.u201011098.p2;

public class P2LightOnCommand implements P2Command{
  
  P2Light light;
  public P2LightOnCommand(P2Light light){
    this.light=light;
  }
  
  public void execute(){
    light.on();
  }
}