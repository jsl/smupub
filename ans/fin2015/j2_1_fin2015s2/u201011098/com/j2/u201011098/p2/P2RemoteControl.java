package com.j2.u201011098.p2;

public class P2RemoteControl{
  
  P2Command slot;
  public P2RemoteControl(){}
  public void setCommand(P2Command command){
    slot=command;
  }
  public void buttonWasPressed(){
    slot.execute();
  }
}