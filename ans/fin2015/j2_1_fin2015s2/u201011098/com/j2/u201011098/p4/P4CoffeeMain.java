package com.j2.u201011098.p4;

public class P4CoffeeMain{
  public static void main(String []args){

    System.out.println("Making coffee...");
    
    P4Coffee coffee = new P4Coffee();
    coffee.prepareRecipe();
       
  }
}