package com.j2.u201011098.p4;

public class P4Coffee extends P4CaffeineBeverage{
  
  public void brew(){
    System.out.println("Dripping Coffee through filter");
  }
  
  public void addCondiments(){
    System.out.println("Adding sugar and milk");
  }
}