package com.j2.u201011098.p4;

public abstract class P4CaffeineBeverage{
  
  public final void prepareRecipe(){
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
 
  public void boilWater(){
    System.out.println("Boiling Water");
  }
  abstract void brew();
  public void pourInCup(){
    System.out.println("Pouring into cup");
  }  
  abstract void addCondiments();
}