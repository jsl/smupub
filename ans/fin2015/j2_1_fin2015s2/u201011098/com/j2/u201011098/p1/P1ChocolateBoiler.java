package com.j2.u201011098.p1;

public class P1ChocolateBoiler{
  private boolean empty;
  private boolean boiled;
  
  private static P1ChocolateBoiler uniqueInstance;
  private static int numCalled;
  
  private P1ChocolateBoiler(){}
  
  public static P1ChocolateBoiler getInstance(){
    if(uniqueInstance==null){
      System.out.println("Creating a new instance...");
      uniqueInstance = new P1ChocolateBoiler();
    }
    System.out.println("numCalled: "+numCalled++);
    return uniqueInstance;
  }
  
  public void drain(){
    if(isEmpty()){
      empty=false;
      boiled=false;
    }
  }
    public boolean isEmpty(){
      return empty;
    }
    
    public boolean isBoiled(){
      return boiled;
    }
}