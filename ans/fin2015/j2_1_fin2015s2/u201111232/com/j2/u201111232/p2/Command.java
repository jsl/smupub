package com.j2.u201111232.p2;

public interface Command {
  public void execute();
}