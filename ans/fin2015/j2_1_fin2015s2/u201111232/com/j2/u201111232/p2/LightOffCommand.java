package com.j2.u201111232.p2;

public class LightOffCommand implements Command {
  Light light;
  
  public LightOffCommand(Light light) {
    this.light = light; 
  }
  public void execute() {
    light.off();
  }
}