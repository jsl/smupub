package com.j2.u201111232.p2;

public class P2CalcMain {
  public static void main(String[] args) {
    Remote remote = new Remote();
    Light light = new Light();
    LightOnCommand lightOn = new LightOnCommand(light);
    LightOffCommand lightOff = new LightOffCommand(light);
    
    remote.setCommand(lightOn, lightOff);
    remote.buttonWasPressed();
  }
}