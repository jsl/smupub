package com.j2.u201111232.p2;

public class Remote {
  Command onCommand;
  Command offCommand;
  
  public Remote() { }
                
  public void setCommand(Command onCommand, Command offCommand) {
    this.onCommand = onCommand;
    this.offCommand = offCommand;
  }
  public void buttonWasPressed() {
    this.onCommand.execute();
    this.offCommand.execute();
  }
}
    