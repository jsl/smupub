package com.j2.u201111232.p1;

public class ChocolateBoiler {
  private static ChocolateBoiler unique;
  
  private ChocolateBoiler() {}
  
  public static synchronized ChocolateBoiler getInstance() {
    drain();
    if(unique == null) {
      unique = new ChocolateBoiler();
    }
    return unique;
  }
  
  public static void drain() {
    if(unique == null) {
      if(isEmpty()&&isBoiled()) {
        System.out.println("drain..");
      }
    }
    else 
      System.out.println("already drain");
  }
  public static boolean isEmpty() { return true; }
  public static boolean isBoiled() { return true; }
  
}
    