package com.j2.u201111232.p4;

public abstract class CaffeineBeverage {
  
  final void prepare() {
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  
  abstract void brew();
  abstract void addCondiments();
  
  public void boilWater() {
    System.out.println("Boiling Water");
  }
  public void pourInCup() {
    System.out.println("Pouring into cup");
  }
}