package com.j2.u201111232.p4;

public class Coffee extends CaffeineBeverage {
  
  public void brew() {
    System.out.println("Dripping Coffee through filter");
  }
  public void addCondiments() {
    System.out.println("Adding Sugar and Milk");
  }
}