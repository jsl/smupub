package com.j2.u201111235.p3;
  
  public interface Iterator 
{
  Object next();
  
  boolean hasnext();
  
}