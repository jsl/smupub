package com.j2.u201111235.p3;
  
  public class Intheclassitem
{
  String name;
  String proname;
  int num;
  boolean pass;
  
  public void intheclassitem( String name,String proname,int num,boolean pass)
  {
    this.name=name;
    this.proname=proname;
    this.num=num;
    this.pass=pass;
  }
  public String getName()
  {
    return name;
    
  }
  public String getProname()
  {
    return proname;
  }
  public int getNum()
  {
    return num;
  }
   public boolean getPass()
  {
    return pass;
  }
  public String toString()
  {
    return (name+", "+proname+", "+num+", "+pass);
  }
  
}