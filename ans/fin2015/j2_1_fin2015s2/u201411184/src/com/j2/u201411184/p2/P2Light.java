package com.j2.u201411184.p2;

public class P2Light {
  public P2Light() {}
  public void on() {
    System.out.println("Light is on");
  }
  public void off() {
    System.out.println("Light is off");
  }
}