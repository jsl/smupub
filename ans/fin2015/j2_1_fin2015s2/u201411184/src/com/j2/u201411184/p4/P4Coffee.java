package com.j2.u201411184.p4;

public class P4Coffee extends P4CaffeineBeverage {
  public P4Coffee() {
    super("coffee");
  }
  
  public void brew() {
    System.out.println("Dripping Coffee through filter");
  }
  
  public void addCondiments() {
    System.out.println("Adding Sugar and Milk");
  }
}