package com.j2.u201411184.p3;

public class P3ArrayIterator implements P3Iterator {
  P3Array array;
  int position;
  
  public P3ArrayIterator(P3Array array) {
    this.array = array;
    position = 0;
  }
  public boolean hasNext() {
    if (position > array.position || array.getItem(position) == null)
      return false;
    else
      return true;
  }
  public String next() {
    return array.getItem(position);
  }
}