package com.j2.u201411184.p3;

public class P3Array {
  P3Item[] array;
  public int position;
  
  public P3Array(int num) {
    this.array = new P3Item[num];
    position = 0;
  }
  
  public void add(String object) {
    array.getItem(position).setName(object);
  }
  public P3Item getItem(int p) {
    return array[p];
  }
}