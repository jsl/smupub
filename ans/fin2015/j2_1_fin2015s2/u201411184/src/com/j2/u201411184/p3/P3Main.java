package com.j2.u201411184.p3;

import java.util.*;

public class P3Main {
  public static void main(String[] args) {
    P3Array array = new P3Array(10);
    P3ArrayIterator i = new P3ArrayIterator(array);
    
    array.add("자바 프로그래밍2");
    array.add("융합 소프트웨어 개발");
    array.add("상명 학습 전략");
    array.add("자기 주도 학습");
    array.add("생활 법률");
    array.add("알고리즘");
    array.add("컴퓨터 구조");
    array.add("사회 봉사");
    
    for (int j = 0; j < 10; j++) {
      if (i.hasNext())
        System.out.println(i.next().getName());
    }
  }
}