package com.j2.u201411184.p4;

public abstract class P4CaffeineBeverage {
  String name;
  public P4CaffeineBeverage(String name) {
    setName(name);
    System.out.println("Making " + getName() + "...");
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  public void boilWater() {
    System.out.println("Boiling water");
  }
  public abstract void brew();
  public void pourInCup() {
    System.out.println("Pouring into cup");
  }
  public abstract void addCondiments();
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
}