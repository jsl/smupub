package com.j2.u201411184.p2;

public class P2LightOffCommand implements P2Command {
  P2Light light;
  
  public P2LightOffCommand(P2Light light) {
    this.light = light;
  }
  
  public void execute() {
    light.off();
  }
}