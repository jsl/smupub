package com.j2.u201411184.p2;

public class P2RemoteControlMain {
  public static void main(String[] args) {
    P2RemoteControl remote = new P2RemoteControl();
    P2Light light = new P2Light();
    
    P2LightOnCommand lightOn = new P2LightOnCommand(light);
    P2LightOffCommand lightOff = new P2LightOffCommand(light);
    
    remote.setCommand(lightOn);
    remote.buttonWasPressed();
    
    remote.setCommand(lightOff);
    remote.buttonWasPressed();
  }
}