package com.j2.u201411184.p1;

public class P1ChocolateBoilerMain {
  public static void main(String[] args) {
    P1ChocolateBoiler cb = new P1ChocolateBoiler();
    
    cb.getInstance();
    
    cb.fill();
    cb.drain();
    
    cb.boil();
    cb.drain();
  }
}