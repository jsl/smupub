package com.j2.u201411184.p1;

public class P1ChocolateBoiler {
  P1ChocolateBoiler instance;
  int numOfInstance;
  boolean empty = false;
  boolean boiled = false;
  
  public P1ChocolateBoiler() {
    instance = null;
    numOfInstance = 0;
  }
  
  public P1ChocolateBoiler getInstance() {
    if (instance == null) {
      instance = new P1ChocolateBoiler();
      
      numOfInstance++;
    }
    System.out.println("Number of Instance : " + numOfInstance);
    
    return instance;
  }
  
  boolean isEmpty() {
    return empty;
  }
  
  boolean isBoiled() {
    return boiled;
  }
  
  public void drain() {
    if (isEmpty() && isBoiled())
      System.out.println("Draining...");
  }
  
  public void fill() {
    if (isEmpty())
      System.out.println("Already filled");
    else {
      empty = true;
      System.out.println("Filling...");
    }
  }
  
  public void boil() {
    if (isBoiled())
      System.out.println("Already boiled");
    else {
      boiled = true;
      System.out.println("Boiling...");
    }
  }
}