package com.j2.u201411184.p2;

public class P2RemoteControl {
  P2Command command;
  
  public P2RemoteControl() {}
  public void setCommand(P2Command command) {
    this.command = command;
  }
  public void buttonWasPressed() {
    command.execute();
  }
}