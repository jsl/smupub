package com.j2.u201411184.p3;

public interface P3Iterator {
  public boolean hasNext();
  public Object next();
}