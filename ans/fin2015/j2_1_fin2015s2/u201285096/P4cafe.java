public abstract class cafe{
  void A(){
    boilWater();
    brew();
    poutInCup();
    if(customerWantsCondiments()){
      addCondiments();
    }
  }
  abstract void brew();
  abstract void addCondiments();
  void boilWater(){
    system.out.println("Boiling water");
  }
  void putInCup(){
    system.out.println("Pouring into cup")
  }
  boolean customerWantsCondiments(){
    return true;
  }
  public class CoffeeWithHook extends A(){
    public void brew(){
      system.out.println("Dripping Coffee through filter");
    }
    public void addCondiments(){
      system.out.println("Adding Sugar and Milk");
    }
    public boolean customerWantsCondiments(){
      Sting answer=getUserInput();
      if (answer,toLowerCase().startsWith("y")){
        return true;
      }
    }
  }
}
