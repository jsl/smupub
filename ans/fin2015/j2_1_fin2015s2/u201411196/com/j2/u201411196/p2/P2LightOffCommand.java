package com.j2.u201411196.p2;

public class P2LightOffCommand implements P2Command{
  P2Light l;
  public P2LightOffCommand(P2Light l){
    this.l =l;
  }
  public void execute(){
    l.off();
  }
}