package com.j2.u201411196.p2;

public class P2LightOnCommand implements P2Command{
  P2Light l;
  public P2LightOnCommand(P2Light l){
    this.l =l;
  }
  public void execute(){
    l.on();
  }
}

  