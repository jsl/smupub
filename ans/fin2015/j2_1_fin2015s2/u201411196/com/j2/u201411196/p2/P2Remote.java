package com.j2.u201411196.p2;

public class P2Remote{
  P2Command command;
  public void setCommand(P2Command command){
    this.command = command;
  }
  public void buttonWasPressed(){
    command.execute();
  }
}