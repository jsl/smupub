package com.j2.u201411196.p2;

public class P2Main{
  public static void main(String[] args){
    P2Light l = new P2Light();
    P2LightOnCommand on= new P2LightOnCommand(l);
    P2LightOffCommand off= new P2LightOffCommand(l);
    P2Remote remote = new P2Remote();
    remote.setCommand(on);
    remote.buttonWasPressed();
    remote.setCommand(off);
    remote.buttonWasPressed();
  }
}