package com.j2.u201411196.p3;

public interface P3Iterator{
  public Object next();
  public boolean hasNext();
}
