package com.j2.u201411196.p3;

public class P3ArrayIterator implements P3Iterator{
  int num=0;
  P3Item[] item;
  public P3ArrayIterator(P3Item[] item){
    this.item = item;
  }
  public Object next(){
    Object i = (Object)item[num];
    num++;
    return i;
  }
  public boolean hasNext(){
    if(num >= item.length) return false;
    else return true;
  }
}
    