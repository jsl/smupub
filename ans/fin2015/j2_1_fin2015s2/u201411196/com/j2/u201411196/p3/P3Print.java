package com.j2.u201411196.p3;

public class P3Print{
  P3MyTime time;
  public P3Print(P3MyTime time){
    time  = new P3MyTime();
  }
  public void print(){
    P3Iterator iterator = time.createIterator();
    print(iterator);
  }
  public void print(P3Iterator iterator){
    while(iterator.hasNext()){
      P3Item item = (P3Item)iterator.next();
      System.out.println(item.getName());
    }
  }
}