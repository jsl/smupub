package com.j2.u201411196.p3;

public class P3MyTime implements P3Time{
  P3Item[] i;
  static final int max=3;
  int p=0;
  public P3MyTime(){
    i = new P3Item[max];
    addItem("java");
    addItem("computer architechture");
    addItem("algorithm");
  }
  public P3Iterator createIterator(){
    return new P3ArrayIterator(i);
  }
  public void addItem(String n){
    P3Item item = new P3Item(n);
    i[p] = item;
    p++;
  }
}
    