package com.j2.u201411196.p1;

public class P1ChocolateBoiler{
  private static P1ChocolateBoiler instance ;
  private static int num = 0;
  boolean empty;
  boolean boiled;
  private P1ChocolateBoiler(){
    empty = true;
    boiled = false;
  }
  public static P1ChocolateBoiler getInstance(){
    if(instance == null){
      instance = new P1ChocolateBoiler();
      System.out.println("Chocolate boiler !");
      return instance;
    }else {
     System.out.println("Chocolate boiler is called");
     instance = getInstance();
     return instance;
    }
    num++;
  }
  public void drain(){
    if(!isEmpty() && isBoiled())
    System.out.println("drain drain...");
  }
  public boolean isEmpty(){
    return empty;
  }
  public boolean isBoiled(){
    return boiled;}
}