package com.j2.u201411196.p4;

public abstract class P4Barista{
  public void prepare(){
    System.out.println("Making Coffee...");
    boil();
    brew();
    pour();
    if(customer())
       add();
  }
  public void boil(){
    System.out.println("Boiling Water");
  }
  public void pour(){
    System.out.println("Pouring in the cup");
  }
  public abstract void brew();
  public abstract void add();
  public boolean customer(){
    return true;
  }
}