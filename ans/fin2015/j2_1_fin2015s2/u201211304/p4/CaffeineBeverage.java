package com.j2.u201211304.p4;

public abstract class CaffeineBeverage{

  final void prepareMenu(){
  
    boilWater();
    brew();
    pourInCup();
    addCondiments();
    
  }
  
  abstract void brew();
  abstract void addCondiments();
  
  void boilWater()
  {
    System.out.println("boiling water");
  }
   void pourInCup()
  {
    System.out.println("Pouring into cup");
  }

}