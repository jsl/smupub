package com.j2.u201211304.p2;

public class RemoteControl{
  LightOnCommand lightOn;
  LightOffCommand lightOff;

  void setCommand( LightOnCommand lightOn,LightOffCommand lightOff){
     this.lightOn=lightOn;
     this.lightOff=lightOff;
    
  }
  void OnbuttonWasPressed()
  {
    lightOn.execute();
  }
  void OffbuttonWasPressed()
  {
    lightOff.execute();
  }

}