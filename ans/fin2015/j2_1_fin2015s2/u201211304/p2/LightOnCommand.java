package com.j2.u201211304.p2;

public class LightOnCommand implements Command
{
  
  Light light;
  
  public void LightOnCommand(Light light){
    this.light=light;
  
  }
  public void execute(){
  light.on();
  }

}