package com.j2.u201285094.p4
  public class P2Coffee {
 
 void prepareRecipe() {
  boilWater();
  brewCoffeeGrinds();
  pourInCup();
  addSugarAndMilk();
 }
 
 public void boilWater() {
  System.out.println("Boiling water");
 }
 
 public void brewCoffeeGrinds() {
  System.out.println("Dripping Coffee through filter");
 }
 
 public void pourInCup() {
  System.out.println("Pouring into cup");
 }
 
 public void addSugarAndMilk() {
  System.out.println("Adding Sugar and Milk");
 }
}