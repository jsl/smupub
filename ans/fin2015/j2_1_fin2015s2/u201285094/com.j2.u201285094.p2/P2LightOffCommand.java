package com.j2.u201285094.p2
 public class P2LightOffCommand implements Command {
 Light light;
 
 public LightOffCommand(Light light) {
  this.light = light;
 }
 
 public void execute() {
  light.off();
 }
}

  