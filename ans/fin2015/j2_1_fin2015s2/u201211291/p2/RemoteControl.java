package com.j2.u201211291.p2;

public class RemoteControl{
  Command[] commands;
  public RemoteControl(){
    commands = new Command[2];
  }
  public void setCommand(int a, Command command){
    commands[a] = command;
  }  
  public void buttonWasPushed(int a){
    commands[a].execute();
  }
}