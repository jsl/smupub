package com.j2.u201211291.p2;

public class P2Main{
  public static void main(String args[]){
    Light light = new Light();
    LightOnCommand loc = new LightOnCommand(light);
    LightOffCommand lof = new LightOffCommand(light);
    RemoteControl rc = new RemoteControl();
    rc.setCommand(0,loc);
    rc.setCommand(1,lof);
    rc.buttonWasPushed(0);
    rc.buttonWasPushed(1);
  }
}