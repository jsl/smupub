package com.j2.u201211291.p2;

public class LightOnCommand implements Command{
  Light light;
  public LightOnCommand(Light light){
   this.light = light; 
  }
  public void execute(){
    light.lighton();
  }
}