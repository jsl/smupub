package com.j2.u201211291.p2;

public interface Command{
  void execute();
}