package com.j2.u201211291.p4;

public abstract class CaffeineBeverage{
  public final void preparerecipie(){
    description();
    boil();
    brew();
    pour();
    addCondiments();
  }
  abstract void description();
  public void boil(){
    System.out.println("Boiling water");
  }
  abstract void brew();
  public void pour(){
    System.out.println("Pouring into cup");
  }
  abstract void addCondiments();
}
                     