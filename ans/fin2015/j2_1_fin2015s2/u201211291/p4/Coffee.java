package com.j2.u201211291.p4;

public class Coffee extends CaffeineBeverage{
  public void description(){
    System.out.println("Making Coffee");
  }
  public void brew(){
    System.out.println("Dripping Coffee through filter");
  }
  public void addCondiments(){
    System.out.println("Adding sugar and Milk");
  }
}