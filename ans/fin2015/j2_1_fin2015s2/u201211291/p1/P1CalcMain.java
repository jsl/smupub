package com.j2.u201211291.p1;

public class P1CalcMain{
  public static void main(String args[]){
    ChocolateBoiler cb  = ChocolateBoiler.getInstance();
    cb.fill();
    cb.boil();
    cb.drain();
    cb.drain();
    cb.boil();
    cb.fill();
    cb.boil();
    cb.drain();
    ChocolateBoiler fb  = ChocolateBoiler.getInstance();
    cb.fill();
    fb.boil();
    fb.drain();
  }
}