package com.j2.u201211291.p1;

public class ChocolateBoiler{
  static ChocolateBoiler uniqueInstance;
  static boolean empty;
  static boolean boil;
  private ChocolateBoiler(){
    empty = true;
    boil = false;
  }
  public static ChocolateBoiler getInstance(){
    if(uniqueInstance == null){
      uniqueInstance = new ChocolateBoiler();
      System.out.println("making chocolateboilerinstance");
    }
    return uniqueInstance;
  }
  public void drain(){
    if(isEmpty() == false && isBoiled() == true){
      System.out.println("chocolate drain");
    }
    empty =true;
    boil = false;
  }
  public void fill(){
    if(empty == true && boil == false){
      System.out.println("filled");
    empty = false;
    }
  }
  public void boil(){
    if(empty != true && boil ==false){
      System.out.println("boilled");
    boil = true;
    }
  }
  public boolean isEmpty(){   
    return empty;
  }
  public boolean isBoiled(){
    return boil;
  }
}

