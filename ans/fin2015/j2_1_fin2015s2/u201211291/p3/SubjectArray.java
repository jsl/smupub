package com.j2.u201211291.p3;
import java.util.*;
public class SubjectArray{
  SubjectItem [] subjects;
  public SubjectArray(){
    subjects = new SubjectItem[7];
    subjects[0] = new SubjectItem("융소개");
    subjects[1] = new SubjectItem("자바");
    subjects[2] = new SubjectItem("이산수학");
    subjects[3] = new SubjectItem("신호해석");
    subjects[4] = new SubjectItem("생명과학");
    subjects[5] = new SubjectItem("알고리즘");
    subjects[6] = new SubjectItem("운동과건강");
  }
  public Iterator createIterator(){
   Iterator arrayiterator = new ArrayIterator(subjects);
   return arrayiterator;
  }
  public void print(){
   Iterator arriter = createIterator();
   while(arriter.hasNext()){
     System.out.println(arriter.next());
  }
}
}