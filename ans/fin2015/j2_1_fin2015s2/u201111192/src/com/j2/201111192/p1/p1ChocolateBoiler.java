package com.j2.u201111192.p1;
public class p1ChocolateBoiler{
  private static p1ChocolateBoiler ch;
  private boolean empty=true;
  private boolean boil=false;
  private p1ChocolateBoiler()
  {
   empty=true;
   boil=false;
  }
  public static p1ChocolateBoiler getInstance(){
    if(ch==null){
     ch=new p1ChocolateBoiler();
     
    }
    return ch;
  }
  public void boil(){
    if(!isEmpty()&&!isBoiled()){
     System.out.println("boil"); 
     boil=true;
     empty=false;
    }
  }
  public void drain(){
    if(!isEmpty() && isBoiled()){
     System.out.println("Drain");
     empty=false;
     
    }
  }
  public void fill(){
    if(isEmpty() && !isBoiled()){
     System.out.println("Fill."); 
     empty=false;
    }
  }
  public boolean isBoiled(){
    return boil;
  }
  public boolean isEmpty(){
   return empty; 
  }
}