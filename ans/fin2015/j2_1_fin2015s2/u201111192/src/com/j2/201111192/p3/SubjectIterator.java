package com.j2.u201111192.p3;
public class SubjectIterator implements Iterator{
  Subject subjects[];
  int pos=0;
  public SubjectIterator(Subject subjects[]){
    this.subjects=subjects;
  }
  public boolean hasNext(){
    if(subjects.length>pos && subjects[pos]!=null){
     return true; 
    }
    else
    return false;
    
  }
  public Object next(){
    return subjects[pos++];
  }
  
}