package com.j2.u201111192.p3;
public class SubjectList{
 Subject[] subjects;
 int num=6;
 int count=0;
 public SubjectList(){
   subjects=new Subject[num];
   add("python");
   add("java1");
   add("java2");
   add("math");
   add("art");
   add("algorithm");
 }
 public void add(String name){
   if(num>count){
    subjects[count++]=new Subject(name); 
   }
   else
     System.out.println("더이상 추가불가");
 }
 public SubjectIterator createIterator(Subject subjects[]){
   return new SubjectIterator(subjects);
 }
}