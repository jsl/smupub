package com.j2.u201111192.p4;
public abstract class Beverage{
  public void prepare(){
    boilWater();
    brew();
    pourInCup();
    addCondiments();
  }
  public void boilWater(){
   System.out.println("Boiling water"); 
  }
  public void pourInCup(){
    System.out.println("Pouring in cup");
  }
  public abstract void addCondiments();
  public abstract void brew();
}