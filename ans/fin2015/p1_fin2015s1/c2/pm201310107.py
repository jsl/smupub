
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def lab1():
  sound=makeSound(getMediaPath("preamble.wav"))
  soundTime=getLength(sound)/getSamplingRate(sound)
  return soundTime
  
def lab2():
  sound=makeSound(getMediaPath("preamble.wav"))
  for index in range(0,getLength(sound)):
    value=getSampleValueAt(sound,index)
    if value<0:
      setSampleValueAt(sound,index,-32768)
  explore(sound)
  return sound
  
def lab3(freq,amplitude):
  sound=makeEmptySound(22050*10)
  interval=1/freq
  sR=getSamplingRate(sound)
  #print sR
  samplesPerCycle=sR*interval
  #print samplesPerCycle
  sampleValue=-(amplitude)
  for index in range(0,getLength(sound)):
    sampleValue+=5
    if sampleValue==abs(amplitude):
      setSampleValueAt(sound,index,-(amplitude))
  explore(sound)
  return sound
  

def lab4():
  phones()
  findPhone("Kim")
  
def phones():
  return """
  Kim:2287-1000:Undergraduate Student
  Lee:2287-2000:Staff
  Lim:2287-3000:Graduate Student
  Hong:2287-4000:Teaching Staff"""
  
def findPhone(person):
  phoneBook=phones()
  phone=phoneBook.split("\n")
  #phoneLast=phone.split(",")
  print phone
  for char in range(0,):
    if phoneLast[0]==person:
      print phone[0:]
  
  
def lab5(word):
  res=""
  mirrorPoint=11
  for char in range(0,mirrorPoint,2):
    res+=word[char]
  return word[:]+res[::-1]
  
  

  
def main():
  lab1()
  lab2()
  lab3(10,5)
  lab4()
  lab5("Subject: Programming 1")

main()
