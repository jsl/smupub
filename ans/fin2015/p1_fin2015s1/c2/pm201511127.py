
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  #lab7()

def lab1():
  preamble=makeSound(getMediaPath('preamble.wav'))
  computeTimeLength(preamble)

def computeTimeLength(sound):
  len=getLength(sound)
  sr=getSamplingRate(sound)
  print int(len/sr)


def lab2():
  preamble=makeSound(getMediaPath('preamble.wav'))
  onlyMinimize(preamble)

def onlyMinimize(sound):
  for sample in getSamples(sound):
    value=getSampleValue(sample)
    if value<0:
      setSampleValue(sample,-32768)
  explore(sound)
  return sound


def lab3():
  sawtooth(10,20000)

def sawtooth(freq,amplitude):
  sound=makeEmptySoundBySeconds(1)
  sr=getSamplingRate(sound)
  spc=1.0/freq
  interval=sr*spc
  sampleVal=amplitude
  s=1
  i=0
  for s in range(0,getLength(sound)):
    if i>amplitude:
      sampleVal=sampleValue*-1
      i+=1
    setSampleValueAt(sound,s,sampleVal)
  explore(sound)
  return sound


def lab4():
  findPhone("Kim")

def phones():
  return"""Kim:2287-1000:Undergraduate Student
Lee:2287-2000:Staff
Lim:2287-3000:Graduate Student
Hong:2287-4000:Teaching Staff"""

def phoneList():
  phone=phones()
  phoneList=phone.split("\n")
  return phone

def findPhone(person):
  phoneLi=phoneList()
  newPhoneList=phoneLi.split(":")
  if newPhoneList[0]==person:
    print newPhoneList[1]


def lab5():
  str="Subject: Programming 1"
  mirrorStr(str)

def mirrorStr(str):
  li=list(str)
  for x in range(0,len(str)):
    if x%2==0:
      for y in range(0,len(str)/2):
        a=x
        b=len(str)-x-1
        li[b]=li[a]
  print li
        

def lab6():
  str="Subject: Programming 1"
  en=encode(str)
  #decode(en)

def encode(str):
  for x in range(0,len(str)):
    a=str[x]
    b=ord(a)+1
    c=chr(b)
    print c

def decode(encodedStr):
  for y in range(0,len(li)):
    a=encodedStr[y]
    b=ord(a)-1
    c=chr(b)
    print c


def lab7():
  makeHomePage(YJL,programming)

#def homepage(name,interest):
#def doctype():
#def title(titlestring):
#def body(bodystring):

main()
