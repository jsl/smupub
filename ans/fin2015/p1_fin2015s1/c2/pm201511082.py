import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def computeTimeLength(sound):
  x=getLength(sound)/getSamplingRate(sound)
  print x
def lab1():
  sound=makeSound(getMediaPath("preamble.wav"))
  computeTimeLength(sound)
def onlyMinimize(sound):
  for i in range(0,getLength(sound)):
    if getSampleValueAt(sound,i)<0:
      setSampleValueAt(sound,i,-32768)
  explore(sound)
  return(sound)
def lab2():
  sound=makeSound(getMediaPath("preamble.wav"))
  onlyMinimize(sound)  
def mirrorStr(str):
  mirrorPoint=len(str)/2
  s=list(str)
  left=s[:mirrorPoint]
  right=s[mirrorPoint::-2]
  all=left+right
  print all   
def lab5():
  mirrorStr("Subject:Programming 1")  
def encode(str):
  s=list(str)
  left=s[len(str)-1:]
  right=s[:len(str)-1]
  all=left+right
  print all
def decode(encodedStr):
  s=encode(str)
  left=s[1:]
  right=s[:1]
  all=left+right
  print all  
def lab6():
  encodedStr=encode("Subject:Programming 1")
def main():
  lab1()
  lab2()
  lab5()
  lab6()

main()
    
