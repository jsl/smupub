
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def main():
  lab1()
  lab2()
  lab3()
  #lab4()
  lab5()
  #lab6()
  lab7()

def lab1():
  sound = makeSound(getMediaPath("preamble.wav"))
  computeTimeLength(sound)

def computeTimeLength(sound):
  per = getSamplingRate(sound)
  len = getLength(sound)
  time = float(len/per)
  print "It takes ", time, " seconds"

def lab2():
  sound = makeSound(getMediaPath("preamble.wav"))
  onlyMinimize(sound)

def onlyMinimize(sound):
  for s in getSamples(sound):
    if getSample(s)<0:
      setSampleValue(s, -32768)
  explore(sound)

def lab3():
  amplitude = 3000
  freq = 3000
  sawtooth(freq,amplitude)
  
def sawtooth(freq, amplitude):
  sound = makeSound(getMediaPath("sec3silence.wav"))
  freqIndex=1
  for s in range(0, getLength(sound)):
    if (freqIndex<(freq/2)):
      setSampleValueAt(sound,s,amplitude)
    if (freqIndex>(freq/2)):
      setSampleValueAt(sound,s,-amplitude)
    if freqIndex==freq:
      freqIndex = 1
    freqIndex += 1
  explore(sound)

def lab4():
  person = "Lim"
  findPhone(person)

def phones():
  return """
  Kim:2287-1000:Undergraduate
  StudentLee:2287-2000:Staff
  Lim:2287-3000:Graduate Student
  Hong:2287-4000:Teaching Staff"""

def findPhone(person):
  str = phones()
  a = str.startswith(person)
  #  list.split()
  print a
  
def lab5():
  str = "Subject: Programming 1"
  mirrorStr(str)

def mirrorStr(str):
  mlist = list(str)
  for i in range(0, len(mlist)/2):
    if(i%2)==0:
      mirror = mlist[i]
      mlist[len(mlist)-1-i] = mirror
  str = "".join(mlist)
  print str

def lab6():
  str = "Subject: Programming 1"
  encode(str)

def encode(str):
  #mlist = list(str)
  for i in range(0, len(str)):
    change = ord(str[i]) + 1
    print change

def lab7():
  name = "Park Ye Seul"
  interest = "watching TV"
  makeHomePage(name, interest)

def makeHomePage(name, interest):
  file = open("C:\\Users\\P400\\Desktop\\mediasources\\mediasources\\test.html","wt")
  file.write(doctype())
  file.write("<html>\n<head>")
  titlestring = name+"'s Home Page"
  file.write(title(titlestring))
  file.write("</head>")
  bodystring = "Welcome to "+name+"'s Home Page</h1>\n<p>I am interested in "+interest+"</p>"
  file.write(body(bodystring))
  file.write("</html>")
  file.close()

def doctype():
  return """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"
  "http://wwww.w3.org/TR/html4/loose.dtd">"""

def title(titlestring):
  string = "<title>"+titlestring+"</title>"
  return string

def body(bodystring):
  string = "<body>\n<h1>"+bodystring+"\n</body>"
  return string

main()
