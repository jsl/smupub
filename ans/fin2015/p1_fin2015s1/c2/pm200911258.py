import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

### LAB 1
def lab1():
  sound =makeSound(getMediaPath("preamble.wav"))
  computeTimeLength(sound)
  
def computeTimeLength(sound):
  sr = getSamplingRate(sound)
  length = getLength(sound)
  time = length/sr
  result = time
  print "Total Time : " 
  print result
  return result
  
  
### LAB 2   o
def lab2():
  sound = makeSound(getMediaPath("preamble.wav"))
  onlyMinimize(sound)

def onlyMinimize(sound):
  for s in range(0, getLength(sound)):
    sample = getSampleValueAt(sound, s)
    if sample < 0 :
      setSampleValueAt(sound, s, -32768)
  explore(sound)
  return sound

### LAB 3  ^
def lab3():
  sawtooth(440, 2200)
  
def sawtooth(freq, amplitude):
  sound = makeSound(getMediaPath("sec1silence.wav"))
  sr = getSamplingRate(sound)
  interval = 1.0/freq
  samplingPerCycle = int(sr * interval)
  v = 1
  for s in range(0, getLength(sound)):
    if s % samplingPerCycle == 0:
      v = v*-1
    if v == 1:
      setSampleValueAt(sound, s, amplitude)
    else:
      setSampleValueAt(sound, s, -amplitude)
  explore(sound)
  return sound
  
### LAB 4  o
def lab4():
  findName = "Lee"  
  findPhone(findName)  ##findPhone(name, list)
  
def phones():
  list = ["Kim:2287-1000:Undergraduate Student",
          "Lee:2287-2000:Staff",
          "Lim:2287-3000:Graduate Student",
          "Hong:2287-4000:Teaching Staff"]
  return list

def findPhone(person):
  list = phones()
  for s in list:
    name = s.find(person)
    if name <> -1 :
      startPhone = s.find(":")
      endPhone = s.rfind(":")
      print s[startPhone+1:endPhone]
    
    
### LAB 5    o
def lab5():
  str = "Subject: Programming 1"
  mirrorStr(str)
  
def mirrorStr(str):
  length = 0
  str2 = ""
  for s in str:
    length = length + 1    
  half = length/2  
  for s in range(0, half):
    str2 = str2 + str[s]
  if length%2 <> 0:
    str2 = str2 + str[s+1]
  for s in range(half, length):
    str2 = str2 + str[length-s-1]
  print str2

### LAB 6 x  ord???....TT
def lab6():
  str = "abcd"
  eStr = encode(str)
  
#def encode(str):
#  for s in str:
#    next = ord(s) + 1

def encode(str):
  for s in str:
    print encode2(s)
    
def encode2(char):
  if char == 'a':
    return 'b'
  if char == 'b':
    return 'c'
  if char == 'c':
    return 'd'
  if char == 'd':
    return 'e'
  ##..etc
    
    
### LAB 7 ### o
def lab7():
  makeHomePage("Lee", "Computer") ### makeHomePahe(your name, your interest)
  
def makeHomePage(name, interest):
  file = open(getMediaPath("homepage.html"), "wt")
  file.write(doctype())
  file.write(title(name))
  file.write(body(interest))
  file.close()
  
def doctype():
  return "<!doctype html public '-//w3c//dtd html 4.01 Transition//en''http://www.w3.org/tr/html4/loose.dtd'>"""

def title(name):
  return "<html><head><title>" + name + "'s Home Page</title></head><body><h1>Welcome to"+name
  
def body(interest):
  return "'s Home Pahe</h1><p>I am interested in "+interest+"</p></body></html>"
 
### main ### 
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab6()
  lab5()
  lab7()

main()
