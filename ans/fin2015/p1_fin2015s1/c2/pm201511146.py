import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  lab1()
  lab2()
  lab3()
  lab5()
  lab6()
  lab7()
  
def lab1():
  sound=makeSound(getMediaPath("Preamble.wav"))
  computeTimeLength(sound)
  
def lab2():
  sound=makeSound(getMediaPath("Preamble.wav"))
  onlyMinimize(sound)
  
def lab3():
  sawtooth(5,20)
  
def lab5():
  mirrorStr("Subject: Programming 1")
  
def lab6():
  str="Subject: Programming 1"
  encode(str)
  encodedstr = "Tvckfdu Qsphsbnnjoh 2"
  decode(encodedstr)
  
def lab7():
  makeHomePage("ChangHee Han","Programming")
  
  
  
  
def computeTimeLength(sound):
  time=getDuration(sound)
  print time
  
def onlyMinimize(sound):
  for s in range(getLength(sound)):
    sample=getSampleValueAt(sound,s)
    if sample < 0:
     setSampleValueAt(sound,s,-32768)
  explore (sound)
  return sound
  
def sawtooth(freq,amplitude):
  sound=makeEmptySound(getLength(makeSound(getMediaPath("Preamble.wav"))))
  second = 1.0
  samplingrate = getSamplingRate(sound)
  inte= 1.0 * second / freq
  SPC=inte*samplingrate
  SPHC=SPC/2
  interval=amplitude
  i=0
  s=0
  for s in range(getLength(sound)):
    if i > SPHC :
      interval=0
      i=0 
    setSampleValueAt(sound,s,interval)
    interval=interval+1
  explore(sound)
  
def mirrorStr(str):
  mirrorpoint=len(str)/2
  answ=""
  left=str[0:mirrorpoint]
  right=""
  for i in range(0,mirrorpoint,2):
    rig=str[mirrorpoint-i-1:mirrorpoint-i]
    rig2=str[mirrorpoint+i+1:mirrorpoint+i+2]
    right=right+rig+rig2
  left=left+right
  return left 
  
def encode(str):
  aplpa="a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9"
  aplpabet=aplpa.split()
  list=""
  for s in str:
    for n in range(len(aplpabet)):
      if aplpabet[n]==s:
        a=aplpabet[n+1]
        list=list+a
  print list
  
  
def decode(encodedstr):
  aplpa="a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9"
  aplpabet=aplpa.split()
  list=""
  for s in encodedstr:
    for n in range(len(aplpabet)):
      if aplpabet[n]==s:
        a=aplpabet[n-1]
        list=list+a
  print list
  
def makeHomePage(name,interest):
  index=open('C:\\Users\\P400\\Desktop\\mediasources-py2ed\\mediasources\\page.html',"wt")    #
  index.write(doctype())
  index.write(title(name))
  index.write(body(interest))
  index.close()


def doctype():
  return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"http://wwww.w3.org/TR/html4/loose.dtd">'+'\n'
  
  
def title(titlestring):
  return "<html>"+"\n"+"<head>"+"\n"+"<title>"+titlestring+"'s Home Page</title>"+"\n"+"</head>"+"\n"+"<body>"+"\n"+"<h1>Welcome to "+ titlestring+"'s Home Page</h1>"
  
  
def body(bodystring):
  return "<p>I am interseted in "+bodystring + "</p>"+"\n"+"</body>"+"</html>"

main()
