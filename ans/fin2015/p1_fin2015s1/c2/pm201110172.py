
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def computeTimeLength():
  sound=makeSound(getMediaPath("preamble.wav"))
  length=getLength(sound)
  sr=getSamplingRate(sound)
  print length/sr
  
def onlyMinimize():
  sound=makeSound(getMediaPath("preamble.wav"))
  sindex=0
  for sample in getSamples(sound):
    sam=getSampleValue(sample)
    if sam<0:
      setSampleValue(sample,-32768)
  explore(sound)
  
def sawtooth(freq,amp):
  sound=makeSound(getMediaPath("sec1silence.wav"))
  sr=getSamplingRate(sound)
  second=1
  interval=1.0/freq
  sampleval=amp
  i=1
  spc=interval*sr
  sphc=int(spc/2)
  for s in range(0,getLength(sound)):
    if(i>sphc):
      sampleval=sampleval*-1
      i=0
      setSampleValueAt(sound,s,sampleval)
    else:
      i=i+1
  explore(sound)
  
def phonebook():
  return"""
Kim:2287-1000:Undergraduate Student
Lee:2287-2000:Staff
Lim:2287-3000:Graduate Student
Hong:2287-4000:Teaching Staff"""

def phones():
  phones=phonebook()
  phonelist=phones.split("\n")
  newlist=[]
  for list in phonelist:
    newlist=newlist+[list.split(":")]
  return newlist
  
def findPhone():
  person="Lee"
  for people in phones():
    if people[0]==person:
      print "Phone number for",person,"is",people[1]
   
def mirrorStr():
  str="Subject: Programming 1"
  length=len(str)
  mp=length/2
  change=""
  orig=""
  for n in range(0,length):
    if n%2==1:
      change=change+str[mp-n-1]
    if n%2==0:
      orig=orig+str[n]
  newword=orig+change
  print newword
  
def encode():
  str='Subject: Programming 1'
  length=len(str)
  word1=""
  word2=""
  for n in range(length):
    if n+1<length:
      orig=str[n]
      word1=word1+orig
    if n+1>=length:
      orig=str[n]
      word2=orig+word2
  newword=word2+word1
  print newword
  
def decode():
  encodestr='1Subject: Programming'
  length=len(encodestr)
  word1=""
  word2=""
  for n in range(length):
    if n+1<=1:
      orig=encodestr[n]
      word1=orig+word1
    if n+1>1:
      orig=encodestr[n]
      word2=word2+orig
  newword=word2+word1
  print newword



def doctype():
  return '<!DOCTYPE HTML>'
def title(titlestring):
  return '<html><head><title>'+titlestring+'</title></head>'
def body(bodystring):
  return '<body>'+bodystring+'</body></html>'
  
def makeHomePage():
  file=open("C:\\Users\\P400\\Desktop\\homepage.html","wt")
  file.write(doctype())
  file.write(title("SeoYeon `s Home Page"))
  file.write(body('<h1>Welcome to SeoYeon `s Home Page'))
  file.write(body('<p>I am interested in chess</p></h1>\n'))
  file.close()
  
def main():
  computeTimeLength() #sound=preamble.wav
  onlyMinimize()
  phones()
  findPhone() #I put person in 'Lee'
  mirrorStr()
  encode() #str='Subject: Programming 1'
  decode() #encodestr='1Subject: Programming'
  makeHomePage() #name=SeoYeon, interest=chess
  
  
main()  
