def computeTimeLength():
  sound=makeSound(getMediaPath("preamble.wav"))
  time=getDuration(sound)
  print(time)
  
def sawtooth():
  sound=makeSound(getMediaPath("frequent.wav"))
  len=getLength(sound)
  mirrorPoint=len/2
  for index in range(0,mirrorPoint):
    left=getSampleObjectAt(sound,mirrorPoint)
    right=getSampleObjectAt(sound,len-index-1)
    value=getSampleValue(left)
    setSampleValue(right,value)
  explore(sound)
  play(sound)
  return(sound)
  
  
def mirrorStr():
  