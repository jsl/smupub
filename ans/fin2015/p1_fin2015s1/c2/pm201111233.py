
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def lab1():
  file=getMediaPath("preamble.wav")
  sound=makeSound(file)
  timelength=computeTimeLength(sound)
  return timelength
  
def computeTimeLength(sound):
  dur=getDuration(sound)
  timeLength=dur
  return timeLength

def lab2():
  file=getMediaPath("preamble.wav")
  sound=makeSound(file)
  onlyMinimize(sound)
  
def onlyMinimize(sound):
  for s in getSamples(sound):
    s1=getSampleValue(s)
    if(s1<0):
      setSampleValue(s,-32768.0)
  explore(sound)  

def lab4():
  p=phones()
  findPhone(person)
  
def phones():
  s1="Kim:2287-1000:Undergraduate Student"
  s2="Lee:2287-2000:Staff"
  s3="Lim:2287-3000:Graduate Student"
  s4="Hong:2287-4000:Teaching Staff"
  l1=list(s1)
  l2=list(s2)
  l3=list(s3)
  l4=list(s4)
  return l1+l2+l3+l4
  
def lab5():
  str="Subject: Programming 1"
  string=mirrorStr(str)
  return string
  
def mirrorStr(str):
  temp=''
  temp1=''
  a=0
  b=int(len(str)-1)
  leng=len(str)/2
  for i in range(leng):
    if((i-1)%2)==0:
      temp=str[i]
      str[b-i]=temp
  return str      
      
def lab6():
  str='abcde'
  encode(str)
  decode(encodedStr)
  
#def encode(str):
#  t1=ord(str[0])
#  t2=ord(str[1])
#  t3=ord(str[2])
#  t4=ord(str[3])
#  t5=ord(str[4])
#  strtemp=''
#  leng=len(str)
#  for i in range(leng):
#    str[i]=  
  
def lab7():
  name="Juseok"
  interest="Soccer"
  makeHomePage(name,interest)
  
def makeHomePage(name,interest):
  file=open("C:\Users\P400\Desktop\mediasources\homepage.html","wt")
  file.write(doctype())
  file.write("""http://www.w3.org/TR/html4/loose.dtd">"""
  +title(name+"""'s Home Page""")
  +body("""<h1> Welcome to """ +name+"""'s Home Page </h1>"""+
  """<p>I am interested in """+interest+"""</p>"""))
  file.close()
  
def doctype():
  return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"'
  
def title(titlestring):
  return "<html><head><title>"+titlestring+"</title></head>"
  
def body(bodystring):
  return "<body>"+bodystring+"</body></html>"
  
def main():
  lab1()
  lab2()
  lab7()

main()
