def main():
  lab1()  #o
  lab2()  #o
  #lab3() #x
  #lab4() #x
  #lab5() #x
  #lab6() #x
  #lab7() #x


def lab1(): #o
  computeTimeLength(sound)

def computeTimeLength(sound):
  soundDuration=getDuration(sound)
  print soundDuration
  return sound
  
def lab2(): #o
  onlyMinimize(sound)
  
def onlyMinimize(sound):
  for sample in getSamples(sound):
    value=getSampleValue(sample)
    if value<0 :
      setSampleValue(sample,-32768)
  explore(sound)
  return sound

#def lab3(): #x

#def lab4(): #x

#def lab5(): #x

#def lab6(): #x

#def lab7(): #x

def makeHomePage(name,interest):
  file.open(getMediaPath("homepage.txt"),'wt')
  file.write(doctype())
  #<html>
  #<head>
  file.write(title(name))
  #</head>
  file.write(body(bodystring))
  #</html>
  file.close()
  
def doctype():
  return """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"http://wwww.w3.org/TR/html4/loose.dtd">"""
  
def title(name):
  return """<title>name's Home Page</title>"""