import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  A=makeSound(getMediaPath("preamble.wav"))
  computeTimeLength(A)
  onlyMinimize(A)
  sawtooth(220,4000)
  str='subject: Programming 1'
  mirrorStr(str)

def computeTimeLength(source):
  #A=makeSound(getMediaPath("preamble.wav"))
  B=getDuration(source)
  print int(B)
  
def onlyMinimize(sound):
  A=makeSound(getMediaPath("preamble.wav"))
  for s in getSamples(sound):
    value=getSampleValue(s)
    if value <0:
      setSampleValue(s,-32768)
  explore(sound)
  
def sawtooth(freq,amplitude):
  mySound=getMediaPath("sec1silence.wav")
  Saw=makeSound(mySound)
  sr=getSamplingRate(Saw)
  interval=1.0*freq
  spc=interval*sr
  sampleVal=-(amplitude)
  for s in range(0,getLength(Saw)):
    sampleVal+=5
    setSampleValueAt(Saw,s,sampleVal)
    if sampleVal==abs(amplitude):
      setSampleValueAt(Saw,s,-(amplitude))
  explore(Saw)
  return Saw


  
def mirrorStr(str):
  str='subject: Programming 1'
  MP= int(len(str)/2)
  res = str[:MP]
  print res + res[::-1]

  

A=makeSound(getMediaPath("preamble.wav"))
#computeTimeLength(A)
#onlyMinimize(A)
sawtooth(220,4000)
#str='subject: Programming 1'
#mirrorStr(str)

