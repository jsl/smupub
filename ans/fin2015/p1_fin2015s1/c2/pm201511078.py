
import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *
def computeTimeLength(sound):
  len=getLength(sound)
  sRate=getSamplingRate(sound)
  time=len/sRate
  print time
  
def lab1():
  sound=makeSound(getMediaPath('preamble.wav'))
  ss=computeTimeLength(sound)
  
def onlyMinimize(sound):
  index=0
  for index in range(0,getLength(sound)):
    value=getSampleValueAt(sound,index)
    if value<0:
      setSampleValueAt(sound,index,-32768)
  return sound      

def lab2():
  sound=makeSound(getMediaPath('preamble.wav'))
  ss=onlyMinimize(sound)
  explore(ss)
  
def sawtooth():
  target=makeSound(getMediaPath('sec3silence.wav'))
  for index in range(0,getLength(target)):
    value=getSampleValueAt(target,index)
    setSampleValueAt(target,index,15000)
    index=index+1
    if index%13230==0:
      setSampleValueAt(target,index-1,-15000)
      index=index+1
  explore(target)
  return target
  
def lab3():
  sawtooth()
 
def main():
  lab1()
  lab2()
  lab3()

main()
