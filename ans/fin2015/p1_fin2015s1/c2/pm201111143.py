import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  lab1()
  lab2()
  lab3()
  #lab4()
  #lab5()
  #lab6()
  #lab7()
  
  
  
def lab1():
  sound = makeSound(getMediaPath("gettysburg.wav"))
  computeTimeLength(sound)
  

def computeTimeLength(sound):
  len = getLength(sound)
  
  preamble = makeSound(getMediaPath("preamble.wav"))
  freq = getLength(preamble)/19
  print(freq)
  time = (len/freq)
  
  print("time is ")
  print(time)
  
  
def lab2():
  sound = makeSound(getMediaPath("always.wav"))
  explore(sound)
  onlyMinimize(sound)
  

def onlyMinimize(sound):
  len = getLength(sound)
  
  for i in range(0,len):
    value = getSampleValueAt(sound, i)
    if  value< 0 :
      setSampleValueAt(sound, i , -32768)
  
  explore(sound)


def lab3():
  sawtooth(1600,18000)
  
  
def sawtooth(freq,amplitude):
  len = 30000
  output = makeEmptySound(len)
  
  freqNumber =1
  for i in range(len):
    if freqNumber > freq:
      freqNumber =1
    setSampleValueAt(output, i , (amplitude*freqNumber/freq))
    freqNumber = freqNumber+1
    
  explore(output)
    
    
def lab4():
  findPhone("Kim")
  
def phones():
  list[0] = "Kim:2287-1000:Undergraduate Student"
  list[1] = "Lee:2287-2000:Staff"
  list[2] = "Lim:2287-3000:Graduate Student"
  list[3] = "Hong:2287-4000:Teaching Staff"
  
  return list

def findPhone(person):
  list = phones()



def lab5():
  str = "Subject : Programming 1"
  mirrorStr(str)
  

def mirrorStr(str):
  len = 23
  
  for i in range(len):
    if i%2 == 0:
      str[i] = str[len -i -1]
      
  print(str)
    

def lab6():
  str = "Subject : Programming 1"
  encodedStr = encode(str)
  print(encodedStr)
  #output = decode(encodedStr)
  #print(output)
  

def encode(str):

  for i in range(10000):
    temp = str[i]
    temp = unicode(temp)
    print(temp)
    
    
  print(str)
  return(str)

main()
