import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  lab1()
  lab2()
  lab3()
  #lab4()
  lab5()
  #lab6()
  #lab7()

def lab1():
  sound=makeSound(getMediaPath('preamble.wav'))
  computeTimeLength(sound)
  
def computeTimeLength(sound):
  time=getLength(sound)/getSamplingRate(sound)
  print time

def lab2():
  sound=makeSound(getMediaPath('preamble.wav'))
  onlyMinimize(sound)
  
def onlyMinimize(sound):
  for i in getSamples(sound):
    value=getSampleValue(i)
    if value<0:
      setSampleValue(i,-32768)
  explore(sound)

def lab3():
  sawtooth(440,4000)

def sawtooth(freq,amplitude):
  mySound=getMediaPath('sec1silence.wav')
  sawSound=makeSound(mySound)
  sr=getSamplingRate(sawSound)
  interval=1.0/freq
  spc=sr*interval
  spcHalf=int(spc/2)
  sampleValue=amplitude
  increment=int(amplitude/spcHalf)
  i=1
  for s in range(0,getLength(sawSound)):
    if i==spcHalf:
      sampleValue=sampleValue*-1
      i=0
    sampleValue=sampleValue-increment
    i=i+1
    setSampleValueAt(sawSound,s,sampleValue)
  explore(sawSound)

#def lab4():

def phonebook():
  return""" Kim:2287-1000:Undergraduate Student
  Lee:2287-2000:Staff
  Lim:2287-3000:Graduate Student
  Hong:2287-4000:Teaching Staff"""

def phones():
  phones=phonebook()
  phonelist=list(phones,['\n'])
  print phonelist

#def findPhone(person):

def lab5():
  str="Subject: Programming 1"
  mirrorStr(str)

def mirrorStr(str):
  totalLen=len(str)
  mirrorPoint=totalLen/2
  left=""
  right=""
  for i in range(0,mirrorPoint):
    if i%2==0:        
      left=left+str[i]
      right=right+str[mirrorPoint-i-1]
  all=left+right
  print all

def lab6():
  encodedStr=encode(str)
  decode(encodedStr)

def encode(str):
  split=list(str)
  for i in range(0,len(split)):
    ascii[i]=ord(split[i])
    ascii2[i]=ascii[i]+1
    #encodedStr=
  return encodedStr

def decode(encodedStr):
  split=list(encodedStr)
  for i in range(0,len(encodedStr)):
    ascii[i]=ord(split[i])
    real_ascii[i]=ascii[i]-1
    #real_Str=
  print real_Str

def lab7():
  makeHomePage('KMJ','take a photo')

def makeHomePage(name,interesting):
  titlestring="""<html> <head> <title>JSL's Home Page</title> </head>"""
  bodystring="""<body> <h1> Welcome to JSL's Home Page</h1> <p>I am interested in programming</p> </body> </html>"""
  bodystring.replace("programming","interest")
  titlestring.replace("JSL","name")
  file=open(getMediaPath('pm201511086.html'),'wt')
  file.write(doctype())
  file.write(title(titlestring))
  file.write(body(bodystring))
  file.close
  
def doctype():
  return '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN" "http://wwww.w3.org/TR/html4/loose.dtd">
  '''

def title(titlestring):
  return titlestring
def body(bodystring):
  return bodystring

#lab3()
lab6()
