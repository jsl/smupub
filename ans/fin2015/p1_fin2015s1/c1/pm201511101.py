import sys,os
dir=os.getenv('HOME')
dir+='/Code/git/bb/p1/src'
#dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  lab1()
  lab2()
  lab5()

def lab1():
  sound = makeSound(getMediaPath("preamble.wav"))
  computeTimeLength(sound)

def lab2():
  sound = makeSound(getMediaPath("preamble.wav"))
  onlyMinimize(sound)
  
def lab5():
  mirrorStr("Subject: Programming 1")
  
def computeTimeLength(sound):
  len = getLength(sound)
  print len
  
def onlyMinimize(sound):
  for s in getSamples(sound):
    if getSampleValue(s) < 0:
      setSampleValue(s, -32768)
  explore(sound)
  return sound

def phones():
  text=getMediaPath("phonebook.txt")
  file=open(text, "wt")
  file.write("Kim:2287-1000:Undergraduate Student"\n)
  file.write("Lee:2287-2000:Staff"\n)
  file.write("Lim:2287-3000:Graduate Student"\n)
  file.write("Hong:2287-4000:Teaching Staff"\n)
  file.close
  
def mirrorStr(str):
  string = ""
  for i in range(0,len(str)):
    string = string + str[i]
  for i in range(len(str)-1, -1, -1):
    string = string + str[i]
  print string


lab1()
#lab2()
#lab5()
