import sys,os
dir=os.getenv('HOME')
dir+='/Code/git/bb/p1/src'
#dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def lab1():
  computeTimeLength()
def computeTimeLength():
  sound=makeSound(getMediaPath('preamble.wav'))
  samplingRate=getSamplingRate(sound)
  length=getLength(sound)
  Time=length/samplingRate
  print Time
  
def lab2():
  onlyMinimize()
def onlyMinimize():
  sound=makeSound(getMediaPath('preamble.wav'))
  for s in range(0,getLength(sound)):
    value=getSampleValueAt(sound,s)
    if value<0:
      setSampleValueAt(sound,s,-32768)
  explore(sound)
  return sound
def lab3():
  sawtooth(220,4000)
def sawtooth(freq,amplitude):
  sound=makeSound(getMediaPath('sec3silence.wav'))
  interval=1.0/freq
  samplingRate=getSamplingRate(sound)
  samplesPerCycle=interval*samplingRate
  samplesPerHalfCycle=int(samplesPerCycle/2)
  for i in range(0,getLength(sound)):
    if i<samplesPerHalfCycle:
      setSampleValueAt(sound,i,i+amplitude)
    if i==samplesPerHalfCycle:
      i=0
    if i>samplesPerHalfCycle:
      setSampleValueAt(sound,i,-i-amplitude)
    i=i+1
  explore(sound)
  return sound
def lab4():
  findPhone(person)
def phones():
  a="""kim:2287-1000:Undergraduate Student
Lee:2287-2000:Staff
LIm:2287-3000:Graduate Student
Hong:2287-4000:Teaching Staff"""
  b=a.split('\n')
  print b

def lab5():
  mirrorStr('Subject: Programming1')
def mirrorStr(str):
  split=list(str)
  for i in range(0,len(split),2):
    split[len(split)-i-1]=split[i]
  print "".join(split)
  return split
  
def doctype():
  file=open('HTML.html','wt')
  file.write("""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"\n
"http://wwww.w3.org/TR/html4/loose.dtd">""")
  file.close
  
def main():
  lab1()
  lab2()
  lab3()
  lab5()
  

#lab1()
lab2()
lab3()
lab5()

  
  
