import sys,os
dir=os.getenv('HOME')
dir+='/Code/git/bb/p1/src'
#dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
 lab1()
 lab2()
 lab3()
 lab5()
 lab7()
 
def lab1():
  sound=makeSound(getMediaPath('preamble.wav'))
  computeTimeLength(sound)
 

def lab2():
  sound=makeSound(getMediaPath('preamble.wav'))
  onlyMinimize(sound)
  
def lab3():
  sawtooth(440,4000)
   
def lab5():
  str='Subject: Programming 1'
  print mirrorStr(str)
  
def lab7():
  makeHomePage('daegyu','game')
  
def sawtooth(freq,amplitude):
  sound=makeEmptySoundBySeconds(10)
  interval = 1.0 / freq
  samplePerCycle = interval * getSamplingRate(sound)
  samplePerHalfCycle = int(samplePerCycle / 2)
  sampleVal = -amplitude
  increment = int( amplitude / samplePerHalfCycle)
  i=0
  for s in range(0,getLength(sound)):
    if i == samplePerHalfCycle :
      sampleVal= -sampleVal
    sampleVal = sampleVal + increment
    setSampleValueAt(sound,s,sampleVal)
    i=i+1
  explore(sound)
  return sound
  
def makeHomePage(name,interest):
  file=open(getMediaPath('daegyu.html'),"wt")
  file.write(doctype())
  file.write(title(name))
  file.write(body(name+"""'s Home page</h1>
  <p>I am interested in """+interest))
  file.close()
  
def doctype():
  return """<!DOCTYPE HTML PUVLIC"-//W3C//DTD HTML 4.01 Transition//EN"
  "http://www.w3.org/TR/html4/;ppse.dtd">"""
  
def title(titlestring):
  return """<html><head><title>"""+titlestring+"""'s Home Page</title></head>"""

def body(bodystring): 
  return """<body><h1>Welcome to """+bodystring+"""</p></body></html>"""
  
  

def mirrorStr(str):
  string = str
  split = list(string)
  mirrorPoint = len(string)/2
  for s in range(0,mirrorPoint):
    left = s
    right = len(string)-1-s
    if (s%2)==1:
      split[right]=split[left]
  return "".join(split)

def onlyMinimize(sound):
  for sample in getSamples(sound):
    value = getSampleValue(sample)
    if value < 0:
      setSampleValue(sample,-32768)
  explore(sound)
  return sound  
  
def computeTimeLength(sound):
  len = getLength(sound)
  rate = getSamplingRate(sound)
  duration = len / rate
  print duration
  
lab1()
lab2()
lab3()
lab5()
lab7()

