
import sys,os
dir=os.getenv('HOME')
dir+='/Code/git/bb/p1/src'
#dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *


def computeTimeLength(sound):
  sr = getSamplingRate(sound)
  sec = sr/getLength(sound)
  print sec
  
def onlyMinimize(sound):
  for s in getSamples(sound):
    value = getSampleValue(s)
    if value < 0:
      setSampleValue(s, -32768)
  explore(sound)

def sawtooth(freq,amplitude):
  sound = makeEmptySound(44100)
  sr = getSamplingRate(sound)
  interval = 1.0 / freq
  spc = int(sr * interval)
  spchalf = int(spc / 2)
  cnt = 0
  for pos in range(0, getLength(sound)):
    if cnt > spchalf and cnt < spc:
      sampleValue = (-1)*amplitude
    elif cnt == spc:
      cnt = 0
    else:
      sampleValue = amplitude
    setSampleValueAt(sound,pos,sampleValue)
    cnt += 1
  explore(sound)

def phones():
  text = """Kim:2287-1000:Undergraduate Student
Lee:2287-2000:Staff
Lim:2287-3000:Graduate Student
Hong:2287-4000:Teaching Staff"""
  
  return text

def findPhone(person):
  phonebook = phones().split("\n")
  personlist = []
  
  for p in phonebook:
    sublist = p.split(":")
    personlist.append(sublist)
    
  for i in range(0,len(personlist)):
    if person == personlist[i][0]:
      print personlist[i][1]
  
def mirrorStr(str):
  textlist = list(str)
  mirrorpoint = len(textlist) / 2
  targetlist = [0 for i in range(0,mirrorpoint+1)]
  j = 0
  for i in range(0,mirrorpoint,2):
    targetlist[j] = textlist[i]
    targetlist[len(targetlist) - 1 - j] = textlist[i]
    j += 1
  print "".join(targetlist)   

def encode(str):
  textlist = list(str)
  for i in range(0,len(textlist)):
    ascii = ord(textlist[i])
    newword = ascii + 1
    textlist[i] = chr(newword)
  return "".join(textlist)
  
def decode(encodedStr):
  textlist = list(encodedStr)
  for i in range(0,len(textlist)):
    ascii = ord(textlist[i])
    newword = ascii - 1
    textlist[i] = chr(newword)
  return "".join(textlist)
  
def doctype():
  return ("""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Tramsition//EN""http://wwww.w3org/TR/html4/loose.dtd">""")
  
def title(titlestring):
  return ("""<html><head><title>""" + titlestring + """</title></head>""")

def body(bodystring):
  return ("""<body>""" + bodystring + """</body></html>""")

def makeHomePage(name,interest):
  file = open(getMediaPath("homepage.html"), "wt")
  file.write(doctype())
  
  titlestr = (name + """\'s Home Page""")
  file.write(title(titlestr))
  
  bodystr = ("""<h1>Welcome to """ + name + """'s Home Page</h1><p>I am interested in """ + interest + """</p>""")
  file.write(body(bodystr))
  
  file.close()
  
  return file
  
def lab1():
  print "lab1: ",
  a = makeSound(getMediaPath("a.wav"))
  computeTimeLength(a)
    
def lab2():
  print "lab2: ",
  a = makeSound(getMediaPath("a.wav"))
  onlyMinimize(a)  
  
def lab3():
  sawtooth(440,22400)

def lab4():
  print "lab4: ",
  findPhone("Kim")
 
def lab5():   
  print "lab5: ",
  mirrorStr("Subject: Programming 1")
  
def lab6():
  print "lab6: ",
  text = "Eunho"
  print "Original Text: "+text,
  encodedStr = encode(text)
  print "Encoded Text: "+encodedStr,
  print "Decoed Text: "+decode(encodedStr)
  
def lab7():
  print "lab: ",
  print makeHomePage("Eunho","Music")   
      
def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  lab7()
