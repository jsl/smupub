import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def lab1():
  sound=makeSound(getMediaPath('preamble.wav'))
  computeTimeLength(sound)

def computeTimeLength(sound):
  a=getLength(sound)
  b=getSamplingRate(sound)
  print a/b
  
def lab2():
  sound=makeSound(getMediaPath('preamble.wav'))
  onlyMinimize(sound)
  
def onlyMinimize(sound):
  for sample in getSamples(sound):
    value=getSampleValue(sample)
    if value<0:
      setSampleValue(sample,-32768)
  explore(sound)
  return sound
  
def lab3():
  sawtooth(440,2000)
  
def sawtooth(freq,amp):
  sound=makeEmptySoundBySeconds(1)
  interval=1.0/freq
  sr=getSamplingRate(sound)
  spc=interval*sr
  increment=500
  sampleVal=-amp
  i=0
  for s in range(0,sr):
    value=getSampleValueAt(sound,s)
    if i==spc:      
      increment=increment*-1
      i=0
    sampleVal=sampleVal+increment
    setSampleValueAt(sound,s,sampleVal)
    i=i+2
  explore(sound)
  return sound

def lab6():
  encode('Subject:Programming1')
  decode('Tvckfdu:Qsphsbnnjoh2')
    
def encode(str):
  for char in str:
    a=ord(char)+1
    b=chr(a)
    print b
    
def decode(encodedStr):
  for char in encodedStr:
    a=ord(char)-1
    b=chr(a)
    print b

def lab5():
  mirrorStr('Subject:Programming1')    
def mirrorStr(str):
  mirrorpoint=len(str)/2
  left=str[:mirrorpoint]
  right=str[mirrorpoint::-2]
  print left+right

def main():
  lab1()
  lab2()
  lab3()
  lab5()
  lab6()
  
#lab1()
#lab2()
lab3()
lab5()
lab6()

