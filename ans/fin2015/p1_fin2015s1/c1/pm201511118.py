import sys,os
dir=os.getenv('HOME')
#dir+='/Code/git/bb/p1/src'
dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  computerTimeLength()
  onlyMinimize()
  sawtooth()
  mirrorstr()
  
  
def computerTimeLength():
  sound=makeSound(getMediaPath("preamble.wav"))
  length=getLength(sound)
  L=length/getSamplingRate(sound)
  print L
  
def onlyMinimize():
  sound=makeSound(getMediaPath("preamble.wav"))
  for sample in getSamples(sound):
    value=getSampleValue(sample)
    if value<0:
      setSampleValue(sample,-32768.0)
  explore(sound)
  return sound
  
def sawtooth():
  sound=makeSound(getMediaPath("preamble.wav"))
  index=4000
  for i in range(1000,getLength(sound)-1000):
    setSampleValueAt(sound,i,index)
    if index>0:
      index=index-1
    else:
      index=4000
  for i in range(1000,getLength(sound)-1000):
    setSampleValueAt(sound,i,index)
    if index<0:
      index=index-1
    else:
      index=-4000
  explore(sound)
  return sound
  
def mirrorstr():
  string="Subject:programming1"
  split=list(string)
  totalLen=len(string)
  mirrorpoint=totalLen/2
  for i in range(0,len(string)):
    left=i
    right=totalLen-1-i
    split[right]=split[left]
  print split

#computerTimeLength()
#onlyMinimize()
sawtooth()
mirrorstr()
