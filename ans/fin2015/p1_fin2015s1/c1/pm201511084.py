import sys,os
dir=os.getenv('HOME')
dir+='/Code/git/bb/p1/src'
#dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

import math

def main():
  lab1()
  lab2()
  lab3()
  lab4()
  lab5()
  lab6()
  lab7()
  
  
def lab1():
  sound=makeSound(getMediaPath('preamble10.wav'))
  computeTimeLength(sound)
def computeTimeLength(sound):
  s=getLength(sound)/getSamplingRate(sound)
  print s

def lab2():
  sound=makeSound(getMediaPath('preamble10.wav'))
  onlyMinimize(sound)
def onlyMinimize(sound):
  for i in range(getLength(sound)):
    value=getSampleValueAt(sound, i)
    if(value<0):
      setSampleValueAt(sound, i, -32768)
  explore(sound)


def lab3():
  freq=440
  amplitude=4000
  sawtooth(freq, amplitude)
def sawtooth(freq, amplitude):
  mySound=makeSound(getMediaPath('sec1silence.wav'))
  sr=getSamplingRate(mySound)
  interval=1.0/freq
  spc=interval*sr
  maxCycle=2*math.pi
  a=0
  b=amplitude
  for i in range(getLength(mySound)):
    setSampleValueAt(mySound, i,a)
    if(i==spc/5):
      a=0
      i=0
    a+=50
  explore(mySound)

def lab4():
  phones()
  
def phonebook():
  return """
Kim:2287-1000:Undergraduate Student
Lee:2287-2000:Staff
Lim:2287-3000:Graduate Student
Hong:2287-4000:Teaching Staff"""
def phones():
  file=phonebook().split('\n')
  print file
  

  
  
def lab5():
  str=["S","u","b","j","e","c","t",":","","P","r","o","g","r","a","m","m","i","n","g","","1"]
  s=mirrorStr(str)
  print s
def mirrorStr(str):
  for i in range(len(str)-1):
    if((i%2)==0):
      str[len(str)-1-i]=str[i]
  return str
  
def lab6():
  str='a'
  encode(str)
  decode(str)

#I don't know function about number to alphabet so I use c just 98, 97
def encode(str):
  a=ord(str)
  b=a+1
  c=98
  print c
def decode(str):
  a=ord(str)
  b=a-1
  c=97
  print c
  

def lab7():
  name="201511084"
  interest="play game"
  makeHomePage(name, interest)
def makeHomePage(name, interest):
  return """
<!DOCTYPE html>
<html>
<head>
<title>name's home page</title>
</head>
<body>
<h1>welcome to name's homepage</h1>
<p>I am interested in interest</p>
</body>
</html>"""
 
lab1()
lab2()
#lab3()
#lab4()
#lab5()
