import sys,os
dir=os.getenv('HOME')
dir+='/Code/git/bb/p1/src'
#dir+='/Code/git/p1/src'
sys.path.append(dir)
from goJES import *

def main():
  lab1()
  lab2()
  lab3()
  lab5()
  lab6()
  lab7()
  
def lab1():
  filename=getMediaPath('preamble.wav')
  sound=makeSound(filename)
  computeTimeLength(sound)

def computeTimeLength(sound):
  filename=getMediaPath('preamble.wav')
  sound=makeSound(filename)
  second=getLength(sound)/getSamplingRate(sound)
  print 'The Time length of this sound is',second,'sec'
  
def lab2():
  filename=getMediaPath('preamble.wav')
  sound=makeSound(filename)
  onlyMinimize(sound)
    
def onlyMinimize(sound):
  filename=getMediaPath('preamble.wav')
  sound=makeSound(filename)
  for sample in getSamples(sound):
    value=getSampleValue(sample)
    if value < 0:
      setSampleValue(sample,-32768)
  explore(sound)
  return sound

def lab3():
  t=sawtooth(400,1000)
  explore (t)

def sawtooth(freq,amplitude):
  triangle=makeSound(getMediaPath('sec1silence.wav'))
  samplingRate=22050
  seconds=1
  interval=1.0*seconds/freq
  samplesPerCycle=interval*samplingRate
  samplesPerHourCycle=int(samplesPerCycle)
  increment=int(amplitude/samplesPerHourCycle)
  sampleVal= -amplitude
  i=0
  for s in range(0,samplingRate):
    if i == samplesPerHourCycle:
      increment=increment*1
      i=0
    sampleVal=sampleVal+increment
    setSampleValueAt(triangle,s,sampleVal)
    i=i+1
  return triangle

def lab5():
  word='Subject: Programming 1'
  mirrorStr(word)

def mirrorStr(str):
  split=list(str)
  totalLen=len(split)
  mirrorPoint=totalLen/2
  for i in range(0, mirrorPoint):
      split[totalLen-1-i]=split[i]
  if totalLen%2 == 0:
    print "".join(split)

def lab6():
  s=['b','a','e','k','d','o','h','y','u','n']
  encode(s)
  decode(s)

def encode(s):
  for i in range(0, len(s)):
    s[i]=ord(s[i])+1
    s[i]=chr(s[i])
  print s
  
def decode(s):
  for i in range(0, len(s)):
    s[i]=ord(s[i])-1
    s[i]=chr(s[i])
  print s

def lab7():
  makeHomePage('DoHyun','cooking')

def makeHomePage(name,interest):
  file=open(getMediaPath('201511112_bdh.html'),'wt')
  file.write(""" <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"
  "http://wwww.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <title> """+name+"""'s Home Page</title>
    </head>
    <body>
    <h1>Welcome to """+name+"""'s Home Page</h1>
    <p>I am interested in """+interest+""" </p>
    </body>
    </html> """)
  file.close()
  
def doctype():
  file=open(getMediaPath('20151112_bdh.html'),'wt')
  file.write(""" <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transition//EN"
  "http://wwww.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <title> JSL's Home Page</title>
    </head>
    <body>
    <h1>Welcome to JSL's Home Page</h1>
    <p>I am interested in programming</p>
    </body>
    </html> """)
    
def title(titlestring):
  JSL= """+name+"""

def body(bodystring):
  JSL="""+name+"""
  programming="""+interest+"""


#lab1()
#lab2()
lab3()
#lab5()
lab6()
lab7()
